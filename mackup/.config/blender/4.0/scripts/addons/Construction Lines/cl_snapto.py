# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from typing import List, Tuple

import bpy  # type: ignore

# from bpy_extras import view3d_utils
from bpy_extras.view3d_utils import (  # type: ignore
    location_3d_to_region_2d,
    region_2d_to_location_3d,
    region_2d_to_vector_3d,
    region_2d_to_origin_3d,
)

# from mathutils.geometry import intersect_line_plane as LinePlaneIntersect
from mathutils import Vector  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg
from . import cl_clobject as clo

# import time
############################################
# CLOSEST VERTEX
############################################
def pr2d(
    context, cl_op, cursor_loc, starting_loc, starting_norm, snp_options=utl.SNP_ALL
):
    loc = cl_op.mouse_pos
    coord = loc[0], loc[1]
    dirty_flag = cl_op.dirty_flag
    vert_cache = cl_op.vert_cache
    snap_obj = cl_op.snap_obj
    snap_geom = cl_op.snap_geom
    focus_obj = cl_op.focus_obj

    region = context.region
    rv3d = context.region_data
    view_vector = region_2d_to_vector_3d(region, rv3d, coord)
    ray_origin = region_2d_to_origin_3d(region, rv3d, coord)
    ray_target = ray_origin + view_vector

    def find_closest_face(obj):
        fail_state = None, None, None

        if not utl.object_is_valid(obj) or obj.hide_get():
            return fail_state
        try:
            if obj.type != "MESH":
                return fail_state
        except ReferenceError:
            return fail_state

        emode = context.mode == "EDIT_MESH"
        if emode:
            bpy.ops.object.mode_set(mode="OBJECT")

        if focus_obj:
            if utl.object_is_valid(focus_obj) and focus_obj.type == "MESH":
                obj = focus_obj
                obj.select_set(True)
            else:
                if emode:
                    return_to_snap_edit(snap_obj)
                    return fail_state

        matrix = obj.matrix_world.copy()
        # get the ray relative to the object
        matrix_inv = matrix.inverted()
        ray_origin_obj = matrix_inv @ ray_origin
        ray_target_obj = matrix_inv @ ray_target
        ray_direction_obj = ray_target_obj - ray_origin_obj

        # cast the ray
        success, location, normal, face_index = obj.ray_cast(
            ray_origin_obj, ray_direction_obj
        )

        if focus_obj:
            obj.select_set(False)

        if emode:
            return_to_snap_edit(snap_obj)

        if success:
            location = matrix @ location
            return face_index, location.copy(), normal
        else:
            return fail_state

    def project_2d(snap_geom):
        # print(location_3d_to_region_2d(region, rv3d, Vector((15.2009734, 0, 0))))
        if not snap_geom:
            return
        try:
            for sg in snap_geom:
                if not sg:
                    continue
                if sg.geom_type in {utl.TYPE_VERT, utl.TYPE_EDGE_C}:
                    loc2d = location_3d_to_region_2d(region, rv3d, sg.w_loc)
                    if loc2d:
                        sg.loc2d = loc2d
                elif sg.geom_type in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E}:
                    v1_loc2d = location_3d_to_region_2d(region, rv3d, sg.w_loc[0])
                    v2_loc2d = location_3d_to_region_2d(region, rv3d, sg.w_loc[1])
                    if not v1_loc2d:
                        v1_loc2d = project_edge_vert_into_view(sg.w_loc[1], sg.w_loc[0])
                    elif not v2_loc2d:
                        v2_loc2d = project_edge_vert_into_view(sg.w_loc[0], sg.w_loc[1])
                    if v1_loc2d and v2_loc2d:
                        sg.loc2d = [v1_loc2d, v2_loc2d]
        except Exception as e:
            print("Snap geometry not valid.")

    # v1 = vert to project
    # fix for edges running behind viewport camera
    # project edge vert into view and keep within region bounds
    def project_edge_vert_into_view(ev1, ev2):
        # print(rv3d.perspective_matrix)
        amnt = 0.1
        v1 = ev1.copy()
        v2 = ev2.copy()
        dist = (Vector(v1) - v2).length
        v_dir = (v2 - v1).normalized()

        while dist > amnt:
            v2 = v2 - v_dir * amnt
            v1_loc2d = location_3d_to_region_2d(region, rv3d, v2)
            if (
                v1_loc2d
                and 0 < v1_loc2d.x <= region.width
                and 0 < v1_loc2d.y <= region.height
            ):
                return v1_loc2d
            dist = (Vector(v1) - v2).length
        return None

    def project_cursor_loc(c_loc):
        try:
            loc2d = location_3d_to_region_2d(region, rv3d, c_loc[0])
            if loc2d:
                return loc2d
        except Exception as e:
            return None
        return None

    def find_closest(geom):
        closest = None
        # geom = verts_cache.return_snap_geom()
        best_dist = utl.CL_BEST_DIST  # / utl.viewport_zoom_dist(context)

        if snp_options & utl.SNP_FLAG_VERT:
            for sg in geom:
                if not sg:
                    continue
                if sg.geom_type not in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E}:
                    if sg.geom_type == utl.TYPE_EDGE_C and not (
                        snp_options & utl.SNP_FLAG_EDGE_C
                    ):
                        continue
                    if not sg.loc2d:
                        continue
                    dist = clg.calc_sq_len_2D(sg.loc2d, coord)
                    if dist < best_dist:
                        best_dist = dist
                        closest = clg.ClosestVert(
                            sg.w_loc,
                            sg.edge,
                            None,
                            sg.geom_type,
                            sg.connected_edges,
                            None,
                            geom_id=sg.geom_id,
                        )

        # If no closest vert or edge center, try edge intersect
        if snp_options & utl.SNP_FLAG_EDGE:
            if not closest:
                # best_dist = utl.CL_BEST_E_DIST  # / utl.viewport_zoom_dist(context)
                best_dist = utl.CL_BEST_DIST
                for sg in geom:
                    if sg:
                        if sg.geom_type in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E}:
                            if not sg.loc2d:
                                continue

                            is_intersect, _, pt = clg.circle_line_intersect(
                                Vector(coord), 20, sg.loc2d[0], sg.loc2d[1]
                            )
                            # if not is_intersect:
                            #     print("Not on edge")
                            if is_intersect:
                                # get real location
                                r_p0 = sg.w_loc[0]
                                r_p1 = sg.w_loc[1]
                                v1 = r_p1 - r_p0
                                depth_location = return_furthest_point_in_view(
                                    ray_origin, sg.w_loc[0], sg.w_loc[1]
                                )
                                pt3d = region_2d_to_location_3d(
                                    region, rv3d, pt, depth_location
                                )

                                # draw ray from pt3d from ray_origin and see if intersects with edge
                                co = clg.find_edge_edge_intersection_verts(
                                    [ray_origin, pt3d, sg.w_loc[0], sg.w_loc[1]]
                                )
                                if co:
                                    # if clg.point_on_edge(co, [sg.w_loc[0], sg.w_loc[1]]):
                                    prcnt = clg.percent_point_on_edge(
                                        co, [sg.w_loc[0], sg.w_loc[1]]
                                    )
                                    if 0 < prcnt < 1:
                                        r_loc = Vector(
                                            [
                                                getattr(r_p0, i)
                                                + prcnt * getattr(v1, i)
                                                for i in "xyz"
                                            ]
                                        )
                                        # r_loc = co
                                        return clg.ClosestVert(
                                            r_loc,
                                            [sg.w_loc[0], sg.w_loc[1]],
                                            None,
                                            sg.geom_type,
                                            sg.connected_edges,
                                            None,
                                            obj=sg.connect_obj
                                            if sg.geom_type == utl.TYPE_GUIDE_E
                                            else None,
                                            geom_id=sg.geom_id,
                                        )
        return closest

    # IN
    # cache result, only project_2d if dirty_flag is True
    if dirty_flag:
        # add in 3D cursor
        snap_geom.append(
            clg.SnapGeom(
                cursor_loc[0], cursor_loc[1], None, None, project_cursor_loc(cursor_loc)
            )
        )
        if snap_geom:
            project_2d(snap_geom)
        vert_cache.update_cache(snap_geom)

    # find closest geometry point
    closest = None
    closest_axis = None
    closest_face = None
    face_loc = None
    face_norm = None
    working_plane_loc = None

    closest = find_closest(vert_cache.return_snap_geom())

    # try to find closest global axis if no closest geometry
    if not closest and (snp_options & utl.SNP_GLB_AXES):
        axes_geom = return_global_axes_as_edges(context)
        project_2d(axes_geom)
        closest_axis = find_closest(axes_geom)
    # find closest face

    if snp_options & utl.SNP_FLAG_FACE and utl.get_space_shading_mode() in {
        "SOLID",
        "MATERIAL",
    }:
        closest_face, face_loc, face_norm = find_closest_face(snap_obj)
        if closest_face and closest:
            closest = compare_pts_to_camera(ray_origin, face_loc, closest)
        else:
            working_plane_loc = clg.intersect_mouse_loc_plane_from_view(
                ray_origin,
                ray_target,
                starting_loc.copy() if starting_loc else Vector((0, 0, 0)),
                starting_norm,
            )
    return closest, closest_face, face_loc, face_norm, closest_axis, working_plane_loc


def compare_pts_to_camera(camera_loc, face_loc, closest):
    d1 = clg.calc_sq_len(camera_loc, face_loc)
    d2 = clg.calc_sq_len(camera_loc, closest.vert)
    if d1 + utl.CL_FACE_SNAP_SENSITIVITY < d2:
        return None
    return closest


def return_furthest_point_in_view(camera_loc, v1, v2):
    d1 = clg.calc_sq_len(camera_loc, v1)
    d2 = clg.calc_sq_len(camera_loc, v2)
    if d1 < d2:
        return v1
    return v2


def return_global_axes_as_edges(context):
    half_len = utl.CL_DEFAULT_H_GUIDE_LENGTH
    gax = utl.return_global_axes_visible(context)
    edges = []
    start = Vector((0, 0, 0))
    for a in gax:
        if a:
            edge = [start - (a * half_len), start + (a * half_len)]
            edges.append(
                clg.SnapGeom([edge[0], edge[1]], utl.TYPE_EDGE, None, None, None)
            )
    return edges


class VertCache:
    def __init__(self, snap_geom):
        self.snap_geom = snap_geom
        # self.edges = edges

    def return_snap_geom(self):
        if self.snap_geom != []:
            return self.snap_geom

        return []

    def update_cache(self, snap_geom):
        self.snap_geom = snap_geom

    def clear_cache(self):
        self.snap_geom = None


############################################
# OBJECT SNAP
############################################
# Faster to use built in ops than ray casting against all objects
def snap(
    cl_op, context, limit_to_edit=False, snp_options=utl.SNP_ALL, ignore_geom=None
):
    if cl_op.close_geom_pt:
        if cl_op.close_geom_pt.geom_type is not utl.TYPE_FACE:
            return
    # cl_op.is_processing = True
    emode = context.mode == "EDIT_MESH"

    geom = []

    edit_obj = cl_op.snap_obj
    loc = cl_op.mouse_pos

    # limits snapping to current object (geom selections, extrude, etc.)
    if limit_to_edit:
        s_obj = context.active_object
    else:
        # Make sure that edit mode object's geometry is always added first
        if emode:
            if context.active_object.type == "MESH":
                geom += clg.select_vis_geom(
                    context.active_object,
                    loc,
                    context,
                    snp_options,
                    ignore_geom=ignore_geom,
                )
        # hover closest object
        s_obj = utl.select_closest_obj(
            context, loc[0], loc[1], "EDIT" if emode else "OBJECT"
        )

    if s_obj:
        if s_obj.type == "MESH":
            geom += clg.select_vis_geom(
                s_obj, loc, context, snp_options, ignore_geom=ignore_geom
            )
        else:
            try:
                s_obj.select_set(False)
                if s_obj.type != "EMPTY":
                    s_obj = cl_op.snap_obj if cl_op.snap_obj else None
                context.view_layer.objects.active = s_obj
            except ReferenceError:
                s_obj = None
        cl_op.dirty_flag = True
        if s_obj:
            s_obj.select_set(False)

        if cl_op.snap_obj is not s_obj:
            cl_op.focus_obj = s_obj
            # only switch object if emode is false
            if not emode:
                cl_op.snap_obj = s_obj

        if not s_obj:
            cl_op.focus_obj = None

    # add all guides to snap geometry along with guide/guide and guide/edge snaps
    edges = [e for e in geom if e.geom_type == utl.TYPE_EDGE]
    guides = utl.return_all_guide_geom(edges, context)
    if guides:
        if geom:
            geom.extend(guides)
        else:
            geom = guides
    cl_op.snap_geom = geom

    # return to current working object
    if emode:
        return_to_snap_edit(edit_obj)


def return_to_snap_edit(snap_obj):
    try:
        if utl.object_is_valid(snap_obj):
            utl.deselect_all_objects()
            snap_obj.select_set(True)
            bpy.context.view_layer.objects.active = snap_obj
            bpy.ops.object.mode_set(mode="EDIT")
            bpy.ops.mesh.select_mode(type="FACE")
            bpy.ops.mesh.select_mode(use_extend=True, type="VERT")
            snap_obj.select_set(False)
            return True
    except ReferenceError:
        print("object no longer exists")
    except Exception as e:
        print(e)
        print("object snapping error")
    return False


############################################
# CLOSEST AXIS SNAP
############################################
# return closest axis for a soft axis lock
def return_soft_constraint(v1, closest_geom, axes) -> List:
    # If closest geom is availible reduce tolerance of snap
    if closest_geom.geom_type == "NONE":
        tolerance = utl.CL_CLOSE_AXIS_TOL
    else:
        tolerance = utl.CL_CLOSE_AXIS_TOL_FINE

    cg, ax = clg.find_closest_axis(v1, closest_geom.vert, axes, tolerance)
    if ax:
        if closest_geom.geom_type == "NONE":
            closest_geom.geom_type = utl.TYPE_AXIS
        else:
            if closest_geom.geom_type != utl.TYPE_VERT:
                closest_geom.vert = cg
            else:
                ax = []
        return [closest_geom, ax]
    return []


############################################
# SNAP ADJUSTMNENTS
############################################
def return_highlights_and_soft_constraints(
    start_v, tape: clo.ConstructLine, close_geom_pt, mouse_pos, context: bpy.context
):

    if not tape:
        return []

    axes = utl.return_global_axes()
    axes.extend(utl.return_global_axes_neg())
    # Flatten if in ortho mode
    if utl.is_in_ortho_view(context):
        t_vs = tape.return_verts()
        tape_start = t_vs[0]
        mouse_pos = utl.flatten_location_to_ortho(
            utl.get_view_orientation_from_matrix(utl.get_view_matrix(context)),
            mouse_pos,
            tape_start,
        )

    # if tape has start geometry then look for constraints
    # and highlights
    s_geom = tape.get_start_geom()
    if s_geom:
        # Edge Perps
        if (
            s_geom.geom_type
            in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E, utl.TYPE_EDGE_C, utl.TYPE_VERT}
            and s_geom.edge
        ):
            perps = []
            dir_vects = []
            # use global axis if no connected edges or no planar connected edges
            dir_vects = axes.copy()
            if s_geom.connected_edges:
                dv = clg.return_planes_from_edges(s_geom.edge, s_geom.connected_edges)
                # clear global axes and replace
                if dv:
                    dir_vects = dv
            for ax in dir_vects:
                perp_axis = clg.check_for_perp(start_v, mouse_pos, s_geom.edge, ax)
                if perp_axis:
                    if perp_axis not in perps:
                        perps.append(perp_axis)
            if perps:
                # add unique axes
                for p in perps:
                    if not utl.val_in_list_pos(axes, p, 0):
                        axes.append(p)

        # Edge Extensions
        if s_geom.geom_type == utl.TYPE_VERT:
            if s_geom.edge:
                extend_axis = clg.check_for_extend(s_geom.edge)
                if extend_axis:
                    axes.extend(extend_axis)
        new_cg = (
            close_geom_pt
            if close_geom_pt
            else clg.ClosestVert(mouse_pos, None, None, "None", None, None)
        )

        # return adjust_pt, soft_constraint
        return return_soft_constraint(start_v, new_cg, axes)


def return_grid_snap(input_pos, context: bpy.context, sub_divs=True):
    grid_scale, scale_unit, grid_subdivisions = utl.return_viewport_grid_dims(context)
    unit_scale = utl.return_b_unit_scale(context)
    
    # apply grid unit scaling
    grid_scale *= scale_unit

    # apply current unit scaling (from unit panel)
    grid_scale *= unit_scale

    # try major
    snp, dist_maj = grid_snap(input_pos, grid_scale, utl.TYPE_GRID_MAJ)
    # try minor
    if sub_divs:
        snp_min, dist_min = grid_snap(
            input_pos, grid_scale / grid_subdivisions, utl.TYPE_GRID_MIN
        )
        if dist_min > -1:
            if dist_maj < 0 or dist_min < dist_maj:
                snp = snp_min

    return snp


def grid_snap(input_pos, grid_scale, g_type):
    if not input_pos or not isinstance(input_pos, Vector):
        return None, -1

    snp_x = round(input_pos[0] / grid_scale) * grid_scale
    snp_y = round(input_pos[1] / grid_scale) * grid_scale
    snp_z = round(input_pos[2] / grid_scale) * grid_scale
    new_x = input_pos[0]
    new_y = input_pos[1]
    new_z = input_pos[2]

    ax = 0
    if 0 < abs(input_pos[0] - snp_x) < utl.CL_CLOSE_GRID_TOL:
        new_x = snp_x
        ax += 1

    if 0 < abs(input_pos[1] - snp_y) < utl.CL_CLOSE_GRID_TOL:
        new_y = snp_y
        ax += 1

    if 0 < abs(input_pos[2] - snp_z) < utl.CL_CLOSE_GRID_TOL:
        new_z = snp_z
        ax += 1

    new_pos = Vector((new_x, new_y, new_z))

    dist = clg.calc_sq_len(input_pos, new_pos)

    if ax > 1 and 0 < dist <= utl.CL_BEST_E_DIST:
        return clg.ClosestVert(new_pos, None, None, g_type, None, None), dist
    return None, -1
