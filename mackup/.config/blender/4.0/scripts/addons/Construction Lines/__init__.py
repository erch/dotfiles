# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

bl_info = {
    "name": "Construction Lines",
    "description": "Measure distances, build construction guides, draw lines and build primitive shapes in place.",
    "author": "Daniel Norris, DN DRAWINGS <https://dndrawings.com>",
    "version": (0, 9, 69),
    "blender": (2, 80, 0),
    "location": "View3D > TOOLS",
    "wiki_url": "http://dndrawings.com/add-ons",
    "doc_url": "http://dndrawings.com/add-ons",
    "tracker_url": "http://dndrawings.com/feedback",
    "category": "3D View",
}


import importlib

from . import construction_lines28
importlib.reload(construction_lines28)

from . import cl_ctxmenu
importlib.reload(cl_ctxmenu)

from . import cl_utils
importlib.reload(cl_utils)

from . import cl_settings_ui
importlib.reload(cl_settings_ui)

from .construction_lines28 import VIEW_OT_ConstructionLines

from .cl_settings_ui import (
    CLSettings,
    CL_PT_tools_options,
)

from .cl_ctxmenu import (
    CL_CTX_Menu,
    VIEW_OT_CLMenuAction,
    ToolbarOverlay,
    register_icons,
    unregister_icons,
)


import bpy  # type: ignore

from bpy.props import (  # type: ignore
    EnumProperty,
    FloatVectorProperty,
    BoolProperty,
    FloatProperty,
    IntProperty,
    CollectionProperty,
    StringProperty,
)
from bpy.types import AddonPreferences  # type: ignore
from bpy.app.handlers import persistent  # type: ignore

# CL_GUIDE_COL = (0.3, 0.3, 0.3, 1.0)
CL_NAME = __name__.split(".")[0]


#############################################
# WORKSPACE TOOL
############################################
from bpy.utils.toolsystem import ToolDef  # type: ignore

addon_keymaps = []
CL_ = "Construction Lines"


@ToolDef.from_fn
def tool_cl():
    import os

    # from . import cl_utils as utl

    def draw_settings(context, layout, tool):
        layout.enabled = False
        # print(layout.menu_contents)

        # # layout.label(text=utl.DEFAULT_STATUS)
        # props = tool.operator_properties("view3d.construction_lines")
        # layout.operator(
        #     "view3d.construction_lines")
        # layout.operator("bpy.ops.view3d.toggle_xray")
        # layout.operator("object.shade_smooth")
        # layout.operator("wm.call_menu", text="Unwrap").name = "VIEW3D_MT_uv_map"
        # pass

    icons_dir = os.path.join(os.path.dirname(__file__), "icons")
    return dict(
        idname="cl.construction_lines",
        label="Construction Lines",
        description="Measure distances, build construction guides, draw lines and build primitive shapes in place.\nShortcut Alt `",
        icon=os.path.join(icons_dir, "ops.view3d.construction_lines"),
        widget="",
        keymap=CL_,
        draw_settings=draw_settings,
    )


# -----------------------------------------------------------------------------
# Tool Registraion
def get_tool_list(space_type, context_mode):
    from bl_ui.space_toolsystem_common import ToolSelectPanelHelper  # type: ignore

    cls = ToolSelectPanelHelper._tool_class_from_space_type(space_type)
    return cls._tools[context_mode]


def register_cl_tool():
    tools = get_tool_list("VIEW_3D", "EDIT_MESH")
    idx = -1
    for idx, tool in enumerate(tools, 1):
        if isinstance(tool, ToolDef) and tool.label == "Measure":
            break

    if idx > -1:
        tools[:idx] += None, tool_cl
    else:
        tools[: len(tools)] += None, tool_cl

    tools = get_tool_list("VIEW_3D", "OBJECT")
    tools += None, tool_cl
    del tools


def unregister_cl_tool():
    tools = get_tool_list("VIEW_3D", "EDIT_MESH")
    idx = tools.index(tool_cl) - 1  # None
    tools.pop(idx)
    tools.remove(tool_cl)
    tools = get_tool_list("VIEW_3D", "OBJECT")
    idx = tools.index(tool_cl) - 1  # None
    tools.pop(idx)
    tools.remove(tool_cl)
    del tools
    del idx


############################################
# Add-on Preferences
############################################
# CREDIT: Keymap code is an edited version of Stephen Leger's CAD Transform keymap code


class CLInputSettings(bpy.types.PropertyGroup):
    name: StringProperty(name="name", default="event")
    label: StringProperty(name="label", default="Event")
    key: EnumProperty(
        name="key",
        default="A",
        items=(
            ("A", "A", "A", 0),
            ("B", "B", "B", 1),
            ("C", "C", "C", 2),
            ("D", "D", "D", 3),
            ("E", "E", "E", 4),
            ("F", "F", "F", 5),
            ("G", "G", "G", 6),
            ("H", "H", "H", 7),
            ("I", "I", "I", 8),
            ("J", "J", "J", 9),
            ("K", "K", "K", 10),
            ("L", "L", "L", 11),
            ("M", "M", "M", 12),
            ("N", "N", "N", 13),
            ("O", "O", "O", 14),
            ("P", "P", "P", 15),
            ("Q", "Q", "Q", 16),
            ("R", "R", "R", 17),
            ("S", "S", "S", 18),
            ("T", "T", "T", 19),
            ("U", "U", "U", 20),
            ("V", "V", "V", 21),
            ("W", "W", "W", 22),
            ("X", "X", "X", 23),
            ("Y", "Y", "Y", 24),
            ("Z", "Z", "Z", 25),
            ("F1", "F1", "F1", 26),
            ("F2", "F2", "F2", 27),
            ("F3", "F3", "F3", 28),
            ("F4", "F4", "F4", 29),
            ("F5", "F5", "F5", 30),
            ("F6", "F6", "F6", 31),
            ("F7", "F7", "F7", 32),
            ("F8", "F8", "F8", 33),
            ("F9", "F9", "F9", 34),
            ("F10", "F10", "F10", 35),
            ("F11", "F11", "F11", 36),
            ("F12", "F12", "F12", 37),
            ("ZERO", "0", "0", 38),
            ("ONE", "1", "1", 39),
            ("TWO", "2", "2", 40),
            ("THREE", "3", "3", 41),
            ("FOUR", "4", "4", 42),
            ("FIVE", "5", "5", 43),
            ("SIX", "6", "6", 44),
            ("SEVEN", "7", "7", 45),
            ("EIGHT", "8", "8", 46),
            ("NINE", "9", "9", 47),
            ("DEL", "DEL", "DEL", 48),
            ("SPACE", "SPACE", "SPACE", "EVENT_SPACEKEY", 49),
            ("ESC", "ESC", "ESC", "EVENT_ESCKEY", 50),
            ("TAB", "TAB", "TAB", "EVENT_TAB", 51),
            ("NUMPAD_SLASH", "NUMPAD_SLASH", "NUMPAD_SLASH", 52),
            ("NOTHING", "NOTHING", "NOTHING", 53),
        ),
    )
    alt: BoolProperty(default=False, name="alt")
    shift: BoolProperty(default=False, name="shift")
    ctrl: BoolProperty(default=False, name="ctrl")
    oskey: BoolProperty(default=False, name="oskey")

    def draw_pref(self, layout):
        row = layout.row(align=True)
        split = row.split(factor=0.5)
        col = split.column()
        row = col.row(align=True)
        row.prop(self, "label", text="")
        row.enabled = False
        col = split.column()
        row = col.row()
        if self.name not in cl_utils.INPT_MODIFIER_NAMES:
            row.prop(self, "key", text="")
        row.prop(self, "ctrl", icon="EVENT_CTRL")
        row.prop(self, "alt", icon="EVENT_ALT")
        row.prop(self, "shift", icon="EVENT_SHIFT")
        row.prop(self, "oskey", icon="EVENT_OS")

    def match(self, signature, value="PRESS"):
        return (self.alt, self.ctrl, self.shift, self.key, self.oskey, value) == signature


class CLAddonPreferences(AddonPreferences):
    bl_idname = __name__

    # Colours
    cl_shape_col: FloatVectorProperty(
        name="Default Drawing",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_DEFAULT_COL,
    )

    cl_guide_col: FloatVectorProperty(
        name="Guide Lines",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_GUIDE_COL,
    )

    cl_vert_snap_col: FloatVectorProperty(
        name="Vert Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_VERT,
    )

    cl_edge_snap_col: FloatVectorProperty(
        name="Edge Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_EDGE,
    )

    cl_edge_c_snap_col: FloatVectorProperty(
        name="Edge Center Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_EDGE_C,
    )

    cl_face_snap_col: FloatVectorProperty(
        name="Face Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_FACE,
    )

    cl_extd_snap_col: FloatVectorProperty(
        name="Edge Extend Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_EXTD_COL,
    )

    cl_grid_snap_col: FloatVectorProperty(
        name="Grid Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_GRID_MAJ
    )

    cl_grid_sdiv_snap_col: FloatVectorProperty(
        name="Grid Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_HIGHLIGHT_COL_GRID_MIN
    )

    cl_perp_snap_col: FloatVectorProperty(
        name="Edge Perpendicular Snap",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_PERP_COL,
    )

    cl_display_box_col: FloatVectorProperty(
        name="Text Display Box",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_DISPLAY_BOX_COL,
    )

    cl_toolbar_col: FloatVectorProperty(
        name="Toolbar button colour",
        subtype="COLOR_GAMMA",
        size=3,
        min=0.0,
        max=1.0,
        default=cl_utils.TOOLBAR_COL,
    )

    cl_toolbar_hover_col: FloatVectorProperty(
        name="Toolbar button hover colour",
        subtype="COLOR_GAMMA",
        size=3,
        min=0.0,
        max=1.0,
        default=cl_utils.TOOLBAR_COL_HL,
    )


    cl_display_dim_col: FloatVectorProperty(
        name="Guide Dimension",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_DISPLAY_DIM_COL,
    )

    cl_text_col: FloatVectorProperty(
        name="Text",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_TEXT_COL,
    )

    cl_text_size: IntProperty(
        name="Text Size",
        description="Text size for dimensions and tips box",
        min=3,
        max=25,
        default=cl_utils.CL_TEXT_SIZE,
    )


    cl_edit_bounds_col: FloatVectorProperty(
        name="Edit Mode Bounds Highlight",
        subtype="COLOR_GAMMA",
        size=4,
        min=0.0,
        max=1.0,
        default=cl_utils.CL_OBJ_BOUNDS_COL
    )

    # Other Prefs
    cl_auto_input: BoolProperty(
        name="Direct Input",
        description="Automatically begin number input mode on tool start (no need to press Enter first)",
        default=True,
    )

    cl_numpad_nav: BoolProperty(
        name="Numpad For Navigation",
        description="Use the numberpad for navigation rather than numeric input",
        default=True,
    )

    cl_unit_override: BoolProperty(
        name="Override Unit Length Scale",
        description="Override Unit Length Scale with Unit Scale when adding geometry or guides",
        default=False,
    )

    cl_guide_point_scale: FloatProperty(
        name="Guide Point Scale",
        description="Scaled size for guide points. Applies to future guide points. \
            Existing guide points can be rescaled to this value through the context menu",
        min=0.0001,
        max=10.0,
        precision=4,
        default=0.1,
    )

    cl_draw_line_width: IntProperty(
        name="Line Drawing Width (Shapes and Guides)",
        description="Width of line for drawing guides and shapes",
        min=1,
        max=5,
        default=1,
    )

    cl_snap_point_size: FloatProperty(
        name="Highlight & Snap Point Size",
        description="Size of snap and highlight points",
        min=1.0,
        max=50.0,
        step=10,
        precision=1,
        default=5.0,
    )

    cl_snap_radius: IntProperty(
        name="Size of snapping radius",
        description="Snapping radius. Adjusting size for complex meshes may improve performance and accuracy",
        min=1,
        max=50,
        default=25,   
    )

    cl_default_tool: EnumProperty(
        name="Defaul Tool",
        description="The tool that Construction Lines will always default to",
        items=(
            ("Tape", "Tape", "Measuring Tool", 0, 0),
            ("Line", "Line", "Line Drawing Tool", 0, 1),
            ("Select", "Select", "Select Tool", 0, 2),
            ("Circle", "Circle", "Circle Tool", 0, 3),
            ("Rectangle", "Rectangle", "Rectangle Tool", 0, 4),
            ("Arc", "Arc", "Arc Tool", 0, 5),
        ),
        default="Select",
    )

    cl_icon_spacing: IntProperty(
        name="Toolbar Icon Spacing",
        description="Spacing between toolbar icons",
        min=30,
        max=100,
        default=40,
    )

    cl_circ_segs: IntProperty(
        name="Default Circle/Arc Segments",
        description="Default number of segments when drawing circles or arcs",
        min=3,
        max=32,
        default=12,
    )

    cl_horz_guide_len: FloatProperty(
        name="Horizontal Guide Length",
        description="Length of horizontal guides",
        min=0.1,
        max=10000,
        default=100,
    )

    cl_dashed_shader: BoolProperty(
        name="Allow dashed line drawing (older Macs)",
        description="Allow dashed line overlay drawing. Can be disabled for older Macs if affected",
        default=True,
    )

    input_settings: CollectionProperty(type=CLInputSettings)

    keyboard_section: BoolProperty(
        name="Keyboard Shortcuts", description="Keyboard Shortcuts", default=False
    )

    colours_section: BoolProperty(
        name="Interface Colours", description="Interface Colours", default=False
    )

    def match(self, name, signature, value="PRESS"):
        shorts = name.split(",")
        for sname in shorts:
            short = self.input_settings.get(sname)
            if short and short.match(signature, value):
                return True
        return False

    def match_any(self, signature):
        for s in self.input_settings:
            if s.match(signature):
                return True
        return False


    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(self, "cl_default_tool")
        row = layout.row()
        row.prop(self, "cl_auto_input")
        sub = row.row()
        sub.prop(self, "cl_numpad_nav")
        sub.enabled = context.preferences.addons[CL_NAME].preferences.cl_auto_input
        row = layout.row()
        row.prop(self, "cl_icon_spacing")
        row = layout.row()
        row.prop(self, "cl_text_size")
        row = layout.row()
        row.prop(self, "cl_circ_segs")
        row = layout.row()
        row.prop(self, "cl_unit_override")
        row = layout.row()
        row.prop(self, "cl_guide_point_scale")
        row = layout.row()
        row.prop(self, "cl_draw_line_width")
        row = layout.row()
        row.prop(self, "cl_snap_point_size")
        row = layout.row()
        row.prop(self, "cl_snap_radius")
        row = layout.row()
        row.use_property_split = True
        row.prop(self, "cl_horz_guide_len")
        row = layout.row()
        row.prop(self, "cl_dashed_shader")

        # Keyboard shortcuts
        layout.prop(
            self,
            "keyboard_section",
            icon="DISCLOSURE_TRI_DOWN"
            if self.keyboard_section
            else "DISCLOSURE_TRI_RIGHT",
        )
        if self.keyboard_section:

            row = layout.row()
            row.label(text="Tools:")
            # # row.prop(self, "cl_tape_key")
            for key in self.input_settings[0:9]:
                row = layout.row()
                key.draw_pref(row)

            row = layout.row()
            row.label(text="Functions:")
            for key in self.input_settings[9:23]:
                row = layout.row()
                key.draw_pref(row)
            
            row = layout.row()
            row.label(text="Modifiers:")
            for key in self.input_settings[23:25]:
                row = layout.row()
                key.draw_pref(row)

        # Colours settings section
        layout.prop(
            self,
            "colours_section",
            icon="DISCLOSURE_TRI_DOWN"
            if self.colours_section
            else "DISCLOSURE_TRI_RIGHT",
        )
        if self.colours_section:
            row = layout.row()
            row.label(text="- Shape and Guides -")
            row = layout.row()
            row.prop(self, "cl_shape_col")
            row = layout.row()
            row.prop(self, "cl_guide_col")
            row = layout.row()
            row = layout.row()
            row.label(text="- Snapping -")
            row = layout.row()
            row.prop(self, "cl_vert_snap_col")
            row = layout.row()
            row.prop(self, "cl_edge_snap_col")
            row = layout.row()
            row.prop(self, "cl_edge_c_snap_col")
            row = layout.row()
            row.prop(self, "cl_face_snap_col")
            row = layout.row()
            row.prop(self, "cl_perp_snap_col")
            row = layout.row()
            row.prop(self, "cl_extd_snap_col")
            row = layout.row()
            row.prop(self, "cl_grid_snap_col")
            row = layout.row()
            row.prop(self, "cl_grid_sdiv_snap_col")
            row = layout.row()
            row = layout.row()
            row.label(text="- Text Box And Dimension Display -")
            row = layout.row()
            row.prop(self, "cl_display_box_col")
            row = layout.row()
            row.prop(self, "cl_display_dim_col")
            row = layout.row()
            row.prop(self, "cl_text_col")
            row = layout.row()
            row.prop(self, "cl_edit_bounds_col")
            row = layout.row()
            row.label(text="- Toolbar -")
            row = layout.row()
            row.prop(self, "cl_toolbar_col")
            row = layout.row()
            row.prop(self, "cl_toolbar_hover_col")

            
            

############################################
# LISTENERS
############################################
@persistent
def end_CL_on_load(val):
    VIEW_OT_ConstructionLines.running = False


############################################
# REG/UN_REG
############################################


classes = (
    VIEW_OT_ConstructionLines,
    CLInputSettings,
    CLSettings,
    CL_PT_tools_options,
    CL_CTX_Menu,
    VIEW_OT_CLMenuAction,
    CLAddonPreferences,
    ToolbarOverlay,
)

enum_menu_items = [
    ("OPT1", "Metric(mm)", "", 1),
    ("OPT2", "Metric(cm)", "", 2),
    ("OPT3", "Metric(m)", "", 3),
    ("OPT4", "Imperial(inch)", "", 4),
    ("OPT5", "Imperial(feet & inch)", "", 5),
]


def km_cl():
    return [
        (
            CL_,
            {"space_type": "VIEW_3D", "region_type": "WINDOW"},
            {
                "items": [
                    (
                        "view3d.construction_lines",
                        {"type": "MOUSEMOVE", "value": "ANY"},
                        {"properties": []},
                    )
                ]
            },
        )
    ]


def register_keymaps():
    keyconfigs = bpy.context.window_manager.keyconfigs
    kc_defaultconf = keyconfigs.default
    kc_addonconf = keyconfigs.addon

    from bl_keymap_utils.io import keyconfig_init_from_data  # type: ignore

    keyconfig_init_from_data(kc_defaultconf, km_cl())
    keyconfig_init_from_data(kc_addonconf, km_cl())

    # keymap
    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.new(
        name="Window", space_type="EMPTY", region_type="WINDOW"
    )
    kmi = km.keymap_items.new(
        "view3d.construction_lines", type="ACCENT_GRAVE", alt=True, value="PRESS"
    )
    addon_keymaps.append((km, kmi))


def register_shortcuts():
    input_map = bpy.context.preferences.addons[CL_NAME].preferences.input_settings

    for name, shortcut in cl_utils.default_input_map.items():
        register_shortcut(input_map, name, shortcut)


def register_shortcut(prefs, name, shortcut):
    short = prefs.get(name)
    if short is None:
        short = prefs.add()
    else:
        return
    slen = len(shortcut)
    short.name = name
    short.label = shortcut[1]
    short.ctrl = slen > 2 and shortcut[2]
    short.alt = slen > 3 and shortcut[3]
    short.shift = slen > 4 and shortcut[4]
    short.key = shortcut[0]


def unregister_keymaps():
    keyconfigs = bpy.context.window_manager.keyconfigs
    defaultmap = None
    addonmap = None
    defaultmap = keyconfigs.default.keymaps
    addonmap = keyconfigs.addon.keymaps
    km_name, km_args, _ = km_cl()[0]
    
    if defaultmap and addonmap:
        addonmap.remove(addonmap.find(km_name, **km_args))
        defaultmap.remove(defaultmap.find(km_name, **km_args))


def register():
    from bpy.utils import register_class  # type: ignore

    register_keymaps()
    register_icons()
    for cls in classes:
        register_class(cls)
    bpy.types.Scene.units_enum = bpy.props.EnumProperty(items=enum_menu_items)
    bpy.types.Scene.cl_settings = bpy.props.PointerProperty(type=CLSettings)

    bpy.types.Scene.gizmo_show_hide = BoolProperty(
        name="Toolbar Gizmo Hide",
        description="Hide or show toolbar gizmo",
        default=False,
    )

    register_shortcuts()

    register_cl_tool()

    if not end_CL_on_load in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(end_CL_on_load)


def unregister():
    from bpy.utils import unregister_class  # type: ignore

    for cls in reversed(classes):
        try:
            unregister_class(cls)
        except RuntimeError:
            pass
    unregister_cl_tool()
    del bpy.types.Scene.units_enum

    # keymap
    # wm = bpy.context.window_manager
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    # clear list
    addon_keymaps.clear()

    unregister_icons()
    unregister_keymaps()

    if end_CL_on_load in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(end_CL_on_load)


if __name__ == "__main__":
    register()
