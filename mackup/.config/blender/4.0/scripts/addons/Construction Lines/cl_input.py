# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from typing import List

import bpy  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg

# write tests for this also


class NumInput:
    def __init__(self, unit_type: str, input_type: str, cur_dist: float, context: bpy.context):
        self._unit_type = unit_type
        self._input_type = input_type
        # convert to current units
        self._cur_dist = cur_dist
        self._context = context

    def parse_input_str(self, input_val: str, override_unit_scale=False) -> List[float]:
        # validate string
        if not isinstance(input_val, str):
            return []

        # remove any whitespace
        input_val.strip()

        if input_val == "":
            return []

        rval = []

        # make sure input only contains accepted characters

        # try to split input string
        inpt_split = input_val.split(",")

        # Always parse as imperial input first if in Imerpial units
        if self._unit_type == "IMPERIAL":
            for i, inpt in enumerate(inpt_split):
                inpt_split[i] = utl.parse_imp_input(
                    inpt.strip(), self._context)

        if self._input_type == utl.CL_INPUT_TYPE_SINGLE:
            rval = [self._parse_dist(inpt_split[0], override_unit_scale)]
        elif self._input_type == utl.CL_INPUT_TYPE_MULTI:  # rect
            try:
                # w = utl.convert_to_cur_scale(
                #     float(inpt_split[0]), self._context)
                w = self._parse_dist(inpt_split[0], override_unit_scale)
                w = utl.convert_to_cur_scale(w, self._context)
                h = w  # same as width unless comma input
                if len(inpt_split) > 1:
                    h = self._parse_dist(inpt_split[1], override_unit_scale)
                    h = utl.convert_to_cur_scale(h, self._context)
                    # h = utl.convert_to_cur_scale(
                    #     float(inpt_split[1]), self._context
                    # )
                rval = [w, h]
            except ValueError:
                print("Error: error in rectangle numeric input")
        return rval

    def _parse_dist(self, input_val: str, override_unit_scale: bool) -> float:
        return clg.parse_dist(
            self._cur_dist,
            input_val,
            override_unit_scale,
            self._context,
        )


##

def parse_angle_input(inpt: str) -> float:
    if utl.str_is_float(inpt) and -360 <= float(inpt) <= 360:
        return float(inpt)
        # rotate_geom(cl_op, context, angle=float(inpt))
    return 0.0


def parse_dupli_input(inpt: str) -> int:
    # pull out * or / from input
    # if no symbol then *
    # return a signed number (-ve for divide)
    try:
        s = inpt[0]
        if s in {"*", "/"}:
            inpt = int(inpt[1:])
            if s == "/":
                inpt *= -1
        return int(inpt)
    except ValueError:
        print("Error: Non numeric input")
    # return no duplication
    return 1
