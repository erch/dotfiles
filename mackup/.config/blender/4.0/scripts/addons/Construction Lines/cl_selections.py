# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from __future__ import annotations
from typing import List
import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector  # type: ignore

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .construction_lines28 import VIEW_OT_ConstructionLines
from . import cl_utils as utl
from . import cl_geom as clg


################################################################################
# 2D selection box (screen space)
################################################################################
class CLSelectBox:
    def __init__(self, loc):
        self._start_loc = []
        self._end_loc = []

        if self.is_input_loc_valid(loc):
            if len(loc) > 2:
                loc = loc[:2]
            self._start_loc = loc
            self._end_loc = loc

    def update_end_loc(self, loc):
        if self.is_input_loc_valid(loc):
            if len(loc) > 2:
                loc = loc[:2]
            self._end_loc = loc.copy()

    def is_input_loc_valid(self, loc):
        return isinstance(loc, list) and all(isinstance(x, int) for x in loc)

    def return_points(self):
        return [self._start_loc.copy(), self._end_loc.copy()]

    def return_dims(self):
        pts = self.return_selection_box()
        w = 0.0
        h = 0.0
        if pts:
            w = abs(pts[1][0] - pts[0][0])
            h = abs(pts[2][1] - pts[0][1])
        return w, h

    def calc_area(self):
        w, h = self.return_dims()
        return w * h

    def return_selection_box(self, loop=False):
        if not self._start_loc or not self._end_loc:
            return []
        # return selection rect
        s, e = self.return_points()

        pts = [s, [e[0], s[1]], e, [s[0], e[1]]]
        if loop:
            pts.append(s)
        return pts

    def is_positive_dir(self):
        pts = self.return_selection_box()
        if pts:
            if pts[1][0] - pts[0][0] > 0 and pts[2][1] - pts[0][1] > 0:
                return True
            elif pts[1][0] - pts[0][0] < 0 and pts[2][1] - pts[0][1] < 0:
                return True
            return pts[1][0] - pts[0][0] > 0 and pts[2][1] - pts[0][1] < 0
        return True


################################################################################
# Geom selection
################################################################################
class SelectObject:
    def __init__(self, obj):
        if utl.object_is_valid(obj):
            self._object = obj
            self._position = obj.original.matrix_world.translation.copy()
            self._origin = obj.location.copy()
            self._rotation = obj.rotation_euler.to_matrix()

    def is_object(self, obj):
        return self._object == obj.get_obj()

    def update_object(self, obj):
        if utl.object_is_valid(obj):
            self._object = obj

    def get_start_obj_rotation(self):
        return self._rotation

    def update_start_obj_rotation(self):
        self._rotation = self._object.rotation_euler.to_matrix()

    def get_obj(self):
        return self._object

    def get_obj_pos(self):
        return self._position.copy()

    def reset_object_origin(self):
        self.set_temp_object_origin(self._origin)

    def reset_obj_location(self):
        self._object.location = self._position

    def set_temp_object_origin(self, loc):
        if self._object.type != "MESH":
            return False
        emode = bpy.context.mode == "EDIT_MESH"
        if not emode:
            bpy.ops.object.mode_set(mode="EDIT")

        obj = self._object
        me = obj.data
        mw = obj.matrix_world
        bm = bmesh.from_edit_mesh(me)

        loc = obj.matrix_world.inverted() @ loc
        mw.translation = mw @ loc
        bmesh.ops.translate(bm, vec=-loc, verts=bm.verts)
        bmesh.update_edit_mesh(me)
        bm.free()

        if not emode:
            bpy.ops.object.mode_set(mode="OBJECT")
        return True


class SelectedGeom:
    def __init__(self):
        self.verts = []
        self.edges = []
        self.faces = []
        self.guides = []
        self.objects = []

    def clear_all(self):
        self.verts = []
        self.edges = []
        self.faces = []
        self.guides = []
        self.objects = []

    def clear(self, geom_type):
        if geom_type == utl.TYPE_VERT:
            self.verts = []
        elif geom_type == utl.TYPE_EDGE:
            self.edges = []
        elif geom_type == utl.TYPE_FACE:
            self.faces = []
        elif geom_type == utl.TYPE_GUIDE_E:
            self.guides = []
        elif geom_type == utl.TYPE_OBJECT:
            self.objects = []

    def clear_objects(self):
        self.objects = []

    def clear_geom(self):
        self.verts = []
        self.edges = []
        self.faces = []

    def clear_geom_ids(self):
        for v in self.verts:
            v[0] = None
        for e in self.edges:
            e[0] = None
        for f in self.faces:
            f[0] = None

    def geom_list_from_type(self, geom_type):
        if geom_type == utl.TYPE_VERT:
            return self.verts
        elif geom_type == utl.TYPE_EDGE:
            return self.edges
        elif geom_type == utl.TYPE_FACE:
            return self.faces
        elif geom_type == utl.TYPE_GUIDE_E:
            return self.guides
        elif geom_type == utl.TYPE_OBJECT:
            return self.objects
        return None

    def edit_geom_selected(self):
        return self.verts or self.edges or self.faces

    def return_first_obj(self):
        if self.objects:
            return self.objects[0].get_obj()
        return None

    # updated geometry selections removing verts from selection
    # where they are selected in edges and edges are removed
    # where they are selected in faces
    def process_selections(self, bm):
        verts = self.verts.copy()
        edges = self.edges.copy()
        faces = self.faces.copy()

        # 1. if verts in edges remove from list
        # 2. if edges in faces remove from list
        # return processed list

        new_vs = []
        new_es = []
        # process verts
        for v in verts:
            in_edge = False
            for e in edges:
                bm_e = utl.bm_geom_from_index(bm, e[0], utl.TYPE_EDGE)
                if not bm_e:
                    continue
                if v[0] in {bm_e.verts[0].index, bm_e.verts[1].index}:
                    in_edge = True
            if not in_edge:
                new_vs.append(v)

        # process edges
        for e in edges:
            in_face = False
            for f in faces:
                bm_f = utl.bm_geom_from_index(bm, f[0], utl.TYPE_FACE)
                if not bm_f:
                    continue
                if e[0] in [e.index for e in bm_f.edges]:
                    in_face = True
            if not in_face:
                new_es.append(e)
        self.verts = new_vs
        self.edges = new_es

    ### Update Selections
    # Takes a BMVert as new_data
    def update_selected_vert_data(self, vert_id, new_data):
        if isinstance(new_data, bmesh.types.BMVert):
            change_v = None
            for v in self.verts:
                if v[0] == vert_id:
                    change_v = v
                    break
            if change_v:
                change_v[0] = new_data.index
                change_v[1] = new_data.co.copy()
                change_v[2] = new_data.co.copy()

    # Takes a BMEdge as new_data
    def update_selected_edge_data(self, edge_id, new_data):
        if isinstance(new_data, bmesh.types.BMEdge):
            change_e = None
            for e in self.edges:
                if e[0] == edge_id:
                    change_e = e
                    break
            if change_e:
                e_verts = [new_data.verts[0].co.copy(), new_data.verts[1].co.copy()]
                change_e[0] = new_data.index
                change_e[1] = e_verts.copy()
                change_e[2] = e_verts.copy()

    # Takes a BMFace as new_data
    def update_selected_face_data(self, face_id, new_data):
        if isinstance(new_data, bmesh.types.BMFace):
            change_f = None
            for f in self.faces:
                if f[0] == face_id:
                    change_f = f
                    break
            if change_f:
                f_verts = [v.co.copy() for v in new_data.verts]
                change_f[0] = new_data.index
                change_f[1] = f_verts.copy()
                change_f[2] = f_verts.copy()

    # updates data in current geom selections
    def update_selected_geom_set(self, new_geom_set, geom_type):
        if geom_type == utl.TYPE_VERT:
            for i, v in enumerate(self.verts):
                self.update_selected_vert_data(v[0], new_geom_set[i])
        if geom_type == utl.TYPE_EDGE:
            for i, e in enumerate(self.edges):
                self.update_selected_edge_data(e[0], new_geom_set[i])
        if geom_type == utl.TYPE_FACE:
            for i, f in enumerate(self.faces):
                self.update_selected_face_data(f[0], new_geom_set[i])

    def update_selected_object(self, orig_obj, new_obj):
        if utl.object_is_valid(orig_obj) and utl.object_is_valid(new_obj):
            for o in self.objects:
                if o.get_obj() == orig_obj:
                    self.objects.remove(o)
                    self.objects.append(SelectObject(new_obj))
                    return

    ### Select
    def select_guide(self, geom, context):
        for g in self.guides:
            if geom.edge[0] in g and geom.edge[1] in g:
                self.guides.remove(geom.edge)
                return
        self.guides.append(geom.edge.copy())
        s_obj = geom.obj
        keys = ["Pair"]
        if utl.return_guide_keys_exist(s_obj, keys):
            e_obj = utl.return_object_by_name(s_obj[keys[0]], context)
            if e_obj:
                # add both ends to objects as new ClosestVerts with SelectObjects attached
                self.select_object(
                    clg.ClosestVert(
                        None, None, None, None, None, None, obj=SelectObject(s_obj)
                    )
                )
                self.select_object(
                    clg.ClosestVert(
                        None, None, None, None, None, None, obj=SelectObject(e_obj)
                    )
                )

    def select_object(self, geom):
        for o in self.objects:
            if o.get_obj() == geom.obj.get_obj():
                self.objects.remove(o)
                return
        self.objects.append(geom.obj)

    # [face index, cur_pos, orig_pos, is_manifold]
    def select_face(self, geom):
        for f in self.faces:
            if f[0] == geom.face:
                self.faces.remove(f)
                return

        if geom.face_verts:
            self.faces.append(
                [
                    geom.face,
                    geom.face_verts.copy(),
                    geom.face_verts.copy(),
                    geom.face_manifold,
                    geom.face_tri_indices,
                ]
            )

    # [edge index, cur_pos, orig_pos]
    def select_edge(self, geom):
        for e in self.edges:
            if geom.geom_id == e[0]:
                self.edges.remove(e)
                return
        self.edges.append([geom.geom_id, geom.edge.copy(), geom.edge.copy()])

    # [vert index, cur_pos, orig_pos]
    def select_vert(self, geom):
        for v in self.verts:
            if geom.geom_id == v[0]:
                self.verts.remove(v)
                return
        self.verts.append([geom.geom_id, geom.vert.copy(), geom.vert.copy()])

    def select(self, geom, context):
        if geom.geom_type == utl.TYPE_VERT:
            self.select_vert(geom)
        elif geom.geom_type == utl.TYPE_EDGE:
            self.select_edge(geom)
        elif geom.geom_type == utl.TYPE_FACE:
            self.select_face(geom)
        elif geom.geom_type == utl.TYPE_EDGE:
            self.select_edge(geom)
        elif geom.geom_type == utl.TYPE_GUIDE_E:
            self.select_guide(geom, context)
        elif geom.geom_type == utl.TYPE_OBJECT:
            self.select_object(geom)

    #################################################
    # ACTIONS
    #################################################
    # Create a face from selected geometry (Edit Mode)
    def create_face_from_selection(self, context: bpy.context) -> bool:
        if context.mode != "EDIT_MESH":
            return False

        obj = context.active_object
        if not utl.object_is_valid(obj):
            return False

        if self.verts and len(self.verts) > 2:
            bpy.ops.object.mode_set(mode="OBJECT")
            bm = bmesh.new(use_operators=True)
            bm.from_mesh(obj.data)
            vert_idxs = [v[0] for v in self.verts]
            utl.update_bmesh(bm)
            # select verts in blender mesh
            for i in vert_idxs:
                bm.verts[i].select_set(True)
            obj.data.update()
            bm.to_mesh(obj.data)
            bm.free()
            bpy.ops.object.mode_set(mode="EDIT")
            # create face
            try:
                bpy.ops.mesh.edge_face_add()
            except Exception as e:
                print(e)
                return False
        return True

    def select_face_list(
        self, obj: bpy.types.Object, face_id: int, context: bpy.context
    ) -> None:
        if not face_id or not utl.object_is_valid(obj):
            return
        # store if face edges are manifold here to work
        # around solidify/bool problems
        face_verts = utl.return_face_verts(obj, face_id, context)

        if not face_verts:
            return
         
        f_verts, is_manifold, tri_indices = face_verts


        cgp = clg.ClosestVert(
            None,
            None,
            face_id,
            utl.TYPE_FACE,
            None,
            None,
            face_verts=f_verts,
            face_manifold=is_manifold,
            face_tri_indices=tri_indices,
            obj=SelectObject(obj),
        )
        self.select(cgp, context)

    # TODO: Join below three functions and use a flag for type
    def select_all_verts(self, obj: bpy.types.Object, context: bpy.context) -> None:
        if not utl.object_is_valid(obj):
            return

        mtrx = obj.matrix_world.copy()

        for v in obj.data.vertices:
            cgp = clg.ClosestVert(
                mtrx @ v.co,
                None,
                None,
                utl.TYPE_VERT,
                None,
                None,
                obj=SelectObject(obj),
                geom_id=v.index,
            )
            self.select(cgp, context)

    def select_all_edges(self, obj: bpy.types.Object, context: bpy.context) -> None:
        if not utl.object_is_valid(obj):
            return

        mtrx = obj.matrix_world.copy()

        for e in obj.data.edges:
            cgp = clg.ClosestVert(
                None,
                [
                    mtrx @ obj.data.vertices[e.vertices[0]].co,
                    mtrx @ obj.data.vertices[e.vertices[1]].co,
                ],
                None,
                utl.TYPE_EDGE,
                None,
                None,
                obj=SelectObject(obj),
                geom_id=e.index,
            )
            self.select(cgp, context)

    def select_all_faces(self, obj: bpy.types.Object, context: bpy.context) -> None:
        if not utl.object_is_valid(obj):
            return

        mtrx = obj.matrix_world.copy()

        for f in obj.data.polygons:
            # select_face_list(cl_op, obj, f.index, context)
            cgp = clg.ClosestVert(
                None,
                None,
                f.index,
                utl.TYPE_FACE,
                None,
                None,
                face_verts=[mtrx @ obj.data.vertices[v].co for v in f.vertices],
                obj=SelectObject(obj),
                geom_id=f.index,
            )
            self.select(cgp, context)

    def any_selections_for_mode(self, context: bpy.context) -> List:
        if context.mode == "EDIT_MESH":
            return self.edit_geom_selected()
        return self.objects

    def select_closest(
        self,
        state: str,
        input_pos: List,
        close_obj: bpy.types.Object,
        close_geom_pt: clg.ClosestVert,
        cl_op: VIEW_OT_ConstructionLines,
        context: bpy.context,
    ) -> None:
        if context.mode == "EDIT_MESH":
            self.select_closest_geom(close_obj, close_geom_pt, context)
        else:
            self.select_closest_obj(state, input_pos, close_geom_pt, context)

    def select_closest_geom(
        self,
        close_obj: bpy.types.Object,
        close_geom_pt: clg.ClosestVert,
        context: bpy.context,
    ) -> None:
        if close_geom_pt:
            if close_geom_pt.geom_type == utl.TYPE_FACE:
                f_verts, is_manifold, face_tri_indices = utl.return_face_verts(
                    close_obj, close_geom_pt.face, context
                )
                close_geom_pt.face_verts = f_verts
                close_geom_pt.face_manifold = is_manifold
                close_geom_pt.face_tri_indices = face_tri_indices
                self.select(close_geom_pt, context)
            elif close_geom_pt.geom_type == utl.TYPE_EDGE:
                self.select(close_geom_pt, context)
            elif close_geom_pt.geom_type == utl.TYPE_VERT:
                self.select(close_geom_pt, context)

    def select_closest_obj(
        self,
        state: str,
        input_pos,
        close_geom_pt: clg.ClosestVert,
        context: bpy.context,
    ) -> bool:
        if close_geom_pt:
            # select whole guide if in rotate mode (not just an end obj)
            if state == utl.CL_STATE_ROT and close_geom_pt.geom_type == utl.TYPE_VERT:
                if not close_geom_pt.obj:
                    return False
                if close_geom_pt.obj.get_obj().type == "EMPTY":
                    close_pt = self.select_complete_guide(
                        close_geom_pt.obj.get_obj(), context
                    )
                    if close_pt:
                        self.select(close_pt, context)
                    return False

            if close_geom_pt.geom_type == utl.TYPE_GUIDE_E:
                self.select(close_geom_pt, context)
            elif close_geom_pt.obj:
                close_geom_pt.geom_type = utl.TYPE_OBJECT
                # This allows for objects overlapping
                try:
                    close_geom_pt.obj.update_object(
                        utl.select_closest_obj_click(
                            context, input_pos[0], input_pos[1], context.mode
                        )
                    )
                    self.select(close_geom_pt, context)
                except Exception as e:
                    print(e)
                    return False

        if not self.objects and not self.guides:
            return False
        return True

    def select_all_objects(self, context: bpy.context) -> None:
        if context.mode == "OBJECT":
            self.select_object_list(context.selectable_objects, context)

    def select_object_list(self, sel_objs: List, context: bpy.context) -> None:
        self.clear(utl.TYPE_OBJECT)
        for o in sel_objs:
            # create a new close_geom_pt with a SelectObject
            cgp = clg.ClosestVert(
                o.location,
                None,
                None,
                utl.TYPE_OBJECT,
                None,
                None,
                None,
                obj=SelectObject(o),
            )
            self.select(cgp, context)

    def select_objects_in_box(
        self, select_box: CLSelectBox, context: bpy.context
    ) -> None:
        sel_objs = []
        sel_bounds = select_box.return_selection_box()
        pos_dir = select_box.is_positive_dir()
        w, h = select_box.return_dims()

        if pos_dir:
            for obj in context.selectable_objects:
                obj_bounds = obj.bound_box
                mtrx = obj.matrix_world.copy()
                i = 0
                for v in obj_bounds:
                    loc2d = utl.convert_loc_2d(mtrx @ Vector(v), context)
                    if not clg.point_in_bounds_2d(
                        loc2d, sel_bounds[0][0], sel_bounds[0][1], w, h
                    ):
                        break
                    i += 1
                if i >= 8:
                    sel_objs.append(obj)
        else:
            # pass to Blender
            self.BLD_select_box(sel_bounds, context)
            sel_objs.extend(context.selected_objects)

        if sel_objs:
            self.select_object_list(sel_objs, context)

    def BLD_select_box(self, sel_bounds: List, context: bpy.context) -> None:
        if context.mode == "EDIT_MESH":
            context.scene.tool_settings.mesh_select_mode = [True, True, True]
        try:
            bpy.ops.view3d.select_box(
                xmin=sel_bounds[1][0],
                xmax=sel_bounds[0][0],
                ymin=sel_bounds[0][1],
                ymax=sel_bounds[3][1],
                wait_for_input=False,
                mode="SET",
            )
        except Exception as e:
            print(e)
            print("Unable to select geometry")

    def select_geom_in_box(self, select_box: CLSelectBox, context: bpy.context) -> None:
        obj = context.active_object
        # might not need to use bmesh
        if not utl.object_is_valid(obj):
            return

        sel_bounds = select_box.return_selection_box()
        pos_dir = select_box.is_positive_dir()
        w, h = select_box.return_dims()
        mtrx = obj.matrix_world.copy()
        obj = context.active_object

        if not pos_dir:
            # blender select box
            self.BLD_select_box(sel_bounds, context)

        bpy.ops.object.mode_set(mode="OBJECT")
        bm = bmesh.new(use_operators=True)
        bm.from_mesh(obj.data)
        utl.update_bmesh(bm)

        # VERTS
        for v in bm.verts:
            sel_v = None
            if pos_dir:
                loc2d = utl.convert_loc_2d(mtrx @ v.co, context)
                if clg.point_in_bounds_2d(
                    loc2d, sel_bounds[0][0], sel_bounds[0][1], w, h
                ):
                    sel_v = v
            else:
                # select only verts that are selected by Blender
                if v.select:
                    sel_v = v

            if sel_v:
                cgp = clg.ClosestVert(
                    mtrx @ v.co,
                    None,
                    None,
                    utl.TYPE_VERT,
                    None,
                    None,
                    obj=SelectObject(obj),
                    geom_id=v.index,
                )
                self.select(cgp, context)

        # EDGES
        for e in bm.edges:
            sel_e = None
            v0 = mtrx @ e.verts[0].co
            v1 = mtrx @ e.verts[1].co
            if pos_dir:
                v0_loc2d = utl.convert_loc_2d(v0, context)
                v1_loc2d = utl.convert_loc_2d(v1, context)

                # make sure both ends are inside select_box bounds
                if clg.point_in_bounds_2d(
                    v0_loc2d, sel_bounds[0][0], sel_bounds[0][1], w, h
                ) and clg.point_in_bounds_2d(
                    v1_loc2d, sel_bounds[0][0], sel_bounds[0][1], w, h
                ):
                    sel_e = e
            else:
                if e.select:
                    sel_e = e

            if sel_e:
                cgp = clg.ClosestVert(
                    None,
                    [v0, v1],
                    None,
                    utl.TYPE_EDGE,
                    None,
                    None,
                    obj=SelectObject(obj),
                    geom_id=e.index,
                )
                self.select(cgp, context)

        # FACES
        for f in bm.faces:
            sel_f = None
            f_vs = [mtrx @ v.co for v in f.verts]
            if pos_dir:
                f_vs_2d = [utl.convert_loc_2d(v, context) for v in f_vs]
                # Make sure all face verts are inside the select_box bounds
                if all(
                    [
                        clg.point_in_bounds_2d(
                            v2d, sel_bounds[0][0], sel_bounds[0][1], w, h
                        )
                        for v2d in f_vs_2d
                    ]
                ):
                    sel_f = f
            else:
                if f.select:
                    sel_f = f

            if sel_f:
                cgp = clg.ClosestVert(
                    None,
                    None,
                    f.index,
                    utl.TYPE_FACE,
                    None,
                    None,
                    face_verts=f_vs,
                    face_tri_indices=utl.return_face_tris(bm, f.index, f),
                    obj=SelectObject(obj),
                    geom_id=f.index,
                )
                self.select(cgp, context)
        # bm.free()
        obj.data.update()
        bm.to_mesh(obj.data)
        bm.free()
        bpy.ops.object.mode_set(mode="EDIT")

    def select_all_edit_geom(self, obj: bpy.types.Object, context: bpy.context) -> None:
        self.select_all_verts(obj, context)
        self.select_all_edges(obj, context)
        self.select_all_faces(obj, context)

    def select_complete_guide(
        self, guide_end: bpy.types.Object, context: bpy.context
    ) -> clg.ClosestVert:
        # return the guide edge from an end
        # connected_obj becomes guide_end
        keys = ["Pair"]
        if utl.return_guide_keys_exist(guide_end, keys):
            obj_pair = utl.return_object_by_name(guide_end[keys[0]], context)
            if not obj_pair:
                return None
            return clg.ClosestVert(
                guide_end.location,
                [guide_end.location, obj_pair.location],
                None,
                utl.TYPE_GUIDE_E,
                None,
                None,
                obj=guide_end,
            )
        return None

    def focus_selected(self, context: bpy.context) -> None:
        for sel_obj in self.objects:
            obj = sel_obj.get_obj()
            if utl.object_is_valid(obj):
                obj.select_set(True)

        if context.selected_objects:
            bpy.ops.view3d.view_selected()

    def local_view(self, context: bpy.context) -> None:
        for sel_obj in self.objects:
            obj = sel_obj.get_obj()
            if utl.object_is_valid(obj):
                obj.select_set(True)

        if context.selected_objects:
            bpy.ops.view3d.localview()

    def remove_geom(self, context: bpy.context, dissolve=False):
        if context.mode == "EDIT_MESH":
            bpy.ops.object.mode_set(mode="OBJECT")
            obj = context.active_object
            if utl.object_is_valid(obj):
                # bm = bmesh.from_edit_mesh(obj.data)
                bm = bmesh.new(use_operators=True)  # create an empty BMesh
                bm.from_mesh(obj.data)
                # Remove Geometry
                try:
                    utl.update_bmesh(bm)
                    # cl_op.selected_geom.process_selections(bm)
                    for v in self.verts:
                        rem_v = utl.bm_geom_from_index(bm, v[0], utl.TYPE_VERT)
                        if rem_v:
                            # bm.verts.remove(rem_v)
                            # utl.update_bmesh(bm)
                            if not dissolve:
                                bm.verts.remove(rem_v)
                            else:
                                bmesh.ops.dissolve_verts(
                                    bm,
                                    verts=[rem_v],
                                    use_face_split=False,
                                    use_boundary_tear=False,
                                )
                            utl.update_bmesh(bm)
                    for e in self.edges:
                        rem_e = utl.bm_geom_from_index(bm, e[0], utl.TYPE_EDGE)
                        if rem_e:
                            # bm.edges.remove(rem_e)
                            # utl.update_bmesh(bm)
                            if not dissolve:
                                bm.edges.remove(rem_e)
                            else:
                                bmesh.ops.dissolve_edges(
                                    bm,
                                    edges=[rem_e],
                                    use_verts=True,
                                    use_face_split=False,
                                )
                            utl.update_bmesh(bm)
                    for f in self.faces:
                        rem_f = utl.bm_geom_from_index(bm, f[0], utl.TYPE_FACE)
                        if rem_f:
                            # bm.faces.remove(rem_f)
                            # utl.update_bmesh(bm)
                            if not dissolve:
                                bm.faces.remove(rem_f)
                            else:
                                bmesh.ops.dissolve_faces(
                                    bm, faces=[rem_f], use_verts=True
                                )
                            utl.update_bmesh(bm)
                except Exception as e:
                    print(e)
                    print("Error removing geometry")
                obj.data.update()
                bm.to_mesh(obj.data)
                bm.free()
                bpy.ops.object.mode_set(mode="EDIT")
            self.clear(utl.TYPE_VERT)
            self.clear(utl.TYPE_EDGE)
            self.clear(utl.TYPE_FACE)
        else:
            for obj in self.objects:
                obj.get_obj().select_set(True)
                bpy.ops.object.delete()
                self.clear(utl.TYPE_OBJECT)
                self.clear(utl.TYPE_GUIDE_E)
