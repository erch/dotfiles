# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from typing import List, Tuple

import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg


def vert_exists(edge: bmesh.types.BMEdge, co: Vector) -> bool:
    for v in edge.verts:
        if v.co == co:
            return True
    return False

def edge_positions_match(check_edges: List[bmesh.types.BMEdge]) -> bool:
    """ 
    Test if two edges match by vert position
    using is_close function.

    """
    ce0_v0 = check_edges[0].verts[0].co
    ce0_v1 = check_edges[0].verts[1].co

    ce1_v0 = check_edges[1].verts[0].co
    ce1_v1 = check_edges[1].verts[1].co

    # Check if points are on both edge verts (=existing edge) and return [] if they are.
    # Use isClose check and not a coordinate comparison (floats often won't compare correctly)
    if (clg.is_close(ce0_v0, ce1_v0) or clg.is_close(ce0_v0, ce1_v1)) and (clg.is_close(ce0_v1, ce1_v0) or clg.is_close(ce0_v1, ce1_v1)):
        return True

    return False


def cal_edge_vert_intersections(bm: bmesh.types.BMesh, vert: bmesh.types.BMVert) -> None:
    if not isinstance(vert, bmesh.types.BMVert):
        return
    edges = [e for e in bm.edges]
    for e in edges:
        e_verts = e.verts
        if e_verts[0] == vert or e_verts[1] == vert:
            continue
        if clg.point_on_edge(vert.co, [e_verts[0].co, e_verts[1].co]):
            p1, p2 = [v.co for v in e_verts]
            if p1 == p2:
                continue
            fac = (vert.co - p1).length / (p2 - p1).length
            try:
                bmesh.utils.edge_split(e, e.verts[0], fac)
            except Exception as e:
                print("can't split edge")
                continue
    utl.update_bmesh(bm)


def calc_edges_intersect(check_edges: List[bmesh.types.BMEdge]) -> Vector:
    co = []

    ce0_v0 = check_edges[0].verts[0].co
    ce0_v1 = check_edges[0].verts[1].co

    ce1_v0 = check_edges[1].verts[0].co
    ce1_v1 = check_edges[1].verts[1].co

    # Check if points are on both edge verts (=existing edge) and return [] if they are.
    # Use isClose check and not a coordinate comparison (floats often won't compare correctly)
    if (clg.is_close(ce0_v0, ce1_v0) or clg.is_close(ce0_v0, ce1_v1)) and (clg.is_close(ce0_v1, ce1_v0) or clg.is_close(ce0_v1, ce1_v1)):
        return []

    # Check for edge lying on existing edge
    for v in check_edges[0].verts:
        if clg.point_on_edge(
            ce0_v0,
            [check_edges[1].verts[0].co, check_edges[1].verts[1].co],
        ) and clg.point_on_edge(
            ce0_v1,
            [check_edges[1].verts[0].co, check_edges[1].verts[1].co],
        ):
            co.append(v.co.copy())

    # Check for intersection
    if not co:
        ix = clg.find_edge_edge_intersection(check_edges)
        if ix:
            co.append(ix)
    return co


def split_edges_at_intersections(bm: bmesh.types.BMesh, edge: bmesh.types.BMEdge) -> List[bmesh.types.BMEdge]:
    edges = [e for e in bm.edges if e != edge]
    edges_to_split = []
    new_edges = []
    chk_edge_splits = []
    new_edges.append([edge.verts[0].co, edge.verts[1].co])
    for e in edges:
        check_edges = [edge, e]
        # co = clg.find_edge_edge_intersection(check_edges)
        co = calc_edges_intersect(check_edges)
        if co:
            for c in co:
                # if clg.check_intersect_possible(check_edges):
                # check point lies on both edges
                if clg.point_on_edge(c, [edge.verts[0].co, edge.verts[1].co]
                                     ) and clg.point_on_edge(c, [e.verts[0].co, e.verts[1].co]):
                    # new edge intersections
                    chk_edge_splits.append(c.copy())
                    # existing edge intersections
                    edges_to_split.append([e, c.copy()])

    # Split edges
    for es in edges_to_split:
        s_edge = es[0]
        co = es[1]
        if not s_edge.is_valid:
            bm.edges.remove(s_edge)
            continue
        if co:
            p1, p2 = [v.co for v in s_edge.verts]
            if p1 == p2:
                continue
            fac = (co - p1).length / (p2 - p1).length

            if fac <= 0:
                continue
            # store new edge splits as edge vert tuples
            new_e = split_edge_from_verts(s_edge.verts, co)
            if not return_edges_from_verts(new_e, bm, exclude=edge):
                new_edges.extend(new_e)
                try:
                    bmesh.utils.edge_split(s_edge, s_edge.verts[0], fac)
                except Exception as e:
                    print("can't split edge")
                    continue

    # remove original edge and build new edges from intersections
    if chk_edge_splits:
        orig_split = build_edges_from_intersections(
            new_edges[0], chk_edge_splits, bm)
        if orig_split:
            # remove original edge
            if edge.is_valid:
                bm.edges.remove(edge)
            new_edges.pop(0)
            new_edges.extend(orig_split)

    utl.update_bmesh(bm)
    # return list of new BMedges from stored split edges
    rtn_edges = return_edges_from_verts(new_edges, bm)
    return rtn_edges


def build_edges_from_intersections(s_edge_vect: List[Vector], splits: List[Vector], bm: bmesh.types.BMesh) -> List[Vector]:
    s_edge_vect = [v for v in s_edge_vect]
    splits = [v for v in splits]

    # remove splits if existing edge verts
    splits = [co for co in splits if co not in s_edge_vect]
    if not splits:
        return []
    # order splits by distance from s_edge[0]
    splits.append(s_edge_vect[1])
    ord_splits = order_edges_splits_by_dist(s_edge_vect[0], splits)
    split_edges = split_edges_to_segs(s_edge_vect[0], ord_splits)

    # replace with new segments
    for e in split_edges:
        nv = [bm.verts.new(e[0]), bm.verts.new(e[1])]
        utl.update_bm_verts(bm)
        bm.edges.new(nv)
        utl.update_bm_edges(bm)
    # return edges as vectors
    return split_edges


def order_edges_splits_by_dist(e0: Vector, splits: List[Vector]) -> List[Vector]:
    dist_list = []
    for s in splits:
        dist_list.append([s, clg.calc_len([e0, s])[0]])
    return [e[0] for e in utl.return_lowest_val_sorted_list(dist_list)]


def split_edges_to_segs(edge_start: Vector, splits: List[Vector]) -> List[Vector]:
    new_edges = [[edge_start, splits[0]]]
    i = 0
    while i < len(splits) - 1:
        new_edges.append([splits[i], splits[i + 1]])
        i += 1
    return new_edges


# returns two new edge's verts
def split_edge_from_verts(evs: List[bmesh.types.BMEdge], split_v: Vector) -> List:
    return [[evs[0].co, split_v], [evs[1].co, split_v]]


def return_edges_from_verts(edge_vs: List[Vector], bm: bmesh.types.BMesh, exclude=None) -> List:
    r_edges = []
    for e in edge_vs:
        edge = utl.bm_edge_from_verts(e[0], e[1], bm)
        if edge == exclude:
            continue
        if edge:
            r_edges.append(edge)
    return list(set(r_edges))


############################
# SPLIT FACES
############################
def split_faces(bm: bmesh.types.BMesh, edges: List[bmesh.types.BMEdge]) -> Tuple:
    face_split = False
    unlinked_edges = [e for e in edges if len(e.link_faces) < 2]
    faces = [f for f in bm.faces]
    used_edges = []

    for e in unlinked_edges:
        for f in faces:
            if e.verts[0] in f.verts and e.verts[1] in f.verts and f.index != -1:
                try:
                    face_split = bmesh.utils.face_split(
                        f, e.verts[0], e.verts[1], use_exist=False
                    )
                    used_edges.append(e)
                except Exception as e0:
                    try:
                        # try to split in the opposite direction
                        face_split = bmesh.utils.face_split(
                            f, e.verts[1], e.verts[0], use_exist=False
                        )
                        used_edges.append(e)
                    except Exception as e1:
                        print(e1)
                        continue
    utl.update_bmesh(bm)

    unused_edges = [e for e in unlinked_edges if e not in used_edges]

    # no edges have been used so edges become unused_edges
    if not unused_edges and not face_split:
        unused_edges = edges

    return (face_split, unused_edges)


def split_face_plane(face_to_split: bmesh.types.BMFace, face: bmesh.types.BMFace, obj: bpy.types.Object) -> List[bmesh.types.BMFace]:
    faces = []

    if not face_to_split.is_valid or not face.is_valid:
        return []
    
    try:
        faces = bmesh.utils.face_split_edgenet(face_to_split, face.edges)
        if len(faces) > 1:
            bmesh.update_edit_mesh(obj.data)
            return faces
    except Exception as e:
        print(e)
        return []
    return []


def split_face_plane_edges(face_to_split: bmesh.types.BMFace, edges: List[bmesh.types.BMEdge], obj: bpy.types.Object) -> List[bmesh.types.BMFace]:
    faces = None
    try:
        faces = bmesh.utils.face_split_edgenet(face_to_split, edges)
        if len(faces) > 1:
            bmesh.update_edit_mesh(obj.data)
            return faces
    except Exception as e:
        print(e)
        return []
    return []
