# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from typing import List
import bpy # type: ignore
import bmesh # type: ignore

from . import cl_utils as utl


class BLContext:
    def __init__(self, context):
        self._sd_o_show_faces = context.space_data.overlay.show_faces
        self._sd_o_show_face_cent = context.space_data.overlay.show_face_center
        self._sd_o_show_text = context.space_data.overlay.show_text
        self.edit_select_mode = context.scene.tool_settings.mesh_select_mode[:]
        self.start_workspace_name = context.workspace.name
        self._object_mods = []
        self.set_cl_context(context)

    def set_cl_context(self, context):
        context.scene.cl_settings.cl_running_bool = True
        context.space_data.overlay.show_faces = True
        context.space_data.overlay.show_face_center = True
        context.space_data.overlay.show_text = False
        context.scene.tool_settings.mesh_select_mode = [True, False, True]
        bpy.ops.wm.tool_set_by_id(name=utl.CL_TOOL_NAME)
        self.store_edit_mode_mods(context)

    def store_edit_mode_mods(self, context):
        for obj in context.view_layer.objects:
            mods = [m for m in obj.modifiers if m.show_in_editmode]
            for m in mods:
                m.show_in_editmode = False
            self._object_mods.append([obj.name, [m.name for m in mods]])
            
    
    def restore_edit_mode_mods(self):
        for obj_mod in self._object_mods:
            obj = bpy.context.scene.objects.get(obj_mod[0])
            if obj:
                for m in obj_mod[1]:
                    mod = obj.modifiers[m]
                    if mod:
                        mod.show_in_editmode = True

    # Store the currently selected objects and geometry (faces for now)
    def store_current_selections(self, obj, context) -> List:
        # return all selected objects if in object mode
        if context.mode == "OBJECT":
            return context.selected_objects
        elif context.mode == "EDIT_MESH":
            face = None
            edges = None
            verts = None
            if utl.object_is_valid(obj) and obj.visible_get():
                bpy.ops.object.mode_set(mode="OBJECT")
                bm = bmesh.new() 
                bm.from_mesh(obj.data)
                for f in bm.faces:
                    if f.select:
                        face = f.index
                        break
                
                # edges = [e.index for e in bm.edges if e.select]
                # verts = [v.index for v in bm.verts if v.select]
                bm.free()
                bpy.ops.object.mode_set(mode="EDIT")
            return [face, edges, verts]
        return []

    def restore_bl_context(self, context):
        '''
        Hand back context to Blender and reset any changed settings.
        Also end CL in both Edit an Object mode
        '''
        try:
            context.space_data.overlay.show_faces = self._sd_o_show_faces
            context.space_data.overlay.show_face_center = self._sd_o_show_face_cent
            context.space_data.overlay.show_text = self._sd_o_show_text
            context.scene.tool_settings.mesh_select_mode = self.edit_select_mode
            self.restore_edit_mode_mods()
        except Exception as e:
            print(e)
            print("Workspace has changed, can't restore original context.")
            return

        try:
            # stop CL running in all modes
            emode = context.mode == "EDIT_MESH"
            context.scene.tool_settings.mesh_select_mode = self.edit_select_mode[:]
            bpy.ops.wm.tool_set_by_id(name="builtin.select_box")
            if emode:
                bpy.ops.object.mode_set(mode="OBJECT")
                bpy.ops.wm.tool_set_by_id(name="builtin.select_box")
                bpy.ops.object.mode_set(mode="EDIT")
            else:
                if bpy.context.scene.objects:
                    for obj in bpy.context.scene.objects:
                        if utl.object_is_valid(obj) and obj.type == "MESH" and obj.name in context.view_layer.objects and not obj.hide_get():
                            context.view_layer.objects.active = obj
                            obj.select_set(True)
                            bpy.ops.object.mode_set(mode="EDIT")
                            bpy.ops.wm.tool_set_by_id(name="builtin.select_box")
                            bpy.ops.object.mode_set(mode="OBJECT")
                            break
            bpy.ops.wm.tool_set_by_id(name="builtin.select_box")
        except RuntimeError:
            print("Error restoring original context.")


