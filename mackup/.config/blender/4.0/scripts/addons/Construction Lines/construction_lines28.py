# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings


import copy
import math
import time
from typing import Optional, List


import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector, Matrix  # type: ignore
from bpy_extras.view3d_utils import (  # type: ignore
    region_2d_to_location_3d,
    region_2d_to_vector_3d,
    region_2d_to_origin_3d,
)

from . import cl_utils as utl
from . import cl_geom as clg
from . import cl_drawing as drw
from . import cl_snapto as snp
from . import cl_ctxmenu as cm
from . import cl_clobject as cl
from . import cl_blcontext as blc
from . import cl_input as cli
from . import cl_extrude as clx
from . import cl_settings_ui as sui
from . import cl_selections as cls
from . import cl_meshedit as cme

CL_NAME = __name__.split(".", maxsplit=1)[0]


class LastAction:
    def __init__(self, tool, val="", obj=None):
        self.tool = tool
        self.val = val
        self.obj_name = obj
        self.start_time = time.time()

    def update_time(self):
        self.start_time = time.time()

    def is_double_click(self, obj=None):
        if 0 < time.time() - self.start_time < 0.7:
            if obj:
                return obj == self.obj_name
        return False


#############################################
# DECORATORS
###########################################
# args[0] is cl_op
def geom_select_check(func):
    def geom_selected(*args, **kwargs):
        if bpy.context.mode == "EDIT_MESH":
            if not args[0].selected_geom.edit_geom_selected():
                utl.msg_log(args[0], utl.USR_MSG_NO_GEOM)
                return False
        else:
            if not args[0].selected_geom.objects:
                utl.msg_log(args[0], utl.USR_MSG_NO_OBJ)
                return False
        return func(*args, **kwargs)

    return geom_selected


def process_event(func):
    def input_event(*args, **kwargs):
        args[0].is_processing = True
        r_val = func(*args, **kwargs)
        args[0].is_processing = False
        return r_val

    return input_event


#############################################
# MAIN OPERATOR CODE
############################################
class VIEW_OT_ConstructionLines(bpy.types.Operator):
    """Measure distances, build construction guides, draw lines and build primitive shapes in place."""

    bl_idname = "view3d.construction_lines"
    bl_label = "Construction Lines"
    bl_options = {"REGISTER", "UNDO", "GRAB_CURSOR"}

    btn_val: bpy.props.StringProperty()
    running = False

    @classmethod
    def poll(cls, context):
        if context.area and not context.area.type == "VIEW_3D":
            return False

        return not VIEW_OT_ConstructionLines.running

    def modal(self, context, event):
        try:
            if not context.space_data:
                return self.cancel_operater(context)

            if context.area:
                context.area.tag_redraw()

            if not VIEW_OT_ConstructionLines.running:
                return self.cancel_operater(context)

            if (
                self.bl_context
                and self.bl_context.start_workspace_name != context.workspace.name
            ):
                return self.cancel_operater(context)

            if self.refresh_selections_flag:
                self.refresh_selections(context)

            k = event.type
            v = event.value

            if not self.is_processing:
                # MENUS AND EXIT
                # handle any menu actions
                if context.scene.cl_settings.cl_menuact_str != "":
                    self.handle_menu_action(
                        context, bpy.context.scene.cl_settings.cl_menuact_str
                    )
                    return {"RUNNING_MODAL"}
                # INPUT MOVE
                if k == "MOUSEMOVE":
                    self.mouse_pos = [event.mouse_region_x,
                                      event.mouse_region_y, 0]
                    self.handle_input_move(event, context)
                    return {"PASS_THROUGH"}

                self.obj_mode = context.scene.cl_settings.cl_objmode_bool
                # handle context menu
                if k == "RIGHTMOUSE" and v == "RELEASE":
                    if self.tape:
                        self.cancel_tape(
                            context, return_to_default=not self.tape)
                    else:
                        if self.state not in utl.STATE_ACTIONS:
                            bpy.ops.wm.call_menu(name=cm.CL_CTX_Menu.bl_idname)
                    return {"RUNNING_MODAL"}

                # USER CONFIGURABLE INPUT
                # END & EXIT
                prefs = context.preferences.addons[CL_NAME].preferences
                signature = utl.compile_input_signature(event, k)
                if prefs.match("END", signature) and v == "RELEASE":
                    self.cancel_tape(context, return_to_default=not self.tape)
                    return {"RUNNING_MODAL"}
                elif prefs.match("EXIT", signature):
                    return self.cancel_operater(context)

                # REGION BASED EVENTS
                # Check ignore viewport UI if releasing a tool -> ending tool over gizmos, etc.
                ignore_vp_ui = self.is_input_releasing_tool(event)

                if clg.click_in_view_3d(self.mouse_pos, context, ignore_vp_ui=ignore_vp_ui):
                    return self.process_view_event(event, context)
                else:
                    return self.process_outside_view_event(event, context)
            return {"PASS_THROUGH"}
        except Exception as e:
            print(e)
            utl.msg_log(
                self, "Error running Construction Lines. See Console for messages")
            return self.cancel_operater(context)

    def __init__(self):
        self.bl_context: Optional[blc.BLContext] = None
        self.u_num: str = ""
        self.u_dist: float = 0.0
        self.close_geom_pt: Optional[clg.ClosestVert] = None
        self.close_face: int = -1
        self.hover_geom = None
        self.selected_geom = cls.SelectedGeom()
        self.temp_object = None
        self.mouse_pos = []
        self.prev_dist = []
        self.tape: Optional[cl.ConstructLine] = None
        self.select_box = None
        self.tape_dist = []
        self.display_text = ""
        self.constraint = []
        self.soft_constraint = []
        self.cur_tape_name = ""
        self.cl_points = []
        self.dirty_flag = True
        self.refresh_selections_flag = False
        self.snap_obj = None
        self.snap_geom = []
        self.snapping_flags: int = 0
        self.focus_obj = None
        self.active_extrude: Optional[clx.ExtrudeOp] = None
        self.vert_cache = snp.VertCache([])
        self.obj_mode = True
        self.circle_segs = utl.CL_DEFAULT_CIR_SEGS
        self.guide_div = utl.CL_DEFAULT_GUIDE_DIVISIONS
        self.state = utl.CL_STATE_POLY_TAPE
        self.num_enter_mode = False
        self.auto_number_inpt = False
        self.numpad_nav = False
        self.override_unit_scale = False
        self.dupli_obj = None
        self.default_tool = utl.CL_STATE_POLY_TAPE
        self.keep_pts = False
        self.undo_counter = 0
        self.last_action: Optional[LastAction] = None
        self.is_processing = False

    def invoke(self, context, event):
        if self.btn_val == "RESET":
            VIEW_OT_ConstructionLines.running = False
            return {"CANCELLED"}

        VIEW_OT_ConstructionLines.running = True

        if (
            context.space_data
            and context.space_data.type == "VIEW_3D"
            and not context.space_data.region_quadviews
        ):
            args = (self, context)
            self._handle_line = bpy.types.SpaceView3D.draw_handler_add(
                drw.cl_draw_line_callback_px, args, "WINDOW", "POST_VIEW"
            )
            self._handle = bpy.types.SpaceView3D.draw_handler_add(
                drw.cl_draw_callback_px, args, "WINDOW", "POST_PIXEL"
            )
            wm = context.window_manager
            wm.modal_handler_add(self)
            self.reset_operator(context, return_to_default=True)

            self.handoff_current_context(True, context)
            # workaround for bug in first undo
            # bpy.ops.ed.undo_push()
            self.push_undo("CL:Start")
            context.scene.gizmo_show_hide = True
            return {"RUNNING_MODAL"}
        else:
            self.report(
                {"WARNING"}, "Active space must be View3d and not Quad View")
            VIEW_OT_ConstructionLines.running = False
            return {"CANCELLED"}

    def create_face_from_selected_verts(self, context: bpy.context) -> None:
        if self.selected_geom:
            if not self.selected_geom.create_face_from_selection(context):
                utl.msg_log(self, "Unable to create face.")

    def tape_finish(self) -> bool:
        if self.tape:
            self.tape_dist = self.tape.get_tape_len()
            self.tape.finish_tape()
            return True if self.tape.return_tape_finished() else False
        return False

    def cancel_tape(self, context: bpy.context, return_to_default=False):
        if self.state == utl.CL_STATE_MOV:
            # finish_move(cl_op, context)
            move_geom(self, context, cancel=True)
            self.end_outstanding_tasks()
        if self.state == utl.CL_STATE_ROT:
            reset_obj_rotation(self, context)
        if self.state == utl.CL_STATE_EXT:
            finish_extrude(self, True)
        self.reset_operator(context, return_to_default=return_to_default)

    def reset_constraint(self, constraint: List):
        if self.constraint and constraint == self.constraint:
            self.constraint = []
        else:
            self.constraint = constraint

    # Constrain to current direction
    def toggle_aribtrary_constraint(self):
        if self.constraint:
            self.reset_constraint(self.constraint)
        else:
            if self.tape:
                dir_v = clg.calc_dir_vect(self.tape.return_verts())
                constraint = utl.make_constraint(dir_v, "ARB")
                self.reset_constraint(constraint)

    def return_snap_flags(self) -> int:
        # returns snapping options (allow for users to chose in panel)
        # If in extrude mode and on selection phase, only return face snap flag

        # setup a bit flag pref for each type of snap
        if self.state == utl.CL_STATE_EXT:
            if not self.tape:
                return utl.SNP_FLAG_FACE
            else:
                # don't snap to global axes when extruding
                return self.snapping_flags ^ utl.SNP_GLB_AXES

        return self.snapping_flags

    def update_snap_flags(self, context: bpy.context) -> None:
        self.snapping_flags = sui.set_snap_flag_from_prefs(context)

    def change_tape_type(self, cl_type: int):
        if self.tape:
            self.tape.set_tape_type(cl_type)

    def toggle_state(self, state: str, context: bpy.context) -> None:
        if state not in utl.STATES_ALL:
            return

        # operation already running
        # if cl_op.state in utl.STATE_ACTIONS:
        if self.tape:
            if self.state != utl.CL_STATE_ROT:
                self.tape.set_visible(True)
            else:
                self.tape.set_visible(False)
            self.cancel_tape(context)
        else:
            if state == utl.CL_STATE_EXT:
                if context.mode != "EDIT_MESH":
                    return
            # TEMP: Remove when edit mode rotation is developed
            if state == utl.CL_STATE_ROT:
                if context.mode == "EDIT_MESH":
                    return

            if state in utl.STATE_SINGLE_MODES:
                self.change_tape_type(utl.TAPE_TYPE_SINGLE)
            else:
                self.change_tape_type(utl.TAPE_TYPE_MULTI)

            # clear face selections
            if state in utl.STATE_POLY_ALL_MODES:
                self.selected_geom.clear(utl.TYPE_FACE)

        context.scene.cl_settings.cl_selected_mode = state
        self.state = state
        self.select_box = None
        self.u_num = ""  # reset on change
        self.display_op_hint()

    def handle_mode_change(self, context):
        # if operation is running - don't allow mode change
        if self.tape:
            return
        if context.mode == "OBJECT":
            if not self.selected_geom.objects:
                self.select_closest(True, context)
            if self.selected_geom.objects:
                obj = self.selected_geom.return_first_obj()
                if obj and utl.object_is_valid(obj):
                    if obj.type == "MESH":
                        context.view_layer.objects.active = obj
                        obj.select_set(True)
                        self.snap_obj = obj
                        bpy.ops.object.mode_set(mode="EDIT")
                        # TEMP: Remove when edit mode rotation is developed
                        if self.state == utl.CL_STATE_ROT:
                            self.toggle_state(utl.CL_STATE_SEL, context)
        else:
            bpy.ops.object.mode_set(mode="OBJECT")
            if self.state == utl.CL_STATE_EXT:
                self.toggle_state(utl.CL_STATE_SEL, context)

    #############################################
    # INPUT
    ############################################
    def is_input_releasing_tool(self, event) -> bool:
        # k = event.type
        v = event.value

        if v != "RELEASE":
            return False

        if self.state in {utl.CL_STATE_EXT, utl.CL_STATE_SEL}:
            return True

        return False

    def handle_arrows_wheel(self, key, ctrl, context: bpy.context) -> None:
        if key == utl.B_ARROWS_WHEEL[0] or (key == utl.B_ARROWS_WHEEL[1] and ctrl):
            if self.state in {utl.CL_STATE_POLY_CIRC, utl.CL_STATE_POLY_ARC}:
                change_circle_segs(self, context, 1)
            elif self.state == utl.CL_STATE_POLY_TAPE:
                change_guide_divisions(self, 1)
        elif key == utl.B_ARROWS_WHEEL[2] or (key == utl.B_ARROWS_WHEEL[3] and ctrl):
            if self.state in {utl.CL_STATE_POLY_CIRC, utl.CL_STATE_POLY_ARC}:
                change_circle_segs(self, context, -1)
            elif self.state == utl.CL_STATE_POLY_TAPE:
                change_guide_divisions(self, -1)

    # Handle typing
    def handle_num_input(self, val, rmve, shift, context: bpy.context):
        if rmve:
            self.u_num = self.u_num[:-1]
        else:
            if val in utl.IMP_OPS and utl.return_scene_unit_sys(context) == "IMPERIAL":
                n_val = val
            elif val in utl.B_NUMPAD_NUMS:
                n_val = utl.convert_bnumpad(val)
            else:
                n_val = utl.convert_bnum(val)
                if shift:
                    n_val = utl.convert_shift_bnum(val)
            # only allow commas in rect mode
            if n_val == "," and self.state != utl.CL_STATE_POLY_RECT:
                return
            self.u_num = self.u_num + n_val
        utl.update_info_bar(context, self.u_num)

    def plot_input_rect(self, rect_dims: List) -> bool:
        if not self.tape:
            return False

        cur_pts = clg.plot_rect(
            self.tape.return_verts(), self.constraint, self.soft_constraint
        )

        # Translate all points to (0,0,0)
        # Scale
        # Then back to cur_pts[0]
        mat_world = Matrix.Translation(-Vector(cur_pts[0].copy()))
        mat_orig = Matrix.Translation(Vector(cur_pts[0].copy()))

        n_pts = []
        for p in cur_pts:
            n_pts.append(mat_world @ Vector(p))

        if len(n_pts) < 4:
            return False

        try:
            w_scale = rect_dims[0] / clg.calc_len([n_pts[3], n_pts[0]])[0]
            h_scale = rect_dims[1] / clg.calc_len([n_pts[1], n_pts[0]])[0]
        except ZeroDivisionError:
            return False

        w_mtrx_scale = Matrix.Scale(
            abs(w_scale),
            4,
            clg.return_direction_vector(n_pts[3], n_pts[0]).normalized(),
        )
        h_mtrx_scale = Matrix.Scale(
            abs(h_scale),
            4,
            clg.return_direction_vector(n_pts[1], n_pts[0]).normalized(),
        )
        self.tape.set_end_vert(
            mat_orig @ w_mtrx_scale @ h_mtrx_scale @ Vector(n_pts[2])
        )

        return True

    # Parse and apply number input
    def handle_num_input_end(self, context: bpy.context) -> None:
        inpt = self.u_num
        if self.tape:
            if inpt != "":
                # create NumInput object to handle parsing of number inputs
                num_input = cli.NumInput(
                    utl.return_scene_unit_sys(context),
                    utl.input_type_from_state(self.state),
                    clg.calc_len(self.tape.return_verts())[0],
                    context,
                )

                if self.state == utl.CL_STATE_ROT:
                    t = cli.parse_angle_input(inpt)
                    rotate_geom(self, angle=t)
                    finish_obj_rotation(self, context)
                    self.end_input(context)
                elif (
                    self.state == utl.CL_STATE_MOV
                    and self.dupli_obj
                    and self.tape.in_second_phase()
                ):
                    inpt = cli.parse_dupli_input(inpt)
                    duplicate_multi(self, inpt, context)
                    self.end_input(context)
                else:
                    dist = num_input.parse_input_str(
                        inpt, self.override_unit_scale)
                    if not dist:
                        utl.msg_log(self, "Invalid Input")
                        return
                    if self.state == utl.CL_STATE_POLY_RECT:
                        w, h = dist
                        r_w, r_h = clg.convert_amnt_by_direction_vec(
                            self.tape.return_verts(), self.constraint, w, h
                        )
                        if not self.plot_input_rect([r_w, r_h]):
                            return
                    else:
                        self.tape.extend_tape_by_amnt(dist[0])
                        # make final move before ending movement
                        if self.state == utl.CL_STATE_MOV:
                            move_geom(self, context)
                    self.end_input(context)

        self.u_num = ""
        self.num_enter_mode = False
        utl.update_info_bar(context, self.u_num, no_clear=True)

    def is_double_click(self) -> bool:
        dbl_click = False
        if not self.last_action:
            self.set_last_action()
        else:
            obj = None
            clk_obj = None
            if (
                self.close_geom_pt
                and self.close_geom_pt.obj
                and self.close_geom_pt.geom_type != utl.TYPE_GUIDE_E
            ):
                obj = self.close_geom_pt.obj.get_obj()
                clk_obj = None
                if obj and obj.type != "EMPTY":
                    clk_obj = obj.name
                dbl_click = self.last_action.is_double_click(
                    obj=clk_obj if obj else None
                )
                self.last_action = None
            else:
                self.last_action.update_time()
        return dbl_click

    def handle_select(self, context, event, double_click: bool) -> None:
        if not event.shift:
            self.selected_geom.clear_all()

        if event.value == "PRESS" and not double_click:
            self.select_box = cls.CLSelectBox(self.mouse_pos)
        else:
            if self.select_box and not double_click:
                if self.select_box.calc_area() > 0:
                    if context.mode == "OBJECT":
                        self.selected_geom.select_objects_in_box(
                            self.select_box, context
                        )
                    else:
                        self.selected_geom.select_geom_in_box(
                            self.select_box, context)
                    # Clear selection Box
                    self.select_box = None
                    return
            if context.mode == "OBJECT":
                if double_click:  # cl_op.selected_geom.objects and double_click:
                    self.handle_mode_change(context)
                else:
                    self.select_closest(True, context)
            else:
                if self.close_geom_pt:
                    self.select_closest(False, context)
                else:
                    if self.select_box and self.select_box.calc_area() <= 0:
                        self.handle_mode_change(context)

            # Clear selection Box
            self.select_box = None

    def select_closest(self, is_obj: bool, context: bpy.context) -> None:
        if not self.close_geom_pt:
            return

        if is_obj:
            if not self.selected_geom.select_closest_obj(
                self.state, self.mouse_pos, self.close_geom_pt, context
            ):
                utl.msg_log(self, utl.USR_MSG_NO_OBJ)
        else:
            self.selected_geom.select_closest_geom(
                self.snap_obj, self.close_geom_pt, context
            )

    # Select all objects of object's edit geometry
    def handle_select_all(self, alt: bool, context: bpy.context) -> None:
        if self.tape:
            return
        if context.mode == "OBJECT":
            self.selected_geom.clear_objects()
            if not alt:
                self.selected_geom.select_all_objects(context)
        else:
            self.selected_geom.clear_geom()
            if not alt:
                self.selected_geom.select_all_edit_geom(
                    context.view_layer.objects.active, context)

    # Left click event
    # If ctrl pressed select and store points
    # else create tape
    def handle_input_click(
        self, event, double_click: bool, context: bpy.context
    ) -> None:
        # SELECT
        if self.state == utl.CL_STATE_SEL:
            self.handle_select(context, event, double_click)
            return

        # MOVE ROTATE
        # if no selections always try to select closest object or geom
        if self.state in {utl.CL_STATE_MOV, utl.CL_STATE_ROT}:
            if not self.selected_geom.any_selections_for_mode(context):
                self.selected_geom.select_closest(
                    self.state,
                    self.mouse_pos,
                    self.snap_obj,
                    self.close_geom_pt,
                    self,
                    context,
                )
            if self.state == utl.CL_STATE_ROT and not self.constraint:
                self.constraint = utl.return_global_axes()[2]

        # EXTRUDE
        # if no selected face try to select closest one
        self.hover_geom = None
        if self.state == utl.CL_STATE_EXT and not self.selected_geom.faces:
            self.selected_geom.clear(utl.TYPE_FACE)
            self.select_closest(False, context)

        # TAPE
        if self.tape:
            # set end tape point
            if not self.tape.return_tape_finished():
                if not self.close_geom_pt or self.constraint:
                    self.tape.drag_tape_end([], None, self.state)
                else:
                    self.tape.drag_tape_end(
                        self.close_geom_pt.vert.copy(), None, self.state
                    )
                self.end_input(context)
        else:
            self.dupli_obj = None
            if self.close_geom_pt:
                new_construction_line(self, context, None, None)
                # if self.state in {utl.CL_STATE_MOV, utl.CL_STATE_EXT}:
                if self.state == utl.CL_STATE_EXT:
                    if self.tape:
                        self.tape.set_visible(False)
                    if self.state == utl.CL_STATE_EXT:
                        if not setup_extrude(self, context):
                            self.cancel_tape(context)
                            return
                if self.state == utl.CL_STATE_ROT:
                    if self.tape:
                        if self.tape.get_start_obj():
                            set_object_origin_to_click(self, context)

            else:
                if self.state in {utl.CL_STATE_MOV, utl.CL_STATE_EXT, utl.CL_STATE_ROT}:
                    self.cancel_tape(context)
                    return
                mouse_pos = self.return_mouse_loc(context)
                if utl.is_in_ortho_view(context):
                    mouse_pos = utl.flatten_location_to_ortho(
                        utl.get_view_orientation_from_matrix(
                            utl.get_view_matrix(context)
                        ),
                        mouse_pos,
                        Vector((0, 0, 0)),
                    )
                self.close_geom_pt = clg.ClosestVert(
                    mouse_pos, None, None, utl.TYPE_CURSOR, None, None
                )
                new_construction_line(self, context, None, None)
        self.display_op_hint()

    # return list of geom to ignore when snapping
    def return_ignore_geom(self, context: bpy.context) -> List[clg.IgnoreGeom]:
        ignore_geom = []
        if context.mode == "OBJECT" and self.state == utl.CL_STATE_MOV:
            if self.state == utl.CL_STATE_MOV:
                for sel_obj in self.selected_geom.objects:
                    obj = sel_obj.get_obj()
                    if utl.object_is_valid(obj):
                        ignore_geom.append(
                            clg.IgnoreGeom(obj.name, utl.TYPE_OBJECT, -1)
                        )
        return ignore_geom

    # Mouse move event
    @process_event
    def handle_input_move(self, event, context: bpy.context) -> None:
        mouse_pos = self.return_mouse_loc(context)
        close_geom_pt = None
        working_plane_loc = None
        adjust_pt = None
        self.soft_constraint = []
        self.hover_geom = None
        # must reset close_geom_pt here
        self.close_geom_pt = None
        start_v = None

        snap_opts = self.return_snap_flags()

        if self.select_box:
            self.select_box.update_end_loc(self.mouse_pos)

        # check for any ignore obj or geometry
        # make sure tape has started before ignoring to allow for initial snap
        ignore_geom = None
        if self.state == utl.CL_STATE_MOV and self.tape:
            ignore_geom = self.return_ignore_geom(context)

        # HANDLE SNAPPING
        if not event.alt:
            snp.snap(
                self,
                context,
                limit_to_edit=is_in_edit_selection_state(self, context),
                snp_options=snap_opts,
                ignore_geom=ignore_geom,
            )
            # tape start
            if self.tape:
                start_v = self.tape.return_verts()[0]

            # working plane
            view_normal = self.return_view_normal(context)
            try:
                close_geom_pt, close_face, close_face_loc, close_face_norm, close_axis, working_plane_loc = snp.pr2d(
                    context,
                    self,
                    utl.cursor_loc(context).copy(),
                    start_v,
                    view_normal,
                    snp_options=snap_opts,
                )
                self.dirty_flag = False
                self.snap_geom = []
            except Exception as e:
                print(e)
                print("Invalid Snap Target")
                return
            # Try grid snap
            if (
                not close_geom_pt
                and snap_opts & utl.SNP_GLB_GRID
                and utl.return_viewport_grid_visible(context)
            ):
                if utl.is_in_ortho_view(context):
                    view_orient = utl.get_view_orientation_from_matrix(
                        utl.get_view_matrix(context)
                    )
                    mouse_pos = utl.flatten_location_to_ortho(
                        view_orient, mouse_pos, Vector((0, 0, 0))
                    )

                    close_geom_pt = snp.return_grid_snap(
                        mouse_pos,
                        context,
                        sub_divs=bool(snap_opts & utl.SNP_GLB_GRID_SUBD),
                    )

            # Stay on face/global axes/working plane
            if not close_geom_pt:
                if close_face_loc:
                    close_geom_pt = clg.ClosestVert(
                        close_face_loc,
                        None,
                        close_face,
                        utl.TYPE_FACE,
                        None,
                        None,
                        face_norm=close_face_norm,
                        obj=cls.SelectObject(self.snap_obj),
                    )
                    if self.state == utl.CL_STATE_EXT:
                        if not self.tape:
                            hover_face(self, close_geom_pt, context)
                # look to global axes if no other snap geometry
                elif close_axis:
                    close_geom_pt = close_axis
                elif working_plane_loc and self.state not in {
                    utl.CL_STATE_SEL,
                    utl.CL_STATE_EXT,
                    utl.CL_STATE_MOV,
                    utl.CL_STATE_ROT,
                }:
                    close_geom_pt = clg.ClosestVert(
                        working_plane_loc,
                        None,
                        close_face,
                        utl.TYPE_PLANE,
                        None,
                        None,
                        face_norm=view_normal.copy(),
                    )

            if snap_opts & utl.SNP_FLAG_DIR:
                # setup reference to closest object
                if close_geom_pt:
                    # Don't log object if guide edge. Guides can handle their own object ends
                    # Working plane should also not have an object
                    if close_geom_pt.geom_type not in {
                        utl.TYPE_GUIDE_E,
                        utl.TYPE_PLANE,
                    }:
                        if utl.object_is_valid(self.snap_obj):
                            close_geom_pt.obj = cls.SelectObject(self.snap_obj)

                # Find highlights and soft_constraints
                if self.tape and not self.constraint:
                    snp_hl = snp.return_highlights_and_soft_constraints(
                        start_v, self.tape, close_geom_pt, mouse_pos, context
                    )

                    if snp_hl:
                        adjust_pt, self.soft_constraint = snp_hl

        # APPLY ADJUSTMENTS - New function
        if not adjust_pt:
            if close_geom_pt:
                self.close_geom_pt = close_geom_pt
            else:
                self.close_geom_pt = None
        else:
            # if closest_geom is an edge -> adjust close_geom_pt to intersection between
            # closest_geom_edge and (start_pt-adjust_pt) edge
            self.close_geom_pt = close_geom_pt  # set first so then can adjust
            if close_geom_pt and close_geom_pt.geom_type in utl.ALL_EDGE_TYPES:
                close_geom_pt.vert = self.adjust_closest_geom(
                    close_geom_pt, adjust_pt, start_v
                )
                self.close_geom_pt = close_geom_pt
            # if closest_geom is a vert/point -> highlight but override any adjustment
            elif close_geom_pt and close_geom_pt.geom_type in utl.ALL_PT_TYPES:
                self.close_geom_pt = close_geom_pt
            else:
                self.close_geom_pt = adjust_pt

        # HANDLE ANY OPERATIONS
        if self.tape:
            if not self.tape.return_tape_finished():
                self.tape.drag_tape_end(
                    self.close_geom_pt.vert if self.close_geom_pt else mouse_pos,
                    self.constraint if not event.alt else [],
                    self.state,
                )

                if self.state == utl.CL_STATE_MOV:
                    prefs = context.preferences.addons[CL_NAME].preferences
                    signature = utl.compile_input_signature(
                        event, event.type, override_k=True)
                    dupli = False
                    dupli_link = False
                    if prefs.match("DUPLICATE", signature):
                        dupli = True

                    if prefs.match("DUPLICATE AND LINK", signature):
                        dupli_link = True

                    # duplicate = event.ctrl or event.alt
                    duplicate = dupli or dupli_link
                    move_geom(self, context, duplicate=duplicate,
                              link_duplicates=dupli_link)
                elif self.state == utl.CL_STATE_EXT:
                    extrude_geom(self)
                elif self.state == utl.CL_STATE_ROT and self.tape.in_second_phase():
                    rotate_geom(self)
            # display_current_op_info(cl_op, context)

    def return_view_normal(self, context: bpy.context) -> Vector:
        loc = self.mouse_pos
        view_normal = Vector((0, 0, 1))
        if self.constraint:
            if self.state == utl.CL_STATE_POLY_RECT:
                if self.constraint[1] == "X":
                    view_normal = Vector((0, 1, 0))
                elif self.constraint[1] == "Y":
                    view_normal = Vector((1, 0, 0))
            else:
                if self.constraint[1] == "Z":
                    region = context.region
                    rv3d = context.region_data
                    coord = loc[0], loc[1]
                    view_vector = region_2d_to_vector_3d(region, rv3d, coord)
                    ray_origin = region_2d_to_origin_3d(region, rv3d, coord)
                    view_normal = -(ray_origin + view_vector)
                    # view_normal = Vector((0, 0, 0))

        else:
            # get ortho direction
            if utl.is_in_ortho_view(context):
                o_view = utl.get_view_orientation_from_matrix(
                    utl.get_view_matrix(context)
                )
                view_normal = utl.normal_from_ortho_view_type(o_view)
        return view_normal

    def display_op_hint(self) -> None:
        phase = 0
        if self.tape:
            phase = self.tape.return_phase() - 1
        d_txt = utl.return_phase_message(self.state, phase)

        if phase == 1 and self.state not in {utl.CL_STATE_POLY_ARC, utl.CL_STATE_ROT}:
            d_txt += utl.return_numeric_message(
                1 if self.auto_number_inpt else 0)
            if self.state == utl.CL_STATE_MOV:
                d_txt += utl.return_phase_message(self.state, phase + 1)
        elif phase == 2 and self.state in {utl.CL_STATE_POLY_ARC, utl.CL_STATE_ROT}:
            d_txt += utl.return_numeric_message(
                1 if self.auto_number_inpt else 0)
        elif phase == 2 and self.state == utl.CL_STATE_MOV:
            d_txt = utl.return_phase_message(self.state, phase + 1)

        self.display_text = d_txt

    # Return the point where close_geom.edge and (start_pt-adjust_pt) edge intersect
    def adjust_closest_geom(
        self, close_geom: clg.ClosestVert, adjust_pt: Vector, start_pt: Vector
    ) -> Vector:
        adj_edge = [start_pt, adjust_pt.vert]
        geom_edge = close_geom.edge

        # find where adj_edge intersects geom_edge
        co = clg.find_edge_edge_intersection_verts(
            [adj_edge[0], adj_edge[1], geom_edge[0], geom_edge[1]]
        )
        if co:
            if clg.point_on_edge(co, [geom_edge[0], geom_edge[1]]):
                return co
        return close_geom.vert

    # Try to finish tape
    # if tape_finish returns false then return and continue
    # user input
    # overrid will force a reset and cleanup
    def end_input(self, context: bpy.context, override=False) -> None:
        reset_to_default = False
        if (self.tape and self.tape_finish()) or override:
            clean_up = True
            if not override:
                # check for state - point, circle, rect, arc
                if self.state in utl.STATE_POLY_ALL_MODES:
                    if self.state == utl.CL_STATE_POLY_CIRC:
                        create_poly(self, utl.CL_C_CIRCLENAME, context)
                    elif self.state == utl.CL_STATE_POLY_RECT:
                        create_poly(self, utl.CL_C_RECTNAME, context)
                    elif self.state == utl.CL_STATE_POLY_ARC:
                        create_poly(self, utl.CL_C_ARCNAME, context)
                    else:
                        clean_up = create_line_axis_point(self, context)
                elif self.state == utl.CL_STATE_ROT:
                    finish_obj_rotation(self, context)
                elif self.state == utl.CL_STATE_EXT:
                    extrude_geom(self)
                    finish_extrude(self, False)
                elif self.state == utl.CL_STATE_MOV:
                    finish_move(self, context)
            self.dirty_flag = True
            self.reset_constraint([])
            self.push_undo("CL:" + self.state)

            if clean_up:
                self.reset_operator(
                    context, return_to_default=reset_to_default)
        else:
            # make sure constraint is off (will affect arcs)
            if self.state != utl.CL_STATE_ROT:
                if self.state == utl.CL_STATE_MOV:
                    finish_move(self, context)
                    self.push_undo("CL:" + self.state)
                self.reset_constraint([])

    def handle_menu_action(self, context: bpy.context, m_act: str) -> None:
        if m_act == utl.ACTION_HIDESHOW:
            show_hide_cls()
        elif m_act == utl.ACTION_REMOVEALL:
            remove_all_cls(self, context)
        elif m_act == utl.ACTION_SELECT:
            self.toggle_state(utl.CL_STATE_SEL, context)
        elif m_act == utl.ACTION_DRAWTAPE:
            self.toggle_state(utl.CL_STATE_POLY_TAPE, context)
        elif m_act == utl.ACTION_DRAWLINE:
            self.toggle_state(utl.CL_STATE_POLY_LINE, context)
        elif m_act == utl.ACTION_DRAWCIRCLE:
            self.toggle_state(utl.CL_STATE_POLY_CIRC, context)
        elif m_act == utl.ACTION_DRAWRECT:
            self.toggle_state(utl.CL_STATE_POLY_RECT, context)
        elif m_act == utl.ACTION_DRAWARC:
            self.toggle_state(utl.CL_STATE_POLY_ARC, context)
        elif m_act == utl.ACTION_MOVE:
            self.toggle_state(utl.CL_STATE_MOV, context)
        elif m_act == utl.ACTION_ROTATE:
            self.toggle_state(utl.CL_STATE_ROT, context)
        elif m_act == utl.ACTION_EXTRUDE:
            self.toggle_state(utl.CL_STATE_EXT, context)
        elif m_act == utl.ACTION_SCALE_CLS:
            scale_all_cls(context)
        elif m_act == utl.ACTION_CONVERT_GUIDE:
            convert_select_guides_to_geom(self, context)
        elif m_act == utl.ACTION_EXITCANCEL:
            self.cancel_operater(context)

        bpy.context.scene.cl_settings.cl_menuact_str = ""
        context.area.tag_redraw()

    #############################################
    def return_mouse_loc(self, context: bpy.context) -> Vector:
        coord = self.mouse_pos
        region = context.region

        if context.space_data:
            rv3d = context.space_data.region_3d
            # rv3d = context.region_data
            if rv3d:
                vec = region_2d_to_vector_3d(region, rv3d, coord)
                loc = region_2d_to_location_3d(region, rv3d, coord, vec)
                return loc.copy()

        return None

    def reset_operator(self, context: bpy.context, return_to_default=False) -> None:
        sui.set_user_prefs(self, context)
        self.reset_constraint([])
        self.selected_geom = cls.SelectedGeom()
        self.cur_tape_name = ""
        self.close_geom_pt = None
        self.close_face = -1
        self.snap_geom = []
        self.focus_obj = None
        self.active_extrude = None
        self.guide_div = utl.CL_DEFAULT_GUIDE_DIVISIONS
        self.tape_dist = []
        # Reset to default state and tool
        self.set_last_action()
        if return_to_default:
            self.reset_to_default_state(context)
        self.num_enter_mode = False
        refresh_all_cls(self, context)

        # remove objects
        if self.tape:
            del self.tape
            self.tape = None
        if self.select_box:
            del self.select_box
            self.select_box = None

        query_selections(self, context)
        utl.update_info_bar(context, utl.DEFAULT_STATUS)
        self.display_op_hint()

    def reset_to_default_state(self, context: bpy.context) -> None:
        self.state = self.default_tool
        context.scene.cl_settings.cl_selected_mode = self.state

    def set_last_action(self, val="") -> None:
        tool = self.state

        if not self.last_action:
            obj = None
            clk_obj = None
            if (
                self.close_geom_pt
                and self.close_geom_pt.obj
                and self.close_geom_pt.geom_type != utl.TYPE_GUIDE_E
            ):
                obj = self.close_geom_pt.obj.get_obj()
                if obj and obj.type != "EMPTY":
                    clk_obj = obj.name

            self.last_action = LastAction(tool, obj=clk_obj if obj else None)
        else:
            self.last_action.tool = tool
            self.last_action.val = self.u_num if not val else val
            self.last_action.update_time()

    def cancel_operater(self, context: bpy.context):
        self.cancel_tape(context, return_to_default=True)
        if self.tape:
            self.tape = None
        if self.select_box:
            self.select_box = None
        utl.update_info_bar(context, "")
        if self._handle:
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, "WINDOW")
            self._handle = None
        if self._handle_line:
            bpy.types.SpaceView3D.draw_handler_remove(
                self._handle_line, "WINDOW")
            self._handle_line = None
        context.window.cursor_modal_restore()
        if self.bl_context:
            self.bl_context.restore_bl_context(context)
        VIEW_OT_ConstructionLines.running = False
        return {"CANCELLED"}

    def process_undo_action(self, context: bpy.context) -> None:
        try:
            # don't allow undo to leave CL
            if self.undo_counter <= 0:
                self.undo_counter = 0
                return
            self.undo_counter -= 1
            emode = context.mode == "EDIT_MESH"
            bpy.ops.ed.undo()
            self.refresh_op_values(emode, context)
            self.selected_geom.clear_all()
            if emode:
                if not snp.return_to_snap_edit(self.snap_obj):
                    self.snap_obj = None
                else:
                    # utl.reset_active_edit_object(context, self.snap_obj)
                    utl.refresh_active_edit_object(
                        context, self.snap_obj, return_to_edit=emode)
            # check if cl_op.snap_obj is still valid
            if not utl.object_is_valid(self.snap_obj):
                self.snap_obj = None
            if not utl.object_is_valid(self.dupli_obj):
                self.dupli_obj = None
            if self.active_extrude:
                for exo in self.active_extrude.extrude_objs:
                    utl.delete_Object(exo.obj_id, context)
                self.active_extrude = None

            # set snap object to edit object if possible
            if not self.snap_obj and context.active_object:
                self.snap_obj = context.active_object
        except RuntimeError:
            print("Can't undo, incorrect context")
            return

    def process_redo_action(self, context: bpy.context) -> None:
        try:
            self.selected_geom.clear_all()
            bpy.ops.ed.redo()
            if not utl.object_is_valid(self.snap_obj):
                self.snap_obj = None
            if not utl.object_is_valid(self.dupli_obj):
                self.dupli_obj = None
            refresh_all_cls(self, context)
            self.undo_counter += 1
        except RuntimeError:
            # redo to incorrect context
            print("Can't redo, incorrect context")
            return

    def push_undo(self, msg="") -> None:
        bpy.ops.ed.undo_push(message=msg)
        self.undo_counter += 1

    @staticmethod
    def process_save_action():
        if bpy.data.is_saved:
            bpy.ops.wm.save_mainfile()
        else:
            bpy.ops.wm.save_as_mainfile("INVOKE_DEFAULT")

    def handoff_current_context(self, context_in: bool, context: bpy.context) -> None:
        # store bl_context and copy current selections
        if context_in:
            self.bl_context = blc.BLContext(context)
            if context.mode == "EDIT_MESH":
                obj = context.active_object
                if obj.visible_get():
                    self.selected_geom.select_object_list([obj], context)
                    # TODO: select verts and edges too
                    fs, es, vs = self.bl_context.store_current_selections(
                        obj, context)
                    self.selected_geom.select_face_list(obj, fs, context)
                    self.snap_obj = obj
                else:
                    bpy.ops.object.mode_set(mode="OBJECT")
            else:
                self.selected_geom.select_object_list(
                    self.bl_context.store_current_selections(
                        None, context), context
                )
        # return bl_context and pass cl selections to bl
        else:
            # TODO: pass all selections on to blender
            pass

    # all other functions
    @process_event
    def process_view_event(self, event, context: bpy.context):
        k = event.type
        v = event.value

        # USER CONFIGURABLE INPUT
        prefs = context.preferences.addons[CL_NAME].preferences
        signature = utl.compile_input_signature(event, k)

        if k in utl.B_INPUT_PT:
            self.push_redraw()
            return {"PASS_THROUGH"}
        # Handle initial numerical input options - passthrough navigation if required
        elif k in utl.B_NUMPAD_NUMS:
            if not self.num_enter_mode and self.numpad_nav:
                self.push_redraw()
                return {"PASS_THROUGH"}
            elif self.auto_number_inpt and not self.numpad_nav:
                self.num_enter_mode = True
        elif k in utl.B_NUMS or k in utl.B_NUM_OPS:
            if self.auto_number_inpt:
                self.num_enter_mode = True
        #
        if v == "PRESS":
            if k == "LEFTMOUSE":
                if not self.tape and not event.alt:
                    if self.state == utl.CL_STATE_SEL and not self.select_box:
                        self.handle_select(context, event, False)
                    # Quit on tool panel click (if not currently mid tool use)
                    if clg.point_in_region(self.mouse_pos, "TOOLS", context):
                        if (
                            not self.close_geom_pt
                            or self.close_geom_pt.geom_type == utl.TYPE_PLANE
                        ):
                            VIEW_OT_ConstructionLines.running = False
                            return {"PASS_THROUGH"}

            if self.tape:
                if k in utl.B_ARROWS_WHEEL:
                    self.handle_arrows_wheel(k, event.ctrl, context)
                    if k in {utl.B_ARROWS_WHEEL[1], utl.B_ARROWS_WHEEL[3]}:
                        self.push_redraw()
                        return {"PASS_THROUGH"}
                elif event.unicode in utl.IMP_OPS:
                    self.handle_num_input(
                        event.unicode, False, event.shift, context)
                elif event.alt:
                    if self.tape and self.tape.starting_at_edge():
                        self.tape.is_horz = not self.tape.is_horz
                elif event.shift and event.ctrl:
                    self.toggle_aribtrary_constraint()
            # handle mac undo and redo
            elif event.oskey and k == "Z":
                if event.shift:
                    self.process_redo_action(context)
                else:
                    self.process_undo_action(context)
            else:
                if not prefs.match_any(signature) and not k == "RIGHTMOUSE":
                    return {"PASS_THROUGH"}
        elif v == "RELEASE":
            if k == "LEFTMOUSE":
                self.handle_input_click(event, self.is_double_click(), context)
            elif k in utl.B_AKEYS and not event.ctrl and self.tape:  # AXIS LOCK
                self.reset_constraint(utl.convert_bakey(k))
            elif (
                k in utl.B_NUMS
                or k in utl.B_NUM_OPS
                or k == "BACK_SPACE"
                or k in utl.B_NUMPAD_NUMS
            ):  # NUM/NUM EXPR INPUT
                if self.num_enter_mode:
                    self.handle_num_input(
                        k, k == "BACK_SPACE", event.shift, context)
            elif k in {"RET", "NUMPAD_ENTER"}:  # END NUMBER INPUT
                if not self.num_enter_mode:
                    self.num_enter_mode = True
                    utl.update_info_bar(context, utl.NUM_INPUT_STATUS)
                else:
                    self.handle_num_input_end(context)

            else:
                change_state = None
                if prefs.match("SELECT", signature):
                    change_state = utl.CL_STATE_SEL
                elif prefs.match("TAPE", signature):
                    change_state = utl.CL_STATE_POLY_TAPE
                elif prefs.match("LINE", signature):
                    change_state = utl.CL_STATE_POLY_LINE
                elif prefs.match("CIRCLE", signature):
                    change_state = utl.CL_STATE_POLY_CIRC
                elif prefs.match("RECTANGLE", signature):
                    change_state = utl.CL_STATE_POLY_RECT
                elif prefs.match("ARC", signature):
                    change_state = utl.CL_STATE_POLY_ARC
                elif prefs.match("ROTATE", signature):
                    change_state = utl.CL_STATE_ROT
                elif prefs.match("MOVE", signature):
                    change_state = utl.CL_STATE_MOV
                elif prefs.match("EXTRUDE", signature):
                    change_state = utl.CL_STATE_EXT
                elif prefs.match("SELECT", signature):
                    change_state = utl.CL_STATE_SEL
                elif prefs.match("FOCUS", signature):
                    self.selected_geom.focus_selected(context)
                elif prefs.match("FACE", signature):
                    self.create_face_from_selected_verts(context)
                elif prefs.match("HIDE", signature):
                    show_hide_cls()
                elif prefs.match("CONVERT", signature):
                    convert_select_guides_to_geom(self, context)
                elif prefs.match("REMOVE", signature):
                    self.remove_geom(False, context)
                elif prefs.match("DISSOLVE", signature):
                    self.remove_geom(True, context)
                elif prefs.match("UNDO", signature):
                    self.process_undo_action(context)
                elif prefs.match("REDO", signature):
                    self.process_redo_action(context)
                elif prefs.match("MODE_TOGGLE", signature):
                    self.handle_mode_change(context)
                elif prefs.match("SELECT_ALL", signature):
                    self.handle_select_all(event.alt, context)
                elif prefs.match("DESELCECT_ALL", signature):
                    self.handle_select_all(event.alt, context)
                elif prefs.match("LOCAL_VIEW", signature):
                    self.selected_geom.local_view(context)
                else:
                    # Clear selection Box
                    self.select_box = None
                    return {"PASS_THROUGH"}

                if change_state:
                    self.toggle_state(change_state, context)

        return {"RUNNING_MODAL"}

    # @process_event
    def process_outside_view_event(self, event, context: bpy.context):
        k = event.type
        v = event.value
        self.is_processing = True
        self.refresh_selections_flag = True
        if v == "RELEASE":
            if k == "LEFTMOUSE":
                # some tasks need to be ended on click if waiting for input
                self.end_outstanding_tasks()
                self.update_snap_flags(context)
        return {"PASS_THROUGH"}

    def end_outstanding_tasks(self) -> None:
        if self.state == utl.CL_STATE_SEL:
            del self.select_box
            self.select_box = None
        elif self.state == utl.CL_STATE_MOV:
            if self.tape and self.tape.in_second_phase():
                self.dupli_obj = None
                self.tape.set_tape_finished(True)

    def refresh_selections(self, context: bpy.context) -> None:
        selected_objs = context.selected_objects
        if selected_objs:
            if self.selected_geom:
                self.selected_geom.select_object_list(selected_objs, context)
        self.refresh_selections_flag = False
        self.is_processing = False

    def remove_geom(self, dissolve: bool, context: bpy.context) -> None:
        # if not self.tape:
        #     return

        self.selected_geom.remove_geom(context, dissolve)
        if context.mode == "OBJECT":
            refresh_all_cls(self, context)
            self.snap_obj = None

        self.push_undo(msg="CL: Geometry Removed")

    def refresh_op_values(self, edit_mode: bool, context: bpy.context) -> None:
        # clear snap_obj if in object mode as may no longer exist
        if not edit_mode:
            self.snap_obj = None
            self.focus_obj = None
            self.dirty_flag = True
        refresh_all_cls(self, context)
        context.view_layer.update()

    def push_redraw(self) -> None:
        self.dirty_flag = True
        self.close_geom_pt = None


###############################################################
def hover_face(cl_op, face_geom, context):
    if not cl_op.tape and not cl_op.selected_geom.faces:
        face_verts = utl.return_face_verts(
            cl_op.snap_obj, face_geom.face, context)
        if face_verts and face_verts[0]:
            cl_op.hover_geom = face_verts[0]
            # Complete face vert loop
            cl_op.hover_geom.append(face_verts[0][0])


def show_hide_cls():
    if utl.CL_COL_NAME in bpy.data.collections:
        CL_collection = bpy.data.collections[utl.CL_COL_NAME]
        CL_collection.hide_viewport = not CL_collection.hide_viewport


def remove_all_cls(cl_op, context):
    e_mode = False
    if bpy.context.mode == "EDIT_MESH":
        e_mode = True
        bpy.ops.object.mode_set(mode="OBJECT")

    for o in bpy.data.objects:
        o.select_set(False)

    for ob in context.scene.objects:
        ob.select_set(ob.type == "EMPTY" and ob.name.startswith(utl.CL_C_NAME))
        cl_op.selected_geom.clear_all()
        # Check: might need to clear cl_op.snap_obj if it's a CLP
        bpy.ops.object.delete()

    if e_mode:
        bpy.ops.object.mode_set(mode="EDIT")

    refresh_all_cls(cl_op, context)


def convert_select_guides_to_geom(cl_op, context):
    for sg in cl_op.selected_geom.guides:
        utl.build_line_obj(sg, context)
    cl_op.selected_geom.clear(utl.TYPE_GUIDE_E)


def scale_all_cls(context):
    scale = utl.CL_CP_SCALE
    v_scale = Vector((scale, scale, scale))
    clps = utl.return_all_CL_POINTS(context)
    for clp in clps:
        clp.scale = v_scale


def refresh_all_cls(cl_op, context):
    cl_op.cl_points = utl.return_all_CL_POINTS(context)


############################################
# SELECTION AND CREATE FACES
############################################
# verts
def select_vert(cl_op, geom, context):
    cl_op.selected_geom.select(geom, context)


def query_selections(cl_op, context):
    exists = False
    cl_list = []
    rem_v = []

    for ob in context.scene.objects:
        if ob.name.startswith(utl.CL_C_NAME):
            cl_list.append(ob.location)

    for v in cl_op.selected_geom.verts:
        for c_l in cl_list:
            if v == c_l:
                exists = True

        if not exists:
            rem_v.append(v)

    for v in rem_v:
        cl_op.selected_geom.remove(utl.TYPE_VERT, v)
        # cl_op.selected_verts.remove(v)


#############################################
def new_construction_line(cl_op, context, start_point, start_geom):
    s_pt = start_point
    if not s_pt:
        s_pt = cl_op.close_geom_pt.vert.copy()

    s_geom = None
    if start_geom:
        s_geom = copy.deepcopy(start_geom)
    elif cl_op.close_geom_pt:
        # Work around for object pickle issue
        cl_op.close_geom_pt.obj = None
        s_geom = copy.deepcopy(cl_op.close_geom_pt)

    cl_op.tape = cl.ConstructLine(
        context,
        utl.TAPE_TYPE_MULTI
        if cl_op.state in utl.STATE_MULTI_MODES
        else utl.TAPE_TYPE_SINGLE,
        [s_pt, s_pt],
        s_geom,
        cl_op.snap_obj,
    )
    cl_op.cur_tape_name = cl_op.tape.get_tape_name()
    cl_op.close_geom_pt = None


def create_poly(cl_op: VIEW_OT_ConstructionLines, p_type: str, context: bpy.context) -> None:

    if not cl_op.tape:
        utl.msg_log(cl_op, "Error: Can't create shape")
        return

    tape_vs = cl_op.tape.return_verts()
    p_verts = []

    working_face = []
    if cl_op.close_geom_pt:
        if cl_op.close_geom_pt.geom_type in {utl.TYPE_FACE, utl.TYPE_PLANE}:
            working_face = cl_op.close_geom_pt

    if p_type == utl.CL_C_CIRCLENAME:
        p_verts = clg.plot_circle(
            clg.calc_len(tape_vs)[0],
            cl_op.circle_segs,
            cl_op.constraint,
            cl_op.soft_constraint,
            tape_vs,
            face=working_face,
        )
    elif p_type == utl.CL_C_RECTNAME:
        p_verts = clg.plot_rect(
            tape_vs, cl_op.constraint, cl_op.soft_constraint, face=working_face
        )
    elif p_type == utl.CL_C_ARCNAME:
        p_verts = clg.plot_arc_2(tape_vs, cl_op.circle_segs)

    loop_edges = False if p_type == utl.CL_C_ARCNAME else True
    find_mesh_faces = context.scene.cl_settings.cl_find_faces_on_edit_bool
    mesh_edit = cme.MeshEdit(cl_op.snap_obj, context,
                             cl_op.undo_counter, find_faces=find_mesh_faces)
    mesh_edit.add_new_geom(p_verts, p_type, loop_edges)

    obj = mesh_edit.working_object

    if utl.object_is_valid(obj):
        cl_op.snap_obj = obj


# expects locs = [ [[v0, v2], Type], [[v1, v3], Type] ]
def create_geom_from_locations(cl_op, locs, context):
    if context.mode != "EDIT_MESH":
        bpy.ops.object.mode_set(mode="EDIT")

    mesh_edit = cme.MeshEdit(cl_op.snap_obj, context, cl_op.undo_counter)
    for new_g in locs:
        if new_g[1] == utl.TYPE_VERT:
            mesh_edit.create_verts(new_g[0])
        elif new_g[1] == utl.TYPE_EDGE:
            mesh_edit.create_line(new_g[0])
        elif new_g[1] == utl.TYPE_FACE:
            mesh_edit.add_new_geom(new_g[0], "", True)


def change_circle_segs(cl_op, context, num):
    min_segs = 3
    if cl_op.state == utl.CL_STATE_POLY_ARC:
        min_segs = 2

    cl_op.circle_segs += num
    if cl_op.circle_segs < min_segs:
        cl_op.circle_segs = min_segs
    utl.update_info_bar(context, utl.CIRCLE_SEG_STATUS +
                        str(cl_op.circle_segs))


def change_guide_divisions(cl_op, num):
    min_divs = 0
    cl_op.guide_div += num
    if cl_op.guide_div < min_divs:
        cl_op.guide_div = min_divs


def create_line_axis_point(cl_op, context):
    if not cl_op.tape:
        return

    obj = None
    tape_verts = cl_op.tape.return_verts()

    # create guide or line based on current state
    if cl_op.state == utl.CL_STATE_POLY_TAPE:
        cl_op.tape.add_final_end(cl_op.guide_div)
    else:
        find_mesh_faces = context.scene.cl_settings.cl_find_faces_on_edit_bool
        mesh_edit = cme.MeshEdit(
            context.active_object, context, cl_op.undo_counter, find_faces=find_mesh_faces)
        obj = mesh_edit.create_line(tape_verts)

    # Continue drawing if line has been created successfully
    if utl.object_is_valid(obj):
        cl_op.snap_obj = obj
        start_v = tape_verts[1]
        sg = clg.SnapGeom(
            start_v, utl.TYPE_VERT, None, None, None, cl_op.tape.return_verts()
        )
        # auto-start a new construction line
        new_construction_line(cl_op, context, start_v, sg)
        return False
    # else:
    #     cl_op.snap_obj = None
    refresh_all_cls(cl_op, context)
    return True


##############################################################
# MOVE, DUPLICATE, ROTATE, EXTRUDE
##############################################################
# returns True if selection state or
# extrude or move(edit mode) states if no tape
def is_in_edit_selection_state(cl_op, context):
    if context.mode == "EDIT_MESH" and not cl_op.tape:
        if cl_op.state == utl.CL_STATE_SEL:
            return True

        if cl_op.state in {utl.CL_STATE_EXT, utl.CL_STATE_MOV}:
            return True

    return False


# MOVE & DUPLICATE
@geom_select_check
def move_geom(cl_op: VIEW_OT_ConstructionLines, context, cancel=False, duplicate=False, link_duplicates=False):
    if not cl_op.tape:
        return
    if context.mode == "EDIT_MESH":
        bpy.ops.object.mode_set(mode="OBJECT")
        obj = context.active_object
        # any location updates need to be converted to object local space
        mtrx_i = obj.matrix_world.inverted().copy()
        if utl.object_is_valid(obj):
            bm = bmesh.new(use_operators=True)  # create an empty BMesh
            bm.from_mesh(obj.data)
            utl.ensure_bm_lookups(bm)
            tvs = cl_op.tape.return_verts()
            c_pos = tvs[1]

            if cancel:
                # only cancel if in first move or duplicate phase
                # if first duplicate has been set down then can no longer cancel
                if not cl_op.tape.in_second_phase():
                    reset_all_geom_locations(cl_op, bm, mtrx_i)
                    delete_dupli_geom(cl_op, bm, context)
            elif duplicate and not cl_op.dupli_obj:
                # Allow for multi end input
                cl_op.change_tape_type(utl.TAPE_TYPE_MULTI)
                dupli_geom_on_move(cl_op, bm, mtrx_i, obj.matrix_world.copy())
            else:
                for v in cl_op.selected_geom.verts:
                    vert = utl.bm_geom_from_index(bm, v[0], utl.TYPE_VERT)
                    cur_pos = v[1]
                    orig_v = v[2]
                    if vert:
                        snap_offset_pos = orig_v - tvs[0]
                        cur_pos = snap_offset_pos + c_pos
                        vert.co = mtrx_i @ cur_pos.copy()
                        v[1] = cur_pos.copy()

                # Edge
                for e in cl_op.selected_geom.edges:
                    edge = utl.bm_geom_from_index(bm, e[0], utl.TYPE_EDGE)
                    cur_pos = e[1]
                    orig_e = e[2]
                    if edge:
                        # v0
                        snap_offset_pos = orig_e[0] - tvs[0]
                        cur_pos[0] = snap_offset_pos + c_pos
                        # v1
                        snap_offset_pos = orig_e[1] - tvs[0]
                        cur_pos[1] = snap_offset_pos + c_pos

                        edge.verts[0].co = mtrx_i @ cur_pos[0].copy()
                        edge.verts[1].co = mtrx_i @ cur_pos[1].copy()

                # Face
                for f in cl_op.selected_geom.faces:
                    face = utl.bm_geom_from_index(bm, f[0], utl.TYPE_FACE)
                    cur_pos = f[1]
                    orig_f = f[2]
                    if face:
                        for i, v in enumerate(cur_pos):
                            snap_offset_pos = orig_f[i] - tvs[0]
                            cur_pos[i] = snap_offset_pos + c_pos
                            face.verts[i].co = mtrx_i @ cur_pos[i]
            bm.to_mesh(obj.data)
            utl.ensure_bm_lookups(bm)
            bm.free()
        bpy.ops.object.mode_set(mode="EDIT")

    # OBJECT MOVE AND DUPLICATE ###################
    else:
        if cancel:
            reset_all_obj_locations(cl_op)
            delete_dupli_objects(cl_op)
        elif duplicate and not cl_op.dupli_obj:
            # Allow for multi end input
            cl_op.change_tape_type(utl.TAPE_TYPE_MULTI)
            dupli_obj_on_move(cl_op, context, link_duplicates=link_duplicates)
        else:
            for sel_obj in cl_op.selected_geom.objects:
                obj = sel_obj.get_obj()
                if not utl.object_is_valid(obj):
                    continue
                snap_offset_pos = sel_obj.get_obj_pos() - \
                    cl_op.tape.return_verts()[0]
                c_pos = cl_op.tape.return_verts()[1]
                obj.matrix_world.translation = snap_offset_pos + c_pos


def finish_move(cl_op, context):
    # If in edit mode and dupli_obj exists
    # remove geom and re-add with create_line or create_poly
    # so that splits and face finiding can happen properly

    if context.mode == "EDIT_MESH":
        bpy.ops.object.mode_set(mode="OBJECT")
        obj = context.active_object
        if utl.object_is_valid(obj):
            bm = bmesh.new(use_operators=True)
            bm.from_mesh(obj.data)
            utl.ensure_bm_lookups(bm)
            locs = []
            rem = []
            if cl_op.dupli_obj:
                for v in cl_op.selected_geom.verts:
                    bm_vert = utl.bm_geom_from_index(bm, v[0], utl.TYPE_VERT)
                    if bm_vert:
                        locs.append([[v[1].copy()], utl.TYPE_VERT])
                        rem.append(bm_vert)
                for e in cl_op.selected_geom.edges:
                    bm_edge = utl.bm_geom_from_index(bm, e[0], utl.TYPE_EDGE)
                    if bm_edge:
                        locs.append([e[1].copy(), utl.TYPE_EDGE])
                        rem.append(bm_edge)
                for f in cl_op.selected_geom.faces:
                    bm_face = utl.bm_geom_from_index(bm, f[0], utl.TYPE_FACE)
                    if bm_face:
                        locs.append([f[1].copy(), utl.TYPE_FACE])
                        rem.append(bm_face)
                utl.remove_bm_geom(bm, rem)
                utl.update_bmesh(bm)
                bm.to_mesh(obj.data)
                bm.free()
                bpy.ops.object.mode_set(mode="EDIT")
                # clear indicies as they will be incorrect after new geom is created
                cl_op.selected_geom.clear_geom_ids()
                create_geom_from_locations(cl_op, locs, context)
            else:
                # Clean mesh of doubles etc. (normal move with no duplicates)
                utl.update_bmesh(bm)
                obj.data.update()
                bm.to_mesh(obj.data)
                bm.free()
            bpy.ops.object.mode_set(mode="EDIT")
    else:
        if cl_op.dupli_obj:
            for obj in cl_op.dupli_obj:
                if utl.object_is_valid(obj):
                    obj.original.matrix_world.translation = (
                        obj.matrix_world.translation.copy()
                    )
            cl_op.selected_geom.clear(utl.TYPE_OBJECT)


# DUPLICATE
def dupli_geom_on_move(cl_op, bm, mtrx_i, mtrx_w):
    if cl_op.dupli_obj == None:
        cl_op.dupli_obj = []

    for g_type in [utl.TYPE_VERT, utl.TYPE_EDGE, utl.TYPE_FACE]:
        # Reset all moving geom to original positions
        reset_geom_locations(
            cl_op.selected_geom.geom_list_from_type(g_type), g_type, mtrx_i, bm
        )

    # process geom selections to remove selected verts from selected edges
    # and edges from faces
    cl_op.selected_geom.process_selections(bm)

    for dg_type in [utl.TYPE_FACE, utl.TYPE_EDGE, utl.TYPE_VERT]:
        # Duplicate
        dupli_geom = [
            utl.duplicate_geom_multi(
                bm,
                [
                    utl.bm_geom_from_index(bm, g[0], dg_type)
                    for g in cl_op.selected_geom.geom_list_from_type(dg_type)
                ],
                dg_type,
                mtrx_w,
            )
        ]

        # Swap selections to new duplicates
        for gd in dupli_geom:
            if gd:
                cl_op.selected_geom.update_selected_geom_set(
                    [utl.bm_geom_from_index(bm, g[0], g[2])
                     for g in gd if g], gd[0][2]
                )

        if dupli_geom:
            cl_op.dupli_obj.append(dupli_geom)


def dupli_obj_on_move(cl_op, context, link_duplicates=False):
    if cl_op.dupli_obj == None:
        cl_op.dupli_obj = []

    reset_all_obj_locations(cl_op)
    switch_objs = []
    ignore_objs = []
    for sel_obj in cl_op.selected_geom.objects:
        obj = sel_obj.get_obj()
        if not utl.object_is_valid(obj) or obj in ignore_objs:
            continue
        # GUIDES
        if obj.type == "EMPTY":
            keys = ["Pair"]
            if utl.return_guide_keys_exist(obj, keys):
                obj_pair = utl.return_object_by_name(obj[keys[0]], context)
                if obj_pair:
                    guide_end_obj_0 = utl.duplicate_object(
                        obj, obj.location.copy(), context
                    )
                    guide_end_obj_1 = utl.duplicate_object(
                        obj_pair, obj_pair.location, context
                    )
                    # make sure other end isn't duplicated again
                    ignore_objs.append(obj_pair)

                    if guide_end_obj_0 and guide_end_obj_1:
                        # set new guide pairings
                        guide_end_obj_0["Pair"] = guide_end_obj_1.name
                        guide_end_obj_1["Pair"] = guide_end_obj_0.name

                        switch_objs.append([obj, guide_end_obj_0])
                        switch_objs.append([obj_pair, guide_end_obj_1])
        # OBJECTS
        elif obj.type == "MESH":
            dupli_obj = utl.duplicate_object(
                obj, obj.location.copy(), context, link_objects=link_duplicates)
            # store original and stored duplicate
            if dupli_obj:
                switch_objs.append([obj, dupli_obj])

    # swap selections to new duplicate objects
    for s_o in switch_objs:
        cl_op.selected_geom.update_selected_object(s_o[0], s_o[1])

    # copy all duplicates to duplicates
    cl_op.dupli_obj = [obj[1] for obj in switch_objs]

    # refresh guide list
    refresh_all_cls(cl_op, context)


def delete_dupli_objects(cl_op):
    if cl_op.dupli_obj:
        for obj in cl_op.dupli_obj:
            if not utl.object_is_valid(obj):
                continue
            utl.remove_obj(obj)


def delete_dupli_geom(cl_op, bm, context):
    if not cl_op.dupli_obj:
        return

    if context.mode == "EDIT_MESH":
        bpy.ops.object.mode_set(mode="OBJECT")

    rem_list = []
    for dg in cl_op.dupli_obj:
        for g in dg:
            for gg in g:
                if gg:
                    rem_list.append(utl.bm_geom_from_index(bm, gg[0], gg[2]))
    if rem_list:
        utl.remove_bm_geom(bm, rem_list)
    utl.ensure_bm_lookups(bm)


# Reset
def reset_all_geom_locations(cl_op, bm, mtrx_i):
    for g_type in [utl.TYPE_VERT, utl.TYPE_EDGE, utl.TYPE_FACE]:
        reset_geom_locations(
            cl_op.selected_geom.geom_list_from_type(g_type), g_type, mtrx_i, bm
        )


def reset_all_obj_locations(cl_op):
    for sel_obj in cl_op.selected_geom.objects:
        obj = sel_obj.get_obj()
        if utl.object_is_valid(obj):
            # cancel will set back to original position
            obj.original.matrix_world.translation = sel_obj.get_obj_pos()


# Reset any geometry list to it's original location
def reset_geom_locations(geom_list, geom_type, local_matrix, bm):
    if geom_type == utl.TYPE_VERT:
        for v in geom_list:
            vert = utl.bm_geom_from_index(bm, v[0], utl.TYPE_VERT)
            if vert:
                vert.co = local_matrix @ v[2].copy()
    if geom_type == utl.TYPE_EDGE:
        for e in geom_list:
            edge = utl.bm_geom_from_index(bm, e[0], utl.TYPE_EDGE)
            if edge:
                edge.verts[0].co = local_matrix @ e[2][0].copy()
                edge.verts[1].co = local_matrix @ e[2][1].copy()
    if geom_type == utl.TYPE_FACE:
        for f in geom_list:
            face = utl.bm_geom_from_index(bm, f[0], utl.TYPE_FACE)
            if face:
                for i, v in enumerate(face.verts):
                    v.co = local_matrix @ f[2][i].copy()


def duplicate_multi(cl_op, amnt, context):
    if not (amnt and isinstance(amnt, int)) or not cl_op.dupli_obj:
        return
    if context.mode == "EDIT_MESH":
        bpy.ops.object.mode_set(mode="OBJECT")
        obj = context.active_object
        if utl.object_is_valid(obj):
            bm = bmesh.new(use_operators=True)  # create an empty BMesh
            bm.from_mesh(obj.data)
            utl.ensure_bm_lookups(bm)
            locs = []
            # Work off of selection rather than dupli_obj here
            # this way the position will be correct it will also have a starting position
            for g_type in [utl.TYPE_VERT, utl.TYPE_EDGE, utl.TYPE_FACE]:
                s_geom = cl_op.selected_geom.geom_list_from_type(g_type)
                for g in s_geom:
                    if g:
                        if g_type == utl.TYPE_VERT:
                            locs.append(
                                [
                                    clg.plot_duplicates_times(
                                        [g[1], g[2]], int(amnt)),
                                    g_type,
                                ]
                            )
                        else:
                            vs = []
                            # base direction on start and end positions of selected geom
                            for i, _ in enumerate(g[1]):
                                vs.append(
                                    clg.plot_duplicates_times(
                                        [g[1][i], g[2][i]], int(amnt)
                                    )
                                )
                            # group duplicates then add to locs
                            for i in range(len(vs[0])):
                                grp = []
                                for j in range(len(vs)):
                                    grp.extend([vs[j][i]])
                                locs.append([grp, g_type])
            utl.ensure_bm_lookups(bm)
            bm.to_mesh(obj.data)
            bm.free()
            # use cl_object to build the new geometry
            create_geom_from_locations(cl_op, locs, context)
    else:
        ignore_objs = []
        for d_obj in cl_op.dupli_obj:
            if not utl.object_is_valid(d_obj) or d_obj in ignore_objs:
                continue
            if d_obj.type == "EMPTY":
                keys = ["Pair"]
                if utl.return_guide_keys_exist(d_obj, keys):
                    obj_pair = utl.return_object_by_name(
                        d_obj[keys[0]], context)
                    if obj_pair:
                        g_end_a_start_obj = utl.return_object_by_name(
                            d_obj["DupliPair"], context
                        )
                        g_end_b_start_obj = utl.return_object_by_name(
                            obj_pair["DupliPair"], context
                        )
                        if g_end_a_start_obj and g_end_b_start_obj:
                            g_end_a_locs = clg.plot_duplicates_times(
                                [d_obj.location, g_end_a_start_obj.location], int(
                                    amnt)
                            )
                            g_end_b_locs = clg.plot_duplicates_times(
                                [obj_pair.location, g_end_b_start_obj.location],
                                int(amnt),
                            )
                            # match multi-duplis with their pairs
                            dupli_g_ends_a = utl.duplicate_object_with_locs(
                                g_end_a_start_obj, g_end_a_locs, context
                            )
                            dupli_g_ends_b = utl.duplicate_object_with_locs(
                                g_end_b_start_obj, g_end_b_locs, context
                            )
                            # set new guide pairings
                            for ge in zip(dupli_g_ends_a, dupli_g_ends_b):
                                ge[0]["Pair"] = ge[1].name
                                ge[1]["Pair"] = ge[0].name
                        ignore_objs.append(obj_pair)
            elif d_obj.type == "MESH":
                start_obj = utl.return_object_by_name(
                    d_obj["DupliPair"], context)
                if start_obj:
                    locs = clg.plot_duplicates_times(
                        [d_obj.location, start_obj.location], int(amnt)
                    )

                    link_objects = utl.bl_objects_data_linked(start_obj, d_obj)
                    utl.duplicate_object_with_locs(
                        start_obj, locs, context, link_duplicates=link_objects)
    cl_op.dupli_obj = None


# ROTATE
@geom_select_check
def finish_obj_rotation(cl_op, context):
    for s_obj in cl_op.selected_geom.objects:
        s_obj.update_start_obj_rotation()
        if s_obj:
            # reset origin
            s_obj.reset_object_origin()
            clear_rotation_pivot_point(cl_op, context)
            bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
            # bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)


@geom_select_check
def reset_obj_rotation(cl_op, context):
    for s_obj in cl_op.selected_geom.objects:
        obj = s_obj.get_obj()
        if utl.object_is_valid(obj):
            bpy.context.view_layer.objects.active = obj
            # reset rotation
            obj.rotation_euler = s_obj.get_start_obj_rotation().to_euler()
            # reset origin
            s_obj.reset_object_origin()
            s_obj.reset_obj_location()
    if cl_op.temp_object:
        clear_rotation_pivot_point(cl_op, context)


def clear_rotation_pivot_point(cl_op, context):
    utl.delete_Object(cl_op.temp_object, context)
    cl_op.temp_object = None


@geom_select_check
def set_object_origin_to_click(cl_op, context):
    # setup temp pivot object if rotating a guide
    if cl_op.selected_geom.guides:
        pt = cl_op.tape.return_verts()[0]
        tmp_obj = utl.add_temp_pivot_object(pt)
        tmp_obj.select_set(True)
        context.view_layer.objects.active = tmp_obj
        cl_op.temp_object = tmp_obj
    # other object rotations
    else:
        for s_obj in cl_op.selected_geom.objects:
            obj = s_obj.get_obj()
            if utl.object_is_valid(obj):
                bpy.context.view_layer.objects.active = obj
                if not s_obj.set_temp_object_origin(cl_op.tape.return_verts()[0]):
                    utl.change_view_transform_pivot("ACTIVE_ELEMENT", context)


@geom_select_check
def rotate_geom(cl_op, angle=None):
    for s_obj in cl_op.selected_geom.objects:
        obj = s_obj.get_obj()
        if utl.object_is_valid(obj):
            obj.select_set(True)
            if cl_op.temp_object:
                # use pivot point if rotating a guide
                cl_op.temp_object.select_set(True)
            else:
                bpy.context.view_layer.objects.active = obj
            axis = (
                utl.return_axis_vect("Z")
                if not cl_op.constraint
                else cl_op.constraint[0]
            )
            t = (
                cl_op.tape.get_tape_angle(cl_op.constraint)
                if not angle
                else math.radians(angle)
            )

            if cl_op.temp_object:
                # rotate based on pivot point
                R = Matrix.Rotation(t, 4, axis)
                T = Matrix.Translation(cl_op.temp_object.location)
                M = T @ R @ T.inverted()
                # obj.location = M @ obj.location
                # obj.rotation_euler.rotate(M)
                obj.location = M @ s_obj.get_obj_pos()
                obj.rotation_euler = (
                    Matrix.Rotation(
                        t, 3, axis) @ s_obj.get_start_obj_rotation()
                ).to_euler()
            else:
                obj.rotation_euler = (
                    Matrix.Rotation(
                        t, 3, axis) @ s_obj.get_start_obj_rotation()
                ).to_euler()


# EXTRUDE
def extrude_geom(cl_op: VIEW_OT_ConstructionLines) -> None:
    if not cl_op.active_extrude or not cl_op.tape:
        return

    cl_op.active_extrude.extrude_geom(
        cl_op.tape.return_verts(), cl_op.tape.is_neg_drag_heading())


def finish_extrude(cl_op: VIEW_OT_ConstructionLines, cancel: bool) -> None:
    if not cl_op.active_extrude:
        return

    cl_op.snap_obj = cl_op.active_extrude.return_active_obj()

    if not cancel:
        if cl_op.tape:
            cl_op.active_extrude.finish_extrude(
                cl_op.tape.is_neg_drag_heading())

    # free memory
    cl_op.active_extrude = None

    # clear selections
    if cl_op.selected_geom:
        cl_op.selected_geom.clear(utl.TYPE_FACE)


def setup_extrude(cl_op: VIEW_OT_ConstructionLines, context: bpy.context) -> bool:
    if cl_op.active_extrude:
        cl_op.active_extrude = None

    obj = cl_op.snap_obj

    if not utl.object_is_valid(obj) or not cl_op.selected_geom:
        return False

    faces = cl_op.selected_geom.faces
    if not faces:
        return False

    face_ids = [f[0] for f in faces]

    # Take boolen option from panel prefs
    boolean_cut_through = context.scene.cl_settings.cl_use_bool_extrude_bool
    cl_op.active_extrude = clx.ExtrudeOp(
        obj, face_ids, context, cl_op.undo_counter, boolean_cut_through)

    if cl_op.active_extrude:
        if not cl_op.active_extrude.setup_extrude_geom():
            finish_extrude(cl_op, True)
            return False
        face_info = cl_op.active_extrude.return_orig_face_info()
        if face_info:
            ext_cent = face_info.center
            ext_nrm = face_info.normal
            if cl_op.tape:
                cl_op.tape.set_start_vert(ext_cent)
                cl_op.constraint = [ext_nrm, "NORM"]
                return True
    return False
