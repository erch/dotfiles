# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings


from dataclasses import dataclass
from math import acos
from typing import List

import bpy  # type: ignore
from mathutils import Vector  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg


@dataclass
class MoveConstriant:
    direction: Vector
    axis_name: str



class ConstructLine:
    def __init__(
        self,
        context: bpy.context,
        cl_type: int,
        verts: List[Vector],
        start_geom: clg.ClosestVert,
        start_obj: bpy.types.Object = None,
        auto_guide_orient: bool = True,
    ):
        self._context = context
        self._type = cl_type
        self._verts = verts.copy()
        self._start_obj = start_obj
        self._start_geom = start_geom

        self._visible: bool = True
        self._neg_drag_heading: bool = False
        self._end_set: bool = False
        self._tape_finished: bool = False

        self.is_horz: bool = False
        if auto_guide_orient:
            self.auto_guide_orientation(start_geom.geom_type)

    def return_verts(self) -> List[Vector]:
        return self._verts.copy()

    def return_tape_finished(self) -> bool:
        return self._tape_finished

    def set_tape_finished(self, val: bool) -> None:
        if isinstance(val, bool):
            self._tape_finished = val

    def set_end_vert(self, new_v: Vector) -> None:
        v_c = len(self._verts)

        if v_c == 2:
            self._verts[1] = new_v.copy()
        elif v_c == 3:
            self._verts[2] = new_v.copy()

    def set_start_vert(self, new_v: Vector) -> None:
        if self._verts:
            self._verts[0] = new_v.copy()

    def cancel_type3_tape(self) -> None:
        if self.in_second_phase():
            del self._verts[2]

        self.set_tape_finished(False)

    def auto_guide_orientation(self, geom_type: str) -> None:
        if geom_type in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E}:
            self.is_horz = True

    def set_tape_type(self, cl_type: int) -> None:
        if self._type == utl.TAPE_TYPE_MULTI:
            self.cancel_type3_tape()
        self._type = cl_type

    def get_tape_type(self) -> int:
        return self._type

    def set_ctrl_vert(self, new_v: Vector) -> None:
        # control vert (mid vert in arc case)
        self._verts.append(new_v)

    def set_visible(self, vis: bool) -> None:
        self._visible = vis

    def is_visible(self) -> bool:
        return self._visible

    def is_neg_drag_heading(self) -> bool:
        return self._neg_drag_heading

    def get_start_obj(self) -> bpy.types.Object:
        return self._start_obj

    def get_start_geom(self) -> clg.ClosestVert:
        return self._start_geom

    def get_tape_name(self) -> str:
        return utl.CL_C_NAME

    def in_second_phase(self) -> bool:
        return len(self.return_verts()) > 2

    def return_phase(self) -> int:
        return len(self.return_verts())

    def starting_at_edge(self) -> bool:
        sg = self.get_start_geom()
        return sg and sg.geom_type in {utl.TYPE_EDGE, utl.TYPE_EDGE_C, utl.TYPE_GUIDE_E}

    def get_tape_len(self) -> List[float]:
        v_c = len(self._verts)
        v1 = self._verts[0]
        v2 = self._verts[1]

        if v_c == 3:
            v1 = clg.find_edge_center(v1, v2)
            v2 = self._verts[2]

        return clg.calc_len([v1, v2])

    def get_tape_angle(self, constraint: List) -> float:
        if self._type == utl.TAPE_TYPE_MULTI and self.in_second_phase():
            a, b, c = self.return_verts()
            m1 = Vector(b - a).normalized()
            m2 = Vector(c - a).normalized()
            # t = m1.angle(m2, 0)
            md = m1.dot(m2)
            if -1 <= md <= 1:
                t = acos(m1.dot(m2))
                # Always default to Z rotation if no constraint
                if not constraint:
                    constraint = utl.return_global_axes()[2]

                # return_element_of_change(axis) returns an int of the element
                if m1.cross(m2)[utl.return_active_axis(constraint)] < 0:
                    t *= -1
                return t
        return 0.0

    def drag_tape_end(self, loc: Vector, constraint: List, state: str) -> None:
        if not loc:
            return

        vs = self.return_verts()
        l = loc.copy()

        # Constrain if a lock is set
        # no effect if in circle or rect
        if constraint and state not in [
            utl.CL_STATE_POLY_RECT,
            utl.CL_STATE_POLY_CIRC,
            utl.CL_STATE_ROT,
        ]:
            newl = loc.copy()
            self._neg_drag_heading = False

            # Later Dev - ray cast from (a) to the next face this will then become a maximum
            # if working with extrude vector (a) must be set to face center

            a = Vector(vs[0])
            # shift a to central point of edge if in arc mode
            if state == utl.CL_STATE_POLY_ARC and self.in_second_phase():
                a = Vector(clg.find_edge_center(vs[0], vs[1]))
            b = Vector(l)
            n = Vector(constraint[0].copy())
            an_dir = a + n
            # find the closet point on the new vector(an_dir) from point b
            # this can then be used to calculate the correct distance along
            # any arbitrary axis
            # percent intersection of b on an
            ix, ix_ban_prcnt = clg.point_point_on_edge(b, [a, an_dir])
            dist = clg.calc_len([a, ix])[0]

            if ix_ban_prcnt < 0:
                self._neg_drag_heading = True
                dist *= -1

            utl.mask_by_constraint(newl, dist, constraint[0])
            l = a + newl
        else:
            if (
                state == utl.CL_STATE_POLY_TAPE and self.is_horz
            ) or state == utl.CL_STATE_POLY_ARC:
                s_edge = self._start_geom.edge
                s_vert = vs[0].copy()
                # handle arc perp from edge
                if state == utl.CL_STATE_POLY_ARC:
                    if len(self._verts) < 3:
                        self.set_end_vert(l)
                        return
                    else:
                        s_edge = [self._verts[0], self._verts[1]]
                        s_vert = clg.find_edge_center(self._verts[0], self._verts[1])
                nl = clg.return_perp_vector_from_edge(s_vert, l, s_edge)
                if nl:
                    self.set_end_vert(nl)
                return

        self.set_end_vert(l)

    def return_vector_elem_ratios(self, vect: Vector) -> List:
        tot = sum([vect.x, vect.y, vect.z])
        elems = []
        for v in vect:
            if v != 0:
                elems.append(v / tot)
            else:
                elems.append(0)
        return elems

    # get tape current delta
    # extend by val in direction
    def extend_tape_by_amnt(self, amnt: float) -> None:
        v_c = len(self._verts)
        verts = self.return_verts()
        v1 = verts[0]
        v2 = verts[1]
        c = clg.find_edge_center(v1, v2)

        # convert amnt to current scale
        amnt = utl.convert_to_cur_scale(amnt, self._context)

        if v_c == 2:
            v_dir = v2 - v1
            v_dir.normalize()
            v2 = v1 + v_dir * amnt
            self._verts[1] = v2
        elif v_c == 3:
            v3 = verts[2]
            v_dir = v3 - c
            v_dir.normalize()
            v3 = c + v_dir * amnt
            self._verts[2] = v3

    # set tape_finished when enough increments for each type
    # type 1 = line, rect, circle = 2 increments
    # type 2 = arc = 3 increments
    def finish_tape(self) -> None:
        v_c = len(self._verts)
        if self._type == utl.TAPE_TYPE_SINGLE and v_c == 2:
            self._tape_finished = True
        elif self._type == utl.TAPE_TYPE_MULTI:
            if v_c == 2:
                # add ctrl point at mid point
                self.set_ctrl_vert(clg.find_edge_center(self._verts[0], self._verts[1]))
            elif v_c == 3:
                self._tape_finished = True

    
    def add_final_end(self, divs: int):
        ''' Builds guide objects
        '''
        verts = self._verts
        # handle horizontal guide
        guide_pts = self.return_verts()
        if self.is_horz:
            hrz_pts = clg.plot_horiz_guide(guide_pts, self.get_start_geom().edge)
            if hrz_pts:
                guide_pts = hrz_pts
        # pass in segments/divisions (clg.plot_guide_pts)
        guide_pts.extend(clg.plot_div_pts(guide_pts, divs))
        self.create_construction_points(guide_pts, verts[0].copy())
      
      
    def create_construction_points(self, pts: List[Vector], loc_start: Vector=None) -> None:
        '''Adds new construction point objects at start and end locations
         - Adds custom property with reference to other end to each end object
        '''

        scale = utl.CL_CP_SCALE
        objs = []
        for gp in pts:
            obj = bpy.data.objects.new(utl.CL_C_POINTNAME, None)
            utl.add_to_collection(obj)
            obj.empty_display_size = 1
            obj.scale = (scale, scale, scale)
            obj.empty_display_type = "PLAIN_AXES"
            obj.location = gp
            if loc_start and gp == pts[1]:
                obj["StartX"] = loc_start[0]
                obj["StartY"] = loc_start[1]
                obj["StartZ"] = loc_start[2]
            obj["Horizontal"] = self.is_horz
            objs.append(obj)

        if objs:
            objs[0]["Pair"] = objs[1].name
            objs[1]["Pair"] = objs[0].name
