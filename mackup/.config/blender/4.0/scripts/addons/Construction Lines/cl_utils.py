# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

import math
import re
# import numpy as np  # type: ignore
from typing import List, Tuple
from fractions import Fraction


import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector  # type: ignore
from bpy_extras.view3d_utils import (  # type: ignore
    location_3d_to_region_2d,
    region_2d_to_vector_3d,
    region_2d_to_origin_3d,
)  # type: ignore
# from itertools import chain

from . import cl_geom as clg


def msg_log(cl_op, msg):
    if cl_op:
        cl_op.report({"INFO"}, msg)
    else:
        print(msg)


CL_DEBUG_MODE = False

CL_COL_NAME = "Construction Lines"  # Parent object - Collection 2.8
CL_C_NAME = "_CL_"  # CL Object
CL_C_POINTNAME = "_CL_POINT"
CL_C_LINENAME = "_CL_LINE"
CL_C_RECTNAME = "_CL_RECT"
CL_C_CIRCLENAME = "_CL_CIRCLE"
CL_C_ARCNAME = "_CL_ARC"
CL_NEW_NAME = "CL_POINT_FACE"
CL_TOOL_NAME = "cl.construction_lines"

ACTION_HIDESHOW = "ACTION_HIDESHOW"
ACTION_REMOVEALL = "ACTION_REMOVEALL"
ACTION_EXITCANCEL = "ACTION_EXITCANCEL"
ACTION_MOVE = "ACTION_MOVE"
ACTION_EXTRUDE = "ACTION_EXTRUDE"
ACTION_ROTATE = "ACTION_ROTATE"
ACTION_FACE = "ACTION_FACE"
ACTION_DRAWTAPE = "ACTION_DRAWTAPE"
ACTION_DRAWLINE = "ACTION_DRAWLINE"
ACTION_DRAWRECT = "ACTION_DRAWRECT"
ACTION_DRAWCIRCLE = "ACTION_DRAWCIRCLE"
ACTION_DRAWARC = "ACTION_DRAWARC"
ACTION_SCALE_CLS = "ACTION_SCALE_CLS"
ACTION_CONVERT_GUIDE = "ACTION_CONVERT_GUIDE"
ACTION_SELECT = "ACTION_SELECT"

SNP_FLAG_VERT = 1
SNP_FLAG_EDGE = 2
SNP_FLAG_EDGE_C = 4
SNP_FLAG_FACE = 8
SNP_FLAG_GUIDES = 16
SNP_GLB_AXES = 32
SNP_FLAG_DIR = 64
SNP_GLB_GRID = 128
SNP_GLB_GRID_SUBD = 256

SNP_ALL = (
    SNP_FLAG_VERT
    | SNP_FLAG_EDGE
    | SNP_FLAG_EDGE_C
    | SNP_FLAG_FACE
    | SNP_FLAG_GUIDES
    | SNP_GLB_AXES
    | SNP_FLAG_DIR
    | SNP_GLB_GRID
)

CL_BEST_DIST = 256
CL_BEST_E_DIST = 16  # sqrt of BEST_DIST
CL_CLOSE_AXIS_TOL = 0.99
CL_CLOSE_AXIS_TOL_FINE = 0.999
CL_CLOSE_AXIS_TOL_O = 0.99
CL_CLOSE_GRID_TOL = 0.1
CL_FACE_SNAP_SENSITIVITY = 1
CL_DEFAULT_CIR_SEGS = 12
CL_DEFAULT_GUIDE_DIVISIONS = 1
CL_DEFAULT_H_GUIDE_LENGTH = 100
CL_CP_SCALE = 0.2
CL_LINE_WIDTH = 1.0
CL_DASH_SCALE = 0.02
CL_DASH_SPACE_SCALE = 0.01
# CL_GRAPH_MAX_DEPTH = 32
CL_SELECT_REGION_SIZE = 25
CL_SELECT_AMNT_LIM = 10

CL_GRAPH_MAX_DEPTH = 1000

CL_PRECISION = 6

CL_HIGHLIGHT_COL = (0.5, 1.0, 0.1, 1.0)
CL_HIGHLIGHT_COL_VERT = (0.5, 1.0, 0.1, 1.0)
CL_HIGHLIGHT_COL_EDGE_C = (0.59, 0.1, 0.7, 1.0)
CL_HIGHLIGHT_COL_EDGE = (0.3, 0.3, 0.9, 1.0)
CL_HIGHLIGHT_COL_FACE = (0.3, 0.9, 0.9, 1.0)
CL_HIGHLIGHT_COL_GRID_MAJ = (1.0, 0.0, 1.0, 1.0)
CL_HIGHLIGHT_COL_GRID_MIN = (0.75, 0.5, 1.0, 1.0)
CL_SELECT_COL = (1.0, 0.6, 0.0, 1.0)
CL_SELECT_FACE_COL = (1, 0.647, 0.322, 0.180)
CL_XAXIS_COL = (1.0, 0.1, 0.1, 1.0)
CL_YAXIS_COL = (0.1, 1.0, 0.1, 1.0)
CL_ZAXIS_COL = (0.1, 0.5, 1, 1.0)
CL_PERP_COL = (0.85, 0.44, 0.57, 1.0)
CL_EXTD_COL = (0.7, 0.0, 0.9, 1.0)
CL_GUIDE_COL = (0.3, 0.3, 0.3, 1.0)
CL_OBJ_BOUNDS_COL = (0.5, 0.5, 0.5, 1.0)
# CL_DISPLAY_BOX_COL = (0.3, 0.5, 0.76, 0.1)
CL_TEXT_COL = (1.0, 1.0, 1.0, 1.0)
CL_DISPLAY_BOX_COL = (0.3, 0.3, 0.3, 0.5)
CL_DISPLAY_DIM_COL = (0.2, 0.2, 0.2, 0.9)
CL_DEFAULT_COL = (1.0, 1.0, 0.2, 1.0)
CL_DEBUG_LINE_COL = (0.9, 0.0, 0.3, 1.0)
CL_EXTRUDE_COL = (1.0, 0.6, 0.0, 1.0)
CL_EXTRUDE_CUT_COL = (1.0, 0.1, 0.1, 1.0)

CL_TEXT_SIZE = 13
CL_TEXT_DPI = 72

CL_HIGHLIGHT_POINT_SIZE = 5

CL_WIDGET_RAD = 0.5
CL_WIDGET_SEGS = 24
CL_GUIDE_RECT_SIZE = 5000

CL_STATE_SEL = "SELECTION STATE"
CL_STATE_MOV = "MOVE STATE"
CL_STATE_ROT = "ROTATE STATE"
CL_STATE_EXT = "EXTRUDE STATE"
CL_STATE_POLY_TAPE = "TAPE STATE"
CL_STATE_POLY_LINE = "LINE STATE"
CL_STATE_POLY_CIRC = "CIRCLE STATE"
CL_STATE_POLY_RECT = "RECTANGLE STATE"
CL_STATE_POLY_ARC = "ARC STATE"

CL_INPUT_TYPE_SINGLE = "INPUT_SINGLE"
CL_INPUT_TYPE_MULTI = "INPUT_MULTI"
CL_INPUT_TYPE_ROT = "INPUT_ROTATION"


def action_desc_from_type(act):
    return {
        ACTION_CONVERT_GUIDE: "Convert guide to geometry",
        ACTION_HIDESHOW: "Hide/Show all guides",
        ACTION_SCALE_CLS: "Scale all guides (based on preferences)",
        ACTION_REMOVEALL: "Remove all guides",
        ACTION_EXITCANCEL: "Exit Construction Lines",
    }.get(act, "")


def title_from_state(state):
    return {
        CL_STATE_SEL: "SELECT",
        CL_STATE_MOV: "MOVE",
        CL_STATE_ROT: "ROTATE",
        CL_STATE_EXT: "EXTRUDE/CUT THROUGH",
        CL_STATE_POLY_TAPE: "MEASURE/GUIDE",
        CL_STATE_POLY_LINE: "DRAW LINE",
        CL_STATE_POLY_CIRC: "DRAW CIRCLE",
        CL_STATE_POLY_RECT: "DRAW RECTANGLE",
        CL_STATE_POLY_ARC: "DRAW ARC",
    }.get(state, "")


# default to single input state
def input_type_from_state(state):
    return {
        CL_STATE_ROT: CL_INPUT_TYPE_ROT,
        CL_STATE_POLY_RECT: CL_INPUT_TYPE_MULTI,
    }.get(state, CL_INPUT_TYPE_SINGLE)


STATES_ALL = {
    CL_STATE_SEL,
    CL_STATE_MOV,
    CL_STATE_ROT,
    CL_STATE_EXT,
    CL_STATE_POLY_TAPE,
    CL_STATE_POLY_LINE,
    CL_STATE_POLY_CIRC,
    CL_STATE_POLY_RECT,
    CL_STATE_POLY_ARC,
}

STATE_ACTIONS = {CL_STATE_MOV, CL_STATE_ROT, CL_STATE_EXT}

STATE_POLY_ALL_MODES = {
    CL_STATE_POLY_TAPE,
    CL_STATE_POLY_LINE,
    CL_STATE_POLY_CIRC,
    CL_STATE_POLY_RECT,
    CL_STATE_POLY_ARC,
}

STATE_POLY_BUILD_MODE = {CL_STATE_POLY_CIRC,
                         CL_STATE_POLY_RECT, CL_STATE_POLY_ARC}

STATE_SINGLE_MODES = {
    CL_STATE_POLY_TAPE,
    CL_STATE_POLY_LINE,
    CL_STATE_POLY_CIRC,
    CL_STATE_POLY_RECT,
    CL_STATE_MOV,
}

STATE_MULTI_MODES = {CL_STATE_POLY_ARC, CL_STATE_ROT}

TYPE_VERT = "VERT"
TYPE_EDGE = "EDGE"
TYPE_EDGE_C = "EDGE_C"
TYPE_FACE = "FACE"
TYPE_GUIDE_E = "GUIDE_EDGE"
TYPE_INTERSECT = "GUIDE INTERSECTION"
TYPE_EMPTY = "EMPTY"
TYPE_AXIS = "AXIS"
TYPE_CURSOR = "CURSOR"
TYPE_OBJECT = "OBJECT"
TYPE_PLANE = "WORKING PLANE"
TYPE_GRID_MAJ = "GRID MAJOR"
TYPE_GRID_MIN = "GRID MINOR"

ALL_EDGE_TYPES = [TYPE_EDGE, TYPE_GUIDE_E]
ALL_PT_TYPES = [TYPE_VERT, TYPE_EDGE_C, TYPE_EMPTY, TYPE_AXIS, TYPE_CURSOR]

TAPE_TYPE_SINGLE = 1
TAPE_TYPE_MULTI = 2

TOOLBAR_HEIGHT_OFFSET = 50
TOOLBAR_BUTTON_SPACING = 40
TOOLBAR_COL = (0.2, 0.2, 0.2)
TOOLBAR_COL_HL = (0.5, 0.5, 0.5)
TOOLBAR_COL_ENBL = (0.33, 0.50, 0.76)

DISPLAY_TEXT_HEIGHT_OFFSET = 50

NAV_GIZMO_WIDTH = 100

BL_BOOL_EXTEND = "UNION"
BL_BOOL_REDUCE = "DIFFERENCE"
BL_BOOL_SOLVER_E = "EXACT"
BL_BOOL_SOLVER_F = "FAST"
BL_BOOL_NAME = "EXTRUDE BOOLEAN"

IMP_PATTERN = re.compile(
    # """^(?:(-?[0-9]{0,}?\.?[0-9]+)\'?)?(?:(\s?[0-9]+)\x22?)?\s?(?:([0-9]+\/[0-9]+)?)?$"""
    """^(?:(-?[0-9]{0,}\.?[0-9]+)\'?)?(?:(\s?[0-9]+\.?[0-9]?)\x22?)?\s?(?:([0-9]+\/[0-9]+)\.?[0-9]?)?$"""
)


B_NUMS = [
    "ZERO",
    "ONE",
    "TWO",
    "THREE",
    "FOUR",
    "FIVE",
    "SIX",
    "SEVEN",
    "EIGHT",
    "NINE",
    "PERIOD",
    "NUMPAD_PERIOD",
]

B_NUM_OPS = [
    "NUMPAD_SLASH",
    "SLASH",
    "MINUS",
    "NUMPAD_MINUS",
    "NUMPAD_PLUS",
    "NUMPAD_ASTERIX",
    "EQUAL",
    "COMMA",
]

B_INPUT_PT = [
    "MIDDLEMOUSE",
    # "TAB",
    "TRACKPADPAN",
    "TRACKPADZOOM",
    "MOUSEROTATE",
    "N",
    "NDOF_MOTION",
    "NDOF_BUTTON_MENU",
    "NDOF_BUTTON_FIT",
    "NDOF_BUTTON_TOP",
    "NDOF_BUTTON_BOTTOM",
    "NDOF_BUTTON_LEFT",
    "NDOF_BUTTON_RIGHT",
    "NDOF_BUTTON_FRONT",
    "NDOF_BUTTON_BACK",
    "NDOF_BUTTON_ISO1",
    "NDOF_BUTTON_ISO2",
    "NDOF_BUTTON_ROLL_CW",
    "NDOF_BUTTON_ROLL_CCW",
    "NDOF_BUTTON_SPIN_CW",
    "NDOF_BUTTON_SPIN_CCW",
    "NDOF_BUTTON_TILT_CW",
    "NDOF_BUTTON_TILT_CCW",
    "NDOF_BUTTON_ROTATE",
    "NDOF_BUTTON_PANZOOM",
    "NDOF_BUTTON_DOMINANT",
    "NDOF_BUTTON_PLUS",
    "NDOF_BUTTON_MINUS",
    "NDOF_BUTTON_ESC",
    "NDOF_BUTTON_ALT",
    "NDOF_BUTTON_SHIFT",
    "NDOF_BUTTON_CTRL",
    "NDOF_BUTTON_1",
    "NDOF_BUTTON_2",
    "NDOF_BUTTON_3",
    "NDOF_BUTTON_4",
    "NDOF_BUTTON_5",
    "NDOF_BUTTON_6",
    "NDOF_BUTTON_7",
    "NDOF_BUTTON_8",
    "NDOF_BUTTON_9",
    "NDOF_BUTTON_10",
    "NDOF_BUTTON_A",
    "NDOF_BUTTON_B",
    "NDOF_BUTTON_C",
]

B_NUMPAD_NUMS = [
    "NUMPAD_0",
    "NUMPAD_1",
    "NUMPAD_2",
    "NUMPAD_3",
    "NUMPAD_4",
    "NUMPAD_5",
    "NUMPAD_6",
    "NUMPAD_7",
    "NUMPAD_8",
    "NUMPAD_9",
    "NUMPAD_PERIOD",
]

B_ARROWS_WHEEL = ["UP_ARROW", "WHEELUPMOUSE", "DOWN_ARROW", "WHEELDOWNMOUSE"]
NUM_OPS = ["/", "*", "-", "+"]
IMP_OPS = ['"', "'", " "]
B_AKEYS = ["X", "Y", "Z"]

DEFAULT_STATUS = "Construction Lines"
NUM_INPUT_STATUS = "Type number or expression"
CIRCLE_SEG_STATUS = "Segments: "
USR_MSG_NO_OBJ = "No Objects Selected"
USR_MSG_NO_GEOM = "No Geometry Selected"
OBJ_TYPES = ["MESH", "EMPTY"]

MSG_PRESS_ENTER_0 = "press Enter to begin numeric input"
MSG_PRESS_ENTER_1 = "enter numeric input"

MSG_TAPE_PHASE_0 = "Click a starting point"
MSG_TAPE_PHASE_1 = "Click an end point or "
MSG_TAPE_PHASE_2 = "Type a number or expression to extend tape. Press Enter to confirm"

MSG_ARC_PHASE_2 = "Drag to extend arc or "
MSG_MOV_PHASE_1 = "Drag to move or "
MSG_MOV_PHASE_2 = ". Press Ctrl to duplicate (or Alt to duplicate and link data)"
MSG_MOV_PHASE_3 = "Type a number to increase duplicates ('/' to distribute)"

MSG_ROT_PHASE_0 = "Click to select a pivot point"
MSG_ROT_PHASE_1 = "Click to select the axis of rotation"
MSG_ROT_PHASE_2 = "Drag to rotate or "

MSG_EXT_PHASE_0 = "Select a face to extrude"
MSG_EXT_PHASE_1 = "Drag to extrude or "

MSG_SELECT_PHASE_0 = "Click an Object, Guide or Face to select (Shift for multi-select)"

MSG_SUBTITLE = "Press X, Y, Z or Ctrl+ Shfit to lock direction"

OP_MESSAGING = {
    (CL_STATE_POLY_TAPE, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_POLY_TAPE, "1"): MSG_TAPE_PHASE_1,
    (CL_STATE_POLY_TAPE, "2"): MSG_TAPE_PHASE_2,
    (CL_STATE_POLY_LINE, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_POLY_LINE, "1"): MSG_TAPE_PHASE_1,
    (CL_STATE_POLY_RECT, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_POLY_RECT, "1"): MSG_TAPE_PHASE_1,
    (CL_STATE_POLY_CIRC, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_POLY_CIRC, "1"): MSG_TAPE_PHASE_1,
    (CL_STATE_POLY_ARC, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_POLY_ARC, "1"): MSG_TAPE_PHASE_1,
    (CL_STATE_POLY_ARC, "2"): MSG_ARC_PHASE_2,
    (CL_STATE_ROT, "0"): MSG_ROT_PHASE_0,
    (CL_STATE_ROT, "1"): MSG_ROT_PHASE_1,
    (CL_STATE_ROT, "2"): MSG_ROT_PHASE_2,
    (CL_STATE_SEL, "0"): MSG_SELECT_PHASE_0,
    (CL_STATE_MOV, "0"): MSG_TAPE_PHASE_0,
    (CL_STATE_MOV, "1"): MSG_MOV_PHASE_1,
    (CL_STATE_MOV, "2"): MSG_MOV_PHASE_2,
    (CL_STATE_MOV, "3"): MSG_MOV_PHASE_3,
    (CL_STATE_EXT, "0"): MSG_EXT_PHASE_0,
    (CL_STATE_EXT, "1"): MSG_EXT_PHASE_1,
}

# Messaging state (tape, move, rotate, etc)
# and phase (before click, first, click, second click, etc)


def return_phase_message(state, phase):
    key = (state, str(phase))
    if key in OP_MESSAGING:
        return OP_MESSAGING[key]
    return ""


def return_numeric_message(opt):
    if opt == 0:
        return MSG_PRESS_ENTER_0
    elif opt == 1:
        return MSG_PRESS_ENTER_1
    return ""


def convert_shift_bnum(b_num):
    return {"EIGHT": "*", "EQUAL": "+"}.get(b_num, "")


def convert_bakey(b_akey):
    return {
        "X": [Vector((1, 0, 0)), "X"],
        "Y": [Vector((0, 1, 0)), "Y"],
        "Z": [Vector((0, 0, 1)), "Z"],
    }.get(b_akey, [])


def return_switch_xy_constraint(constraint):
    if constraint:
        if constraint[1] == "X":
            return [constraint[0], "Y"]
        elif constraint[1] == "Y":
            return [constraint[0], "X"]


def convert_bnum(b_num):
    return {
        "ZERO": "0",
        "ONE": "1",
        "TWO": "2",
        "THREE": "3",
        "FOUR": "4",
        "FIVE": "5",
        "SIX": "6",
        "SEVEN": "7",
        "EIGHT": "8",
        "NINE": "9",
        "PERIOD": ".",
        "SLASH": "/",
        "NUMPAD_SLASH": "/",
        "MINUS": "-",
        "NUMPAD_MINUS": "-",
        "NUMPAD_PLUS": "+",
        "NUMPAD_ASTERIX": "*",
        "COMMA": ",",
    }.get(b_num, "")


def convert_bnumpad(b_num):
    return {
        "NUMPAD_0": "0",
        "NUMPAD_1": "1",
        "NUMPAD_2": "2",
        "NUMPAD_3": "3",
        "NUMPAD_4": "4",
        "NUMPAD_5": "5",
        "NUMPAD_6": "6",
        "NUMPAD_7": "7",
        "NUMPAD_8": "8",
        "NUMPAD_9": "9",
        "NUMPAD_PERIOD": ".",
    }.get(b_num, "")


def unit_scale(b_unit):
    return {
        "KILOMETERS": 1000.0,
        "METERS": 1.0,
        "CENTIMETERS": 0.01,
        "MILLIMETERS": 0.001,
        "MICROMETERS": 0.0000001,
        "MILES": 1609.34,
        "FEET": 0.3048,
        "INCHES": 0.0254,
        "THOU": 2.54e-5,
    }.get(b_unit, 1.0)


INCH_TO_CM = 2.54
INCHES_PER_FEET = 12
INCHES_PER_MILE = 5280 * INCHES_PER_FEET
THOU_PER_INCH = 1000
# Conversion factor from Blender Units to Inches / Feet
BL_U_TO_INCHES = 100.0 / INCH_TO_CM
BL_U_TO_FEET = 100.0 / (INCH_TO_CM * INCHES_PER_FEET)


def unit_symbol(b_unit):
    return {
        "KILOMETERS": "km",
        "METERS": "m",
        "CENTIMETERS": "cm",
        "MILLIMETERS": "mm",
        "MICROMETERS": "μm",
        # "MILES": 1609.34,
        # "FEET": 0.3048,
        # "INCHES": 0.0254,
        # "THOU": 2.54e-5,
    }.get(b_unit, "")


def state_type_from_name(key):
    return {
        "TAPE": CL_STATE_POLY_TAPE,
        "LINE": CL_STATE_POLY_LINE,
        "CIRCLE": CL_STATE_POLY_CIRC,
        "RECTANGLE": CL_STATE_POLY_RECT,
        "ARC": CL_STATE_POLY_ARC,
        "ROTATE": CL_STATE_ROT,
        "MOVE": CL_STATE_MOV,
        "EXTRUDE": CL_STATE_EXT,
        "SELECT": CL_STATE_SEL,
    }.get(key, None)


default_input_map = {
    # name, key, label, tip, ctrl, alt, shift, oskey
    # 0-8
    "SELECT": ("S", "Select"),
    "TAPE": ("T", "Tape"),
    "LINE": ("L", "Line"),
    "CIRCLE": ("C", "Circle"),
    "RECTANGLE": ("R", "Rectangle"),
    "ARC": ("U", "Arc"),
    "ROTATE": ("O", "Rotate"),
    "MOVE": ("G", "Move"),
    "EXTRUDE": ("E", "Extrude"),
    # 9-22
    "REMOVE": ("X", "Remove Geometry"),
    "DISSOLVE": ("X", "Dissolve Geometry", False, True, False, False),
    "FACE": ("F", "Create Face"),
    "CONVERT": ("K", "Convert Guide To Geom"),
    "HIDE": ("H", "Hide/Show Guides"),
    "END": ("ESC", "End Tool"),
    "EXIT": ("ESC", "Exit", False, False, True, False),
    "FOCUS": ("F", "Focus Selected Objects", False, False, True, False),
    "LOCAL_VIEW": ("NUMPAD_SLASH", "Focus Selected Objects", False, False, False, False),
    #
    "UNDO": ("Z", "Undo previous action", True, False, False, False),
    # Might be able to use platform.system
    # "UNDO": ("Z", "Undo previous action(alternative)", False, False, False, True),
    "REDO": ("Z", "Redo previous action", True, False, True, False),
    "SELECT_ALL": ("A", "Select All"),
    "DESELCECT_ALL": ("A", "Deselect All", False, True, False, False),
    "MODE_TOGGLE": ("TAB", "Toggle between Object and Edit Modes"),
    # # 18-19
    "DUPLICATE": ("NOTHING", "Duplicate on Move", True, False, False, False),
    "DUPLICATE AND LINK": ("NOTHING", "Duplicate and link on Move", False, True, False, False),
}

INPT_MODIFIER_NAMES = ("DUPLICATE", "DUPLICATE AND LINK")


def compile_input_signature(event, k, override_k=False) -> Tuple:
    if override_k:
        k = "NOTHING"

    signature = (event.alt, event.ctrl, event.shift,
                     k, event.oskey, "PRESS")

    return signature


def poly_type_from_name(name):
    return {
        "Tape": CL_STATE_POLY_TAPE,
        "Line": CL_STATE_POLY_LINE,
        "Select": CL_STATE_SEL,
        "Circle": CL_STATE_POLY_CIRC,
        "Rectangle": CL_STATE_POLY_RECT,
        "Arc": CL_STATE_POLY_ARC,
    }.get(name, CL_STATE_POLY_TAPE)


def str_is_float(val):
    try:
        float(val)
    except ValueError:
        return False
    return True


# [Vector(), "XYZP.etc"]
def get_constraint_col(constraint):
    if constraint:
        if constraint[1] == "X":
            return CL_XAXIS_COL
        elif constraint[1] == "Y":
            return CL_YAXIS_COL
        elif constraint[1] == "Z":
            return CL_ZAXIS_COL
        elif constraint[1] == "PERP":
            return CL_PERP_COL
        elif constraint[1] == "EXTD":
            return CL_EXTD_COL  # TEMP
    return CL_DEFAULT_COL


def return_global_axes() -> List:
    return [
        [Vector((1, 0, 0)), "X"],
        [Vector((0, 1, 0)), "Y"],
        [Vector((0, 0, 1)), "Z"],
    ]


def return_global_axes_neg() -> List:
    return [
        [Vector((-1, 0, 0)), "X"],
        [Vector((0, -1, 0)), "Y"],
        [Vector((0, 0, -1)), "Z"],
    ]


def return_extrude_col(cut_through: bool) -> Tuple:
    if cut_through:
        return CL_EXTRUDE_CUT_COL
    return CL_EXTRUDE_COL


def return_global_axes_visible(context) -> List:
    return [
        Vector((1, 0, 0)) if context.space_data.overlay.show_axis_x else None,
        Vector((0, 1, 0)) if context.space_data.overlay.show_axis_y else None,
        Vector((0, 0, 1)) if context.space_data.overlay.show_axis_z else None,
    ]


def return_axis_vect(axis):
    return {"X": Vector((1, 0, 0)), "Y": Vector((0, 1, 0)), "Z": Vector((0, 0, 1))}.get(
        axis, Vector((0, 0, 1))
    )


def return_global_axes_vects():
    return [Vector((1, 0, 0)), Vector((0, 1, 0)), Vector((0, 0, 1))]


def return_active_axis(constraint):
    elm = -1
    if constraint:
        for i, c in enumerate(constraint[0]):
            if c != 0:
                elm = i
    return elm


def make_constraint(dir_vect, name):
    return [Vector(dir_vect), name]


def return_constraint_plane(constraint):
    if constraint:
        if constraint[1] == "X":
            return [Vector((1, 0, 1)), "X"]
        elif constraint[1] == "Y":
            return [Vector((0, 1, 1)), "Y"]
        elif constraint[1] == "Z":
            return [Vector((1, 1, 0)), "Z"]
    return [Vector((1, 1, 0)), "Z"]


# GRID
def return_viewport_grid_visible(context):
    return context.space_data.overlay.show_ortho_grid


def return_viewport_grid_dims(context):
    ovl = context.space_data.overlay
    return [ovl.grid_scale, ovl.grid_scale_unit, ovl.grid_subdivisions]


# NONE, METRIC or IMPERIAL
def return_scene_unit_sys(context):
    return context.scene.unit_settings.system


# def return_model_mode(context):
#     if bpy.context.mode == "EDIT_MESH":
#         return "EDIT MODE"
#     return "OBJECT MODE"


# return feet, inches, hundredths
# from input string
def parse_imp_str(str_inpt):
    match = re.match(IMP_PATTERN, str_inpt)
    if match:
        return match.groups()
    return []


def parse_imp_input(inpt, context) -> str:
    parse_elms = parse_imp_str(inpt)
    units = return_b_length_unit(context)
    r_val = ""
    if parse_elms:
        if units == "FEET":
            feet = float(parse_elms[0]) if parse_elms[0] else 0
            inches = float(parse_elms[1]) if parse_elms[1] else 0
            hunds = float(eval(str(parse_elms[2]))) if parse_elms[2] else 0
            to_feet = inches + hunds

            if to_feet > 0:
                to_feet = to_feet / 12
            r_val = str(feet + to_feet)
        elif units == "INCHES":
            inches = float(parse_elms[0]) if parse_elms[0] else 0
            inches += float(parse_elms[1]) if parse_elms[1] else 0
            inches += float(eval(str(parse_elms[2]))) if parse_elms[2] else 0
            r_val = str(inches)
        elif units == "THOU":
            thou = float(parse_elms[0]) if parse_elms[0] else 0
            r_val = str(thou)
    return r_val


# unit scale from Blender property
def return_b_unit_scale(context: bpy.context):
    return context.scene.unit_settings.scale_length


def return_b_length_unit(context: bpy.context):
    return context.scene.unit_settings.length_unit


def return_b_unit_separate(context: bpy.context):
    return context.scene.unit_settings.use_separate


def return_cl_display_precision(context: bpy.context):
    return context.scene.cl_settings.cl_display_precision_int


def highlight_col(s_type):
    return {
        TYPE_VERT: CL_HIGHLIGHT_COL_VERT,
        TYPE_EDGE_C: CL_HIGHLIGHT_COL_EDGE_C,
        TYPE_EDGE: CL_HIGHLIGHT_COL_EDGE,
        TYPE_FACE: CL_HIGHLIGHT_COL_FACE,
        TYPE_GUIDE_E: CL_HIGHLIGHT_COL_EDGE,
        TYPE_GRID_MAJ: CL_HIGHLIGHT_COL_GRID_MAJ,
        TYPE_GRID_MIN: CL_HIGHLIGHT_COL_GRID_MIN,
    }.get(s_type, CL_HIGHLIGHT_COL)


# mask a list by constraint
def mask_by_constraint(val, dist, constraint):
    for i, c in enumerate(constraint):
        if c == 0:
            val[i] = 0
        else:
            val[i] = dist * constraint[i]
            # if i == negate:
            #     val[i] *= -1


def convert_loc_2d(vert, context):
    new_2dco = location_3d_to_region_2d(
        context.region, context.space_data.region_3d, vert
    )
    return new_2dco


def convert_to_cur_scale(val, context: bpy.context) -> float:
    unit = context.scene.unit_settings.length_unit
    return round(val * (unit_scale(unit)), CL_PRECISION)


def return_length_in_cur_unit(val, context, precision=4):
    unit = context.scene.unit_settings.length_unit
    return round(val / (unit_scale(unit)), precision)


def reverse_scale(val, override_unit_scale, context):
    if override_unit_scale:
        return round(val / return_b_unit_scale(context), CL_PRECISION)
    return round(val, 4)


# expected range[low, high], target range[low, high], value
def map_range(a, b, n):
    (a1, a2), (b1, b2) = a, b
    return b1 + ((n - a1) * (b2 - b1) / (a2 - a1))


def map_range_2(val_min, val_max, new_min, new_max, value):
    old_range = val_max - val_min
    new_range = new_max - new_min
    return (((value - val_min) * new_range) / old_range) + new_min


def cursor_loc(context):
    return [context.scene.cursor.location, TYPE_CURSOR]


def viewport_zoom_dist():
    for area in bpy.context.screen.areas:
        if area.type == "VIEW_3D":
            return round(area.spaces[0].region_3d.view_distance, 4)


# do any useable objects exist in the scene
def objs_exist(context):
    for obj in context.visible_objects:
        if obj.type == "MESH" or obj.type == "EMPTY":
            return True

    return False


def deselect_all_objects():
    for obj in bpy.data.objects:
        obj.select_set(False)


def return_all_CL_POINTS(context):
    clp = []
    for ob in context.scene.objects:
        if not ob:
            continue
        if ob.type == "EMPTY" and ob.name.startswith(CL_C_NAME):
            clp.append(ob)
    return clp


# EXTRUDE AND BOOLEANS
def solid_modifer_to_obj(obj: bpy.types.Object, manifold: bool, thickness=0.0) -> None:
    if not object_is_valid(obj):
        return

    mod = obj.modifiers.new(name="CL_Solidify", type="SOLIDIFY")
    mod.solidify_mode = "NON_MANIFOLD"
    if not manifold:
        mod.use_flip_normals = True
    mod.use_quality_normals = True
    mod.use_flat_faces = True
    mod.use_even_offset = True
    mod.use_rim = True
    mod.use_rim_only = False
    # mod.offset = -1.0000
    mod.thickness = thickness


def bool_modifer_to_obj(obj: bpy.types.Object, bool_obj: bpy.types.Object) -> None:
    if not object_is_valid(obj):
        return

    mod = obj.modifiers.new(name=BL_BOOL_NAME, type="BOOLEAN")
    mod.operation = BL_BOOL_REDUCE
    if bpy.app.version >= (2, 91, 0):
        mod.solver = BL_BOOL_SOLVER_E
    mod.object = bool_obj
    mod.show_viewport = True


def bool_modifiers_remove(obj):
    if not object_is_valid(obj):
        return
    for m in obj.modifiers:
        if m.name.startswith(BL_BOOL_NAME):
            obj.modifiers.remove(m)
            return


def return_extrude_boolean_mods(obj):
    if not object_is_valid(obj):
        return []

    return [m for m in obj.modifiers if m.name.startswith(BL_BOOL_NAME)]


def bool_modifiers_apply(cl_op, obj, context):
    if not object_is_valid(obj):
        return

    # mods = [m for m in obj.modifiers if m.name.startswith(BL_BOOL_NAME)]

    if context.mode == "EDIT_MESH":
        bpy.ops.object.mode_set(mode="OBJECT")
    context.view_layer.objects.active = obj
    obj.select_set(True)

    mods = return_extrude_boolean_mods(obj)

    try:
        for m in mods:
            bpy.ops.object.modifier_apply(modifier=m.name)
            bpy.context.view_layer.update()
        # bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="MEDIAN")
        bpy.ops.object.transform_apply(rotation=True, scale=True)
        # bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_all(action="SELECT")
        bpy.ops.mesh.remove_doubles(threshold=0.001, use_unselected=True)
        bpy.ops.mesh.normals_make_consistent(inside=False)
        bpy.ops.mesh.select_all(action="DESELECT")
    except RuntimeError as e:
        print(e)
        msg_log(cl_op, "Can't extrude multi-user objects.")


def bool_modifier_switch(bool_obj, ext_obj_id, push):
    if not bool_obj:
        return

    mods = return_extrude_boolean_mods(bool_obj)

    for m in mods:
        if not m:
            continue
        if m.type == "BOOLEAN" and m.object.name == ext_obj_id:
            if push:
                m.operation = BL_BOOL_REDUCE
                if bpy.app.version >= (2, 91, 0):
                    m.solver = BL_BOOL_SOLVER_E
                else:
                    m.solver = BL_BOOL_SOLVER_F
            else:
                m.operation = BL_BOOL_EXTEND
                m.solver = BL_BOOL_SOLVER_F


####################
def only_select(obj):
    deselect_all_objects()
    obj.select_set(True)


def setup_new_obj(name, context):
    obj_name = name
    mesh = bpy.data.meshes.new("mesh")
    obj = bpy.data.objects.new(obj_name, mesh)
    context.scene.collection.objects.link(obj)
    context.view_layer.objects.active = obj
    obj.select_set(True)
    mesh = obj.data
    bm = bmesh.new()
    return obj, mesh, bm


def apply_transformations(obj, reset_mode):
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.transform_apply(location=True, scale=True, rotation=True)
    bpy.ops.object.mode_set(mode=reset_mode)
    obj.select_set(False)


def flag_switch(flags, f_num):
    tmp = flags[f_num]
    clear_flags(flags)
    flags[f_num] = not tmp


def clear_flags(flags):
    for i in range(len(flags)):
        flags[i] = False


def is_in_ortho_view(context):
    for area in context.screen.areas:
        if area.type == "VIEW_3D":
            for space in area.spaces:
                if space.type == "VIEW_3D":
                    if space.region_3d.is_perspective:
                        return False

    return True


def get_view_matrix(context):
    for area in context.screen.areas:
        if area.type == "VIEW_3D":
            r3d = area.spaces.active.region_3d
            return r3d.view_matrix

    return []


def get_view_orientation_from_matrix(view_matrix):
    def r(x): return round(x, 2)
    view_rot = view_matrix.to_euler()

    orientation_dict = {
        (0.0, 0.0, 0.0): "TOP",
        (r(math.pi), 0.0, 0.0): "BOTTOM",
        (r(-math.pi / 2), 0.0, 0.0): "FRONT",
        (r(math.pi / 2), 0.0, r(-math.pi)): "BACK",
        (r(-math.pi / 2), r(math.pi / 2), 0.0): "LEFT",
        (r(-math.pi / 2), r(-math.pi / 2), 0.0): "RIGHT",
    }

    return orientation_dict.get(tuple(map(r, view_rot)), "UNDEFINED")


def normal_from_ortho_view_type(view_type: str) -> Vector:
    return {
        "TOP": Vector((0, 0, 1)),
        "BOTTOM": Vector((0, 0, -1)),
        "FRONT": Vector((0, -1, 0)),
        "BACK": Vector((0, 1, 0)),
        "LEFT": Vector((-1, 0, 0)),
        "RIGHT": Vector((1, 0, 0)),
    }.get(view_type, Vector((0, 0, 1)))


def grid_plane_from_ortho_view_type(view_type):
    return {
        "TOP": Vector((1, 1, 0)),
        "BOTTOM": Vector((1, 1, 0)),
        "FRONT": Vector((1, 0, 1)),
        "BACK": Vector((1, 0, 1)),
        "LEFT": Vector((0, 1, 1)),
        "RIGHT": Vector((0, 1, 1)),
    }.get(view_type, Vector((0, 0, 0)))


# remove depth dependent on ortho orientation
# base off of click location
def flatten_location_to_ortho(ort, loc, start_loc):
    n_loc = loc.copy()
    s_loc = start_loc.copy()
    if ort in {"TOP", "BOTTOM"}:
        n_loc[2] = s_loc[2]  # no Z
    elif ort in {"FRONT", "BACK"}:
        n_loc[1] = s_loc[1]  # no Y
    elif ort in {"LEFT", "RIGHT"}:
        n_loc[0] = s_loc[0]  # no X

    return n_loc.copy()


def change_view_transform_pivot(p_type, context):
    context.scene.tool_settings.transform_pivot_point = p_type
    # for area in context.screen.areas:
    #     if area.type == "VIEW_3D":
    #         area.spaces.pivot_point = p_type


def get_space_view_3d():
    for area in bpy.context.screen.areas:
        if area.type == "VIEW_3D":
            for space in area.spaces:
                if space.type == "VIEW_3D":
                    return space
    return None


def get_space_shading_mode():
    space = get_space_view_3d()
    if space:
        return space.shading.type
    return "SOLID"


def toggle_x_ray_mode():
    space = get_space_view_3d()
    if space:
        if space.shading.type == "SOLID":
            space.shading.type = "WIREFRAME"
        else:
            space.shading.type = "SOLID"


def ray_cast_obj(coord, context):
    region = context.region
    rv3d = context.region_data
    if region and rv3d:
        view_vector = region_2d_to_vector_3d(region, rv3d, coord)
        ray_origin = region_2d_to_origin_3d(region, rv3d, coord)
        ray_target = ray_origin + view_vector
        ray_direction = ray_target - ray_origin
        depsgraph = context.evaluated_depsgraph_get()
        depsgraph.update()
        result, _, _, _, obj, _ = context.scene.ray_cast(
            depsgraph, ray_origin, ray_direction
        )
        if result:
            return obj
    return None


def select_closest_obj(context, x, y, cur_mode):
    if cur_mode == "EDIT":
        bpy.ops.object.mode_set(mode="OBJECT")
    context.view_layer.objects.active = None
    deselect_all_objects()

    obj = ray_cast_obj([x, y], context)
    if obj and obj.visible_in_viewport_get(get_space_view_3d()):
        obj.select_set(True)
        context.view_layer.objects.active = obj
        return obj

    try:
        bpy.ops.view3d.select(location=(x, y), toggle=True)
    except RuntimeError:
        return None
    return context.selected_objects[-1] if context.selected_objects else None


# Fixes issues with selections of overlapping objects
def select_closest_obj_click(context, x, y, cur_mode):
    if cur_mode == "EDIT":
        bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.view3d.select(location=(x, y))
    return context.selected_objects[0] if context.selected_objects else None


def return_all_guide_geom(edges, context):
    guides = []
    clps = return_all_CL_POINTS(context)
    for obj in clps:
        guide = return_guide_geom(obj, context)
        if guide:
            guides.extend(guide)

    # add in guide/guide and guide/edge intersections
    if guides:
        guides.extend(return_guide_intersections(edges, guides))
    return guides


def return_guide_geom(obj, context, inc_horiz=True):
    return return_guide_data(obj, context, inc_horiz)


def return_guide_data(obj, context, inc_horiz, only_vis=True) -> List[clg.SnapGeom]:
    geom = []
    if object_is_valid(obj):
        obj_pair = None
        if obj.type == "EMPTY":
            if only_vis and not obj.visible_get():
                return []
            keys = ["Pair"]
            if return_guide_keys_exist(obj, keys):
                obj_pair = return_object_by_name(obj[keys[0]], context)

            # separate from above to allow for backwards compatibility
            horz_key = ["Horizontal"]
            if return_guide_keys_exist(obj, horz_key):
                horiz = obj[horz_key[0]]
                if horiz and not inc_horiz:
                    return []

            if not obj_pair:
                return []

            # guide vert
            geom.append(
                clg.SnapGeom(
                    Vector((obj.location)),
                    TYPE_VERT,
                    [[obj.location, obj_pair.location]]
                    if return_guide_keys_exist(obj, keys)
                    else None,
                    None,
                    None,
                    edge=(obj.location, obj_pair.location)
                    if return_guide_keys_exist(obj, keys)
                    else None,
                    connect_obj=obj,
                )
            )
            if return_guide_keys_exist(obj, keys):
                # guide edge if exists
                geom.append(
                    clg.SnapGeom(
                        (obj.location, obj_pair.location),
                        TYPE_GUIDE_E,
                        [[obj.location, obj_pair.location]],
                        None,
                        None,
                        # always give guide start obj
                        connect_obj=obj,
                    )
                )
                # guide edge center
                geom.append(
                    clg.SnapGeom(
                        (clg.find_edge_center(obj.location, obj_pair.location)),
                        TYPE_EDGE_C,
                        [[obj.location, obj_pair.location]],
                        None,
                        None,
                        edge=[obj.location, obj_pair.location],
                        connect_obj=obj,
                    )
                )
            return geom
    return []


def return_guide_keys_exist(obj, keys) -> bool:
    # if hasattr(obj,'_RNA_UI'):
    if object_is_valid(obj) and all(k in obj.keys() for k in keys):
        return True
    return False


# set geom.type to TYPE_GUIDE_E if center
# is on a guide
def set_guide_from_center(close_geom_pt) -> None:
    # check if connected object is an empty
    c_obj = close_geom_pt.obj
    if c_obj:
        if c_obj.get_obj().type == "EMPTY":
            close_geom_pt.geom_type = TYPE_GUIDE_E


# return guide/guide and guide/edge intersection points
def return_guide_intersections(edges, guide_geom) -> List[clg.SnapGeom]:
    edges_guides = guide_geom + edges
    geom = []
    for g in edges_guides:
        if g.geom_type in {TYPE_EDGE, TYPE_GUIDE_E}:
            for cg in guide_geom:
                if g != cg and cg.geom_type == TYPE_GUIDE_E:
                    co = clg.find_edge_edge_intersection_verts(
                        [g.w_loc[0], g.w_loc[1], cg.w_loc[0], cg.w_loc[1]]
                    )
                    if co:
                        g_dir = g.w_loc[1] - g.w_loc[0]
                        prcnt = clg.percent_point_on_edge(co, g.w_loc)
                        if 0 < prcnt < 1:
                            loc = Vector(
                                [
                                    getattr(g.w_loc[0], i) +
                                    prcnt * getattr(g_dir, i)
                                    for i in "xyz"
                                ]
                            )
                            geom.append(
                                clg.SnapGeom(
                                    Vector((loc)),
                                    TYPE_VERT,
                                    g.connected_edges,
                                    None,
                                    None,
                                )
                            )
    return geom


# For grid snapping
# def return_grid_data(context):
#     overlay = context.space_data.overlay
#     print(overlay.grid_scale)


# INFO BAR DISPLAY
def update_info_bar(context, val, no_clear=False) -> None:
    if not context.area:
        return
    if val != "":
        context.area.header_text_set(text=val)
        # context.workspace.status_text_set(text=val)
    else:
        context.area.header_text_set(text=None)

    if no_clear:
        context.area.header_text_set(DEFAULT_STATUS)


# not b_unit scale
def return_scaled_dist(context, dist) -> List[float]:
    scale = return_b_unit_scale(context)
    full_dist = dist[0] * scale
    x_dist = dist[1] * scale
    y_dist = dist[2] * scale
    z_dist = dist[3] * scale

    return [full_dist, x_dist, y_dist, z_dist]


def display_cur_tape_angle(context, angle) -> None:
    update_info_bar(context, "Angle: " + str(round(math.degrees(angle), 2)))


# First Generate a list of normal imperial fractions with denominators 2, 4, 8, 16, 32, and 64
# that are less than or equal to the fractional part of the input value.
# If such fractions exist, return the largest one.
# If no such fractions exist, rreturns the fraction with a denominator of 64
# that is closest to the input value.
def inches_as_fraction(inches: float) -> Tuple[int, int, int]:
    frac, int_ = math.modf(inches)
    normal_fractions = [Fraction(n, d) for d in [2, 4, 8, 16, 32, 64] for n in range(
        1, d) if Fraction(n, d) <= Fraction(frac)]
    if normal_fractions:
        fr = max(normal_fractions, key=lambda f: f)
    else:
        fr = Fraction(frac).limit_denominator(64)
    return (int(int_), fr.numerator, fr.denominator)


def format_length_IMP(value: float, precision: int, unit_length="INCHES") -> str:
    if unit_length in ("INCHES", "FEET"):
        value *= BL_U_TO_INCHES
        (inches, n, d) = inches_as_fraction(value)
        if unit_length == "FEET":
            (feet, inches) = divmod(inches, INCHES_PER_FEET)
        else:
            feet = 0

        # Format
        if feet > 0 and n > 0:
            return "{}′ {}-{}⁄{}″".format(feet, inches, n, d)
        elif feet > 0:
            return "{}′ {}″".format(feet, inches)
        elif n > 0:
            return "{}-{}⁄{}″".format(inches, n, d)
        else:
            return "{}″".format(inches)
    return "0"


# def format_length_IMP(value: float, precision: int, unit_length="INCHES") -> str:
#     if unit_length in ("INCHES", "FEET"):
#         value *= BL_U_TO_INCHES
#         (inches, n, d) = inches_as_fraction(value)
#         if unit_length == "FEET":
#             (feet, inches) = divmod(inches, INCHES_PER_FEET)
#         else:
#             feet = 0

#         # Format
#         if feet > 0 and n > 0:
#             return "{}′ {}-{}⁄{}″".format(feet, inches, n, d)
#         elif feet > 0:
#             return "{}′ {}″".format(feet, inches)
#         elif n > 0:
#             return "{}-{}⁄{}″".format(inches, n, d)
#         else:
#             return "{}″".format(inches)
#     return "0"
############################

def return_cur_tape_dist(context: bpy.context, dist: List[float]) -> str:
    scn_system = return_scene_unit_sys(context)
    unit = return_b_length_unit(context)
    full_dist, _, _, _ = return_scaled_dist(context, dist)
    dsp_unit_sep = return_b_unit_separate(context)
    dsp_precision = return_cl_display_precision(context)

    if scn_system != "NONE":
        if scn_system == "METRIC":
            full_dist = return_length_in_cur_unit(
                full_dist, context, precision=dsp_precision
            )
            ddist = str(full_dist) + " " + unit_symbol(unit)
        else:
            if unit in {"FEET", "INCHES"}:
                ddist = format_length_IMP(full_dist, dsp_precision, unit)
            else:
                ddist = bpy.utils.units.to_string(
                    scn_system,
                    "LENGTH",
                    full_dist,
                    precision=dsp_precision,
                    split_unit=dsp_unit_sep,
                    compatible_unit=True,
                )
    else:
        ddist = str(full_dist)
    return ddist


def find_CL_object(vert, type_name: str, context: bpy.context) -> bpy.types.Object:
    for obj in context.scene.objects:
        if obj.name.startswith(type_name):
            verts = obj.data.vertices
            for v in verts:
                if v.co == vert:
                    return obj
    return None


def delete_Object(obj, context: bpy.context, return_to_edit_obj=None) -> None:
    if not object_is_valid(obj):
        return
    # store mode if in edit mode
    e_mode = bpy.context.mode == "EDIT_MESH"
    if e_mode:
        e_mode = True
        bpy.ops.object.mode_set(mode="OBJECT")

    for o in bpy.data.objects:
        o.select_set(False)

    # Can't select a hidden object
    obj.hide_set(False)
    obj.select_set(True)
    bpy.ops.object.delete()

    # reset to edit mode if was in edit
    if return_to_edit_obj:
        if bpy.context.mode == "OBJECT":
            return_to_edit_obj.select_set(True)
            context.view_layer.objects.active = return_to_edit_obj
            bpy.ops.object.mode_set(mode="EDIT")


def refresh_active_edit_object(context: bpy.context, obj, return_to_edit=True, update_view=False):
    if not object_is_valid(obj):
        return
    context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode="OBJECT")
    obj.select_set(True)
    obj.data.update()
    context.active_object.data.update()
    context.active_object.data.update(calc_edges=False)
    if update_view:
        bpy.context.view_layer.depsgraph.update()
        bpy.context.view_layer.update()
    if return_to_edit:
        bpy.ops.object.mode_set(mode="EDIT")


# use after undo
# def reset_active_edit_object(context, obj, return_to_edit=True):
#     refresh_active_edit_object(context, obj, return_to_edit=False)
#     bpy.ops.object.transform_apply()
#     bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
#     refresh_active_edit_object(context, obj, return_to_edit=False)
#     if return_to_edit:
#         bpy.ops.object.mode_set(mode="EDIT")


def refresh_edit_mode():
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.mode_set(mode="EDIT")


def return_object_by_name(name: str, context: bpy.context) -> bpy.types.Object:
    for obj in context.scene.objects:
        if obj.name == name:
            return obj
    return None


# BMESH
def update_bmesh(bm):
    update_bm_verts(bm)
    update_bm_edges(bm)
    update_bm_faces(bm)
    bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=0.001)
    bmesh.ops.recalc_face_normals(bm, faces=bm.faces)


def update_bm_verts(bm):
    bm.verts.index_update()
    bm.verts.ensure_lookup_table()


def update_bm_edges(bm):
    bm.edges.index_update()
    bm.edges.ensure_lookup_table()


def update_bm_faces(bm):
    bm.faces.index_update()
    bm.faces.ensure_lookup_table()


def ensure_bm_lookups(bm):
    bm.verts.ensure_lookup_table()
    bm.edges.ensure_lookup_table()
    bm.faces.ensure_lookup_table()


def bm_geom_from_index(bm, index, geom_type):
    if geom_type not in {TYPE_VERT, TYPE_EDGE, TYPE_FACE}:
        return None

    geom = None
    try:
        if geom_type == TYPE_VERT:
            geom = bm.verts[index]
        elif geom_type == TYPE_EDGE:
            geom = bm.edges[index]
        elif geom_type == TYPE_FACE:
            geom = bm.faces[index]
    except Exception as e:
        return None

    return geom


def bm_geom_from_verts(bm, verts):
    v_len = len(verts)
    if v_len == 1:
        # Vert
        pass
    elif v_len == 2:
        return bm_edge_from_verts(verts[0], verts[1], bm)
    elif v_len > 2:
        # Face
        pass

    return None


# This should also be in cl_geom*******************************************
# def bm_edge_from_verts(v1, v2, bm):
#     v1 = clg.round_vect_4(v1)
#     v2 = clg.round_vect_4(v2)
#     for e in bm.edges:
#         if clg.round_vect_4(e.verts[0].co) in (v1, v2) and clg.round_vect_4(
#             e.verts[1].co
#         ) in (v1, v2):
#             return e
#     return None


def bm_edge_from_verts(v0: Vector, v1: Vector, bm: bmesh) -> bmesh.types.BMEdge:
    '''Check to see if both v1 and v2 vectors match the vector coordinates
    of a mesh edge.
    If there is a match then return the matched edge.
    '''
    for e in bm.edges:
        ev0 = e.verts[0].co
        ev1 = e.verts[1].co
        if (clg.is_close(ev0, v0) or clg.is_close(ev0, v1)) and (clg.is_close(ev1, v0) or clg.is_close(ev1, v1)):
            return e
    return None


def remove_bm_geom(bm: bmesh.types.BMesh, geom_list: List) -> None:
    if not isinstance(geom_list, list):
        return
    else:
        for g in geom_list:
            try:
                if isinstance(g, bmesh.types.BMVert):
                    bmesh.ops.delete(bm, geom=[g], context="VERTS")
                    update_bm_verts(bm)
                elif isinstance(g, bmesh.types.BMEdge):
                    bmesh.ops.delete(bm, geom=[g], context="EDGES")
                    update_bm_edges(bm)
                elif isinstance(g, bmesh.types.BMFace):
                    bmesh.ops.delete(bm, geom=[g], context="FACES")
                    update_bm_faces(bm)
                else:
                    raise Exception("Invalid Remove Geomerty")
            except Exception as e:
                print("Unable to remove: Invalid Geometry type")


def build_line_obj(verts, context):
    if isinstance(verts, list) and all(isinstance(x, Vector) for x in verts):
        obj, mesh, bm = setup_new_obj(CL_C_LINENAME, context)
        bm = bmesh.new()
        bm.from_mesh(mesh)

        n_vs = []
        for v in verts:
            n_vs.append(bm.verts.new(v))

        if n_vs:
            update_bm_verts(bm)
            make_new_edge(bm, n_vs)
            update_bmesh(bm)
        else:
            return None

        bm.to_mesh(obj.data)
        bm.free()
        # Apply transforms and set origin correctly
        # bpy.ops.object.transform_apply(rotation=True, scale=True)
        # bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
        refresh_active_edit_object(
            context, obj, return_to_edit=True, update_view=True)
        return obj
    return None


def make_new_edge(bm, end_verts):
    try:
        e = bm.edges.new((end_verts[0], end_verts[1]))
        update_bm_edges(bm)
        return e
    except ValueError:
        print("Edge already exists")
    return None


def duplicate_geom(bm, geom, geom_type, mtrx):
    # returns [new_geom, original_location]
    try:
        if geom_type == TYPE_VERT and isinstance(geom, bmesh.types.BMVert):
            n_v = bm.verts.new(mtrx @ geom.co)
            # make sure a new index is set
            update_bm_verts(bm)
            return [n_v.index, geom.co.copy(), TYPE_VERT]
        if geom_type == TYPE_EDGE and isinstance(geom, bmesh.types.BMEdge):
            n_v0 = bm.verts.new(mtrx @ geom.verts[0].co)
            n_v1 = bm.verts.new(mtrx @ geom.verts[1].co)
            update_bm_verts(bm)
            n_e = bm.edges.new([n_v0, n_v1])
            # make sure a new index is set
            update_bm_edges(bm)
            return [n_e.index, [n_v0.co.copy(), n_v1.co.copy()], TYPE_EDGE]
        if geom_type == TYPE_FACE and isinstance(geom, bmesh.types.BMFace):
            n_vs = []
            for v in geom.verts:
                n_vs.append(bm.verts.new(mtrx @ v.co))
            update_bm_verts(bm)
            n_f = bm.faces.new(n_vs)
            # make sure a new index is set
            update_bm_faces(bm)
            return [n_f.index, [v.co.copy() for v in n_vs], TYPE_FACE]
    except Exception as e:
        print("Invalid Geometry. Can't duplicate")
    return None


def duplicate_geom_multi(bm, geom_multi, geom_type, mtrx):
    d_geom = []
    for g in geom_multi:
        d_geom.append(duplicate_geom(bm, g, geom_type, mtrx))
    return d_geom


def return_face_verts(obj, face_id, context: bpy.context) -> List:
    if not object_is_valid(obj) or not isinstance(face_id, int):
        return []

    if context.mode == "EDIT_MESH":
        refresh_active_edit_object(context, obj)
        bm = bmesh.new()
        bm.from_mesh(obj.data)
        bm.faces.ensure_lookup_table()
        f_vs = []
        face_tri_indices = []
        mtrx = obj.matrix_world.copy()
        is_manifold = True
        if bm.faces and face_id <= len(bm.faces) - 1:
            face = bm.faces[face_id]
            f_vs = [mtrx @ v.co.copy() for v in face.verts]
            # assume True and then break on any false.
            for e in face.edges:
                if not e.is_manifold:
                    is_manifold = False
                    break

            face_tri_indices = return_face_tris(bm, face_id, face)

        bm.free()
        return [f_vs, is_manifold, face_tri_indices]
    return []


def return_face_tris(bm, face_id, bm_face):
    if not bm or not isinstance(bm_face, bmesh.types.BMFace):
        return []

    face_tri_indices = []
    ft_indices = [
        [loop.vert.index for loop in looptris if loop.face.index == face_id]
        for looptris in bm.calc_loop_triangles()
    ]

    for v in ft_indices:
        if v:
            idx = []
            for vv in v:
                for i, fv in enumerate(bm_face.verts):
                    if fv.index == vv:
                        idx.append(i)
            if idx:
                face_tri_indices.append(idx)

    return face_tri_indices


def return_face_tangent(obj, face_id):
    if not object_is_valid(obj) or not isinstance(face_id, int):
        return None

    ft = []
    bm = bmesh.new()
    bm.from_mesh(obj.data)
    bm.faces.ensure_lookup_table()
    # matrix = obj.matrix_world.copy().inverted()
    mtrx = obj.matrix_world.copy()
    if bm.faces and face_id <= len(bm.faces) - 1:
        face = bm.faces[face_id]
        # loop = face.loops[0]
        # ft = loop.calc_tangent()
        # ft = face.calc_tangent_edge()

        v0 = mtrx@face.edges[0].verts[0].co
        v1 = mtrx@face.edges[0].verts[1].co

        ft = v1-v0

        # ft = face.calc_tangent_edge_diagonal()
    bm.free()
    if ft:
        return ft.normalized()
    else:
        return []


def return_2d_object_edges_by_face(obj: bpy.types.Object, context: bpy.context) -> List:
    if not object_is_valid(obj):
        return []

    depsgraph = context.evaluated_depsgraph_get()
    mtrx = obj.matrix_world.copy()
    bm = bmesh.new()
    bm.from_object(obj, depsgraph)
    bm.edges.ensure_lookup_table()
    geom = []
    for e in bm.edges:
        geom.append([convert_loc_2d(mtrx @ v.co.copy(), context)
                    for v in e.verts])
    bm.free()
    return geom


# LIST OPS
def not_list(l1, l2):
    return [i for i in l1 if i not in l2]


def and_list(l1, l2):
    return [i for i in l1 if i in l2]


def val_in_list_pos(lst, val, pos):
    idxs = [x[pos] for x in lst]
    return val in idxs


def return_highest_val_item(li):
    li.sort(key=lambda x: x[1])
    return li[0][0]


def return_top_val_item(li):
    li.sort(key=lambda x: x[1])
    return li[-1][0]


def return_highest_val_list(li):
    li.sort(key=lambda x: x[1])
    return li[-1]


def return_highest_list_vals(li):
    li.sort(key=lambda x: x[1])
    if len(li) > 1:
        return [li[-1], li[-2]]
    return [li[-1]]


def return_lowest_val_list(li):
    li.sort(key=lambda x: x[1])
    return li[0]


def return_lowest_val_sorted_list(li):
    li.sort(key=lambda x: x[1])
    return li


def return_constraint_list_weighted(li, weight, sec_weight):
    for i in li:
        if i[0][1] in weight:
            return i
    for i in li:
        if i[0][1] == sec_weight:
            return i
    return li[0]


# check if l1 items are in l2
def all_items_in_list(l1, l2):
    for i in l1:
        if i not in l2:
            return False
    return True


# ***********************
# CL COLLECTION ACCESS
# ***********************
def find_collection(col_name: str):
    def traverse_tree(t):
        yield t
        for child in t.children:
            yield from traverse_tree(child)

    scene_col = bpy.context.scene.collection

    for c in traverse_tree(scene_col):
        if c.name.startswith(col_name):
            return c
    return None


def add_to_collection(obj: bpy.types.Object, collection_name=CL_COL_NAME):
    cl_col = find_collection(collection_name)

    if not cl_col:
        cl_col = bpy.data.collections.new(name=CL_COL_NAME)  # create new one
        bpy.context.scene.collection.children.link(cl_col)
        cl_col.hide_render = True

    if cl_col:
        cl_col.objects.link(obj)


def object_is_valid(obj: bpy.types.Object):
    if not obj:
        return False
    try:
        # if not hasattr(obj, "_RNA_UI"):
        # return False
        if obj.location:
            return True
    except Exception as e:
        print("object no longer exists")
        return False
    return True


def remove_obj(obj):
    if object_is_valid(obj):
        obj.select_set(True)
        bpy.ops.object.delete()


def duplicate_object(obj: bpy.types.Object, location: Vector, context:bpy.context, link_objects=False) -> bpy.types.Object:
    if object_is_valid(obj) and isinstance(location, Vector):
        dupli_obj = obj.copy()
        if obj.data:
            dupli_obj.data = obj.data.copy()
            dupli_obj.animation_data_clear()
        # context.collection.objects.link(dupli_obj)

        # add to original object collection
        obj_collection = obj.users_collection
        if obj_collection:
            add_to_collection(dupli_obj, obj_collection[0].name)

        dupli_obj.original.matrix_world.translation = location
        dupli_obj["DupliPair"] = obj.name

        if link_objects:
            bl_data_link_objects(obj, dupli_obj, context)

        return dupli_obj
    return None


def duplicate_object_with_locs(obj, locations, context, link_duplicates=False):
    dupli_objs = []
    for l in locations:
        dupli_objs.append(duplicate_object(obj, l, context, link_objects=link_duplicates))
    return dupli_objs


def bl_data_link_objects(link_to_obj: bpy.types.Object, obj: bpy.types.Object, context: bpy.context) -> None:
    if object_is_valid(obj) and object_is_valid(link_to_obj):
        obj.select_set(True)
        link_to_obj.select_set(True)
        context.view_layer.objects.active = link_to_obj
    try:
        bpy.ops.object.make_links_data(type="OBDATA")
    except Exception as e:
        print(e)
        print("Unable to link object data")


def bl_objects_data_linked(obj_1: bpy.types.Object, obj_2: bpy.types.Object) -> bool:
    """Compare bl object data blocks. Return True if match"""

    if object_is_valid(obj_1) and object_is_valid(obj_2):
        if obj_1.data == obj_2.data:
            return True
    return False


def add_temp_pivot_object(pos):
    obj = bpy.data.objects.new("TEMP_PIVOT", None)
    add_to_collection(obj)
    obj.empty_display_size = 0
    # obj.scale = (scale, scale, scale)
    obj.empty_display_type = "PLAIN_AXES"
    obj.location = pos
    return obj
