# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from __future__ import annotations
import math
import bpy  # type: ignore

from bpy.props import (  # type: ignore
    BoolProperty,
    StringProperty,
    IntProperty,
    FloatProperty,
)

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .construction_lines28 import VIEW_OT_ConstructionLines
from . import cl_utils as utl
from . import cl_drawing as drw



CL_NAME = __name__.split(".")[0]


def set_user_prefs(cl_op:VIEW_OT_ConstructionLines, context:bpy.context) -> None:
    prefs = context.preferences.addons[CL_NAME].preferences
    cl_op.default_tool = utl.poly_type_from_name(prefs.cl_default_tool)
    cl_op.auto_number_inpt = prefs.cl_auto_input
    cl_op.numpad_nav = prefs.cl_numpad_nav
    cl_op.override_unit_scale = prefs.cl_unit_override
    drw.override_dash_shader = not prefs.cl_dashed_shader

    # Snapping flags
    cl_op.snapping_flags = set_snap_flag_from_prefs(context)

    utl.CL_CP_SCALE = prefs.cl_guide_point_scale
    utl.CL_LINE_WIDTH = prefs.cl_draw_line_width
    utl.CL_HIGHLIGHT_POINT_SIZE = prefs.cl_snap_point_size
    utl.CL_SELECT_REGION_SIZE = prefs.cl_snap_radius
    utl.CL_DEFAULT_CIR_SEGS = prefs.cl_circ_segs
    utl.CL_DEFAULT_H_GUIDE_LENGTH = prefs.cl_horz_guide_len
    utl.TOOLBAR_BUTTON_SPACING = prefs.cl_icon_spacing

    # Colours
    utl.CL_DEFAULT_COL = prefs.cl_shape_col
    utl.CL_GUIDE_COL = prefs.cl_guide_col

    utl.CL_HIGHLIGHT_COL_VERT = prefs.cl_vert_snap_col
    utl.CL_HIGHLIGHT_COL_EDGE = prefs.cl_edge_snap_col
    utl.CL_HIGHLIGHT_COL_EDGE_C = prefs.cl_edge_c_snap_col
    utl.CL_HIGHLIGHT_COL_FACE = prefs.cl_face_snap_col
    utl.CL_PERP_COL = prefs.cl_perp_snap_col
    utl.CL_EXTD_COL = prefs.cl_extd_snap_col
    utl.CL_HIGHLIGHT_COL_GRID_MAJ = prefs.cl_grid_snap_col
    utl.CL_HIGHLIGHT_COL_GRID_MIN = prefs.cl_grid_sdiv_snap_col

    utl.CL_DISPLAY_BOX_COL = prefs.cl_display_box_col
    utl.CL_DISPLAY_DIM_COL = prefs.cl_display_dim_col
    utl.CL_TEXT_COL = prefs.cl_text_col
    utl.CL_TEXT_SIZE = prefs.cl_text_size
    utl.CL_OBJ_BOUNDS_COL = prefs.cl_edit_bounds_col

    utl.TOOLBAR_COL = prefs.cl_toolbar_col
    utl.TOOLBAR_COL_HL = prefs.cl_toolbar_hover_col

    # calculate snap distances
    calc_snap_distances(utl.CL_SELECT_REGION_SIZE)


def calc_snap_distances(select_region_size: int) -> None:
    # TODO: base on screen resolution
    if select_region_size <= 0:
        return

    utl.CL_BEST_DIST = select_region_size * 10
    utl.CL_BEST_E_DIST = math.ceil(math.sqrt(utl.CL_BEST_DIST))



def set_snap_flag_from_prefs(context: bpy.context) -> int:
    snp_setting = 0

    if context.scene.cl_settings.cl_snap_vert_bool:
        snp_setting = snp_setting | utl.SNP_FLAG_VERT

    if context.scene.cl_settings.cl_snap_edge_bool:
        snp_setting = snp_setting | utl.SNP_FLAG_EDGE

    if context.scene.cl_settings.cl_snap_edgec_bool:
        snp_setting = snp_setting | utl.SNP_FLAG_EDGE_C

    if context.scene.cl_settings.cl_snap_face_bool:
        snp_setting = snp_setting | utl.SNP_FLAG_FACE

    if context.scene.cl_settings.cl_snap_grid_bool:
        snp_setting = snp_setting | utl.SNP_GLB_GRID
        # only set if maj grid is set
        if context.scene.cl_settings.cl_snap_grid_sdiv_bool:
            snp_setting = snp_setting | utl.SNP_GLB_GRID_SUBD

    # if context.scene.cl_settings.cl_snap_guide_bool:
    #     snp_setting = snp_setting | utl.SNP_FLAG_GUIDES

    if context.scene.cl_settings.cl_snap_glbaxes_bool:
        snp_setting = snp_setting | utl.SNP_GLB_AXES

    if context.scene.cl_settings.cl_snap_dir_bool:
        snp_setting = snp_setting | utl.SNP_FLAG_DIR

    return snp_setting



#############################################
# PROPERTIES
############################################
class CLSettings(bpy.types.PropertyGroup):
    cl_objmode_bool: BoolProperty(
        name="", description="Object/Measure Mode", default=True
    )

    cl_selected_mode: StringProperty(
        name="", description="Current Operation", default=utl.CL_STATE_SEL
    )

    cl_snap_vert_bool: BoolProperty(name="", description="Snap To Verts", default=True)
    cl_snap_edge_bool: BoolProperty(name="", description="Snap To Edges", default=True)
    cl_snap_edgec_bool: BoolProperty(
        name="", description="Snap To Edge Centers", default=True
    )
    cl_snap_face_bool: BoolProperty(name="", description="Snap To Faces", default=True)
    cl_snap_glbaxes_bool: BoolProperty(
        name="", description="Snap To Global Axes", default=True
    )
    cl_snap_dir_bool: BoolProperty(
        name="", description="Snap To Directions (X, Y, Z, Perp, etc.)", default=True
    )
    cl_snap_grid_bool: BoolProperty(
        name="", description="Snap To Grid (Ortho Mode)", default=True
    )

    cl_snap_grid_sdiv_bool: BoolProperty(
        name="", description="Snap To Grid Sub Divs (Ortho Mode)", default=False
    )

    cl_use_bool_extrude_bool: BoolProperty(
        name="", description="Use Booleans For Extrude Cut-Through", default=True
    )

    cl_find_faces_on_edit_bool: BoolProperty(
        name="", description="Find faces when adding new geometry to a mesh", default=True
    )

    cl_display_precision_int: IntProperty(
        name="Numeric Display Precision",
        description="Precision of displayed numeric values",
        min=1,
        max=6,
        default=4,
    )

    cl_guide_display_bool: BoolProperty(
        name="", description="Display Guide Dimensions", default=True
    )

    cl_tips_display_bool: BoolProperty(
        name="", description="Display Tool Tip Box", default=True
    )

    cl_em_bounds_display_bool: BoolProperty(
        name="", description="Display Edit Mode Bounding Box", default=True
    )


    cl_running_bool: BoolProperty(
        name="PROP_CL_Running",
        description="Operator is running",
        default=False,
        options={"SKIP_SAVE"},
    )

    cl_menuact_str: StringProperty(name="", description="Menu Action", default="")

    # DEBUG
    dbg_u_scale: FloatProperty(
        name="u_scale", description="u_scale", min=0.001, max=10000.0, default=2.0
    )

    dbg_u_spacing: FloatProperty(
        name="u_spacing", description="u_spacing", min=0.001, max=100.0, default=0.2
    )

    dbg_u_line_width: FloatProperty(
        name="u_line_width", description="u_line_width", min=0.1, max=100, default=3
    )
    # /DEBUG


#############################################
# PANEL
############################################
class CL_PT_tools_options(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "CL"
    bl_label = "Construction Lines"

    def draw(self, context):
        layout = self.layout
        # layout.use_property_split = True
        layout.use_property_decorate = False

        # DEBUG
        # box = layout.box()
        # [a.tag_redraw() for a in context.screen.areas]
        # space = context.area.spaces.active
        ## region = space.region_3d
        # box.label(text="DEBUG:")
        # box.prop(region, "view_distance")
        # box.prop(context.scene.cl_settings, "dbg_u_scale", text="u_scale")
        # box.prop(context.scene.cl_settings, "dbg_u_spacing", text="dbg_u_spacing")
        # box.prop(context.scene.cl_settings, "dbg_u_line_width", text="dbg_u_line_width")
        # /DEBUG

        box = layout.box()
        box.label(text="Snap To:")
        box.prop(context.scene.cl_settings, "cl_snap_vert_bool", text="Verts")
        box.prop(context.scene.cl_settings, "cl_snap_edge_bool", text="Edges")
        box.prop(context.scene.cl_settings, "cl_snap_edgec_bool", text="Edge Centers")
        box.prop(context.scene.cl_settings, "cl_snap_face_bool", text="Faces")
        box.prop(context.scene.cl_settings, "cl_snap_glbaxes_bool", text="Global Axes")
        box.prop(context.scene.cl_settings, "cl_snap_dir_bool", text="Directions")
        # grid
        box.prop(
            context.scene.cl_settings, "cl_snap_grid_bool", text="Grid (Ortho Mode)"
        )
        sub = box.row()
        sub.prop(
            context.scene.cl_settings,
            "cl_snap_grid_sdiv_bool",
            text="Grid Sub Divs (Ortho Mode)",
        )
        sub.enabled = context.scene.cl_settings.cl_snap_grid_bool

        box = layout.box()
        box.label(text="Extrude & Mesh Edit:")
        sub = box.row()
        sub.prop(
            context.scene.cl_settings,
            "cl_use_bool_extrude_bool",
            text="Boolean Mesh Cut-Through",
        )
        sub = box.row()
        sub.prop(
            context.scene.cl_settings,
            "cl_find_faces_on_edit_bool",
            text="Find Faces On Mesh Edit",
        )

        box = layout.box()
        box.label(text="Length Unit (Quick Access):")
        box.prop(context.scene.unit_settings, "length_unit", text="")
        box.prop(context.scene.unit_settings, "use_separate", text="Separate Units")

        box = layout.box()
        box.label(text="Display:")
        box.prop(
            context.scene.cl_settings,
            "cl_guide_display_bool",
            text="Show Guide Dimensions",
        )

        box.prop(
            context.scene.cl_settings, "cl_tips_display_bool", text="Show Tips Box"
        )

        box.prop(
            context.scene.cl_settings, "cl_em_bounds_display_bool", text="Show Edit Mode Bounds Box"
        )

        box.prop(
            context.scene.cl_settings,
            "cl_display_precision_int",
            text="Length Precision:",
        )


        box = layout.box()
        box.label(text="Shortcuts (Default):")
        box.label(text="S = Select")
        box.label(text="T = Tape")
        box.label(text="L = Line")
        box.label(text="R = Rectangle")
        box.label(text="C = Circle")
        box.label(text="U = Arc")
        box.label(text="O = Rotate")
        box.label(text="G = Move")
        box.label(text="E = Extrude")
        box.label(text="X = Delete")
        box.label(text="F = Face From Selection")
        box.label(text="Esc = Cancel Tool")
        box.label(text="Esc + Shift = Exit")
