# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

import os

import bpy # type: ignore

# import blf
from bpy.props import StringProperty # type: ignore
from bpy.types import GizmoGroup # type: ignore

from . import cl_utils as utl
from . import cl_drawing as cld
from . import construction_lines28 as cl

ICONS = "CL_VIS CL_SCL CL_DEL CL_CVT".split(
    " "
)


icon_collection = {}
CL_ACTION = "wm.cl_menu_action"


class VIEW_OT_CLMenuAction(bpy.types.Operator):
    """Construction Lines"""

    bl_idname = "wm.cl_menu_action"
    bl_label = "CL Menu Action"
    bl_options = {"UNDO"}
    act_val: StringProperty()

    @classmethod
    def description(cls, context, properties):
        return utl.action_desc_from_type(properties.act_val)

    def execute(self, context):
        bpy.context.scene.cl_settings.cl_menuact_str = self.act_val
        return {"FINISHED"}
        


class CL_CTX_Menu(bpy.types.Menu):
    bl_label = "Construction Lines"
    bl_idname = "OBJECT_MT_cl_menu"

    def draw(self, context):
        prev_coll = icon_collection["main"]

        def get_icon(name):
            return prev_coll[name].icon_id

        layout = self.layout
        layout.operator(
            CL_ACTION, text="Convert To Geometry   K", icon_value=get_icon("CL_CVT")
        ).act_val = utl.ACTION_CONVERT_GUIDE
        layout.operator(
            CL_ACTION, text="Hide/Show All      H", icon_value=get_icon("CL_VIS")
        ).act_val = utl.ACTION_HIDESHOW
        layout.operator(
            CL_ACTION, text="Scale All             S", icon_value=get_icon("CL_SCL")
        ).act_val = utl.ACTION_SCALE_CLS
        layout.operator(
            CL_ACTION, text="Remove All         DEL", icon_value=get_icon("CL_DEL")
        ).act_val = utl.ACTION_REMOVEALL
        layout.separator()
        layout.operator(
            CL_ACTION, text="Exit             Shift+ESC", icon="X"
        ).act_val = utl.ACTION_EXITCANCEL

def draw_item(self, context):
    layout = self.layout
    layout.menu(CL_CTX_Menu.bl_idname)


# BUTTON OVERLAY GIZMO
class ToolbarOverlay(GizmoGroup):
    bl_idname = "CONSTRUCTION_LINES_GGT_2dgizmo_light"
    bl_label = "Construction Lines Toolbar"
    bl_space_type = "VIEW_3D"
    bl_region_type = "WINDOW"
    bl_options = {"PERSISTENT", "SCALE", "SHOW_MODAL_ALL", "SELECT"}

    @classmethod
    def poll(cls, context):
        return cl.VIEW_OT_ConstructionLines.running

    def draw_prepare(self, context):
        i = 2
        width = context.region.width / 5
        icon_width = utl.TOOLBAR_BUTTON_SPACING
        height = (
            utl.TOOLBAR_HEIGHT_OFFSET
        )  # context.region.height - utl.TOOLBAR_HEIGHT_OFFSET

        for btn in self.buttons:
            btn[0].hide = False
            btn[0].matrix_basis[0][3] = width + icon_width * i
            btn[0].matrix_basis[1][3] = height
            btn[0].use_draw_modal = True
            cld.draw_highlight_gizmo(btn[0], btn[1], width + icon_width * i, height)
            i += 1

    def setup(self, context):
        scale_basis = (80 * 0.35) / 2

        # SELECT
        self.select_button_widget = self.create_2d_gizmo(
            "RESTRICT_SELECT_OFF", scale_basis, CL_ACTION, utl.ACTION_SELECT
        )
        # MEASURE
        self.tape_button_widget = self.create_2d_gizmo(
            "DRIVER_DISTANCE", scale_basis, CL_ACTION, utl.ACTION_DRAWTAPE
        )
        # LINE
        self.line_button_widget = self.create_2d_gizmo(
            "OUTLINER_DATA_GP_LAYER", scale_basis, CL_ACTION, utl.ACTION_DRAWLINE
        )
        # RECT
        self.rect_button_widget = self.create_2d_gizmo(
            "MESH_PLANE", scale_basis, CL_ACTION, utl.ACTION_DRAWRECT
        )
        # CIRCLE
        self.circle_button_widget = self.create_2d_gizmo(
            "MESH_CIRCLE", scale_basis, CL_ACTION, utl.ACTION_DRAWCIRCLE
        )
        # ARC
        self.arc_button_widget = self.create_2d_gizmo(
            "MOD_CURVE", scale_basis, CL_ACTION, utl.ACTION_DRAWARC
        )
        # MOVE
        self.move_button_widget = self.create_2d_gizmo(
            "EMPTY_ARROWS", scale_basis, CL_ACTION, utl.ACTION_MOVE
        )
        # ROTATE
        self.rotate_button_widget = self.create_2d_gizmo(
            "FILE_REFRESH", scale_basis, CL_ACTION, utl.ACTION_ROTATE
        )

        # ROTATE
        self.extrude_button_widget = self.create_2d_gizmo(
            "UV_FACESEL", scale_basis, CL_ACTION, utl.ACTION_EXTRUDE
        )

        # EXIT
        self.exit_button_widget = self.create_2d_gizmo(
            "PANEL_CLOSE", scale_basis, CL_ACTION, utl.ACTION_EXITCANCEL
        )

        self.buttons = [
            [self.select_button_widget, "Select (S)"],
            [self.tape_button_widget, "Measure\Guide (T)"],
            [self.line_button_widget, "Draw Line (L)"],
            [self.rect_button_widget, "Draw Rectangle (R)"],
            [self.circle_button_widget, "Draw Circle (C)"],
            [self.arc_button_widget, "Draw Arc (U)"],
            [self.move_button_widget, "Move (G)"],
            [self.rotate_button_widget, "Rotate (O)"],
            [self.extrude_button_widget, "Extrude (E)"],
            [self.exit_button_widget, "Exit (Shift+Esc)"],
        ]

    def refresh(self, context):
        widget = self.widget_from_state(context.scene.cl_settings.cl_selected_mode)
        for btn in self.buttons:
            btn[0].color = utl.TOOLBAR_COL
        if widget:
            widget.color = utl.TOOLBAR_COL_ENBL

    def widget_from_state(self, state):
        if state == utl.CL_STATE_SEL:
            return self.select_button_widget
        elif state == utl.CL_STATE_MOV:
            return self.move_button_widget
        elif state == utl.CL_STATE_ROT:
            return self.rotate_button_widget
        elif state == utl.CL_STATE_EXT:
            return self.extrude_button_widget
        elif state == utl.CL_STATE_POLY_LINE:
            return self.line_button_widget
        elif state == utl.CL_STATE_POLY_TAPE:
            return self.tape_button_widget
        elif state == utl.CL_STATE_POLY_RECT:
            return self.rect_button_widget
        elif state == utl.CL_STATE_POLY_CIRC:
            return self.circle_button_widget
        elif state == utl.CL_STATE_POLY_ARC:
            return self.arc_button_widget
        return None

    def create_2d_gizmo(self, icon_name, scale_basis, operator, action):
        alpha = 0.5
        alpha_highlight = 1.0
        color = utl.TOOLBAR_COL
        color_highlight = utl.TOOLBAR_COL_HL

        gizmo = self.gizmos.new("GIZMO_GT_button_2d")
        op = gizmo.target_set_operator(operator)
        op.act_val = action
        gizmo.icon = icon_name
        gizmo.show_drag = False
        gizmo.draw_options = {"BACKDROP", "OUTLINE", "HELPLINE"}
        gizmo.alpha = alpha
        gizmo.color = color
        gizmo.color_highlight = color_highlight
        gizmo.alpha_highlight = alpha_highlight
        gizmo.scale_basis = scale_basis
        gizmo.use_tooltip = False
        return gizmo


def register_icons():
    import bpy.utils.previews  # type: ignore

    prev_coll = bpy.utils.previews.new()
    icons_dir = os.path.join(os.path.dirname(__file__), "icons")
    for icon_name in ICONS:
        prev_coll.load(icon_name, os.path.join(icons_dir, icon_name + ".png"), "IMAGE")
    icon_collection["main"] = prev_coll


def unregister_icons():
    for prev_coll in icon_collection.values():
        bpy.utils.previews.remove(prev_coll)
    icon_collection.clear()
