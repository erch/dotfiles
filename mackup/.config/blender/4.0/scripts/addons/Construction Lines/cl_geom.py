# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from dataclasses import dataclass
import math
import numpy as np
from math import copysign, degrees, sqrt
from typing import List

import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector, Matrix  # type: ignore
from mathutils.geometry import intersect_line_line as LineIntersect  # type: ignore
from mathutils.geometry import (  # type: ignore
    intersect_point_line as PointLineIntersect,
)
from mathutils.geometry import (  # type: ignore
    intersect_line_plane as LinePlaneIntersect,
)

from mathutils.geometry import distance_point_to_plane  # type: ignore

from bpy_extras.view3d_utils import (  # type: ignore
    location_3d_to_region_2d,
)

from . import cl_utils as utl


@dataclass
class IgnoreGeom:
    __slots__ = ["obj_id", "geom_type", "geom_index"]
    obj_id: str
    geom_type: str
    geom_index: int


class ClosestVert:
    def __init__(
        self,
        vert,
        edge,
        face,
        geom_type,
        connected_edges,
        connected_faces,
        face_verts=None,
        face_manifold=True,
        face_norm=None,
        face_tri_indices=None,
        obj=None,
        geom_id=None,
    ):
        self.vert = vert
        self.edge = edge
        self.face = face  # face num
        self.geom_type = geom_type
        self.connected_edges = connected_edges
        self.connected_faces = connected_faces
        self.face_verts = face_verts
        self.face_manifold = face_manifold
        self.face_norm = face_norm
        self.face_tri_indices = face_tri_indices
        self.obj = obj
        self.geom_id = geom_id


class SnapGeom:
    __slots__ = (
        "w_loc",
        "geom_type",
        "connected_edges",
        "connected_faces",
        "loc2d",
        "edge",
        "connect_obj",
        "geom_id",
    )

    def __init__(
        self,
        w_loc,
        geom_type,
        connected_edges,
        connected_faces,
        loc2d,
        edge=None,
        connect_obj=None,
        geom_id=None,
    ):
        self.w_loc = w_loc
        self.geom_type = geom_type
        self.edge = edge
        self.geom_id = geom_id
        self.connected_edges = connected_edges
        self.connected_faces = connected_faces
        self.loc2d = loc2d
        # Start object if guide
        self.connect_obj = connect_obj


#############################################
def find_edge_center(pt1, pt2):
    mx = (pt1.x + pt2.x) / 2
    my = (pt1.y + pt2.y) / 2
    mz = (pt1.z + pt2.z) / 2
    v = Vector((mx, my, mz))
    return v


# return closest with edge type
def point_on_edge_type(pt, edges):
    best_dist = utl.CL_BEST_E_DIST
    closest = []

    for e in edges:
        a, b = e[0], e[1]
        x, y = PointLineIntersect(pt, a, b)

        if y > 1:
            continue
        dist = (pt - x).length

        if dist < best_dist:
            best_dist = dist
            closest = [x, utl.TYPE_EDGE_C]
    return closest


# returns whether point lies on edge
def point_on_edge(pt: Vector, edge: Vector) -> bool:
    r_edge = edge
    p, _percent = PointLineIntersect(pt, *r_edge)
    on_line = (pt - p).length < 1.0e-5
    return on_line and (0.0 <= _percent <= 1.001)


def percent_point_on_edge(pt, edge):
    _, _percent = PointLineIntersect(pt, *edge)
    # on_line = (pt - p).length < 1.0e-5
    return _percent


def point_point_on_edge(pt, edge):
    p, _percent = PointLineIntersect(pt, *edge)
    # on_line = (pt - p).length < 1.0e-5
    return p, _percent


def find_edge_edge_intersection(edges):
    p1, p2 = [v.co for v in edges[0].verts]
    p3, p4 = [v.co for v in edges[1].verts]
    l_is = LineIntersect(p1, p2, p3, p4)
    if not l_is:
        return None
    return (l_is[0] + l_is[1]) / 2


# check edge edge interssction from vertex coords
def find_edge_edge_intersection_verts(verts):
    l_is = LineIntersect(verts[0], verts[1], verts[2], verts[3])

    if not l_is:
        return None

    return (l_is[0] + l_is[1]) / 2


def check_intersect_possible(edges):
    if get_edges_share_verts(edges):
        return False
    return True


def intersect_mouse_loc_plane_from_view(
    mouse_ray_origin, mouse_ray_target, plane_pt, plane_norm
):
    return LinePlaneIntersect(mouse_ray_origin, mouse_ray_target, plane_pt, plane_norm)


def circle_line_intersect(circ_center, radius, line_pt1, line_pt2):
    if line_pt1 == line_pt2:
        return False, None, None
    Q = circ_center
    r = radius
    P1 = line_pt1
    V = line_pt2 - line_pt1

    # calc coefficients
    a = V.dot(V)
    b = 2 * V.dot(P1 - Q)
    c = P1.dot(P1) + Q.dot(Q) - 2 * P1.dot(Q) - r ** 2

    # check intersections
    disc = b ** 2 - 4 * a * c
    if disc < 0:
        return False, None, None

    sqrt_disc = sqrt(disc)
    t1 = (-b + sqrt_disc) / (2 * a)
    t2 = (-b - sqrt_disc) / (2 * a)

    if not (0 <= t1 <= 1 or 0 <= t2 <= 1):
        return False, None, None

    t = max(0, min(1, -b / (2 * a)))
    # return True, P1 + t * V
    return True, t, P1 + t * V


def get_edges_share_verts(edges):
    e = edges[0]
    for v1 in e.verts:
        for v2 in edges[1].verts:
            if v1.co == v2.co:
                return True
    return False


def point_dist_to_plane(pts, plane):
    x1 = pts[0]
    y1 = pts[1]
    z1 = pts[2]
    a = plane[0]
    b = plane[1]
    c = plane[2]
    d = plane[3]

    d = abs((a * x1 + b * y1 + c * z1 + d))
    e = math.sqrt(a * a + b * b + c * c)
    # Perpendicular dist
    return d / e


def project_vecs_onto_face(face: bmesh.types.BMFace, vecs: List[Vector], direction: Vector) -> List[Vector]:
    """Take a list of vectors and project along a given direction
    until the vectors interesect the face. 
    Return list of projected vectors
    """
    f_norm = face.normal
    f_co = face.verts[0].co
    prj_vecs = []

    if is_close(f_norm, direction):
        direction *= -1

    for v in vecs:
        new_v = v + direction * distance_point_to_plane(v, f_co, f_norm)
        prj_vecs.append(new_v)

    return prj_vecs


def faces_planar(face1: bmesh.types.BMFace, face2: bmesh.types.BMFace) -> bool:
    """check if two faces are planar"""

    v1 = face1.verts[0].co
    other_v1 = face2.verts[0].co

    # Calculate the normal vectors of both faces
    normal1 = face1.normal
    normal2 = face2.normal

    # Check if the normals are parallel
    if abs(normal1.dot(normal2)) == 1:
        # Check if the first vertices of both faces lie on the same plane
        if abs(normal1.dot(other_v1 - v1)) < 1e-5:
            return True
    return False


def build_bm_face_from_vecs(bm: bmesh.types.BMesh, vecs: List[Vector], i_mtrx: Matrix) -> bmesh.types.BMFace:
    if not bm:
        return

    new_verts = []
    for v in vecs:
        new_verts.append(bm.verts.new(i_mtrx @ v))
    utl.update_bm_verts(bm)

    new_face = bm.faces.new(new_verts)
    utl.update_bm_faces(bm)
    return new_face


# requires vector3, vector3
def return_direction_vector(v1, v2):
    dx = v2[0] - v1[0]
    dy = v2[1] - v1[1]
    dz = v2[2] - v1[2]
    return Vector([dx, dy, dz])


def return_abs_direction_vector(v1, v2):
    dx = abs(v2[0] - v1[0])
    dy = abs(v2[1] - v1[1])
    dz = abs(v2[2] - v1[2])
    return Vector([dx, dy, dz])


# check if face edges are completely inside an existing face
# if same edge hits diferent edges of same face then must be inside
def new_all_points_on_face(ex_face, chk_face):
    # ce = check edge
    # ee = existing edge
    ixs = []
    ex_face_verts = [v.index for v in ex_face.verts]
    # keeps count of how many edges are discarded
    # because they are already in existing face
    discard = 0
    for ce in chk_face.edges:
        if not (
            ce.verts[0].index in ex_face_verts and ce.verts[1].index in ex_face_verts
        ):
            for ee in ex_face.edges:
                co = find_edge_edge_intersection([ce, ee])
                if co:
                    # if 0 < percent_point_on_edge(co, [ee.verts[0].co, ee.verts[1].co]) < 1:
                    if point_on_edge(co, [ee.verts[0].co, ee.verts[1].co]):
                        # dir of ce to co
                        ix_dir = co - ce.verts[0].co
                        ix_dir.normalize()
                        # ix_dir = ix_dir

                        # make sure the same edge dosen't intersect muliplte times
                        # at a a corner
                        no_match = True
                        for ix in ixs:
                            if ce.index == ix[0] and co == ix[4]:
                                no_match = False
                        if no_match:
                            # [check edge index, existing face index, confirmed, ix_co]
                            ixs.append([ce.index, ee.index, ix_dir, False, co])
        else:
            discard += 1
    result = []

    if not ixs:
        return False

    # Outside of face if number of hits in the same direction are even
    # will fail if any edge is outside
    for ie in ixs:
        count = 1
        for je in ixs:
            if ie != je:
                if ie[0] == je[0] and ie[2] == je[2]:
                    count += 1
        if count % 2 == 0:
            # even number of hits must be outside
            return False

    # check edge has two different intersections
    # in same face
    for ix in ixs:
        for iix in ixs:
            if not iix[3]:
                if ix != iix:
                    if ix[0] == iix[0] and ix[1] != iix[1]:
                        ix[3] = True
                        iix[3] = True
                        if ix[0] not in result:
                            result.append(ix[0])
    # check if all edges match
    return len(result) == len(chk_face.edges) - discard


def point_in_bounds(p, x_max, x_min, y_max, y_min, z_max, z_min):
    x = abs(p.x)
    y = abs(p.y)
    z = abs(p.z)
    return (
        x <= x_max
        and x >= x_min
        and y <= y_max
        and y >= y_min
        and z <= z_max
        and z >= z_min
    )


def point_in_bounds_2d(p, x, y, w, h):
    if p:
        px = p[0]
        py = p[1]
        return px >= x and px <= x + w and py <= y and py >= y - h
    return False


def point_in_screen_bounds_2d(p, x, y, w, h):
    if p:
        px = p[0]
        py = p[1]
        return px >= x and px <= x + w and py >= y and py <= y + h
    return False


def point_in_region(p, region_name, context):
    r_exists = False
    bw, bh, bx, by = 0, 0, 0, 0
    for r in context.area.regions:
        if r.type == region_name:
            bw, bh, bx, by = r.width, r.height, r.x, r.y
            r_exists = True
    if r_exists:
        return point_in_screen_bounds_2d(p, bx, by, bw, bh)
    return False


def click_in_view_3d(mouse_loc, context, ignore_vp_ui=False):
    if not mouse_loc or not context:
        return False
    viewport, ui_width = get_3d_area_region()

    if not viewport:
        return False

    nav_gizmo_width = 0
    if context.space_data.show_gizmo_navigate:
        nav_gizmo_width = utl.NAV_GIZMO_WIDTH

    # Ignore the viewport UI and Gizmos
    if ignore_vp_ui:
        return (
            0 < mouse_loc[0] < viewport.width and 0 < mouse_loc[1] < viewport.height
        )

    # TODO: sizes need to be resolution based***
    return (
        0 < mouse_loc[0] < viewport.width - nav_gizmo_width - ui_width
        and utl.TOOLBAR_HEIGHT_OFFSET + 20 < mouse_loc[1] < viewport.height
    )  # - utl.TOOLBAR_HEIGHT_OFFSET - 20


def get_3d_area_region():
    window_region = None
    ui_region_width = 0
    for window in bpy.context.window_manager.windows:
        for area in window.screen.areas:
            if area.type == "VIEW_3D":
                for region in area.regions:
                    if region.type == "UI" and region.width > 10:
                        ui_region_width = region.width
                    if region.type == "WINDOW":
                        window_region = region

    return window_region, ui_region_width


def is_face_normal_to_view(context, obj, face):
    rv3d = context.space_data.region_3d
    view = Vector(rv3d.view_matrix[2][:3])
    view.length = rv3d.view_distance
    view_location = rv3d.view_location + view

    face_normal = obj.matrix_world.to_3x3() @ face.normal
    world_coord = obj.matrix_world @ face.verts[1].co
    dot_value = face_normal.dot(view_location - world_coord)
    return not dot_value < 0.0


# correct pt position on edge based on edge angle to view vector
def return_view_corrected_pt(pt, edge, view_vector):
    n_edge = Vector((edge[1] - edge[0])).normalized()
    t = n_edge.angle(view_vector)
    normDeg = 90 - degrees(t) % 360
    map_dir = utl.map_range_2(0, 360, 0.01, 0.1, abs(normDeg))
    def sign(x): return copysign(1, x)
    if sign(normDeg) > 0:
        map_dir -= 0.09
    new_pt = max(min(pt + map_dir, 1), 0)
    return new_pt


# plot guide points based on num divsions
# takes [start, end]
# i = len(divs) n = divs-i
# (n/divs) * x1 + (i/divs) + x2
def plot_div_pts(pts, divs):
    if divs <= 1:
        return []

    new_pts = []
    pt1 = pts[0]
    pt2 = pts[1]

    for i in range(1, divs):
        n = divs - i
        dx = (n / divs) * pt1.x + i / divs * pt2.x
        dy = (n / divs) * pt1.y + i / divs * pt2.y
        dz = (n / divs) * pt1.z + i / divs * pt2.z
        new_pts.append(Vector((dx, dy, dz)))
    return new_pts


# plot forward from points
# takes [first, last] = first, last geom/object pos
# returns a list of positions forward from [first, last] direction
# calculates distance based on distance between first and last
def plot_duplicates_times(pts, count):
    if not (isinstance(pts, list) and all(isinstance(x, Vector) for x in pts)):
        return []

    extd_points = []

    if count < 0:
        extd_points = plot_div_pts(pts, abs(count))
    elif count > 0:
        dist = calc_len(pts)[0]
        direction = (pts[1] - pts[0]) * -1
        direction.normalize()
        # already duplicated once
        for i in range(1, count):
            extd_points.append(pts[1] + (direction * dist * (i + 1)))
    return extd_points


def plot_horiz_guide(pts, start_edge):
    half_len = utl.CL_DEFAULT_H_GUIDE_LENGTH / 2
    e_dir = calc_dir_vect(start_edge)
    return [pts[1] - (e_dir * half_len), pts[1] + (e_dir * half_len)]


# takes drag points and returns rect verts
def plot_rect(pts, constraint, soft_constraint, face=None) -> List:
    #     b
    #  d /
    #   /t
    #  a------c

    if not pts:
        return pts

    if calc_len(pts)[0] == 0:
        return pts

    if constraint or face:
        # travel direction default
        td = Vector((1, 0, 0))

        # handle basic axis constraints
        if constraint:
            if constraint[1] == "X":
                pts[1][1] = pts[0][1]
                td = Vector((0, 0, -1))
            elif constraint[1] == "Y":
                pts[1][0] = pts[0][0]
                td = Vector((0, 0, -1))
            elif constraint[1] == "Z":
                pts[1][2] = pts[0][2]
        else:
            if face and face.geom_type != utl.TYPE_PLANE:
                ft = utl.return_face_tangent(face.obj.get_obj(), face.face)
                if ft:
                    mtrx = face.obj.get_obj().matrix_world.copy()
                    # No need to use location
                    _, rot, sca = mtrx.decompose()
                    rs_mtrx = Matrix.LocRotScale(None, rot, sca)
                    face_norm = rs_mtrx@face.face_norm
                    td = face_norm.cross(ft)
                    # print(td.normalized())
        # if constraint and constraint[1] != "Z":
        #     td = constraint[0]
        #     if constraint[1] == "X":
        #         td = Vector((0, 0,-1))
        a = Vector(pts[0].copy())
        b = Vector(pts[1].copy())
        td_prj = (b - a).project(td)
        # c & d = a & b projections on direction of constraint/face
        c = a + td_prj
        d = b - td_prj
        verts = [pts[0].copy(), c, pts[1].copy(), d]
    else:
        verts = plot_rect_pts_3d(pts[0], pts[1])
    return verts


def plot_rect_dims(w, h):
    # counter-clockwise
    verts = []
    verts.append(Vector((0, 0, 0)))
    verts.append(Vector((w, 0, 0)))
    verts.append(Vector((w, h, 0)))
    verts.append(Vector((0, h, 0)))
    return verts


def plot_rect_pts_3d(pt1, pt2):
    dx = pt1[0] - pt2[0]
    dy = pt1[1] - pt2[1]
    dz = pt1[2] - pt2[2]

    a = [pt1[0], pt1[1], pt1[2]]
    b = [pt1[0], pt1[1], pt1[2]]
    c = [pt2[0], pt2[1], pt2[2]]
    d = [pt2[0], pt2[1], pt2[2]]

    if all(math.isclose(val, 0, abs_tol=1e-5) for val in (dx, dy, dz)):
        return []

    if math.isclose(dx, 0, abs_tol=1e-5):  # no change on x
        b = [pt1[0], pt2[1], pt1[2]]
        d = [pt1[0], pt1[1], pt2[2]]
    elif math.isclose(dy, 0, abs_tol=1e-5):  # no change on y
        b = [pt2[0], pt1[1], pt1[2]]
        d = [pt1[0], pt1[1], pt2[2]]
    elif math.isclose(dz, 0, abs_tol=1e-5):  # no change on z
        b = [pt1[0], pt2[1], pt1[2]]
        d = [pt2[0], pt1[1], pt1[2]]
    return [Vector((a)), Vector((b)), Vector((c)), Vector((d))]


def convert_amnt_by_direction_vec(dir_verts, constraint, w, h):
    dx = dir_verts[1][0] - dir_verts[0][0]
    dy = dir_verts[1][1] - dir_verts[0][1]
    dz = dir_verts[1][2] - dir_verts[0][2]

    # Default x,y
    if dx < 0:
        w = w * -1
    if dy < 0:
        h = h * -1

    if constraint:
        if constraint[1] == "X":  # x,z
            if dx < 0:
                w = w * -1
            if dz < 0:
                h = h * -1
        elif constraint[1] == "Y":  # z,-y
            if dy < 0:
                h = h * -1
            if dz > 0:
                w = w * -1
    return w, h


# takes radius and num segments and verts circle verts
def plot_circle(rad, segs, constraint, soft_constraint, pts, face=None):
    theta = 2 * math.pi / segs
    dx = 0
    dy = 0
    dz = 0
    verts = []
    for i in range(segs):
        dx = rad * math.sin(theta * i)
        dy = rad * math.cos(theta * i)
        verts.append(Vector((dx, dy, dz)))
    # Translate and rotate
    if face and not constraint:
        verts = rotate_verts_to_face(verts.copy(), face)
        translate_pos(pts[0], verts)

    else:
        verts = rotate_translate(
            constraint, soft_constraint, pts[0], verts, True)
    return verts


def rotate_verts_to_face(verts, target_face):
    # Allow for face to be a working plane and not an object
    verts_norm = Vector((0, 0, 1))
    mat_world = Matrix.Identity(4)
    target_norm = target_face.face_norm
    n_verts = []
    if target_face.obj:
        target = target_face.obj.get_obj()
        mat_world = target.matrix_world
        mat_world_in = target.matrix_world.inverted()
        # rotate face normal to world rotation
        _, rot, _ = mat_world.decompose()
        target_norm = rot.to_matrix() @ target_face.face_norm

        # transform face verts to target object local space
        for v in verts:
            n_verts.append(mat_world_in @ v.copy())
    else:
        n_verts = verts.copy()

    # Get the rotation difference
    # Build the matrix and rotate around face.center in world space
    mat = verts_norm.rotation_difference(target_norm).to_matrix().to_4x4()

    return [mat @ mat_world @ v for v in n_verts]


def find_closest_axis(start_v, loc, axis_list, tolerance):
    if isinstance(start_v, Vector):
        v1 = start_v.copy()
        l = loc.copy()
        dist = calc_len([v1, l])[0]
        v_dir = l - v1
        v_dir.normalize()
        dir_amnts = []

        for a in axis_list:
            dir_amnt = a[0].dot(v_dir)
            if dir_amnt > tolerance:
                dir_amnts.append([a, dir_amnt])

        # find closest value
        if dir_amnts:
            closest_axis_list = utl.return_highest_list_vals(dir_amnts)
            if closest_axis_list:
                # prioritise XYZ over other constraints (Perp, Parallel, etc.)
                closest_axis = utl.return_constraint_list_weighted(
                    closest_axis_list, {"X", "Y", "Z"}, "PERP"
                )
                if closest_axis and closest_axis[1] > tolerance:
                    # constrain to closest axis vector
                    newl = l.copy()
                    utl.mask_by_constraint(newl, dist, closest_axis[0][0])
                    for i, _ in enumerate(newl):
                        l[i] = v1[i] + newl[i]
                    return l, closest_axis[0]
    return [], []


def check_for_perp(v1, loc, edge, axis):

    #       F   D
    #       |  /
    #       | /
    #       |/
    # A-----C---E-------B

    l = loc.copy()
    if axis:
        # project to plane
        norm = get_edge_axis_normal(edge, axis[0])
        l = project_to_axis(l, v1, norm)

    A = edge[0].copy()
    B = edge[1].copy()
    C = v1.copy()
    D = l
    pt = D
    dp = pt - A
    AB = B - A
    dl = AB.length
    if dl != 0:
        t = AB.dot(dp) / (dl * dl)
        if 1 >= t >= 0:
            E = A + t * AB
            CE = E - C
            F = D - (CE.normalized() * CE.length)
            val = (F - C).normalized()
            nval = Vector(
                (round(val[0], 4), round(val[1], 4), round(val[2], 4)))
            if nval != Vector((0, 0, 0)):
                return [nval, "PERP"]
    return []


# returns adjusted perpendicular direction from current edge in
# current drag direction - no requirement for axis plane
def return_perp_vector_from_edge(v1, loc, edge):
    l = loc.copy()
    A = edge[0].copy()
    B = edge[1].copy()
    C = v1.copy()
    D = l
    pt = D
    dp = pt - A
    AB = B - A
    dl = AB.length
    if dl != 0:
        t = AB.dot(dp) / (dl * dl)
        E = A + t * AB
        CE = E - C
        F = D - (CE.normalized() * CE.length)
        val = F
        nval = Vector((round(val[0], 4), round(val[1], 4), round(val[2], 4)))
        if nval != Vector((0, 0, 0)):
            return nval
    return []


def check_for_extend(edge):
    return [
        [(edge[1] - edge[0]).normalized(), "EXTD"],
        [(edge[0] - edge[1]).normalized(), "EXTD"],
    ]


def project_to_plane(val, plane):
    val[2] = plane[2]


def get_edge_axis_normal(e1, axis):
    dir1 = Vector((e1[1] - e1[0]))
    return dir1.cross(axis)


def project_to_axis(pt, origin, project_axis):
    o, pa = origin, project_axis
    a = pt - o
    t = pa.dot(a)
    return o + t * pa


def angle_triangle_3d(x1, x2, x3, y1, y2, y3, z1, z2, z3):
    num = (x2 - x1) * (x3 - x1) + (y2 - y1) * (y3 - y1) + (z2 - z1) * (z3 - z1)
    den = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2) * math.sqrt(
        (x3 - x1) ** 2 + (y3 - y1) ** 2 + (z3 - z1) ** 2
    )
    angle = math.degrees(math.acos(num / den))
    return round(angle, 3)


def plot_arc_2(pts, segs):
    st = pts[0].copy()
    md = pts[2].copy()
    ed = pts[1].copy()
    cm = find_edge_center(st, ed)

    # # bulge length
    h = -calc_len([cm, md])[0]

    if h == 0:
        return []

    # Build Arc
    w = abs(calc_len([ed, st])[0])
    m = [w / 2, 0]
    verts = plot_2_point_arc(m, w, h, segs)

    # set heading
    target_heading = cm.copy() - md.copy()
    target_heading.normalize()

    d = (ed.copy() - st.copy()).normalized()
    target_norm = d.cross(target_heading)

    if Vector(target_heading).magnitude > 0:
        verts = rotate_points_towards(verts, target_heading, target_norm, d)

    translate_pos(st, verts)
    return verts


# move verts into new target coordinates system to rotate
def rotate_points_towards(verts: List[Vector], target_heading: Vector, target_norm: Vector, d: Vector) -> List[Vector]:
    x = d
    y = target_heading
    z = target_norm

    # build matrix for new coordinates system
    m = Matrix(
        (
            (x[0], y[0], z[0], 0),
            (x[1], y[1], z[1], 0),
            (x[2], y[2], z[2], 0),
            (0, 0, 0, 1),
        )
    )
    m.normalize()
    # return verts in new coordinates system
    return [m @ v for v in verts]


def plot_2_point_arc(m, w, h, segs):
    neg = False

    if h < 0:
        neg = True

    if h == 0:
        return [Vector([0, 0, 0]), Vector([w / 2, 0, 0]), Vector([w, 0, 0])]

    h = abs(h)
    # pts
    pt1 = [m[0] - w / 2, 0]  # chord start
    pt2 = [m[0] + w / 2, 0]  # chord end
    pt3 = [m[0], m[1] + h]

    # calc dimensions (mid pt between pt1 & pt2,
    # chord, height, radius, dist from chord to circle center)
    c = calc_len_2D([pt1, pt2])[0]  # pt2[0] - pt1[0]
    r = (h / 2) + ((c * c) / (8 * h))
    d = r - h
    if h > c / 2:
        d = r - (2 * r - h)
    # circle center
    mx = pt3[0]
    my = pt3[1] - r

    # start and end angles
    et = math.atan2(pt2[1] - my, pt2[0] - mx)
    # 180 degrees from et
    st = math.pi - et

    if neg:
        et -= math.pi
        st -= math.pi

    step = (st - et) / segs
    # start forward 1 step as appending start point vector
    i = st - step
    j = 360
    verts = []

    if not neg:
        verts.append(Vector([pt1[0], pt1[1], 0]))
    else:
        verts.append(Vector([pt2[0], pt2[1], 0]))

    dx = 0.0
    dy = 0.0
    # -theta to theta
    while i > et:
        dx = round(mx + math.cos(i) * r, 4)
        dy = round(my + math.sin(i) * r, 4)
        if neg:
            # fixes issues when bulge gets shallower
            if h < w / 2:
                dy += d * 2
            else:
                dy -= d * 2
        verts.append(Vector([dx, dy, 0]))
        i -= step

        # failsafe
        if j <= 0:
            break
        j -= 1

    if dx != pt2[0] and dy != pt2[1] and not neg:
        verts.append(Vector([pt2[0], pt2[1], 0]))
    elif dx != pt1[0] and dy != pt1[1] and neg:
        verts.append(Vector([pt1[0], pt1[1], 0]))

    if neg:
        verts.reverse()
    return verts


def vector_angle(pts):
    p1 = Vector(pts[0])
    p2 = Vector(pts[1])

    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    dz = p2[2] - p1[2]

    tz = math.atan2(dy, dx)
    ty = 0.0
    tx = 0.0
    # ty = math.atan2(dx, dz)
    tx = math.atan2(dz, dx)
    return [tx, ty, tz]


def angle_between_vects(v1, v2):
    a = v1.normalized()
    b = v2.normalized()

    n = a.dot(b)  # a[0]*b[0] + a[1]*b[1] + a[2]*b[2]
    d = math.sqrt(a[0] ** 2 + a[1] ** 2 + a[2] ** 2) * math.sqrt(
        b[0] ** 2 + b[1] ** 2 + b[2] ** 2
    )

    cos_t = 0
    if n != 0 and d != 0:
        cos_t = n / d
    return cos_t


def translate_pos(v1, mtrx):
    for v in mtrx:
        v[0] += v1[0]
        v[1] += v1[1]
        v[2] += v1[2]


def return_valid_lists(lists):
    r_l = []
    for l in lists:
        if l:
            r_l.append(l)
    return r_l


# translate and rotate points - only when axis locked
# or if geom not built at origin
def rotate_translate(constraint, soft_constraint, v1, verts, override_trans=False):
    if not verts:
        return verts
    c = [l[1] for l in [constraint, soft_constraint] if l]
    if "X" in c:
        rotate_x_mtrx(verts)
    elif "Y" in c:
        rotate_y_mtrx(verts)

    if v1:
        if override_trans or verts[0] == [0, 0, 0]:
            translate_pos(v1, verts)
    return verts


def rotate_x_mtrx(mtrx):
    t = math.pi / 2
    c = math.cos(t)
    s = math.sin(t)

    for v in mtrx:
        x = v[0]
        y = v[1]
        z = v[2]

        dx = x + 0 + 0
        dy = 0 + y * c - z * s
        dz = 0 + y * s + z * c

        v[0] = dx
        v[1] = dy
        v[2] = dz


def rotate_y_mtrx(mtrx):
    t = math.pi / 2
    c = math.cos(t)
    s = math.sin(t)

    for v in mtrx:
        x = v[0]
        y = v[1]
        z = v[2]

        dx = x * c + 0 + z * s
        dy = 0 + y + 0
        dz = x * -s + 0 + z * c

        v[0] = dx
        v[1] = dy
        v[2] = dz


# point 4 is always the point to check
# points are 3d [x,y,z]
def points_coplanar(p1, p2, p3, p4):
    a1 = p2[0] - p1[0]
    b1 = p2[1] - p1[1]
    c1 = p2[2] - p1[2]

    a2 = p3[0] - p1[0]
    b2 = p3[1] - p1[1]
    c2 = p3[2] - p1[2]

    a = b1 * c2 - b2 * c1
    b = a2 * c1 - a1 * c2
    c = a1 * b2 - b1 * a2
    d = -a * p1[0] - b * p1[1] - c * p1[2]

    # equation of plane: a*p1x + b*p1y + c*p1z = 0
    # check if the 4th point satisfies equation
    p = a * p4[0] + b * p4[1] + c * p4[2] + d
    if p == 0:
        return True
    return False


# loop through edges and group by plane (always with edge in it)
# normalise to direction and return direction list
def return_planes_from_edges(edge, edges):
    planar_dirs = []
    for e in edges:
        if e != edge:
            if points_coplanar(edge[0], edge[1], e[0], e[1]):
                e_dir = e[1] - e[0]
                a_norm = get_edge_axis_normal(edge, e_dir)
                if a_norm != Vector((0, 0, 0)):
                    planar_dirs.append([a_norm, "PERP"])
    return planar_dirs


# LINE INTERSECTIONS
# Find edges that intersect with this edge
def get_edge_intersects(bm, edge):
    ips = []
    for e in bm.edges:
        ip = edges_interset(edge, e)
        if ip:
            ip = (ip[0] + ip[1]) / 2
            ips.append(ip)
    return ips


def edges_interset(e1, e2):
    verts = return_edge_verts(e1, e2)
    return LineIntersect(*verts)


def return_edge_verts(e1, e2):
    return [e1.verts[0].co, e1.verts[1].co, e2.verts[0].co, e2.verts[1].co]


def edge_is_loose(edge: bmesh.types.BMEdge) -> bool:
    return len(edge.verts[0].link_edges) <= 1 and len(edge.verts[1].link_edges) <= 1


def parse_dist(dist: List[float], user_val: str, override_unit_scale: bool, context: bpy.context) -> float:
    if not user_val:
        return 0

    r_val = 0.0
    op = False

    # if /*+- as first character then apply this to the current distance
    # else return current user value/expression
    if user_val[0] in utl.NUM_OPS:
        op = True
        scale_dist = utl.return_length_in_cur_unit(dist[0], context)
        r_val = str(scale_dist) + user_val
    else:
        if any(exp in user_val for exp in utl.NUM_OPS):
            op = True
        r_val = user_val

    try:
        # evaluate expression
        if op:
            r_val = float(eval(str(r_val)))
        # scale if required
        r_val = utl.reverse_scale(float(r_val), override_unit_scale, context)
    except ValueError:
        return 0.0
    except SyntaxError:
        return 0.0
    return r_val


def calc_len(verts: List[Vector]) -> List[float]:
    precision = utl.CL_PRECISION
    vert1 = verts[0]
    vert2 = verts[1]
    dx = vert1[0] - vert2[0]
    dy = vert1[1] - vert2[1]
    dz = vert1[2] - vert2[2]
    dist = math.sqrt((dx) ** 2 + (dy) ** 2 + (dz) ** 2)
    return [
        round(dist, precision),
        round(dx, precision),
        round(dy, precision),
        round(dz, precision),
    ]


def calc_len_2D(verts: List[Vector]) -> List[float]:
    precision = utl.CL_PRECISION
    vert1 = verts[0]
    vert2 = verts[1]
    dx = vert1[0] - vert2[0]
    dy = vert1[1] - vert2[1]
    dist = math.sqrt((dx) ** 2 + (dy) ** 2)
    return [round(dist, precision), round(dx, precision), round(dy, precision)]


def calc_sq_len(vert1: Vector, vert2: Vector) -> float:
    dx = vert1[0] - vert2[0]
    dy = vert1[1] - vert2[1]
    dz = vert1[2] - vert2[2]
    dist = (dx) ** 2 + (dy) ** 2 + (dz) ** 2
    return dist


def calc_sq_len_2D(vert1: Vector, vert2: Vector) -> float:
    dx = vert1[0] - vert2[0]
    dy = vert1[1] - vert2[1]
    dist = (dx) ** 2 + (dy) ** 2
    return dist


def calc_dir_vect(verts: Vector) -> Vector:
    return Vector((verts[1] - verts[0])).normalized()


def is_close(v0: Vector, v1: Vector, tol=1e-05):
    '''Check if two vectors are close enough to call the same
    - Handles issues with floating point comparisons.
    '''
    if not isinstance(v0, Vector) or not isinstance(v1, Vector):
        print("Not valid Vectors")

    if (v0-v1).length < tol:
        return True

    return False

# **** prev verts_to_vect3
# def list_to_vector3D(verts: List[float]) -> Vector:
#     vects = []
#     for v in verts:
#         vects.append(Vector((v[0], v[1], v[2])))
#     return vects


def extract_link_edges(w_matrix: Matrix, edge: bmesh.types.BMEdge) -> List[Vector]:
    links = []
    for v in edge.verts:
        for e in v.link_edges:
            if e != edge:
                links.append([w_matrix @ e.verts[0].co,
                             w_matrix @ e.verts[1].co])
    if links:
        return links
    return []


def extract_link_edges_from_vert(w_matrix, vert):
    edge = extract_single_link_edge_bm(vert)
    if edge:
        return extract_link_edges(w_matrix, edge)
    return None


def extract_single_link_edge_bm(vert):
    if vert:
        if vert.link_edges:
            return vert.link_edges[0]
    return None


# edge is current edge
def extract_single_link_edge(w_matrix, vert):
    if vert:
        if vert.link_edges:
            e = vert.link_edges[0]
            return [w_matrix @ e.verts[0].co, w_matrix @ e.verts[1].co]
    return None


# check if vert couple already forms an edge
# takes [BMVert, BMVert] as vect_edge
def vert_couple_in_edges(bm: bmesh.types.BMesh, vert_couple: List) -> bool:
    for e in bm.edges:
        if vert_couple[0] in e.verts and vert_couple[1] in e.verts:
            return True
    return False


def find_face_from_vects(faces: List[bmesh.types.BMFace], vects: List[Vector]) -> bmesh.types.BMFace:
    # is_close
    for f in faces:
        for v in vects:
            for fv in f.verts:
                if not is_close(v, fv.co):
                    break
                return f
    return None


def in_ignore_geom(obj_id: str, ignore_geom: List[IgnoreGeom]) -> bool:
    if ignore_geom:
        for ig in ignore_geom:
            if obj_id == ig.obj_id:
                return True
    return False


# REGION
def update_view(context):
    for area in context.screen.areas:
        if area.type == 'VIEW_3D':
            area.spaces.active.region_3d.update()
            break

# GEOM SELECTION


def select_vis_geom(obj: bpy.types.Object, input_loc: List[float], context: bpy.context, geom_include, ignore_geom=None) -> List:
    if not utl.object_is_valid(obj):
        return []

    if ignore_geom and in_ignore_geom(obj.name, ignore_geom):
        return []

    emode = context.mode == "EDIT_MESH"

    if not emode:
        bpy.ops.object.mode_set(mode="EDIT")

    xl = input_loc[0]
    yl = input_loc[1]

    geom = []

    rad = utl.CL_SELECT_REGION_SIZE
    lim = utl.CL_SELECT_AMNT_LIM
    bm = bmesh.from_edit_mesh(obj.data)
    matrix = obj.matrix_world.copy()
    bm.verts.ensure_lookup_table()

    try:
        bpy.context.tool_settings.mesh_select_mode = (True, True, False)
        bpy.ops.view3d.select_circle(
            x=xl, y=yl, radius=rad, wait_for_input=False, mode="SET"
        )

        selected = [v.index for v in bm.verts if v.select]
        # Check if the number of selected vertices or faces exceeds the maximum
        if len(selected) > lim:
            # Calculate the ratio of selected geometry to the maximum allowed
            ratio = lim / len(selected)
            # Reduce the radius by the square root of the ratio to maintain a consistent area
            rad *= ratio ** 0.5
            bpy.ops.view3d.select_circle(
                x=xl, y=yl, radius=int(rad), wait_for_input=False, mode="SET"
            )
    except RuntimeError:
        return geom

    if geom_include & utl.SNP_FLAG_VERT:
        geom.extend(
            [
                SnapGeom(
                    matrix @ v.co,
                    utl.TYPE_VERT,
                    extract_link_edges_from_vert(matrix, v),
                    None,
                    None,
                    edge=extract_single_link_edge(matrix, v),
                    geom_id=v.index,
                )
                for v in bm.verts
                if v.select
            ]
        )

    for e in bm.edges:
        if e.select:
            v0 = matrix @ e.verts[0].co
            v1 = matrix @ e.verts[1].co
            idx = e.index
            links = extract_link_edges(matrix, e)
            if geom_include & utl.SNP_FLAG_EDGE:
                geom.append(
                    SnapGeom((v0, v1), utl.TYPE_EDGE,
                             links, None, None, geom_id=idx)
                )
            if geom_include & utl.SNP_FLAG_EDGE_C:
                geom.append(
                    SnapGeom(
                        find_edge_center(v0, v1),
                        utl.TYPE_EDGE_C,
                        links,
                        None,
                        None,
                        edge=(v0, v1),
                        geom_id=idx,
                    )
                )

    bpy.ops.mesh.select_mode(type="FACE")
    bpy.ops.mesh.select_all(action="DESELECT")
    if not emode:
        bpy.ops.object.mode_set(mode="OBJECT")
    return geom


# returns a bounds square from edge projected in 2D
# expects [Vector, Vector] edge
def project_pt_2d(pt: Vector, context: bpy.context) -> Vector:
    rv3d = context.region_data
    region = context.region
    return location_3d_to_region_2d(region, rv3d, pt)


def view_bounds_from_edge(edge: List[float], context: bpy.context) -> List:
    if not (isinstance(edge, list) and all(isinstance(x, Vector) for x in edge)):
        return []

    v0 = project_pt_2d(edge[0], context)
    v1 = project_pt_2d(edge[1], context)
    l = calc_len_2D([v0, v1])[0]

    bounds = [
        [v0[0] - l / 2, v0[1]],
        [v0[0] + l / 2, v0[1]],
        [v0[0] + l / 2, v0[1] + l],
        [v0[0] - l / 2, v0[1] + l],
    ]

    return bounds


def scale_obj_bounds(obj: bpy.types.Object, scale_factor: float, context: bpy.context) -> List[Vector]:
    if not utl.object_is_valid(obj):
        return []

    bounds = np.array([v for v in obj.bound_box])
    # Calculate the mean along each axis to find bounds centre
    bounds_centre = np.mean(bounds, axis=0)

    # transform and scale bounds coordinates
    trans_bounds = bounds - bounds_centre
    scale_bounds = scale_factor * trans_bounds
    final_bounds = scale_bounds + bounds_centre

    matrix_world = obj.matrix_world.copy()
    coords = [
        utl.convert_loc_2d(
            matrix_world @ Vector(v), context)
        for v in list(final_bounds)
    ]

    if check_bounds_straight_line(coords):
        coords = []

    return coords


def check_bounds_straight_line(vectors):
    a = vectors[0]
    if a in vectors[2:]:
        return True
