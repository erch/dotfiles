# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from __future__ import annotations
import math
from typing import List, Optional
from dataclasses import dataclass, field
import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Vector, Matrix  # type: ignore
from mathutils.geometry import intersect_line_plane  # type: ignore

import numpy as np

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .construction_lines28 import VIEW_OT_ConstructionLines
from . import cl_utils as utl
from . import cl_geom as clg
from . import cl_mesh_intersections as cmi
from . import cl_meshedit as cme


# New extrude, without booleans
@dataclass
class ExtrudeFaceInfo:
    """Takes extrude face details, the id of the affected object and the num faces"""

    obj_id: str
    num_faces: int
    verts: List[Vector]

    def __init__(self, face_info: FaceInfo):
        self.obj_id = face_info.obj_id
        self.verts = face_info.verts.copy()


@dataclass
class FaceInfo:
    """Stores information about starting face to extrude"""

    obj_id: str
    bm_idx: int
    verts: List[Vector]
    is_on_outer_boundary: bool
    center: Vector
    normal: Vector
    neighbour_face_idxs: List[int]
    neg_limit_face_ids: List[int]
    pos_limit_face_ids: List[int]
    is_manifold: bool = field(default_factory=bool)
    # is_plane: bool = field(default_factory=bool)

    # if neighbor faces are perpendicular to normal
    # is not confined and can simply grab and move
    # face to extrude - confined means that new faces need to be built
    is_confined: bool = field(default_factory=bool)

    # furthest point in own mesh -> face along -normal
    # limit_face_verts: List[Vector] = field(default_factory=List)
    neg_limit_faces_verts: List[List[Vector]] = field(default_factory=List)
    pos_limit_faces_verts: List[List[Vector]] = field(default_factory=List)


class ExtrudeOp:
    def __init__(
        self,
        active_obj: bpy.types.Object,
        selected_faces_idxs: List[int],
        context: bpy.context,
        undo_counter: int,
        bool_based: bool
    ):
        self._active_obj = active_obj
        self._selected_faces_idxs = selected_faces_idxs
        self.context = context

        self._orig_face_info: Optional[FaceInfo]
        self._extrude_face_info: Optional[ExtrudeFaceInfo]

        self.beyond_limit = False
        self.undo_counter = undo_counter
        self.bool_based = bool_based

    def return_active_obj(self) -> bpy.types.Object:
        return self._active_obj

    def return_orig_face_info(self) -> FaceInfo:
        return self._orig_face_info

    @staticmethod
    def face_loop_from_verts(verts) -> List[List]:
        frame = []
        for i in range(len(verts) - 1):
            frame.append([verts[i], verts[i + 1]])
        frame.append([verts[len(verts) - 1], verts[0]])
        return frame

    def calc_extrude_dist(self) -> float:
        dist = 0.0
        if self._orig_face_info and self._extrude_face_info:
            start = self._orig_face_info.verts[0]
            end = self._extrude_face_info.verts[0]
            dist = clg.calc_len([start, end])[0]
        return dist

    def create_connect_faces_bm(self, connect_faces: List[Vector], i_mtrx: Matrix, bm: bmesh.types.BMesh) -> List[bmesh.types.BMFace]:
        new_faces = []
        for f in connect_faces:
            connect_face_verts = []
            for v in f:
                connect_face_verts.append(bm.verts.new(i_mtrx @ v))
            utl.update_bm_verts(bm)
            new_faces.append(bm.faces.new(connect_face_verts))
            utl.update_bm_faces(bm)
        utl.update_bmesh(bm)
        bmesh.ops.recalc_face_normals(bm, faces=bm.faces)
        return new_faces

    def return_extrude_frame_2d(self) -> List[List]:
        """Returns a vector frame of the extrude"""

        if not self._orig_face_info or not self._extrude_face_info:
            return []

        edges = []

        start_vs = self._orig_face_info.verts.copy()
        end_vs = self._extrude_face_info.verts.copy()

        # Convert to screen space
        start_vs = [utl.convert_loc_2d(v, self.context) for v in start_vs]
        end_vs = [utl.convert_loc_2d(v, self.context) for v in end_vs]

        # Build a list of edges that define the frame faces
        edges = self.face_loop_from_verts(start_vs)
        edges.extend(self.face_loop_from_verts(end_vs))
        # Add in the face cennecting edges
        edges.extend(self.return_face_connecting_edges(start_vs, end_vs))

        return edges

    @staticmethod
    def return_face_connecting_edges(
        face_0: List[Vector], face_1: List[Vector]
    ) -> List[Vector]:
        """
        Builds a list of edges which define the vert to vert connections
        between two faces - original and extruded
        """

        return [list(f) for f in zip(face_0, face_1)]

    @staticmethod
    def return_face_connecting_faces(
        face_0: List[Vector], face_1: List[Vector]
    ) -> List[Vector]:
        """
        Builds a list of faces which define the faces between two faces - 
        original and extruded.

        Works in sets of 4 [f0[v0], f1[v0], f0[v1], f1[v1]]...
        Assumes that length of face_0 and face_1 are the same
        """

        if len(face_0) != len(face_1):
            # TODO: log a face shape mismatch error -> pass in logging object
            return []

        face_vert_list = []
        l = len(face_0) - 1
        for i in range(l):
            f = [face_0[i], face_1[i], face_1[i + 1], face_0[i + 1]]
            face_vert_list.append(f)

        # Add in final face
        f = [face_0[l], face_1[l], face_1[0], face_0[0]]
        face_vert_list.append(f)

        return face_vert_list

    def return_limit_faces_ids(
        self, obj: bpy.types.Object, origins: List[Vector], ray_dir: Vector
    ) -> List[int]:
        """Cast a ray from multi origins in ray direction - Moving origins off of starting face by a small amount
        in the ray direction to stop origin face being detected.
        Store any faces by id that are hit.
        Return only unique ids.
        """

        if not utl.object_is_valid(obj):
            return []

        hit_faces = []

        for p in origins:
            success, _, _, idx = obj.ray_cast(p + (ray_dir * 0.001), ray_dir)
            if success:
                hit_faces.append(idx)
        return list(set(hit_faces))

    def raycast_to_face_from_face(self, obj: bpy.types.Object, from_face: bmesh.types.BMFace, to_face: bmesh.types.BMFace) -> bool:
        if not utl.object_is_valid(obj):
            return False

        ray_dir = to_face.normal * -1
        # print(from_face.calc_center_median())

        success, _, _, idx = obj.ray_cast(
            from_face.calc_center_median(), ray_dir)
        # success, _, _, idx = obj.ray_cast(Vector((0,1,0)), Vector((0,1,0)))
        # print(from_face.index)
        # print(success)

        if success and idx == to_face.index:
            return True

        return False

    def return_neighbour_faces(self, face: bmesh.types.BMFace) -> List[bmesh.types.BMFace]:
        neighbours = []
        for e in face.edges:
            for f in e.link_faces:
                if f is not face:
                    neighbours.append(f)
        return neighbours

    def face_is_confined(self, face: bmesh.types.BMFace, neighbour_faces: List[bmesh.types.BMFace]):
        # Treat single plane as confined - so that volume will be built on extrude
        if not neighbour_faces:
            return True

        for f in neighbour_faces:
            dot = face.normal.dot(f.normal)
            if dot != 0:
                return True
        return False

    def exturde_beyond_limit(self) -> bool:
        '''
        Check if extrude position has past limit face
        Must be all extrude face vertices
        '''

        if not (self._orig_face_info and self._extrude_face_info):
            return False

        ex_verts = np.asarray(self._extrude_face_info.verts, dtype=np.float32)
        norm = np.array(self._orig_face_info.normal, dtype=np.float32)
        # lim_face_verts = self._orig_face_info.neg_limit_faces_verts

        ex_verts.shape = (len(ex_verts), 3)

        # only need single vert from face
        if not self._orig_face_info.neg_limit_faces_verts:
            return False

        origin = np.array(
            self._orig_face_info.neg_limit_faces_verts[0][0], dtype=np.float32)
        trans_vecs = ex_verts - origin
        dot = trans_vecs @ norm

        # Make sure all vertices are beyond limit face
        if np.all(dot < 0):
            return True

        # Or vertices are at limit
        if np.allclose(dot, 0, atol=1e-5):
            return True

        return False

    def setup_extrude_geom(self) -> bool:
        act_obj = self._active_obj
        if not act_obj:
            return False

        if not utl.object_is_valid(act_obj) or not self._selected_faces_idxs:
            return False

        face_id = self._selected_faces_idxs[0]
        if face_id >= 0:
            self.face_normal_to_view(face_id)
            utl.refresh_active_edit_object(
                self.context, act_obj, return_to_edit=True)
            face_info = self.build_orig_face_info(act_obj, face_id)
            if face_info:
                self._orig_face_info = face_info
                self._extrude_face_info = ExtrudeFaceInfo(face_info)
                return True
        return False
    
    def nudge_outer_boundary_edge(self, boundary_edges: List[bmesh.types.BMEdge], face_center: Vector, face_normal: Vector) -> None:        
        nudge_edges = []

        for e in boundary_edges:
            distance = 0.1
            # Calculate the projection of the edge onto the face plane
            edge_vector = e.verts[1].co - e.verts[0].co
            # edge_projection = edge_vector - edge_vector.project(face_normal)

            # Calculate the direction perpendicular to the original edge position
            perpendicular_direction = edge_vector.cross(face_normal).normalized()

            # make sure perp always points away from the face centre
            if face_center.dot(perpendicular_direction) < 0:
                distance *= -1

            # Calculate the translation vector
            translation_vector = perpendicular_direction * distance

            # Move the edge along the face plane away from the face center
            for vert in e.verts:
                vert.co += translation_vector

            nudge_edges.append([e, perpendicular_direction.copy(),  distance*-1])


        for ne in nudge_edges:
            for nee in nudge_edges:
                e = ne[0]
                ee = nee[0]
                if e != ee:
                    for v in e.verts:
                        if v in ee.verts:
                            e.other_vert(v).co -= nee[1] * ne[2]
                            break

    def build_orig_face_info(self, obj: bpy.types.Object, face_id: int) -> FaceInfo:
        """Store original face information"""
        try:
            verts = []
            bm = bmesh.new()
            bm.from_mesh(obj.data)
            matrix = obj.matrix_world.copy()
            utl.update_bmesh(bm)
            face = bm.faces[face_id]
            face_center = face.calc_center_median()
            face_normal = face.normal.copy()
            verts = [matrix @ v.co for v in face.verts]
            # outer_boundary_edges = []
            outer_boundary_count = 0
            is_on_outer_boundary = False
            is_manifold = True

            for e in face.edges:
                if not e.is_manifold:
                    is_manifold = False
                    break
                # check if edge is an outer boundary
                # for f in e.link_faces:
                #     if -1 < round(f.normal.dot(face_normal), 1) < 1:
                #         outer_boundary_edges.append(e)

                # Temp***until above implimented
                for f in e.link_faces:
                    if -1 < round(f.normal.dot(face_normal), 1) < 1:
                        outer_boundary_count+=1


            # # Nudge outer boundary edges along perp
            # if outer_boundary_edges:
            #     self.nudge_outer_boundary_edge(outer_boundary_edges, face_center, face_normal)
            if outer_boundary_count >= 1:
                is_on_outer_boundary = True

            
           

            # STORE LIMITS
            # Store face limits
            emode = self.context.mode == "EDIT_MESH"
            if emode:
                bpy.ops.object.mode_set(mode="OBJECT")
            # Get face limits in either direction along normal
            neg_limit_fs = self.return_limit_faces_ids(
                obj, [face_center], face_normal * -1
            )
            pos_limit_fs = self.return_limit_faces_ids(
                obj, [face_center], face_normal)
            neg_face_verts = []
            pos_face_verts = []

            for f in neg_limit_fs:
                neg_face_verts.append(
                    [matrix @ v.co for v in bm.faces[f].verts])

            for f in pos_limit_fs:
                pos_face_verts.append(
                    [matrix @ v.co for v in bm.faces[f].verts])

            # STORE NEIGHBOURS
            neighbour_faces = self.return_neighbour_faces(face)
            is_confined = self.face_is_confined(face, neighbour_faces)

            neighbour_faces_idxs = [f.index for f in neighbour_faces]

            bm.free()

            bpy.ops.object.mode_set(mode="EDIT")

            w_face_center = matrix @ face_center

            face_info = FaceInfo(
                obj.name,
                face_id,
                verts,
                is_on_outer_boundary,
                w_face_center,
                face_normal,
                neighbour_faces_idxs,
                neg_limit_fs,
                pos_limit_fs,
                is_manifold,
                is_confined,
                neg_face_verts,
                pos_face_verts,
            )

            return face_info
        except Exception as e:
            print(e)
            print("Unable to access face")
        return None

    def extrude_geom(self, move_vectors: List, is_neg_heading: bool) -> None:
        """Move new face by extrude distance"""
        if not move_vectors or not self._extrude_face_info:
            return

        if not self._orig_face_info:
            return

        # calculate face normal in world space
        obj = utl.return_object_by_name(
            self._orig_face_info.obj_id, self.context)
        world_mtrx = obj.matrix_world.copy()
        rotation_matrix = world_mtrx.to_3x3()
        norm = rotation_matrix@self._orig_face_info.normal
        norm.normalize()

        dist = clg.calc_len(move_vectors)[0]

        if is_neg_heading:
            dist *= -1

        # move extrude face verts by dragged distance
        new_verts = []
        for v in self._orig_face_info.verts:
            new_verts.append(v + (norm * dist))

        self._extrude_face_info.verts = new_verts
        self.beyond_limit = self.exturde_beyond_limit()

    # Unconfined face extrude - out/in
    def move_face(self, face: bmesh.types.BMFace, new_positions: List[Vector]) -> None:
        for i, v in enumerate(face.verts):
            v.co = new_positions[i]

    # Confined face extrude - In

    def inwards_extrude(self, bm: bmesh.types.BMesh, orig_face: bmesh.types.BMFace, connect_faces: List[bmesh.types.BMFace], orig_neighbour_faces: List[int], obj: bpy.types.Object) -> None:
        """
        Calculates which faces are neighbours to connecting faces and splits
        neighbours (faces and edges) where connecting faces intersect. 
        Stores any new faces that are created through splits so that
        the mesh can be cleaned later of unrequired geometry

        """

        # return all bm neighbour faces by index
        n_faces = []
        for idx in orig_neighbour_faces:
            n_faces.append(bm.faces[idx])

        # new faces created after faces are split
        new_faces = []
        # if normals match split neighbour face
        for f in connect_faces:
            for n in n_faces:
                if not f.is_valid:
                    continue
                if n.index != f.index:
                    dot = n.normal.dot(f.normal)
                    if abs(dot) == 1:
                        # first split edges
                        if f.normal != n.normal:
                            f.normal_flip()
                        for e in f.edges:
                            cmi.split_edges_at_intersections(bm, e)

                        split_faces = cmi.split_face_plane(n, f, obj)
                        if not split_faces:
                            print("Couldn't split face")
                        else:
                            # Store all new split faces for later clean-up
                            new_faces.extend(split_faces)
                            bm.faces.remove(f)
                            utl.update_bm_faces(bm)
                            bmesh.ops.remove_doubles(
                                bm, verts=bm.verts[:], dist=0.001)
                            break

        if orig_face:
            bm.faces.remove(orig_face)

        # Clean-up unrequired faces, edges & verts after extrude
        rem_edges = []
        new_edges = []
        for f in new_faces:
            for e in f.edges:
                if len(e.link_faces) <= 1:
                    rem_edges.append(e)
                else:
                    new_edges.append(e)

        for e in rem_edges:
            bm.edges.remove(e)
            utl.update_bm_edges(bm)

        # remove loose verts
        for e in new_edges:
            if e.is_valid:
                for v in e.verts:
                    if len(v.link_faces) <= 0:
                        bm.verts.remove(v)

    def cut_through_limit_faces(self, bm: bmesh.types.BMesh, prj_face_vecs: List[Vector]) -> None:
        """Cuts through the limit face using the extrude/cut face geometry"""

        utl.update_bmesh(bm)
        if not self.bool_based:
            mesh_edit = cme.MeshEdit(
                self._active_obj, self.context, self.undo_counter)
            mesh_edit.add_new_geom(prj_face_vecs, "TEMP",
                                   True, bm=bm, apply_mesh=False)
            utl.update_bmesh(bm)
        else:
            # perform a boolean based cut
            self.cut_boolean()

    def cut_boolean(self) -> None:
        """Cut-through mesh using solidify and boolean modifiers
        - Build boolean volume and apply to mesh as boolean cut
        """

        if not (self._orig_face_info and self._active_obj):
            return

        obj = self._active_obj
        orig_face_vecs = self._orig_face_info.verts
        dist = self.calc_extrude_dist()

        # **Might need to set face normal to opposite of original face
        # create new mesh from original face
        bm = bmesh.new(use_operators=True)
        for v in orig_face_vecs:
            bm.verts.new(v)
        bm.faces.new(bm.verts)
        bm.normal_update()

        # build new extrude object and add new face to mesh
        me = bpy.data.meshes.new("mesh")
        bm.to_mesh(me)
        extr_ob = bpy.data.objects.new("BOOL_OBJ", me)
        extr_ob.data.update()
        bm.to_mesh(extr_ob.data)
        bm.free()

        # if boolean preference turned off or has outer boundary edges  **TEMP
        if self._orig_face_info.is_on_outer_boundary or (not self.bool_based):
            # scale by a very small factor to make boolean work
            scale_factor = 1.00003
            extr_ob.scale.x *= scale_factor
            extr_ob.scale.y *= scale_factor
            extr_ob.scale.z *= scale_factor


        # Add solidify modifier with thickness of extrude distance
        # apply boolean as a modifier to current object
        if extr_ob:
            utl.add_to_collection(extr_ob)

            # add solidify and boolean to current object
            utl.solid_modifer_to_obj(extr_ob, True, thickness=dist)
            utl.bool_modifer_to_obj(obj, extr_ob)
            extr_ob.hide_set(True)

            # apply boolean to cut through
            utl.bool_modifiers_apply(None, obj, self.context)

            # delete temp extrude object
            utl.delete_Object(extr_ob, self.context, return_to_edit_obj=obj)

            self.context.view_layer.objects.active = obj

    # def cleanup_extrude(self, obj: bpy.types.Object, prj_face_vecs: List[Vector], i_mtrx: Vector):
    #     """
    #     Adds a new face where extrude has cut-through and added new geometry
    #     Checks if any faces are within that face and removes if they are.
    #     i.e. allows for one to many cut-throughs
    #     """

    #     bm = bmesh.from_edit_mesh(obj.data)
    #     utl.update_bmesh(bm)
    #     new_face = clg.build_bm_face_from_vecs(bm, prj_face_vecs, i_mtrx)
    #     utl.update_bm_verts(bm)
    #     utl.update_bm_edges(bm)
    #     utl.update_bm_faces(bm)
    #     inside_faces = []
    #     for f in bm.faces:
    #         if f and f.is_valid and f != new_face:
    #             if clg.new_all_points_on_face(new_face, f):
    #                 inside_faces.append(f)

    #     inside_faces.append(new_face)
    #     bmesh.ops.delete(bm, geom=inside_faces, context='FACES')
    #     utl.update_bmesh(bm)
    #     bmesh.update_edit_mesh(obj.data, destructive=True)
            
    def cleanup_extrude(self, bm: bmesh.types.BMesh) -> None:
        """
        Removes any loose edges or leftover verts
        """

        if bpy.context.mode == "EDIT_MESH":
            bpy.ops.mesh.delete_loose(use_verts=True, use_edges=True, use_faces=False)
            bpy.ops.mesh.dissolve_degenerate(threshold=0.0001)
        else:
            print("Incorrect context to remove loose geometry")


    def finish_extrude(self, is_neg_heading: bool) -> bool:
        """Uses orginal face info and new extrude face info 
        to build/remove faces for extrusion"""

        if not self._orig_face_info or not self._extrude_face_info:
            return False

        orig_face_info = self._orig_face_info

        obj_id = orig_face_info.obj_id
        new_face_vs = self._extrude_face_info.verts

        obj = utl.return_object_by_name(obj_id, self.context)
        i_mtrx = obj.matrix_world.inverted().copy()

        if not obj:
            return False

        try:
            bm = bmesh.from_edit_mesh(obj.data)
            utl.update_bmesh(bm)
            orig_bm_face = bm.faces[orig_face_info.bm_idx]
            lim_face = None
            # Stop extrude at limit face - project extrude face onto limit face
            if self.beyond_limit:
                # will need to change later for multi
                lim_face = bm.faces[orig_face_info.neg_limit_face_ids[0]]
                new_face_vs = clg.project_vecs_onto_face(
                    lim_face, new_face_vs, orig_face_info.normal)

            # Join original face with extruded face with connecting faces
            connect_faces = self.return_face_connecting_faces(
                orig_face_info.verts, new_face_vs)


            # UNCONFINED
            # Just move the face if inward and unconfined
            # outwards will always add a new face
            if (not orig_face_info.is_confined) and is_neg_heading:
                self.move_face(orig_bm_face, [i_mtrx @ v for v in new_face_vs])
            else:
                # CONFINED
                # Inwards Extrude
                if is_neg_heading:
                    # Solves inwards extrude and cut-through issues
                    # until better solution can be found
                    self.cut_boolean()
                    bm = bmesh.from_edit_mesh(obj.data)
                    utl.update_bmesh(bm)

                    # boolean discards bm so rebuild
                    # remove final face
                    if self.beyond_limit:
                        limit_face = clg.find_face_from_vects(bm.faces, new_face_vs)
                        if limit_face:
                            bm.faces.remove(limit_face)

                # Outwards Extrude
                else:
                    clg.build_bm_face_from_vecs(bm, new_face_vs, i_mtrx)
                    new_faces = self.create_connect_faces_bm(connect_faces, i_mtrx, bm)
                    # TODO: 
                    #  4. Outward extrude also needs to cut-into faces if there are any neighbours
                    # *** send new faces to a function that will cut into any neighbour faces
                    # get neighbour faces from new_faces and split/connect where possible
                    
            # clean up
            utl.update_bmesh(bm)
            self.cleanup_extrude(bm)
            utl.update_bmesh(bm)
            bmesh.update_edit_mesh(obj.data, destructive=True)
            utl.refresh_active_edit_object(
                self.context, obj, return_to_edit=True, update_view=True
            )

            self.undo_counter += 1

        except Exception as e:
            print(e)
            print("Unable to extrude face")
            return False
        return True

    def face_normal_to_view(self, fac_index: int) -> None:
        self.context.view_layer.objects.active = self._active_obj
        bpy.ops.object.mode_set(mode="OBJECT")

        act_obj = self._active_obj
        if utl.object_is_valid(act_obj):
            bm = bmesh.new(use_operators=True)
            bm.from_mesh(act_obj.data)
            utl.ensure_bm_lookups(bm)
            face = utl.bm_geom_from_index(bm, fac_index, utl.TYPE_FACE)
            if face and not clg.is_face_normal_to_view(self.context, act_obj, face):
                face.normal_flip()
            utl.update_bmesh(bm)
            act_obj.data.update()
            bm.to_mesh(act_obj.data)
            bm.free()

        bpy.ops.object.mode_set(mode="EDIT")
