# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from __future__ import annotations
import math

from typing import TYPE_CHECKING, List, Tuple
if TYPE_CHECKING:
    from .construction_lines28 import VIEW_OT_ConstructionLines

import bpy  # type: ignore
import gpu  # type: ignore
import blf  # type: ignore
from gpu.types import GPUShader  # type: ignore
from gpu_extras.batch import batch_for_shader  # type: ignore
from mathutils import Vector, Matrix  # type: ignore
from bpy_extras.view3d_utils import location_3d_to_region_2d  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg


import sys

# Check python version for use of cache or lru_cache
if sys.version_info >= (3, 9):
    from functools import cache
else:
    from functools import lru_cache

    cache = lru_cache(maxsize=None)


BUILT_IN_UNIFORM_3D = "3D_UNIFORM_COLOR"
BUILT_IN_UNIFORM_2D = "2D_UNIFORM_COLOR"
BL_VERSION_GROUP = 3

if bpy.app.version >=(4, 0):
    BUILT_IN_UNIFORM_3D = "UNIFORM_COLOR"
    BUILT_IN_UNIFORM_2D = "UNIFORM_COLOR"
    BL_VERSION_GROUP = 4



############################################
# OPENGL DRAWING
############################################
# Backwards compatible to bl versions pre GPU module changes in 3.5
if bpy.app.version >=(3, 5):
    vert_out = gpu.types.GPUStageInterfaceInfo("cl_shader_interface")
    vert_out.smooth('FLOAT', "v_LineLength")

    shader_info = gpu.types.GPUShaderCreateInfo()
    shader_info.push_constant('MAT4', "u_ViewProjectionMatrix")
    shader_info.push_constant('FLOAT', "u_DashSize")
    shader_info.push_constant('FLOAT', "u_Spacing")
    shader_info.push_constant('VEC4', "u_col")
    shader_info.vertex_in(0, 'VEC3', "position")
    shader_info.vertex_in(1, 'FLOAT', "lineLength")
    shader_info.vertex_out(vert_out)
    shader_info.fragment_out(0, 'VEC4', "FragColor")


    shader_info.vertex_source(
        "void main()"
        "{"
        "   v_LineLength = lineLength;"
        "   gl_Position = u_ViewProjectionMatrix * vec4(position, 1.0f);"
        "}"
    )

    shader_info.fragment_source(
        "void main()"
        "{"   
        "   float val = mod(v_LineLength, u_DashSize);"
        "   if (val < u_Spacing)"
        "       val = 1.0f;"
        "   else"
        "       val = 0.0f;"
        "   FragColor = vec4(u_col.x * val, u_col.y * val, u_col.z * val, u_col.a);"
        "}"
    )

    cl_dash_shader = gpu.shader.create_from_info(shader_info)
    del vert_out
    del shader_info

else:
    @cache
    def return_shader_3d() -> GPUShader:
        vertex_shader = """
                uniform mat4 u_ViewProjectionMatrix;

                in vec3 position;
                in float lineLength;

                out float v_LineLength;

                void main()
                {
                    v_LineLength = lineLength;
                    gl_Position = u_ViewProjectionMatrix * vec4(position, 1.0f);
                }
            """

        fragment_shader = """
            uniform float u_DashSize;
            uniform float u_Spacing;
            uniform vec4 u_col;

            in float v_LineLength;
            out vec4 fragColor;

            void main()
            {   
                float val = mod(v_LineLength, u_DashSize);
                if (val < u_Spacing)
                    val = 1.0f;
                else
                    val = 0.0f;
                fragColor = vec4(u_col.x * val, u_col.y * val, u_col.z * val, u_col.a);
            }
        """
        return GPUShader(vertex_shader, fragment_shader)
    
    cl_dash_shader = return_shader_3d()


cube_indices = (
    (0, 1),
    (0, 3),
    (1, 2),
    (2, 3),
    (4, 7),
    (5, 6),
    (5, 4),
    (6, 7),
    (0, 4),
    (1, 5),
    (2, 6),
    (3, 7),
)


rect_indices = ((0, 1), (1, 2), (2, 3), (3, 0))
# rect_coords = [(0, 0), (1, 0), (1, 1), (0, 1)]

font_info = {"font_id": 0, "handler": None}
BL_FONT_ID = font_info["font_id"]

override_dash_shader = False


def set_blf_text_size(text_size):
    if BL_VERSION_GROUP < 4:
        blf.size(BL_FONT_ID, text_size, utl.CL_TEXT_DPI)
    else:
        blf.size(BL_FONT_ID, text_size)
        

def batch_draw(coords: List[float], solid_line: bool, col: Tuple, context: bpy.context, line_style="LINE_STRIP") -> None:
    gpu.state.line_width_set(utl.CL_LINE_WIDTH)

    dist = context.area.spaces.active.region_3d.view_distance
    u_scale = utl.CL_DASH_SCALE * dist
    u_spacing = utl.CL_DASH_SPACE_SCALE * dist

    if override_dash_shader:
        solid_line = True

    if not solid_line:
        line_lengths = [0.0]
        for a, b in zip(coords[:-1], coords[1:]):
            line_lengths.append(float(line_lengths[-1] + (a - b).length))

        # shader = gpu.types.GPUShader(vertex_shader, fragment_shader)
        # shader = return_shader_3d()
        shader = cl_dash_shader
        batch = batch_for_shader(
            shader, line_style, {"position": coords,
                                 "lineLength": line_lengths}
        )

        shader.bind()
        matrix = bpy.context.region_data.perspective_matrix
        shader.uniform_float("u_ViewProjectionMatrix", matrix)
        shader.uniform_float("u_DashSize", u_scale)
        shader.uniform_float("u_Spacing", u_spacing)
        shader.uniform_float("u_col", col)
    else:
        shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_3D)
        batch = batch_for_shader(shader, line_style, {"pos": coords})
        shader.bind()
        shader.uniform_float("color", col)
    batch.draw(shader)


def draw_line_3d(col: Tuple, start: List[float], end: List[float], obj_mode: bool, context: bpy.context):
    coords = []
    coords.append(start)
    coords.append(end)
    batch_draw(coords, obj_mode, col, context)


def draw_poly_2d(p_verts: List[float], constraint: List, obj_mode: bool, context: bpy.context, closed=True) -> None:
    col = utl.get_constraint_col(constraint)
    coords = []

    if not p_verts:
        return
    for v in p_verts:
        coords.append(Vector(v))
    # add fist vert again to complete loop - if closed poly
    if closed:
        # vec = p_verts[0][0], p_verts[0][1], p_verts[0][2]
        coords.append(Vector(p_verts[0]))

    batch_draw(coords, obj_mode, col, context)


def draw_arc_2d(p_verts: List[float], constraint: List, obj_mode: bool, context: bpy.context) -> None:
    if not p_verts:
        return
    coords = []
    for p in p_verts:
        coords.append(p)

    # col = utl.get_axis_col(constraint)
    batch_draw(coords, obj_mode, utl.get_constraint_col(constraint), context)


def draw_tape_line(verts, constraint, soft_constraint, obj_mode, col, context):
    if verts:
        if soft_constraint and not constraint:
            col = utl.get_constraint_col(soft_constraint)
        draw_line_3d(col, verts[0], verts[1], obj_mode, context)

        # draw_dimension(verts, 100)


def draw_guide_lines(guides: List, selected_guides: List, context: bpy.context) -> None:
    for g in guides:
        guide_geom = utl.return_guide_geom(g, context)
        if not guide_geom:
            continue
        guide_edges = [
            gg for gg in guide_geom if gg.geom_type == utl.TYPE_GUIDE_E]
        for ge in guide_edges:
            col = utl.CL_GUIDE_COL
            if selected_guides:
                for sg in selected_guides:
                    if ge.w_loc[0] in sg and ge.w_loc[1] in sg:
                        # change colour to highlight if guide is selected
                        col = utl.CL_SELECT_COL
            draw_guide_line(ge.w_loc, col, context)


def draw_guide_line(coords: List[float], col: Tuple, context: bpy.context) -> None:
    if coords:
        if len(coords) > 1:
            draw_line_3d(col, coords[0], coords[1], False, context)


def draw_2d_box_select(coords: List[float], col: Tuple) -> None:
    if coords:
        shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
        batch = batch_for_shader(shader, "LINE_STRIP", {"pos": coords})
        shader.bind()
        shader.uniform_float("color", col)
        batch.draw(shader)


# highlight vert or cursor point
def highlight_point(point2d: List[float], col: Tuple, point_size=5.0) -> None:
    if point2d and col:
        coords = []
        coords.append(point2d)
        shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
        batch = batch_for_shader(shader, "POINTS", {"pos": coords})
        gpu.state.point_size_set(point_size)
        shader.bind()
        shader.uniform_float("color", col)
        batch.draw(shader)


def highlight_face(coords: List[float], col: Tuple, tri_indices=None) -> None:
    if coords:
        if tri_indices:
            shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
            batch = batch_for_shader(
                shader, "TRIS", {"pos": coords}, indices=tri_indices
            )
        else:
            shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
            batch = batch_for_shader(shader, "LINE_STRIP", {"pos": coords})
        shader.bind()
        shader.uniform_float("color", col)
        batch.draw(shader)


# try except block to handle error when coords behind viewport camera
def highlight_object(coords: List[float], col: Tuple) -> None:
    if coords:
        try:
            shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
            batch = batch_for_shader(
                shader, "LINES", {"pos": coords}, indices=cube_indices
            )
            shader.bind()
            shader.uniform_float("color", col)
            batch.draw(shader)
        except Exception as e:
            # print(e)
            return


def highlight_edge(coords: List[float], col: Tuple) -> None:
    if coords:
        try:
            shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
            batch = batch_for_shader(shader, "LINES", {"pos": coords})
            shader.bind()
            shader.uniform_float("color", col)
            batch.draw(shader)
        except Exception as e:
            print(e)
            return


def highlight_frame(coords: List[float], col: Tuple) -> None:
    try:
        if coords:
            shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
            batch = batch_for_shader(shader, "LINE_STRIP", {"pos": coords})
            shader.bind()
            shader.uniform_float("color", col)
            batch.draw(shader)
    except Exception as e:
        print(e)
        return


def draw_box_text(
    screen_cent: List[float], txt: str, title: str, sub_text: str, col: Tuple, txt_size: int
) -> None:
    
    s_pos = [screen_cent[0] + 200, 100]
    rect = return_display_rect(s_pos)
    draw_filled_rect_2d(rect, utl.CL_DISPLAY_BOX_COL)
    
    # display text
    blf.color(BL_FONT_ID, *col)
    
    t_pos = [s_pos[0] + 20, 75]
    blf.position(BL_FONT_ID, t_pos[0], t_pos[1], 0)
    set_blf_text_size(txt_size + 4)
    blf.draw(BL_FONT_ID, title)

    blf.position(BL_FONT_ID, t_pos[0], t_pos[1] - 30, 0)
    set_blf_text_size(txt_size)
    blf.draw(BL_FONT_ID, txt)

    blf.position(BL_FONT_ID, t_pos[0], t_pos[1] - 55, 0)
    set_blf_text_size(txt_size-2)
    blf.draw(BL_FONT_ID, sub_text)
    


def draw_filled_rect_2d(coords: List[Tuple], col: Tuple):
    if not coords:
        return
    indices = ((0, 1, 2), (2, 1, 3))
    shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
    batch = batch_for_shader(shader, "TRIS", {"pos": coords}, indices=indices)
    shader.bind()
    shader.uniform_float("color", col)
    batch.draw(shader)


def return_display_rect(pos: List[float]):
    n_pos = (pos[0], 100)
    return [
        n_pos,
        (n_pos[0] + utl.CL_GUIDE_RECT_SIZE, n_pos[1]),
        (n_pos[0], n_pos[1] - 100),
        (n_pos[0] + utl.CL_GUIDE_RECT_SIZE, n_pos[1] - 100),
    ]


def return_dim_rect(pos: List[float], txt_dims: List) -> List[Tuple]:
    if not pos:
        return []
    o_x = txt_dims[0] * 1.3
    o_y = txt_dims[1]
    h_o_y = o_y / 2
    return [
        (pos[0] - 5, pos[1] + o_y + h_o_y),
        (pos[0] + o_x, pos[1] + o_y + h_o_y),
        (pos[0] - 5, pos[1] - o_y),
        (pos[0] + o_x, pos[1] - o_y),
    ]


def draw_dimension(pos: List[float], dim: str, col: Tuple, txt_size: int, context: bpy.context):
    if not pos:
        return

    rv3d = context.region_data
    region = context.region
    pos_text = location_3d_to_region_2d(region, rv3d, pos)
    if not pos_text:
        return
    
    blf.color(BL_FONT_ID, *col)

    rect = return_dim_rect(pos_text, blf.dimensions(BL_FONT_ID, dim))
    if rect:
        draw_filled_rect_2d(rect, utl.CL_DISPLAY_DIM_COL)
        # display text
        blf.position(BL_FONT_ID, pos_text[0], pos_text[1], 0)
        set_blf_text_size(txt_size)
        blf.draw(BL_FONT_ID, dim)


# wdigets
def draw_rotation_widget(cl_op: VIEW_OT_ConstructionLines, pts: List, constraint: List, context: bpy.context) -> None:
    col = utl.get_constraint_col(constraint)
    draw_poly_2d(
        clg.plot_circle(
            utl.CL_WIDGET_RAD,
            utl.CL_WIDGET_SEGS,
            utl.return_switch_xy_constraint(constraint),
            cl_op.soft_constraint,
            pts,
            face=None,
        ),
        cl_op.constraint,
        cl_op.obj_mode,
        context,
    )
    draw_line_3d(col, pts[0], pts[1], True, context)


# LINE Callback - draws tape line and drag box
def cl_draw_line_callback_px(cl_op: VIEW_OT_ConstructionLines, context: bpy.context) -> None:
    gpu.state.line_width_set(utl.CL_LINE_WIDTH)
    d_pts = []
    if cl_op.tape:
        if cl_op.tape.is_visible():
            d_pts = cl_op.tape.return_verts()
            d_pts_len = len(d_pts)
            r = clg.calc_len(d_pts)[0]

            working_face = []
            if cl_op.close_geom_pt:
                if cl_op.close_geom_pt.geom_type in {utl.TYPE_FACE, utl.TYPE_PLANE}:
                    working_face = cl_op.close_geom_pt

            if cl_op.state == utl.CL_STATE_POLY_CIRC:
                draw_poly_2d(
                    clg.plot_circle(
                        r,
                        cl_op.circle_segs,
                        cl_op.constraint,
                        cl_op.soft_constraint,
                        d_pts,
                        face=working_face,
                    ),
                    cl_op.constraint,
                    cl_op.obj_mode,
                    context,
                )
            elif cl_op.state == utl.CL_STATE_POLY_RECT:
                # Do plots in cl_obj and store - cl_obj can check if exists - if not then create
                draw_poly_2d(
                    clg.plot_rect(
                        d_pts,
                        cl_op.constraint,
                        cl_op.soft_constraint,
                        face=working_face,
                    ),
                    cl_op.constraint,
                    cl_op.obj_mode,
                    context,
                )
                draw_tape_line(
                    [d_pts[0], d_pts[1]], None, None, False, utl.CL_GUIDE_COL, context
                )
            elif cl_op.state == utl.CL_STATE_POLY_ARC and d_pts_len > 2:
                if d_pts_len > 2:
                    draw_arc_2d(
                        clg.plot_arc_2(d_pts, cl_op.circle_segs),
                        cl_op.constraint,
                        cl_op.obj_mode,
                        context,
                    )
                    draw_tape_line(
                        [clg.find_edge_center(d_pts[0], d_pts[1]), d_pts[2]],
                        None,
                        None,
                        False,
                        utl.CL_GUIDE_COL,
                        context,
                    )
            else:
                obj_mode = cl_op.obj_mode
                # override obj_mode if in measure mode
                if cl_op.state == utl.CL_STATE_POLY_TAPE:
                    obj_mode = False
                    # draw horizontal guide
                    if cl_op.tape.is_horz:
                        hrz_pts = clg.plot_horiz_guide(
                            d_pts, cl_op.tape.get_start_geom().edge
                        )
                        draw_tape_line(
                            hrz_pts, None, None, obj_mode, utl.CL_GUIDE_COL, context
                        )

                draw_tape_line(
                    d_pts,
                    cl_op.constraint,
                    cl_op.soft_constraint,
                    obj_mode,
                    utl.get_constraint_col(cl_op.constraint),
                    context,
                )

    # draw widgets
    if cl_op.state == utl.CL_STATE_ROT:
        if cl_op.tape:
            draw_rotation_widget(cl_op, d_pts, cl_op.constraint, context)

    # draw construction lines
    if cl_op.cl_points:
        draw_guide_lines(cl_op.cl_points, cl_op.selected_geom.guides, context)


# POINT Callback
def cl_draw_callback_px(cl_op: VIEW_OT_ConstructionLines, context: bpy.context) -> None:
    if cl_op:
        if cl_op.close_geom_pt and cl_op.close_geom_pt.geom_type != utl.TYPE_PLANE:
            highlight_point(
                utl.convert_loc_2d(cl_op.close_geom_pt.vert, context),
                utl.highlight_col(cl_op.close_geom_pt.geom_type),
                point_size=utl.CL_HIGHLIGHT_POINT_SIZE,
            )

        # HIGHLIGHT SELECTIONS
        if cl_op.selected_geom:
            if context.mode == "EDIT_MESH":
                for f in cl_op.selected_geom.faces:
                    if f[1]:
                        coords = [utl.convert_loc_2d(v, context) for v in f[1]]
                        highlight_face(
                            coords, utl.CL_SELECT_FACE_COL, tri_indices=f[4])
                for e in cl_op.selected_geom.edges:
                    coords = [utl.convert_loc_2d(v, context) for v in e[1]]
                    highlight_edge(coords, utl.CL_SELECT_COL)
                for v in cl_op.selected_geom.verts:
                    highlight_point(
                        utl.convert_loc_2d(v[1], context),
                        utl.CL_SELECT_COL,
                        point_size=utl.CL_HIGHLIGHT_POINT_SIZE,
                    )
                if cl_op.hover_geom:
                    coords = [utl.convert_loc_2d(v, context)
                              for v in cl_op.hover_geom]
                    highlight_face(coords, utl.CL_SELECT_FACE_COL)

                # Edit mode indicator (bounding box - scaled)
                if context.scene.cl_settings.cl_em_bounds_display_bool:
                    obj = context.active_object
                    if utl.object_is_valid(obj):
                        coords = clg.scale_obj_bounds(obj, 1.5, context)
                        highlight_object(coords, utl.CL_OBJ_BOUNDS_COL)
                # Draw Extrude cage
                if cl_op.active_extrude:
                    ex_vs = cl_op.active_extrude.return_extrude_frame_2d()
                    col = utl.return_extrude_col(
                        cl_op.active_extrude.beyond_limit)
                    for v in ex_vs:
                        highlight_frame(v, col)
                    # for exo in cl_op.active_extrude.extrude_objs:
                    #     for vs in utl.return_2d_object_edges_by_face(exo.extrude_face_obj, context):
                    #         highlight_frame(vs, utl.CL_SELECT_COL)

            elif context.mode == "OBJECT":
                # for v in cl_op.selected_geom.verts:
                #     highlight_point(utl.convert_loc_2d(v, context), utl.CL_SELECT_COL)
                for sel_obj in cl_op.selected_geom.objects:
                    obj = sel_obj.get_obj()
                    if utl.object_is_valid(obj):
                        matrix = obj.matrix_world.copy()
                        coords = [
                            utl.convert_loc_2d(matrix @ Vector(v), context)
                            for v in obj.bound_box
                        ]
                        if obj.type == "EMPTY":
                            highlight_point(
                                coords[0],
                                utl.CL_SELECT_COL,
                                point_size=utl.CL_HIGHLIGHT_POINT_SIZE,
                            )
                        else:
                            highlight_object(coords, utl.CL_SELECT_COL)
        if cl_op.select_box:
            draw_2d_box_select(
                cl_op.select_box.return_selection_box(loop=True),
                utl.CL_SELECT_COL,
            )

        ###############
        # DISPLAY Text
        ###############
        # Tips Box
        if context.scene.cl_settings.cl_tips_display_bool:
            txt_box_dims = [context.region.width / 2,
                            context.region.height - utl.DISPLAY_TEXT_HEIGHT_OFFSET]
            draw_box_text(
                txt_box_dims,
                cl_op.display_text,
                utl.title_from_state(cl_op.state),
                utl.MSG_SUBTITLE,
                utl.CL_TEXT_COL,
                utl.CL_TEXT_SIZE,
            )
        ##############

        if cl_op.tape and cl_op.guide_div > 0:
            guide_divs = clg.plot_div_pts(
                cl_op.tape.return_verts(), cl_op.guide_div)
            for v in guide_divs:
                v = utl.convert_loc_2d(v, context)
                highlight_point(v, utl.CL_SELECT_COL,
                                point_size=utl.CL_HIGHLIGHT_POINT_SIZE)

        # if utl.CL_DEBUG_MODE:
        #     draw_debug_lines(cl_op)

        # Dimension Display - Active
        if cl_op.tape:
            draw_dimension(
                get_cur_state_dim_loc(cl_op),
                get_cur_state_value(cl_op, context),
                utl.CL_TEXT_COL,
                utl.CL_TEXT_SIZE,
                context,
            )

        # Dimension Display - Continuous
        if context.scene.cl_settings.cl_guide_display_bool:
            draw_guide_dims_cont(cl_op.cl_points, context)


def get_cur_state_value(cl_op: VIEW_OT_ConstructionLines, context: bpy.context) -> str:
    # if cl_op.state in {utl.CL_STATE_SEL, utl.CL_STATE_MOV}:
    if cl_op.state == utl.CL_STATE_SEL:
        return ""

    if not cl_op.tape:
        return ""

    # TEMP****
    if cl_op.state == utl.CL_STATE_POLY_RECT:
        working_face = []
        if cl_op.close_geom_pt:
            if (
                cl_op.close_geom_pt.geom_type == utl.TYPE_FACE
                or cl_op.close_geom_pt.geom_type == utl.TYPE_PLANE
            ):
                working_face = cl_op.close_geom_pt

            verts = clg.plot_rect(
                cl_op.tape.return_verts(),
                cl_op.constraint,
                cl_op.soft_constraint,
                face=working_face,
            )
            if verts and len(verts) > 3:
                len_w = clg.calc_len([verts[0], verts[1]])
                len_h = clg.calc_len([verts[0], verts[3]])
                return (
                    utl.return_cur_tape_dist(
                        context, len_w
                    )
                    + " x "
                    + utl.return_cur_tape_dist(
                        context, len_h
                    )
                    # + str(round(clg.calc_len([verts[0], verts[3]])[0], 3))
                )
            else:
                return ""
        else:
            return ""

    if cl_op.state == utl.CL_STATE_ROT:
        if cl_op.tape.in_second_phase():
            return str(
                round(math.degrees(cl_op.tape.get_tape_angle(cl_op.constraint)), 2)
            )
        return ""

    return utl.return_cur_tape_dist(context, cl_op.tape.get_tape_len())


def get_cur_state_dim_loc(cl_op: VIEW_OT_ConstructionLines) -> List:
    pos = []
    if cl_op.tape:
        verts = cl_op.tape.return_verts()
        if cl_op.tape.in_second_phase():
            pos = clg.find_edge_center(
                clg.find_edge_center(verts[0], verts[1]), verts[2]
            )
        else:
            pos = clg.find_edge_center(*verts)
    return pos


def draw_guide_dims_cont(guides: List, context: bpy.context) -> None:
    for g in guides:
        guide_geom = utl.return_guide_geom(g, context, inc_horiz=False)
        if not guide_geom:
            continue
        guide_edges = [
            gg for gg in guide_geom if gg.geom_type == utl.TYPE_GUIDE_E]
        for ge in guide_edges:
            draw_dimension(
                clg.find_edge_center(ge.w_loc[0], ge.w_loc[1]),
                utl.return_cur_tape_dist(
                    context, clg.calc_len([ge.w_loc[0], ge.w_loc[1]])
                ),
                utl.CL_TEXT_COL,
                utl.CL_TEXT_SIZE,
                context,
            )


# GIZMO DRAWING
def draw_highlight_gizmo(gizmo, text: str, x: float, y: float) -> None:
    if gizmo.is_highlight:
        text_size = utl.CL_TEXT_SIZE
        set_blf_text_size(text_size)
        blf.shadow(0, 3, 0, 0, 0, 1)
        blf.shadow_offset(0, 0, 0)
        blf.enable(0, blf.SHADOW)
        blf.color(BL_FONT_ID, *utl.CL_TEXT_COL)
        blf.position(0, x - blf.dimensions(0, text)[0] / 2, y - 40, 0)
        blf.draw(0, text)
        blf.disable(0, blf.SHADOW)


# DEBUG DRAWING
def draw_debug_lines(cl_op: VIEW_OT_ConstructionLines) -> None:
    if cl_op.vert_cache:
        geom = cl_op.vert_cache.return_snap_geom()
        if geom:
            for sg in geom:
                if not sg:
                    continue
                if sg.geom_type in {utl.TYPE_EDGE, utl.TYPE_GUIDE_E}:
                    coords = []
                    coords.append(sg.loc2d[0])
                    coords.append(sg.loc2d[1])
                    shader = gpu.shader.from_builtin(BUILT_IN_UNIFORM_2D)
                    batch = batch_for_shader(shader, "LINES", {"pos": coords})
                    shader.bind()
                    shader.uniform_float("color", utl.CL_DEBUG_LINE_COL)
                    batch.draw(shader)
