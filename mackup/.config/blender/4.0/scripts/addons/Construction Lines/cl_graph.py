# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

from typing import List

import bmesh  # type: ignore
from mathutils import Vector  # type: ignore

from . import cl_geom as clg


#############################################
# NODE
#############################################
class mesh_node:
    def __init__(self, n_id: str):
        self.n_id = n_id
        self.connections = []
        self.visited = False
        self.bm_vert = None
        self.connection_store = []

    def store_connections(self) -> None:
        if self.connections:
            self.connection_store = self.connections.copy()
            self.connections = []

    def reset_connections(self) -> None:
        if self.connection_store:
            self.connections = self.connection_store.copy()
            self.connection_store = []

    def loc(self) -> Vector:
        if self.bm_vert and self.bm_vert.is_valid:
            return self.bm_vert.co
        return None


#############################################
# GRAPH
#############################################
class mesh_graph:
    def __init__(self, edges: List[bmesh.types.BMEdge]) -> None:
        self.nodes = []
        self.build_nodes(edges)

    def gen_node_id(self, edge_idx: int, vert_idx: int) -> str:
        return str(edge_idx) + "_" + str(vert_idx)

    def build_nodes(self, edges: List[bmesh.types.BMEdge]) -> None:
        """Stores nodes with a unique index based on vert.index"""
        for e in edges:
            n_id0 = str(e.verts[0].index)
            n_id1 = str(e.verts[1].index)

            self.add_node(n_id0, n_id1, e.verts[0])
            self.add_node(n_id1, n_id0, e.verts[1])

    # Loop all nodes, if a node has an id the same as another's connection
    # add other id to connections
    def add_node(self, n_id: str, connected_n_id: str, bm_v: bmesh.types.BMVert) -> None:
        exists = False
        for n in self.nodes:
            if n.n_id == n_id:
                n.connections.append(connected_n_id)
                exists = True
        if exists:
            return

        m = mesh_node(n_id)
        m.bm_vert = bm_v
        m.connections.append(connected_n_id)
        self.nodes.append(m)

    def return_node_by_id(self, n_id: str) -> mesh_node:
        for n in self.nodes:
            if n.n_id == n_id:
                return n
        return None

    def return_node_by_bmv(self, bm_v: bmesh.types.BMVert) -> mesh_node:
        """Return a node by it's bmesh vert reference"""
        for n in self.nodes:
            if n.bm_vert == bm_v:
                return n
        return None

    def return_bm_nodes_uniq(self, cycle: List[mesh_node]) -> List[mesh_node]:
        bm_nodes = [n.bm_vert for n in cycle]
        u_list = []
        for n in bm_nodes:
            if n not in u_list:
                u_list.append(n)
        return u_list

    def reset_nodes(self) -> None:
        """Set all nodes to unvisited"""

        for n in self.nodes:
            n.visited = False
            n.reset_connections()

    def return_bmesh_verts(self, cycles: List[List[mesh_node]]) -> List[List[mesh_node]]:
        """Return node cycles such that each cycle only contains unique nodes (based on vert data)"""
                
        bm_cycle = []
        for c in cycles:
            bm_cycle.append(self.return_bm_nodes_uniq(c))
        return bm_cycle

    def return_cycles(self) -> List[List[mesh_node]]:
        rtn_cycles = []
        if not self.nodes:
            return []

        for n in self.nodes:
            cycles = self.find_cycles(n)
            if cycles:
                # limit returned cycles
                if len(cycles) > 5:
                    cycles = cycles[:5]
            rtn_cycles.extend(cycles)
            self.reset_nodes()
        unique_c = self.unique_cycles(rtn_cycles)
        if rtn_cycles:
            return self.return_bmesh_verts(unique_c)
        return []

    def find_cycles(self, home: mesh_node) -> List[List[mesh_node]]:
        """ Return a list of node cycles which form a complete node cycle back to home
        The returned list of cycles is sorted by the shortest cycle first 
        """
        cycles = []
        cycle = []
        cycle_cache = {}
        self.find_home(home, home, cycle, cycles, None, cycle_cache)
        if cycles:
            shortest_c = self.order_cycles_by_shortest(cycles)
            return shortest_c
        return []

    def find_home(self, node: mesh_node, home: mesh_node, cycle: List[mesh_node], cycles: List[List[mesh_node]], prev_node: mesh_node, cycle_cache: dict) -> None:
        """ Recursive function to find path through stored nodes back to home node.
        Appends all cycles that return to home to a cycles list.
        """
        node.visited = True
        coplanar = True
        cache_key = (node.n_id, home.n_id)
        home_found = False

        if cache_key in cycle_cache:
            result = cycle_cache[cache_key]
            if result is not None:
                if result[1]:
                    cycles.append(result[0][:])
            return

        cycle.append(node)

        if coplanar:
            for c in node.connections:
                n = self.return_node_by_id(c)
                if not n:
                    continue
                if not n.visited:
                    self.find_home(n, home, cycle, cycles, node, cycle_cache)
                elif n == home and n != prev_node:
                    cycles.append(cycle[:])
                    home_found = True

        if cycle:
            cycle_cache[cache_key] = [cycle[:], home_found]
            cycle[-1].visited = False
            cycle.pop()
  

    def order_cycles_by_shortest(self, cycles: List[List[mesh_node]]) -> List[List[mesh_node]]:
        c_list = []
        for c in cycles:
            dist = self.calc_cycle_dist(c)
            if dist > 0:
                c_list.append([c, dist])
        return self.sort_cycles_list(c_list)

    def calc_cycle_dist(self, cycle: List[mesh_node]) -> float:
        """Calculate a perimiter distance of each cycle in the cycle list"""

        dist = 10000000
        for i, c in enumerate(cycle):
            if i < len(cycle) - 1:
                cur_node = c
                nxt_node = cycle[i + 1]
                if not cur_node and nxt_node:
                    return -1
                dist += clg.calc_sq_len(cur_node.loc(), nxt_node.loc())
        end_node = cycle[-1]
        start_node = cycle[0]
        dist += clg.calc_sq_len(end_node.loc(), start_node.loc())
        return dist
    
    def unique_cycles(self, cycles: List[List[mesh_node]]) -> List[List[mesh_node]]:
        """iterate cycles and only return unique ones"""
        unique_objects = set()
        unique_lists = []
        for c in cycles:
            # Sort the list of objects by their ids
            sorted_lst = sorted(c, key=lambda node: node.n_id)
            # Convert the sorted list to a tuple
            lst_tuple = tuple(node.n_id for node in sorted_lst)
            # If this tuple has not been seen before, add the list to the unique_lists
            # and add the tuple to the unique_objects set
            if lst_tuple not in unique_objects:
                unique_objects.add(lst_tuple)
                unique_lists.append(c)

        filtered_lists = self.remove_overlaps(unique_lists)
        return filtered_lists
    
    def remove_overlaps(self, unique_cycles):
        filtered_lists = []
        for lst in unique_cycles:
            if not any(set(lst).issuperset(set(l)) for l in unique_cycles if l != lst):
                filtered_lists.append(lst)
        return filtered_lists

    # sort list based on sort value [i][1]
    # return first list without sort value [0][0]
    @staticmethod
    def sort_node_list(li):
        li.sort(key=lambda x: x[1])
        return li[0][0]

    # sort list based on sort value [i][1]
    @staticmethod
    def sort_cycles_list(li):
        # sort by shortest cycle
        li.sort(key=lambda x: x[1])
        return [l[0] for l in li]
