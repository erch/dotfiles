# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Project Name:         Construction Lines
# License:              GPL
# Authors:              Daniel Norris, DN Drawings

import itertools
from typing import List, Tuple

import bpy  # type: ignore
import bmesh  # type: ignore
from mathutils import Matrix, Vector  # type: ignore

from . import cl_utils as utl
from . import cl_geom as clg
from . import cl_graph as gph
from . import cl_mesh_intersections as cmi


# Calling functions can keep a reference to this object to either keep using or delete when finished with

class MeshEdit:
    def __init__(self, obj: bpy.types.Object, context: bpy.context, undo_counter: int, find_faces=True):
        self._obj = obj
        self._context = context

        # Need to look into this - dosen't seem to make too much sense how it's
        # found later on.
        self._keep_working = True
        self.undo_counter = undo_counter
        self._unused_edges = []
        # find faces will control whether new geometry
        # will try to find and build faces
        self._find_faces = find_faces

    @property
    def working_object(self) -> bpy.types.Object:
        return self._obj

    @working_object.setter
    def working_object(self, obj):
        if utl.object_is_valid(obj):
            self._obj = obj

    def get_bmesh_ref(self) -> bmesh.types.BMesh:
        """build and return a reference to the working objects
        bmesh"""
        if not utl.object_is_valid(self.working_object):
            return None

        return bmesh.from_edit_mesh(self.working_object.data)

    def define_construction_plane(self, edges: List[Vector], imtrx: Matrix) -> List[Vector]:
        """Define a working plane by taking 3 points from given edges
        Use Object's inverted matrix to set construction plane based on new edges
        """

        if len(edges) < 2:
            return []

        # TODO: look at this - might not work correctly
        verts = [Vector(edges[0]), Vector(edges[1]),
                 Vector(edges[len(edges)-1])]
        construct_plane = [imtrx @ v for v in verts]

        return construct_plane

    # #######################
    # MESH CLEAN & APPLY
    # #######################
    def mesh_apply(self):
        if not self.working_object:
            return

        if self._context.mode == "EDIT_MESH":
            bpy.ops.object.mode_set(mode="OBJECT")

        # if self.working_object.data.users == 1:
        #     bpy.ops.object.transform_apply()

        # bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="BOUNDS")
        utl.refresh_active_edit_object(
            self._context, self.working_object, return_to_edit=True
        )

    def clean_up_mesh(self, bm: bmesh.types.BMesh, obj: bpy.types.Object) -> None:
        bm.normal_update()
        self.clean_double_edges(bm)
        utl.update_bmesh(bm)
        bmesh.update_edit_mesh(obj.data, destructive=True)
        obj.data.update()

    def clean_double_edges(self, bm):
        rem_edges = []
        edges = [e for e in bm.edges]
        for e1, e2 in itertools.combinations(edges, 2):
            if e1.verts[0] in e2.verts and e1.verts[1] in e2.verts:
                if e2.is_wire:
                    rem_edges.append(e2)
                elif e1.is_wire:
                    rem_edges.append(e1)

        for rem_e in rem_edges:
            if rem_e.is_valid:
                bm.edges.remove(rem_e)

    # #######################
    # ADD NEW GEOM
    # #######################
    # might join this with below
    def create_line(self, verts: List[Vector]) -> bpy.types.Object:
        obj = self.working_object
        add_exist = False

        if utl.object_is_valid(obj) and self._context.mode == "EDIT_MESH":
            # verts = clg.verts_to_vect3(verts)
            # continue line until face has been created or split
            imtrx = obj.matrix_world.inverted().copy()
            bm = self.get_bmesh_ref()
            if not bm:
                return
            add_exist = not self.add_edge_to_existing_mesh(
                bm, verts, True, [], imtrx)
            # reset mode to update
            utl.refresh_active_edit_object(self._context, obj)
        else:
            obj = utl.build_line_obj(verts, self._context)
            add_exist = True
            utl.only_select(obj)

        self.working_object = obj
        self.mesh_apply()

        return obj if add_exist else None

    def add_new_geom(self, new_edges: List[Vector], new_obj_name: str, loop_edges: bool, bm: bmesh.types.BMesh = None, apply_mesh=True) -> None:
        """Adds new geometry to either existing mesh or new
        If in edit mode will add to an existing mesh
        otherwise create a new mesh to add to.
        - looping edges will attempt to create a face 
        - new_edges is a list of vectors not edge couples
        """

        if not new_edges:
            return None

        obj = self.working_object
        add_exist = False
        # keep_in_edit = True

        if utl.object_is_valid(obj) and self._context.mode == "EDIT_MESH":
            # keep a bmesh ref instead of constantly re-creating each time
            # only get a reference if one hasn't been passed in
            if not bm:
                bm = self.get_bmesh_ref()
                if not bm:
                    return

            build_face = False
            for i, _ in enumerate(new_edges):
                new_edge = []
                if i < len(new_edges) - 1:
                    new_edge = [new_edges[i], new_edges[i + 1]]

                if loop_edges and i >= len(new_edges) - 1:
                    new_edge = [new_edges[i], new_edges[0]]
                    build_face = True

                imtrx = self.working_object.matrix_world.inverted().copy()
                construction_plane = self.define_construction_plane(
                    new_edges, imtrx)
                add_exist = not self.add_edge_to_existing_mesh(
                    bm, new_edge, build_face, construction_plane, imtrx
                )

            if not add_exist:
                self.working_object = None
        else:
            new_obj = self.add_edges_to_new_mesh(
                new_edges, new_obj_name, loop_edges)
            add_exist = True
            self.working_object = new_obj

        if apply_mesh:
            self.mesh_apply()

    def create_verts(self, verts):
        obj = self.working_object
        if utl.object_is_valid(obj) and bpy.context.mode == "EDIT_MESH":
            imtrx = obj.matrix_world.inverted().copy()
            verts = [imtrx @ v for v in verts]
            bm = self.get_bmesh_ref()
            for v in verts:
                n_v = bm.verts.new(v)
                utl.update_bm_verts(bm)
                cmi.cal_edge_vert_intersections(bm, n_v)

            self.clean_up_mesh(bm, obj)
            self.mesh_apply()

            # Refresh edit object - switch in and out of modes
            utl.refresh_edit_mode()

    def add_edges_to_new_mesh(self, new_edges: List[Vector], new_obj_name: str, create_face: bool) -> bpy.types.Object:
        """Create a new object and assosiated mesh
        then add new geometry to it
        - new_edges is a list of edge vert locations
        """

        obj, _, bm = utl.setup_new_obj(new_obj_name, self._context)

        for v in new_edges:
            bm.verts.new(v)
        utl.update_bm_verts(bm)
        bm.verts.sort()

        # might be a better way to allow for arc case
        if create_face:
            # New function for this also
            new_face = bm.faces.new(bm.verts)
            # flip normals to view if required
            bm.normal_update()
            if not clg.is_face_normal_to_view(self._context, obj, new_face):
                new_face.normal_flip()
        else:
            for i in range(len(bm.verts) - 1):
                bm.verts.ensure_lookup_table()
                bm.edges.new((bm.verts[i], bm.verts[i + 1]))
        utl.update_bmesh(bm)
        obj.data.update()
        bm.to_mesh(obj.data)
        bm.free()

        self.working_object = obj
        # finally apply mesh transforms etc. and return to edit mode
        self.mesh_apply()

        return obj

    def add_edge_to_existing_mesh(self, bm: bmesh.types.BMesh, edge: List[Vector], create_face: bool, construction_plane: List[Vector], imtrx: Matrix):
        """Use bmesh reference to add new geometry
        """

        face_added_split = False

        if not edge:
            return False

        try:
            obj = self.working_object
            new_edge = bm.edges.new(
                (bm.verts.new(imtrx@edge[0]), bm.verts.new(imtrx@edge[1])))
            utl.update_bm_verts(bm)

            # Query mesh for edge intersections and split if found
            split_edges = cmi.split_edges_at_intersections(bm, new_edge)
            # Split any faces if needed
            face_added_split, unused_edges = cmi.split_faces(bm, split_edges)
            bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=0.001)

            if unused_edges:
                # reset so that line drawing can carry on if edges unused
                face_added_split = False
                # Find cycles & split faces
                # if final edge (free_obj flag) then pass in final edge (edges if intersections)
                final_edges = []
                self._unused_edges.extend(unused_edges)
                if create_face:
                    final_edges = self._unused_edges

                new_faces = []
                if self._find_faces:
                    new_faces = self.find_faces(
                        bm, final_edges, construction_plane)

                if new_faces:
                    self._unused_edges.clear()

                for new_face in new_faces:
                    if not (new_face and new_face.is_valid):
                        continue
                    face_added_split = True
                    # make sure normals are correct to view
                    utl.update_bmesh(bm)
                    bm.normal_update()
                    if not clg.is_face_normal_to_view(self._context, self.working_object, new_face):
                        new_face.normal_flip()
                    # check if new face is inside an existiing face
                    hit_face = None
                    for f in bm.faces:
                        if f is not new_face:
                            if clg.new_all_points_on_face(f, new_face):
                                hit_face = f
                                break
                    if hit_face:
                        # match face normals
                        if new_face.normal != hit_face.normal:
                            new_face.normal_flip()
                        # try to split face
                        if cmi.split_face_plane(hit_face, new_face, obj):
                            bm.faces.remove(new_face)
                            utl.update_bm_faces(bm)
                        else:
                            # Can't split face so try to add geometry to help split.
                            # Might be middle face case
                            split_faces = self.cut_face_into_face(
                                bm, hit_face, new_face)
                            if split_faces:
                                face_added_split = True
                            # TODO: need to check for intersections and faces by new edge geometry
                            # feed back into this function (version 0.9.7)
                            # face_added_split = ei.split_faces(bm, new_edges)
                    utl.update_bmesh(bm)

            self.clean_up_mesh(bm, obj)
        except Exception as e:
            print(e)
            print("Error: Unable to add geometry.")
            return False

        return face_added_split

    # #######################
    # CUT INTO EXISTING FACE
    # #######################
    def cut_face_into_face(self, bm: bmesh.types.BMesh, hit_face: bmesh.types.BMFace, new_face: bmesh.types.BMFace) -> List[bmesh.types.BMFace]:
        obj = self.working_object
        edges = [e for e in new_face.edges]
        new_edges = []
        # first vert couple
        vc_1, make_edge = self.return_closest_vert_couple(
            bm, hit_face, new_face)
        if vc_1 and make_edge:
            new_edges.append(utl.make_new_edge(bm, vc_1))
            # edges.append(e1)
        # second vert couple
        vc_2, make_edge = self.return_closest_vert_couple(
            bm, hit_face, new_face, exclude=vc_1
        )
        if vc_2 and make_edge:
            new_edges.append(utl.make_new_edge(bm, vc_2))
            # edges.append(e2)

        edges.extend(new_edges)
        # split the face and clean up
        split_faces = cmi.split_face_plane_edges(hit_face, edges, obj)
        if split_faces:
            bm.faces.remove(new_face)
            self.clean_up_mesh(bm, obj)
            return split_faces
        else:
            # remove new edges from bmesh and
            # current edge list
            for rem_e in new_edges:
                if rem_e:
                    edges.remove(rem_e)
                    bm.edges.remove(rem_e)
            utl.update_bmesh(bm)

        # remove all face verts if can't split
        for v in new_face.verts:
            bm.verts.remove(v)
        self.clean_up_mesh(bm, obj)
        return []

    # return the closest two verts between two face vert arrays
    def return_closest_vert_couple(self, bm: bmesh.types.BMesh, exist_face: bmesh.types.BMFace, new_face: bmesh.types.BMFace, exclude=None) -> Tuple:
        cpls = []
        for nf_v in new_face.verts:
            for ex_v in exist_face.verts:
                if exclude:
                    if ex_v in exclude or nf_v in exclude:
                        continue
                # Make sure not an existing edge or the same vert
                if clg.vert_couple_in_edges(bm, [ex_v, nf_v]) or ex_v == nf_v:
                    continue
                cpls.append([[ex_v, nf_v], clg.calc_sq_len(ex_v.co, nf_v.co)])
        if cpls:
            # Make sure that edge formed by couple dosen't intersect existing edge
            # move to next couple if intersection exists
            # add edges as coords instead of BMEdges so that exclude edge can be queried
            chk_edges = [[e.verts[0].co, e.verts[1].co]
                         for e in exist_face.edges]
            if exclude:
                chk_edges.append([exclude[0].co, exclude[1].co])
            for c in utl.return_lowest_val_sorted_list(cpls):
                discard_cpl = False
                for i, e in enumerate(chk_edges):
                    co = clg.find_edge_edge_intersection_verts(
                        [c[0][0].co, c[0][1].co, e[0], e[1]]
                    )
                    if co:
                        if clg.point_on_edge(co, [e[0], e[1]]) and co not in [
                            e[0],
                            e[1],
                        ]:
                            # if intersection on final edge check, discard couple (else will be returned)
                            if i >= len(chk_edges) - 1:
                                discard_cpl = True
                            continue
                if not discard_cpl:
                    return (c[0], True)
        return (None, False)

    # ***********************
    # FIND FACES
    # ***********************
    # return list of wire edges and list of wire edges and edge links
    def return_wire_edges_and_links(self, bm: bmesh.types.BMesh) -> Tuple:
        w_edges = [
            e for e in bm.edges if e.is_wire and not clg.edge_is_loose(e)]
        v_edges = []
        edges_and_links = []
        for e in w_edges:
            for v in e.verts:
                # get link_edges of wire_edges
                for le in v.link_edges:
                    if not le.is_wire:
                        v_edges.append(le)
        edges_and_links = w_edges.copy()
        for e in v_edges:
            if e not in w_edges:
                # if e not in edges_and_links: This will stop cutting into meshes (centre)
                edges_and_links.append(e)
        return (w_edges, edges_and_links)

    def return_boundary_empty_edges(self, bm: bmesh.types.BMesh, construct_plane: List[Vector]) -> List[bmesh.types.BMEdge]:
        l_edges = [e for e in bm.edges if len(e.link_faces) < 2]
        rtn_edges = []
        for e in l_edges:
            if construct_plane and len(construct_plane) > 2:
                if clg.points_coplanar(
                    construct_plane[0],
                    construct_plane[1],
                    construct_plane[2],
                    e.verts[0].co,
                ) and clg.points_coplanar(
                    construct_plane[0],
                    construct_plane[1],
                    construct_plane[2],
                    e.verts[1].co,
                ):
                    rtn_edges.append(e)
            else:
                rtn_edges.append(e)
        return rtn_edges

    # Cycles
    def find_faces(self, bm: bmesh.types.BMesh, final_edges: List[bmesh.types.BMEdge], construct_plane: List[Vector]) -> List[bmesh.types.BMFace]:
        w_edges, e_and_l = self.return_wire_edges_and_links(bm)
        if not w_edges:
            # no wire edges so try to make face from any boundary edges
            # in the mesh (try to limit to construct plane)
            # and final_edges
            e_and_l = self.return_boundary_empty_edges(bm, construct_plane)
            w_edges = final_edges
            if not w_edges:
                return []

        chk_edges = e_and_l + w_edges

        mg = gph.mesh_graph(chk_edges)
        new_faces = []
        cycles = mg.return_cycles()
        if cycles:
            for c in cycles:
                if not self.face_exists(bm.faces, c):
                    nf = bm.faces.new(c)
                    nf.normal_update()
                    utl.update_bm_faces(bm)
                    # new_face = nf
                    # break
                    new_faces.append(nf)
        del mg
        return new_faces

    def face_exists(self, faces: bmesh.types.BMFace, new_face_vs: List[Vector]) -> bool:
        for f in faces:
            # if lengths don't match can't be an existing face
            if len(f.verts) == len(new_face_vs):
                # check existing face against new_vs not the other way
                if utl.all_items_in_list(f.verts, new_face_vs):
                    return True
        return False
