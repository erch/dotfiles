# -*- coding:utf-8 -*-

# #
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110- 1301, USA.
#
# 
# <pep8 compliant>

# ----------------------------------------------------------
# Author: Stephen Leger (s-leger)
#
# ----------------------------------------------------------
# noinspection PyUnresolvedReferences
from mathutils import Vector, Matrix
from math import pi
from .geom import (
    Geom3d,
    Z_AXIS
)
from .types import (
    TransformType,
    ConstraintType,
    SnapItemType
)
from .logger import get_logger
logger = get_logger(__name__, "DEBUG")


class Constraint:

    @classmethod
    def to_plane(cls, trs,  pos: Vector, about: Matrix) -> Vector:
        """
        Project a point on nearest plane location - along normal direction
        :param trs: TransformAction
        :param pos: Vector
        :param about: Matrix of plane
        :return:
        """
        if trs.snapitem is not None and (trs.transformtype & TransformType.MOVE) > 0:
            # project line to the plane
            if trs.snapitem.type == SnapItemType.LINE:
                p0, p1 = trs.snapitem.coords[0:2]
                it = Geom3d.intersect_line_plane(p0, p1, about)

                if it is not None:
                    return it

        # fallback to closest point
        return Geom3d.neareast_point_plane(pos, about)

    @classmethod
    def to_normal(cls, pos: Vector, about: Matrix) -> Vector:
        """
        Project a point on nearest face normal location - perpendicular
        :param pos:
        :param about: Matrix of face at center with z axis aligned to normal
        :return: intersection
        """
        p0 = about.translation
        p1 = about @ Z_AXIS
        return Geom3d.neareast_point_on_line(pos, p0, p1)

    @classmethod
    def to_axis(cls, trs, pos: Vector, about: Matrix) -> Vector:
        """
        Project a point on nearest axis location
        :param trs: TransformAction
        :param pos:
        :param about:
        :return:
        """
        p0 = about.translation
        axis = about.to_3x3() @ Z_AXIS

        if trs.snapitem is not None and (trs.transformtype & (TransformType.MOVE | TransformType.SCALE)) > 0:
            it = None
            # intersection of axis and line on axis
            # quite not certain about the meaning of this intersection
            # as intersection is in 3d (closest point) but should be projected
            if trs.snapitem.type == SnapItemType.LINE:
                it = Geom3d.intersect_line_line(p0, p0 + axis, *trs.snapitem.coords[0:2])

            elif trs.snapitem.type == SnapItemType.TRI:
                it = Geom3d.intersect_ray_plane(p0, axis, trs.snapitem.coord, trs.snapitem.normal)

            if it is not None:
                return it

        # fallback to closest point (perpendicular)
        return Geom3d.neareast_point_on_line(pos, p0, p0 + axis)

    @staticmethod
    def rotation_plane(trs, about: Matrix) -> Matrix:
        """ Rotate about so z axis fit with a rotation axis
        :param trs:
        :param about:
        :return:
        """
        res = about

        if trs.has_constraint(ConstraintType.X):
            res = about @ Matrix.Rotation(pi / 2, 4, 'Y')
        elif trs.has_constraint(ConstraintType.Y):
            res = about @ Matrix.Rotation(pi / 2, 4, 'X')

        return res

    @classmethod
    def apply(cls, trs, pos: Vector, about: Matrix) -> Vector:
        """
        Apply a constraint to a pos
        :param trs: TransformAction
        :param pos:
        :param about: a snap target
        :return:
        """
        co = pos
        if trs.has_constraint(ConstraintType.PLANE):
            # Constraint to plane
            co = cls.to_plane(trs, pos, cls.rotation_plane(trs, about))

        elif trs.has_constraint(ConstraintType.AXIS):
            co = cls.to_axis(trs, pos, cls.rotation_plane(trs, about))

        # elif trs.has_constraint(ConstraintType.NORMAL):
        #    co = cls.to_normal(pos, about)

        # elif trs.has_constraint(ConstraintType.PERPENDICULAR):
        #    # NOTE *about is wrong for a line
        #    co = cls.to_line(pos, *about)

        return co
