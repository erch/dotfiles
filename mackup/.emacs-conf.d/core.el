;; -*- lexical-binding:t -*-
(message "loading core ...")
(defconst ech:conf/emacs-dir (file-name-directory load-file-name) "emacs configuration Root directory")
(defconst ech:core/confs-dir (file-name-as-directory (expand-file-name "confs" ech:conf/emacs-dir)) "directory with all configuration files")
(defconst ech:core/local-dir (file-name-as-directory (expand-file-name "local" ech:conf/emacs-dir)) "directory local packages")
(defconst ech:core/statics-dir (file-name-as-directory (expand-file-name "statics" ech:conf/emacs-dir)) "directory for static files")
(setq user-emacs-directory (file-name-as-directory (expand-file-name ".emacs-ech.d" "~")))
(defconst ech:core/cache-dir (file-name-as-directory (expand-file-name "cache" user-emacs-directory)) "cache directory for all emacs stuff.")
(setq custom-save-file (expand-file-name "custom.el" ech:core/cache-dir))
;; need to create some dir if they don't exists.
(unless (file-exists-p user-emacs-directory) (make-directory user-emacs-directory t))
(unless (file-exists-p ech:core/cache-dir) (make-directory  ech:core/cache-dir t))

;; don't know why but if not defined will crash emacs startup
(setq personal-keybindings ())

(add-to-list 'load-path ech:conf/emacs-dir)
(add-to-list 'load-path ech:core/confs-dir)
(add-to-list 'load-path ech:core/local-dir)

(defvar ech:core/package-load-hook () "hook that loads the modules")

(defun ech:core/compile-file-if-modified (file)
  "Byte compile file.el if newer than file.elc."
  (let* ((file-ext (file-name-extension file))
         (file-name
          (if (or (equal "el" file-ext) (equal "elc" file-ext))
              (file-name-sans-extension file)
            (file))))
    (if (file-newer-than-file-p (concat file-name ".el")
                                (concat file-name ".elc"))
        (byte-compile-file (concat file-name ".el")))))

(defun ech:core/requires-file(file)
  "load all elisp files in dir with a require"
  (let* ((sym (file-name-sans-extension (file-name-nondirectory file))))
    (progn
      (require (intern sym) nil t))))

(setq ech:core/processed-file-list ())

(defun advice-fun-for-load(file &rest other)
  (add-to-list 'ech:core/processed-file-list (file-name-sans-extension (file-name-nondirectory file))))

(let ((conf-files (sort (directory-files ech:core/confs-dir t ".*\\.el$") 'string< ) ))
  (defun ech:core/compile-modified-conf-files ()
    "compile all elisp files in the configuration directory"

    (advice-add 'load :before #'advice-fun-for-load)
    (mapc (lambda(x)
	    (let ((file-name (file-name-sans-extension (file-name-nondirectory x))))
	      (message (concat "**** start Compiling " file-name))
	      (setq ech:core/processed-file-list ())
	      (add-to-list 'ech:core/processed-file-list file-name)
	      (ech:core/compile-file-if-modified x)
	      (message (concat "**** end Compiling " (mapconcat #'identity (reverse ech:core/processed-file-list) " -> ")))
	      (setq ech:core/processed-file-list ())
	      )) conf-files)
  (advice-remove 'load #'advice-fun-for-load)
  )

  (defun ech:core/requires-conf-files()
    "load all elisp files in the configuration directory with a require"
    (advice-add 'load :before #'advice-fun-for-load)
    (mapc (lambda(x)
	    (let ((file-name (file-name-sans-extension (file-name-nondirectory x))))
	      (message (concat "**** start Loading " file-name))
	      (setq ech:core/processed-file-list ())
	      (ech:core/requires-file x)
	      (message (concat "**** end Loading " (mapconcat 'identity (reverse ech:core/processed-file-list) " <- ")))
	      (setq ech:core/processed-file-list ())
	      )) conf-files)
    (advice-remove 'load #'advice-fun-for-load)
    (run-hooks 'ech:core/package-load-hook)                    ;;(ref:callhook)
    )
  )

(defvar ech-use-cygwin
  (and (or (string= system-type "ms-dos") (string= system-type "windows-nt")) (getenv "CYGWIN_ROOT"))
  "use cygwin in ech-env")

(defvar cygwin-root-directory (if ech-use-cygwin
				                  (getenv "CYGWIN_ROOT")
				                "\\" ) "Root directory of cygwin installation")

(when ech-use-cygwin
  (let* ((bin-dir  (expand-file-name "bin" cygwin-root-directory))
	     (paths (list
		         bin-dir
		         (expand-file-name "usr/bin" cygwin-root-directory)
		         (expand-file-name "sbin" cygwin-root-directory)
		         (expand-file-name "usr/local/bin" cygwin-root-directory)))
	     (bash-path (expand-file-name "bash.exe" bin-dir)))
    (mapc (lambda (x) (add-to-list 'exec-path x)) paths))
  (require 'setup-cygwin)
  ;;   LOGNAME and USER are expected in many Emacs packages. Check these environment variables.
  (when (null (getenv "USER"))
    (cond
     ((getenv "USERNAME") (setenv "USER" (getenv "USERNAME")))
     ((getenv "LOGNAME") (setenv "USER" (getenv "LOGNAME")))))
  (when (and (getenv "USER") (null (getenv "LOGNAME")))
    (setenv "LOGNAME" (getenv "USER"))))

(message "Finished loading core ...")
(provide 'core)
