(setq debug-on-error t)
(setq load-prefer-newer t)

;; load core environment
(require 'core (expand-file-name "core" (file-name-directory load-file-name)))

;; compile configuration files if not yet done
(message "-- Start of compilation of Modified configuration files ...")
;; workaround until solution found for issue with error "Unknown theme ‘use-package’"
(unless (string= "wls" (getenv "CONTAINER"))
  (ech:core/compile-modified-conf-files))
(message "-- End of compilation of Modified configuration files.")
;; require the configuration files
(message "-- Start Loading configuration files ...")
(ech:core/requires-conf-files)
(message "-- End Loading configuration files.")
