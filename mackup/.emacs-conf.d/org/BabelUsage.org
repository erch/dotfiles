# -*- coding: utf-8 -*-
#+TITLE: Examples  Babel / ein
#+OPTIONS: toc:2 H:2
#+STARTUP: showall
#+STARTUP: latexpreview

* Ein

#+NAME: c5e39567-9a5d-4751-a62c-683d63fa0304
#+BEGIN_SRC ein-python  :session localhost :results output verbatim :eval no-export
import sys
a = 14500
b = a+1000
sys.version
print('hello')
#+END_SRC

#+RESULTS: c5e39567-9a5d-4751-a62c-683d63fa0304
: [....]
* Jupyter
:PROPERTIES:
:header-args:jupyter-python:    :session defpysess
:header-args:jupyter-python:    :cache yes
:END:
#+BEGIN_SRC jupyter-python :eval no-export
print('hello2')
#+END_SRC

#+RESULTS:
: hello2

#+BEGIN_SRC jupyter-python :eval no-export
 for i in range(0,5):
     print('net = %s, cidr = %s' % ((i % 3),int((i / 3))))
#+END_SRC

#+RESULTS:
: net = 0, cidr = 0
: net = 1, cidr = 0
: net = 2, cidr = 0
: net = 0, cidr = 1
: net = 1, cidr = 1
** Drawing
Need to have this in the configuration
  #+BEGIN_SRC lisp
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)
#+END_SRC
*** Session initialisation
  #+BEGIN_SRC jupyter-python :results output :exports both :cache no :eval no-export
    %matplotlib inline
    import matplotlib
    import matplotlib.pyplot as plt
    import numpy as np

    print ("Finished imports")
#+END_SRC

#+RESULTS[912536e295a5830d46d8e3ec327fd437ee563b81]:
: Finished imports
*** plotting of a simple graph
#+BEGIN_SRC jupyter-python :file /tmp/res2.png :exports both :cache no :eval no-export
  #matplotlib.use('Agg')
  prices = range(1,10)
  def compute_cmf(p,low,high):
      if low == high:
        return 0
      return (p - low) - (high - p) / (high - low)

  fig=plt.figure(figsize=(4,2))
  cmfs_low = [compute_cmf(x,x-3,x)  for x in prices]
  plt.plot(prices,cmfs_low)
  plt.show()
#+END_SRC

#+RESULTS[c1d3058d3f4e2ea65bf44086632e5276e8bff0a7]:
[[file:/tmp/res2.png]]
*** Test calcul moyenne
(C-c C-x C-l to show a latex preview, toggle with a prefix)
\begin{eqnarray*}
M_{n} =\frac{\sum_{i=1}^{n} x_{i}}{n} \\
n \cdot M_{n} = x_{n} + \sum_{i=1}^{n-1} x_{i} \\
\frac{n  \cdot M_{n}}{n-1} = \frac{x_{n}}{n-1} + M_{n-1} \\
M_{n} = \frac{x_{n} + (n -1) M_{n}}{n}
\end{eqnarray*}
#+BEGIN_SRC jupyter-python  :exports both :cache no :eval no-export
  import statistics
  nums = [4,5,8,1,5]
  cur_av = 0
  for i,x in enumerate(nums):
      n = i + 1
      av = (x + ((n -1) * cur_av)) / n
      print("n=%s => av = %s [%s]" % (n,av,statistics.mean(nums[:n])))
      cur_av = av
#+END_SRC

#+RESULTS:
: n=1 => av = 4.0 [4]
: n=2 => av = 4.5 [4.5]
: n=3 => av = 5.666666666666667 [5.666666666666667]
: n=4 => av = 4.5 [4.5]
: n=5 => av = 4.6 [4.6]
