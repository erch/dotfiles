((org-mode . ((org-src-preserve-indentation t)
          (eval . (setq-local org-ech:Org/root-dir (f-join (projectile-project-root) "mackup" ".emacs-conf.d")))
          (eval . (unless (f-exists? org-ech:Org/root-dir) (f-mkdir org-ech:Org/root-dir)))

          (eval . (setq-local org-ech:Org/conf-dir (f-join org-ech:Org/root-dir "confs")))
          (eval . (unless (f-exists? org-ech:Org/conf-dir) (f-mkdir org-ech:Org/conf-dir)))

          (eval . (setq-local org-ech:Org/local-dir (f-join org-ech:Org/root-dir "local")))
          (eval . (unless (f-exists? org-ech:Org/local-dir) (f-mkdir org-ech:Org/local-dir)))

          (eval . (setq-local org-ech:Org/statics-dir (f-join org-ech:Org/root-dir "statics")))
          (eval . (unless (f-exists? org-ech:Org/statics-dir) (f-mkdir org-ech:Org/statics-dir)))

          (eval . (setq-local org-ech:Org/publish-dir (f-join org-ech:Org/root-dir "docs")))
          (eval . (unless (f-exists? org-ech:Org/publish-dir) (f-mkdir org-ech:Org/publish-dir)))

          (eval . (setq-local org-ech:Org/test-dir (f-join org-ech:Org/root-dir "test")))
          (eval . (unless (f-exists? org-ech:Org/test-dir) (f-mkdir org-ech:Org/test-dir)))

          (eval . (setq-local org-publish-project-alist
                 (list (list "org-conf"
                        :base-directory (f-join org-ech:Org/root-dir "org")
                        :publishing-directory (f-join org-ech:Org/publish-dir "html")
                        :base-extension "org"
                        :exclude ()
                        :recursive t
                        :section-numbers nil
                        :table-of-contents nil
                        :publishing-function 'org-html-publish-to-html
                        :html-head (concat
                                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://fniessen.github.io/org-html-themes/styles/readtheorg/css/htmlize.css\"/>"
                                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://fniessen.github.io/org-html-themes/styles/readtheorg/css/readtheorg.css\"/>"
                                    "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>"
                                    "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>"
                                    "<script type=\"text/javascript\" src=\"https://fniessen.github.io/org-html-themes/styles/lib/js/jquery.stickytableheaders.min.js\"></script>"
                                    "<script type=\"text/javascript\" src=\"https://fniessen.github.io/org-html-themes/styles/readtheorg/js/readtheorg.js\"></script>")))))
          )))
