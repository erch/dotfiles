(require 'package-conf)
(require 'utility-funcs) ;; for libs
(require 'completion) ;; for company
(require 'global-conf) ;; for flycheck
(require 'babel-conf)
(require 'programming) ;; for eglot

(use-package pylookup
 :straight '(pylookup :type built-in)
 :diminish ""
 :custom
   ;; directory where to find the pylookup.py program
   (pylookup-root ech:core/local-dir )
   ;; directory where the database is installed by running something like this:
   ;; ./pylookup.py -u http://docs.python.org
   (pylookup-db2-file (expand-file-name  "pylookup/pylookup.db" ech:core/statics-dir))
   (pylookup-search-options '("--insensitive" "0" "--desc" "0")))

  (defvar ech:python//module-find-exclude-dirs
    (list "^\\..*"
          "^__.*"
          ".*\\.egg-info$") "list of directory name patterns to exclude when searching for python module directories")

  (defun ech:python//module-candidate-p (x)
    "private predicate that check if a file name is a good candidate for searching for python module roots"
    (if (file-regular-p x) t
      (let ((file-name (file-name-nondirectory x)))
        (not (seq-some (lambda (x) (string-match-p x file-name)) ech:python//module-find-exclude-dirs)))))

  (defun ech:python//find-python-module-here (dir)
    "private function that search for directories that contain a file named __init__.py and returns
          their parent parent directories in a list.
          In python when we have import a.b the file b.py is search in a directory X/a/ with:
            - a and b must contain a file named __init__.py
            - X is a directory name that can be a component of the PYTHONPATH.
          Here we want to return potential Xs, so with search for first directories with an __init__.py file
          and return a/..
         "
    (let ((content
           (if dir (seq-filter 'ech:python//module-candidate-p (directory-files dir t)) nil)))
      (cond
       ((null content) ())
       ((seq-contains (mapcar 'file-name-nondirectory content) "__init__.py")
        (list (file-name-directory dir)))
       (t
        (let ((content-dirs (seq-filter 'file-directory-p content)))
          (seq-uniq (mapcan 'ech:python//find-python-module-here content-dirs)))))))

  (defun ech:python/find-python-module-roots (&optional dir)
    (when (or dir (projectile-project-root))
      (let* (
             (proj-root (or dir (projectile-project-root)))
             (candidates (list
                          (expand-file-name "src/python" proj-root)
                          (expand-file-name "src/main" proj-root)
                          (expand-file-name "python" proj-root)
                          (expand-file-name "main" proj-root))))
        (ech:python//find-python-module-here
         (seq-find
          (lambda (x) (and (file-exists-p x) (file-directory-p x)))
          candidates nil)))))
  ;;(ech:python/find-python-module-roots)

  (defun ech:python/adjust-python-path (root-dir)
    (when root-dir
      (let* ((pypath (getenv "PYTHONPATH"))
             (module-roots (ech:python/find-python-module-roots root-dir))
             (new-pypath (append
                          module-roots
                          (if pypath (list pypath) ()))))
        (setenv "PYTHONPATH"  (mapconcat 'identity new-pypath path-separator))
        (setenv "MYPYPATH" (getenv "PYTHONPATH")))))

  (defvar ech:python/after-python-env-functions () "a hook run after the presence of a virtual env has been checked. Its functions must accept a parameter set to the virtual env directory or nil if none is found.")

  (defvar-local ech:python/venv-dir nil "set to the directory that contains the python virtual environment related to the buffer when one is found, otherwise equal nil")

  (defun ech:python/find-venv-root (proj-dir dir)
    (if (file-exists-p (f-join dir ".venv"))
        dir
      (unless (string-equal (f-slash (f-canonical proj-dir)) (f-slash (f-canonical dir)))
        (ech:python/find-venv-root proj-dir (f-dirname dir))))
    )

  (defun ech:python/set-env(&optional dir)
    "detect a python virtual environment and change the process environment variables accordingly to this env.
     Note: make the assumption that projectile is loaded.
    "
    (message "python/set-env , buffer: %s" (current-buffer))
    (let* ((proj-root (projectile-project-root dir))
           (buff-file (buffer-file-name (current-buffer)))
           (venv-root-dir (if (and proj-root buff-file) (ech:python/find-venv-root proj-root (f-dirname buff-file)) nil))
           (venv-dir (if venv-root-dir (f-canonical (f-join venv-root-dir ".venv" "bin"))))
           (new-path (if venv-dir (ech-path-mungle venv-dir) nil)))
      (when venv-root-dir
        (message (format "Found virtual Env for dir: %s" venv-root-dir))
        (make-local-variable 'process-environment)
        (setenv "PATH"  (mapconcat 'identity  new-path path-separator))
        (setenv "VIRTUAL_ENV" venv-dir)
        (setenv "PYTHON_HOME" (format "%s:%s" venv-dir venv-dir))
        (setenv "VIRTUAL_ENV_DISABLE_PROMPT" "yes")
        (setq-local exec-path new-path)
        (setq-local ech:python/venv-dir venv-dir)
        (message (format "exec-path = %s, venvdir = %s" exec-path venv-dir)))
      (ech:python/adjust-python-path (or venv-root-dir proj-root))
    (run-hook-with-args 'ech:python/after-python-env-functions ech:python/venv-dir)))

 (add-hook 'python-mode-hook #'ech:python/set-env)

(eval-and-compile
  (use-package python-mode)
  ;;  (use-package lsp-python-ms
  ;;    :ensure t
  ;;    :init
  ;;    (setq lsp-python-ms-auto-install-server t
  ;;          lsp-python-ms-executable "~/opt/python-language-server/output/bin/Release/linux-x64/publish/Microsoft.Python.LanguageServer")
  ;;  )
  )

(defun ech:python/setup-hook (virtdir)
  "hook to python mode"
  (message "running python mode setup")
  ;;(flyspell-prog-mode)
  (setq-local python-indent-offset 4)
  ;; Set `forward-sexp-function' to nil in python-mode. See
  ;; http://debbugs.gnu.org/db/13/13642.html
  (setq-local forward-sexp-function nil)
  ;; browse documentation with eww
  (when virtdir
    (eglot-ensure)
    )
  )

(add-hook 'ech:python/after-python-env-functions #'ech:python/setup-hook)

(add-to-list 'ech:babel/babel-languages 'python)
;; we want also that org buffer inside a virtual env have their environment setup properly.
(add-hook 'ech:core/package-load-hook (lambda ()  (add-hook 'org-mode-hook #'ech:python/set-env)))

(provide 'python-conf)
