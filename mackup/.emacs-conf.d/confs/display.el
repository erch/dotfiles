(add-to-list 'ech:core/processed-file-list "display")
(require 'core)
(require 'package-conf)
(require 'global-conf)

(eval-and-compile
  ;; nicer icons
  (use-package all-the-icons)
  (use-package all-the-icons-dired)
  )

(defun ech-load-themes(themes)
  (mapc (lambda (theme) (load-theme theme t t)) themes))

(defun ech-use-theme (th)
  (progn
    (mapc 'disable-theme custom-enabled-themes)
    (when (not (custom-theme-p th))
      (load-theme th t t)
      )
    (enable-theme th))
  )

(setq ech-theme-list '(user))
(setq cur-theme-pos 0)

(defun ech-next-theme()
  (interactive)
  (unless (null ech-theme-list)
    (let* ((next-theme-pos (% (1+ cur-theme-pos) (length  ech-theme-list)))
           (theme (nth next-theme-pos ech-theme-list)))
      (ech-use-theme theme)
      (setq cur-theme-pos next-theme-pos)
      (message (format "Current theme: %s (%d/%d)" theme cur-theme-pos (length  ech-theme-list))))))

(defun ech-print-theme()
  (interactive)
  (message (format "%s" (nth cur-theme-pos ech-theme-list)))
  )

(key-chord-define-global ",t" 'ech-next-theme)

(defun ech-add-to-themes (theme-list)
  (mapc (lambda (th) (add-to-list 'ech-theme-list th)) theme-list)
  )

;; emacs default themes
(ech-add-to-themes
 '(
   deeper-blue
   light-blue
   manoj-dark
   misterioso
   tango-dark
   tango
   tsdh-dark
   tsdh-light
   wheatgrass
   whiteboard
   wombat))

;; spacemacs themes
;; TODO: check them to known how to install them
;;       '(afternoon-theme :themes ()
;;         alect-themes
;;         ample-theme
;;         ample-zen-theme
;;         apropospriate-theme
;;         anti-zenburn-theme
;;         badwolf-theme
;;         birds-of-paradise-plus-theme
;;         bubbleberry-theme
;;         busybee-theme
;;         cherry-blossom-theme
;;         clues-theme
;;         color-theme-sanityinc-solarized
;;         color-theme-sanityinc-tomorrow
;;         cyberpunk-theme
;;         dakrone-theme
;;         darkburn-theme
;;         darkmine-theme
;;         darkokai-theme
;;         darktooth-theme
;;         django-theme
;;         dracula-theme
;;         espresso-theme
;;         exotica-theme
;;         farmhouse-theme
;;         flatland-theme
;;         flatui-theme
;;         gandalf-theme
;;         gotham-theme
;;         grandshell-theme
;;         gruber-darker-theme
;;         gruvbox-theme
;;         hc-zenburn-theme
;;         hemisu-theme
;;         heroku-theme
;;         inkpot-theme
;;         ir-black-theme
;;         jazz-theme
;;         jbeans-theme
;;         light-soap-theme
;;         lush-theme
;;         madhat2r-theme
;;         majapahit-theme
;;         material-theme
;;         minimal-theme
;;         moe-theme
;;         molokai-theme
;;         monokai-theme
;;         monochrome-theme
;;         mustang-theme
;;         naquadah-theme
;;         noctilux-theme
;;         obsidian-theme
;;         occidental-theme
;;         omtose-phellack-theme
;;         oldlace-theme
;;         organic-green-theme
;;         phoenix-dark-mono-theme
;;         phoenix-dark-pink-theme
;;         planet-theme
;;         professional-theme
;;         purple-haze-theme
;;         railscasts-theme
;;         rebecca-theme
;;         reverse-theme
;;         seti-theme
;;         smyx-theme
;;         soft-charcoal-theme
;;         soft-morning-theme
;;         soft-stone-theme
;;         solarized-theme
;;         soothe-theme
;;         spacegray-theme
;;         subatomic-theme
;;         subatomic256-theme
;;         sublime-themes
;;         sunny-day-theme
;;         tango-2-theme
;;         tango-plus-theme
;;         tangotango-theme
;;         tao-theme
;;         ;; contains error
;;         ;; tommyh-theme
;;         toxi-theme
;;         twilight-anti-bright-theme
;;         twilight-bright-theme
;;         twilight-theme
;;         ujelly-theme
;;         underwater-theme
;;         white-sand-theme
;;         zen-and-art-theme
;;         zenburn-theme)))
;;  (mapc)

(straight-use-package 'ample-theme)
(ech-add-to-themes '(ample
                     ample-flat
                     ample-light))

(straight-use-package 'poet-theme)
(ech-add-to-themes '(poet))

(straight-use-package '(darkokai-theme :type git :host github :repo "NicolasPetton/zerodark-theme"))
(ech-add-to-themes `(darkokai))

(straight-use-package 'dracula-theme)
(ech-add-to-themes `(dracula))

(straight-use-package 'panda-theme)
(ech-add-to-themes `(panda))

(straight-use-package 'monotropic-theme)
(ech-add-to-themes `(monotropic))
;; this theme needs some evolution in next theme : next theme should be able to call a function
;; that return the theme.
;;(defun load-sanityinc-theme(mode)
;;  (let ((name (color-theme-sanityinc-tomorrow--theme-name mode)))
;;     (load-theme name t t)))
;;(use-package color-theme-sanityinc-tomorrow
;;  :ensure t
;;  :defer t
;;  :config
;;  (progn
;;    (mapc 'load-sanityinc-theme '(day night blue bright eighties)))
;;  )

(straight-use-package 'apropospriate-theme)
(ech-add-to-themes '(apropospriate-dark
                     apropospriate-light))

;;;; change treemacs icons
(straight-use-package 'doom-themes)

;; TODO: see how to set these vars after the load of the theme
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
(ech-add-to-themes
 `(
   doom-one ;doom-themes' flagship theme, inspired by Atom's One Dark themes
   doom-one-light ;light version of doom-one (thanks to ztlevi)
   doom-vibrant; a slightly more vibrant version of doom-one
   doom-city-lights
   doom-dracula
   doom-Iosvkem; adapted from Iosvkem (thanks to neutaaaaan)
   doom-molokai; based on Textmate's monokai
   doom-nord
   doom-nord-light
   doom-opera
   doom-opera-light
   doom-nova; adapted from Nova (thanks to bigardone)
   doom-peacock; based on Peacock from daylerees' themes (thanks to teesloane)
   doom-solarized-light; light variant of Solarized (thanks to fuxialexander)
   doom-sourcerer; based on Sourcerer (thanks to defphil)
   doom-spacegrey; I'm sure you've heard of it (thanks to teesloane)
   doom-tomorrow-night; by Chris Kempson
   doom-tomorrow-day; by Chris Kempson (thanks to emacswatcher)
   ))


;;(straight-use-package '(leuven-theme
;;	:fetcher github
;;	:repo "fniessen/emacs-leuven-theme" ) )
;;(ech-add-to-themes
;; '(leuven
;;   leuven-dark))

(straight-use-package 'alect-themes)
(ech-add-to-themes
 `(alect-black-alt
   alect-dark-alt
   alect-light-alt
   alect-black
   alect-dark
   alect-light
   ))

(straight-use-package 'zenburn-theme)
(ech-add-to-themes
 `(zenburn))

(ech-use-theme 'doom-one-light)
					;(ech-use-theme 'leuven)

(eval-and-compile
  ;; nice mode line - not working
  ;; (use-package powerline
  ;;   :config
  ;;   (custom-set-faces '(mode-line ((t (:foreground "#030303" :background "grey" :box nil)))))
  ;;   (powerline-default-theme))

  (use-package smartparens
    :diminish ""
    :config
    (progn
      (setq sp-show-pair-from-inside nil)
      (require 'smartparens-config)
      (add-hook 'prog-mode-hook #'smartparens-mode)
      ))

  (use-package rainbow-delimiters
    :config
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

  (use-package ace-window
    :config
    (progn
      (setq aw-keys '(?q ?s ?d ?f ?j ?k ?l ?m ?q))
      (key-chord-define-global "ww" 'ace-window)))

  ;;(use-package helm-spaces
  ;;  :ensure t
  ;;  :chords (",s" . helm-spaces)
  ;;  :bind ("C-c s" . helm-spaces)
  ;;  :init
  ;;  (progn
  ;;    (use-package spaces
  ;;      :straight-use-package (spaces
  ;;               :fetcher github
  ;;               :repo "chumpage/chumpy-windows"
  ;;               :files ("spaces.el"))))
  ;;  :config
  ;;  (setq helm-spaces-new-space-query nil))

  (use-package winner
    :init
    (progn
      (winner-mode t)
      (setq ech-boring-buffers '("*Completions*"
				 "*Compile-Log*"
				 "*inferior-lisp*"
				 "*Fuzzy Completions*"
				 "*Apropos*"
				 "*Help*"
				 "*cvs*"
				 "*Buffer List*"
				 "*Ibuffer*"
				 "*esh command on file*"
				 ))
      (setq winner-boring-buffers
            (append winner-boring-buffers ech-boring-buffers))
      (winner-mode t)))

;;  (use-package windmove
;;    :bind ("C-c w" . #'hydra-window/body)
;;    :config
;;    (defhydra hydra-window ()
;;      "
;;Movement^^        ^Split^         ^Switch^		^Resize^
;;----------------------------------------------------------------
;;_h_ ←       	_v_ertical    	_b_uffer		_q_ X←
;;_j_ ↓        	_x_ horizontal	_f_ind files	_w_ X↓
;;_k_ ↑        	_z_ undo      	_a_ce 1		_e_ X↑
;;_l_ →        	_Z_ reset      	_s_wap		_r_ X→
;;_F_ollow		_D_lt Other   	_S_ave		max_i_mize
;;_SPC_ cancel	_o_nly this   	_d_elete
;;"
;;      ("h" windmove-left )
;;      ("j" windmove-down )
;;      ("k" windmove-up )
;;      ("l" windmove-right )
;;      ("q" hydra-move-splitter-left)
;;      ("w" hydra-move-splitter-down)
;;      ("e" hydra-move-splitter-up)
;;      ("r" hydra-move-splitter-right)
;;      ("b" helm-mini)
;;0      ("f" helm-find-files)
;;      ("F" follow-mode)
;;      ("a" (lambda ()
;;             (interactive)
;;             (ace-window 1)
;;             (add-hook 'ace-window-end-once-hook
;;                       'hydra-window/body))
;;       )
;;      ("v" (lambda ()
;;             (interactive)
;;             (split-window-right)
;;             (windmove-right))
;;       )
;;      ("x" (lambda ()
;;             (interactive)
;;             (split-window-below)
;;             (windmove-down))
;;       )
;;      ("s" (lambda ()
;;             (interactive)
;;             (ace-window 4)
;;             (add-hook 'ace-window-end-once-hook
;;                       'hydra-window/body)))
;;      ("S" save-buffer)
;;      ("d" delete-window)
;;      ("D" (lambda ()
;;             (interactive)
;;             (ace-window 16)
;;             (add-hook 'ace-window-end-once-hook
;;                       'hydra-window/body))
;;       )
;;      ("o" delete-other-windows)
;;      ("i" ace-maximize-window)
;;      ("z" (progn
;;             (winner-undo)
;;             (setq this-command 'winner-undo))
;;       )
;;      ("Z" winner-redo)
;;      ("SPC" nil)
;;      ))

  ;;(defvar windmove-map (make-sparse-keymap))
  ;;(define-key sacha/windmove-map "h" 'windmove-left)
  ;;(define-key sacha/windmove-map "t" 'windmove-up)
  ;;(define-key sacha/windmove-map "n" 'windmove-down)
  ;;(define-key sacha/windmove-map "s" 'windmove-right)
  ;;(define-key sacha/windmove-map "[left]" 'windmove-left)
  ;;(define-key sacha/windmove-map "[up]" 'windmove-up)
  ;;(define-key sacha/windmove-map "[down]" 'windmove-down)
  ;;(define-key sacha/windmove-map "[right]" 'windmove-right)
  ;;(key-chord-define-global "yy"     sacha/windmove-map)


  ;; not anymore usefull will use display-alist instead.
  ;;(use-package shackle
  ;;  :diminish shackle-mode
  ;;  :init
  ;;  (shackle-mode)
  ;;  :config
  ;;  (setq shackle-default-alignment 'below
  ;;        shackle-default-rule '(:select t)
  ;;        shackle-rules
  ;;        ;; CONDITION             :regexp    :select     :inhibit-window-quit   :size+:align|:other  :same|:popup
  ;;        '((compilation-mode                 :select nil                                                         )
  ;;          ("*Shell Command Output*"         :select nil                                                         )
  ;;          ("\\*terminal.*?\\*\\'" :regexp t :align t :popup t :ratio 0.4)
  ;;          (term-mode                         :regexp nil :align t :popup t :ratio 0.4)
  ;;          ("*Help*"                         :select t                          :other t  :align t               )
  ;;          ("*Completions*"                                                     :size 0.3 :align t               )
  ;;          ("*Messages*"                     :select nil :inhibit-window-quit t :other t                         )
  ;;          ("\\*[Wo]*Man.*\\*"    :regexp t  :select t   :inhibit-window-quit t :other t                         )
  ;;          ("*Calendar*"                     :select t                          :size 0.3 :align below           )
  ;;          ("*info*"                         :select t   :inhibit-window-quit t                         :same t  )
  ;;          ))
  ;;  )
  )
(provide 'display)
