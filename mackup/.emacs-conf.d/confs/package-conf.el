(require 'core)

(defvar bootstrap-version)
(setq straight-check-for-modifications nil
      straight-use-package-version 'straight
      straight-use-package-by-default t
      straight-enable-use-package-integration t
      straight-recipes-gnu-elpa-use-mirror t
      straight-allow-recipe-inheritance nil)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

;; we want use the use-package hooks : pre-init-hook, post-init-hook, pre-config-hook, post-config-hook
(setq use-package-inject-hooks t)
;; by default use package will defer the load of features.
(setq use-package-always-defer t)
;; we want to use :diminish and :delight keywords with use-package
;; loading them directly with straight
(straight-use-package  'diminish)

(straight-use-package 'delight)

;; Key chords let us bind functions to sequential key presses like jj
;; we can add a keyword to use-package for the key chords
(eval-and-compile
  (use-package key-chord
    :demand t
    :config
    (progn
      (key-chord-mode 1)
      (setq key-chord-two-keys-delay 0.1  	; 0.05 or 0.1
            key-chord-one-key-delay 0.2))))

(provide 'package-conf)
