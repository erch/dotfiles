;; install package that are use globaly
(add-to-list 'ech:core/processed-file-list "global-conf")
(require 'package-conf)
(require 'core)


(eval-and-compile
  (use-package hydra
    :demand t  ;; used by other packages that are not lazy loaded
    )

  ;; Emacs undo model has not a real concept of "redo" - you simply undo the undo.
  ;; This lets you use C-x u (undo-tree-visualize) to visually walk through the changes you've made,
  ;; undo back to a certain point (or redo), and go down different branches.
  (use-package undo-tree
    :diminish ""
    :demand t
    :config
    (progn
      (global-undo-tree-mode)
      (setq undo-tree-visualizer-timestamps t)
      (setq undo-tree-visualizer-diff t)))

  ;; file manager
  ;; DISABLED: needs functions in display.el which needs function in global.el
  ;;(use-package treemacs
  ;;  :ensure t
  ;;  :quelpa t
  ;;  :commands (treemacs-select-window treemacs--window-number-ten
  ;;                                    treemacs-current-visibility)
  ;;  :bind ("C-c t" . treemacs)
  ;;  :config
  ;;  (progn
  ;;    (setq treemacs-follow-after-init t
  ;;          treemacs-width 35
  ;;          treemacs-position 'left
  ;;          treemacs-is-never-other-window nil
  ;;          treemacs-silent-refresh nil
  ;;          treemacs-indentation 2
  ;;          treemacs-change-root-without-asking nil
  ;;          treemacs-sorting 'alphabetic-desc
  ;;          treemacs-show-hidden-files t
  ;;          treemacs-never-persist nil
  ;;          treemacs-goto-tag-strategy 'refetch-index
  ;;          ;; bug in git icon update
  ;;          treemacs-git-mode nil
  ;;          )
  ;;    (treemacs-follow-mode t)
  ;;    (treemacs-filewatch-mode t)
  ;;    ;; workaround because treemacs windows was not taken into account by ace-window
  ;;    (setq aw-ignored-buffers (delete 'treemacs-mode aw-ignored-buffers))
  ;;    ;; changing the root icon that is ugly by default
  ;;    (setq treemacs-icon-root-png
  ;;          (format " %s "
  ;;                  (all-the-icons-octicon
  ;;                   "package"
  ;;                   :v-adjust -0.1
  ;;                   :height 1.1
  ;;                   :face 'font-lock-string-face)))
  ;;
  ;;    ;;(add-to-list 'spacemacs-window-split-ignore-prefixes
  ;;    ;;             treemacs--buffer-name-prefix))
  ;;    (use-package golden-ratio
  ;;      :ensure t
  ;;      :quelpa t
  ;;      :config
  ;;      (add-to-list 'golden-ratio-exclude-buffer-regexp
  ;;                   (rx "*Treemacs" (0+ any))))
  ;;    ;(use-package treemacs-projectile
  ;;    ;  :ensure t
  ;;                                        ;  :quelpa t)
  ;;    ))

  ;; Which Key
  ;;displays the key bindings following your currently entered incomplete command (a prefix) in a popup.
  ;;For example, after enabling the minor mode if you enter C-x and wait for the default of 1 second the
  ;;minibuffer will expand with all of the available key bindings that follow C-x (or as many as space
  ;;allows given your settings).
  (use-package which-key
    :demand t
    :init
    (setq which-key-separator " ")
    (setq which-key-prefix-prefix "+")
    :config
    (which-key-mode))

  ;; https://jamiecollinson.com/blog/my-emacs-config/#setup
  (use-package flycheck
    :custom
    (flycheck-highlighting-mode 'lines)
    (flycheck-indication-mode 'left-fringe) ;How Flycheck indicates errors and warnings in the buffer fringes
    ;; don't check on mode-enabled which creates issue on new file
    (flycheck-check-syntax-automatically '(save))
    :hook
    (after-init-hook . #'global-flycheck-mode)
    :config
    (progn

      (flycheck-define-error-level 'error
				                   :severity 2
				                   :overlay-category 'flycheck-error-overlay
				                   :fringe-bitmap 'flycheck-fringe-bitmap-ball
				                   :fringe-face 'flycheck-fringe-error)
      (flycheck-define-error-level 'warning
				                   :severity 1
				                   :overlay-category 'flycheck-warning-overlay
				                   :fringe-bitmap 'flycheck-fringe-bitmap-ball
				                   :fringe-face 'flycheck-fringe-warning)
      (flycheck-define-error-level 'info
				                   :severity 0
				                   :overlay-category 'flycheck-info-overlay
				                   :fringe-bitmap 'flycheck-fringe-bitmap-ball
				                   :fringe-face 'flycheck-fringe-info)

      ;; install: pip install proselint
      (flycheck-define-checker proselint
			                   "A linter for prose."
			                   :command ("proselint" source-inplace)
			                   :error-patterns
			                   ((warning line-start (file-name) ":" line ":" column ": "
				                         (id (one-or-more (not (any " "))))
				                         (message (one-or-more not-newline)
						                          (zero-or-more "\n" (any " ") (one-or-more not-newline)))
				                         line-end))
			                   :modes (text-mode markdown-mode gfm-mode org-mode))
      (add-to-list 'flycheck-checkers 'proselint))))

;; an hydra for dired mode
(use-package dired
  :straight '(dired :type built-in)
  :demand t
  :bind (:map dired-mode-map ("." . #'hydra-dired/body))
  :config
  (defhydra hydra-dired (:hint nil :color pink)
    "
_+_ mkdir          _v_iew           _m_ark             _(_ details        _i_nsert-subdir    wdired
_C_opy             _O_ view other   _U_nmark all       _)_ omit-mode      _$_ hide-subdir    C-x C-q : edit
_D_elete           _o_pen other     _u_nmark           _l_ redisplay      _w_ kill-subdir    C-c C-c : commit
_R_ename           _M_ chmod        _t_oggle           _g_ revert buf     _e_ ediff          C-c ESC : abort
_Y_ rel symlink    _G_ chgrp        _E_xtension mark   _s_ort             _=_ pdiff
_S_ymlink          ^ ^              _F_ind marked      _._ toggle hydra   \\ flyspell
_r_sync            ^ ^              ^ ^                ^ ^                _?_ summary
_z_ compress-file  _A_ find regexp
_Z_ compress       _Q_ repl regexp

T - tag prefix
"
    ("\\" dired-do-ispell)
    ("(" dired-hide-details-mode)
    (")" dired-omit-mode)
    ("+" dired-create-directory)
    ("=" diredp-ediff)         ;; smart diff
    ("?" dired-summary)
    ("$" diredp-hide-subdir-nomove)
    ("A" dired-do-find-regexp)
    ("C" dired-do-copy)        ;; Copy all marked files
    ("D" dired-do-delete)
    ("E" dired-mark-extension)
    ("e" dired-ediff-files)
    ("F" dired-do-find-marked-files)
    ("G" dired-do-chgrp)
    ("g" revert-buffer)        ;; read all directories again (refresh)
    ("i" dired-maybe-insert-subdir)
    ("l" dired-do-redisplay)   ;; relist the marked or singel directory
    ("M" dired-do-chmod)
    ("m" dired-mark)
    ("O" dired-display-file)
    ("o" dired-find-file-other-window)
    ("Q" dired-do-find-regexp-and-replace)
    ("R" dired-do-rename)
    ("r" dired-do-rsynch)
    ("S" dired-do-symlink)
    ("s" dired-sort-toggle-or-edit)
    ("t" dired-toggle-marks)
    ("U" dired-unmark-all-marks)
    ("u" dired-unmark)
    ("v" dired-view-file)      ;; q to exit, s to search, = gets line #
    ("w" dired-kill-subdir)
    ("Y" dired-do-relsymlink)
    ("z" diredp-compress-this-file)
    ("Z" dired-do-compress)
    ("q" nil)
    ("." nil :color blue)))

(use-package recentf
  :demand t
  :hook
  (find-file-hook . (lambda () (unless recentf-mode
                                 (recentf-mode)
                                 (recentf-track-opened-file))))
  :custom
  (recentf-save-file (expand-file-name "recentf" ech:core/cache-dir))
  (recentf-max-saved-items 1000)
  (recentf-auto-cleanup 'never)
  :config
  (progn
    ;; run recentf-cleanup if run changed
    (mapc (lambda (e) (add-to-list 'recentf-exclude e))  (list (expand-file-name ech:core/cache-dir)
                                                               "/usr/share/emacs/"))
    (recentf-cleanup)
    ))

(use-package savehist
  :demand t
  :init
  (progn
    ;; Minibuffer history
    (setq savehist-file (expand-file-name "savehist" ech:core/cache-dir)
          enable-recursive-minibuffers t ; Allow commands in minibuffers
          history-length 1000
          savehist-additional-variables '(mark-ring
                                          global-mark-ring
                                          search-ring
                                          regexp-search-ring
                                          extended-command-history)
          savehist-autosave-interval 60)
    (savehist-mode t)))

(use-package saveplace
  :straight '(saveplace :type built-in)
  :custom
  (save-place-file (expand-file-name "places" ech:core/cache-dir))
  :hook
  (after-init-hook . #'save-place-mode))

(provide 'global-conf)
