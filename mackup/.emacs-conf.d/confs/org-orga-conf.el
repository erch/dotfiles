;;---- loading dependencies
(require 'org-conf)
(require 'package-conf)
(require 'utility-funcs)
(require 'debug-conf)
(require 'ert)                  ;; for unit tests

;;---- constant definition
  (defconst ech:org/project-dname "Projects" "Projects dir")
  (defconst ech:org/area-dname "Areas" "Areas dir")
  (defconst ech:org/resource-dname "Resources" "Resources dir")
  (defconst ech:org/archive-dname "Archives" "Archives dir")
  (defconst ech:org/inbox-fname "Inbox.org" "Archives dir")
  (defconst ech:org/tickler-fname "Ticklers.org" "Archives dir")
  ;; orga-root-dir and orga-directory can be different.
  (defconst ech:org/orga-root-dir (file-name-as-directory (expand-file-name "Orga" (getenv "PROJECT_DIR"))) "root dir for Orga")
  (defconst ech:org/orga-dir ech:org/orga-root-dir)
  (defconst ech:org/project-dir (file-name-as-directory (expand-file-name ech:org/project-dname ech:org/orga-dir)) "directory where to find project files")
  (defconst ech:org/area-dir (file-name-as-directory (expand-file-name ech:org/area-dname ech:org/orga-dir)) "directory where to find area files")
  (defconst ech:org/resource-dir (file-name-as-directory (expand-file-name ech:org/resource-dname ech:org/orga-dir)) "directory where to find resource files")
  (defconst ech:org/archive-dir (file-name-as-directory (expand-file-name ech:org/archive-dname ech:org/orga-dir)) "directory where to find area files")
  (defconst ech:org/archived-area-dir (file-name-as-directory (f-join ech:org/archive-dir ech:org/area-dname)) "directory where to find archived area files")
  (defconst ech:org/archived-project-dir (file-name-as-directory (expand-file-name ech:org/project-dname ech:org/archive-dir)) "directory where to find archived project files")
  (defconst ech:org/archived-resource-dir (file-name-as-directory (expand-file-name ech:org/resource-dname ech:org/archive-dir)) "directory where to find archived resource files")
  (defconst ech:org/inbox-filepath (expand-file-name ech:org/inbox-fname ech:org/orga-dir) "Inbox file")
  (defconst ech:org/tickler-filepath (expand-file-name ech:org/tickler-fname ech:org/orga-dir) "Tickler file")
  (mapc (lambda (x) (unless (file-exists-p x) (make-directory  x t))) (list ech:org/project-dir ech:org/area-dir ech:org/resource-dir ech:org/archive-dir ech:org/archived-area-dir ech:org/archived-project-dir ech:org/archived-resource-dir))
  (defconst ech:org/orga-file-regex (concat (regexp-quote ech:org/orga-dir) ".*?\\([^/]*\\)\\.org$") "regex for testing org files")
  (defconst ech:org/project-file-regex (concat (regexp-quote ech:org/project-dir) "\\([^/]*\\)\\.org$") "regex for testing Project org files")
  (defconst ech:org/area-file-regex (concat (regexp-quote ech:org/area-dir) "\\([^/]*\\)\\.org$") "regex for testing Area org files")
  (defconst ech:org/resource-file-regex (concat (regexp-quote ech:org/resource-dir) "\\([^/]*\\)/\\([^/]*\\)\\.org$") "regex for testing resource org files")
  (defconst ech:org/archive-file-regex (concat (regexp-quote ech:org/archive-dir) "\\([^/]*\\)/.*?\\([^/]*\\)\\.org$") "regex for testing Archive org files")

  (ert-deftest ech:org/test-regexps ()
    "Test the regexps defined in org configuration variables"
    (progn
      (let ((fname (f-join ech:org/orga-dir "DIR/orga.org")))
        (should (string-match ech:org/orga-file-regex fname))
        (should (string= (match-string 1 fname) "orga")))
      (let ((fname (f-join ech:org/project-dir "projectfile.org")))
        (should (string-match ech:org/project-file-regex fname))
        (should (string= (match-string 1 fname) "projectfile")))
      (let ((fname (f-join ech:org/area-dir "areafile.org")))
        (should (string-match ech:org/area-file-regex fname))
        (should (string= (match-string 1 fname) "areafile")))
      (let ((fname (f-join ech:org/resource-dir "resourcedir" "resourcefile.org")))
        (should (string-match ech:org/resource-file-regex fname))
        (should (string= (match-string 1 fname) "resourcedir"))
        (should (string= (match-string 2 fname) "resourcefile")))
      (let ((fname (f-join ech:org/archive-dir ech:org/project-dname "archivefile.org")))
        (should (string-match ech:org/archive-file-regex fname))
        (should (string= (match-string 1 fname) ech:org/project-dname))
        (should (string= (match-string 2 fname) "archivefile")))))

;;---- org agenda settings
(defun ech:org/config-agenda-vars ()
  (progn
    (setq
     org-log-into-drawer t
     org-agenda-dim-blocked-tasks t ;; TODO entries that cannot be marked as done because of unmarked children are shown in a dimmed font
     org-enforce-todo-dependencies t ;;blocks entries from changing state to DONE while they have TODO children that are not DONE
     ;;org-agenda-time-grid '((daily today require-timed) "----------------------" nil)
     org-agenda-skip-scheduled-if-done t ;; don’t show scheduled items in agenda when they are done. It applies only to the actual date of the scheduling knowing
     ;;  that items with a past scheduling dates are always turned off when the item is DONE.
     org-agenda-skip-deadline-if-done t  ;; same as previous for deadlines
     org-agenda-include-deadlines t      ;; include entries within their deadline warning period.
     org-agenda-include-diary t          ;; include in the agenda entries from the Emacs Calendar’s diary.
     org-agenda-block-separator nil      ;; The separator between blocks in the agenda, nil means none
     org-agenda-compact-blocks t         ;; Non-nil means make the block agenda more compact. This is done globally by leaving out lines like the agenda span
     ;; name and week number or the separator lines.
     org-cycle-emulate-tab  'whitestart ;; TAB does not emulate TAB Only at the beginning of lines, before the first non-white char, elsewhere it cycles visibility
     ))
  t)
(with-eval-after-load 'org (ech:org/config-agenda-vars))

;;---- todo keyword and tags definitions
  (defun ech:org/config-agenda-kwd ()
    (progn
      (setq
       org-todo-keywords
         '((sequence "INBOX(i!)" "TODO(t!)" "NEXT_ACTION(n!)" "ON_HOLD(h!@)" "IN_PROGRESS(p!)"  "SCHEDULED(s!)" "|" "DONE(d!@)" "CANCEL(c!@)"))
         org-todo-repeat-to-state "SCHEDULED"
         org-todo-keyword-faces
         '(("INBOX" . "dark gray")
           ("TODO" . "orange")
           ("NEXT_ACTION" . "red")
           ("ON_HOLD" . "firebrick")
           ("IN_PROGRESS" . "spring green")
           ("SCHEDULED" . "DarkMagenta")
           ("DONE" . "dark cyan")
           ("CANCELED" . "dark gray"))

         org-tag-alist '(
                         (:startgroup . nil) ; liste
                         ("ACTION" . ?A)
                         ("WAITING_FOR" . ?W)
                         ("AGENDA" . ?G)
                         ("CALL" . ?C)
                         ("READ_REVIEW_1" . ?1)
                         ("READ_REVIEW_2" . ?2)
                         ("READ_REVIEW_3" . ?3)
                         ("MEETING" . ?E)
                         ("REMAINDER" . ?R)
                         (:endgroup . nil)
                         (:startgroup . nil)
                         ("crypt" . ?Y)
                         (:endgroup . nil)
                         (:startgroup . nil)
                         ("TODAY" . ?t)
                         ("TOMORROW" . ?T)
                         ("THIS_WEEK" . ?k)
                         ("NEXT_WEEK" . ?K)
                         ("THIS_MONTH" . ?m)
                         ("NEXT_MONTH" . ?M)
                         ("LATER" . ?l)
                         ("SOMEDAY_MAYBE" . ?L)
                         (:endgroup . nil)
                         (:startgroup . nil) ; type
                         ("Perso" . ?z)
                         ("Work" . ?w)
                         ("Entreprise" . ?e)
                         ("DevPro" . ?d)
                         (:endgroup . nil)
                         )
      ))
    t)

(with-eval-after-load 'org(ech:org/config-agenda-kwd))

;;--- Orga file management
(defun orga (c)
  "Brings directly to an orga file"
  (interactive "c (t)=tickler (i)=inbox (r)=resource (a)=area (p)=projects")
  (cond
   ((char-equal c ?t) (find-file ech:org/tickler-filepath))
   ((char-equal c ?i) (find-file ech:org/inbox-filepath))
   ((char-equal c ?r) (helm-find-files-1 ech:org/resource-dir))
   ((char-equal c ?p) (helm-find-files-1 ech:org/project-dir))
   ((char-equal c ?a) (helm-find-files-1 ech:org/area-dir))
   ))
(global-set-key (kbd "C-c o g") #'orga)

;;--- Automatic templeting of orga files
;; will be expand with format, each %s will be replaced by a paramater from the format call.
(defconst ech:org/orga-snippet "# -*- coding: utf-8 -*-\n#+TITLE: %1$s\n#+OPTIONS: toc:2 H:2\n#+STARTUP: content\n#+STARTUP: latexpreview\n#+STARTUP: inlineimages\n#+FILETAGS: %2$s%3$s\n------------------------\n%4$s")   ;;(ref:snipet)

(defun ech:org/orga-file-parts(fname) ;;(ref:parts)
  "test if a file is an orga org file, return a plist with :dir and :fname for respectively the
  file directory and the file name"
  (cond
   ((string= ech:org/inbox-filepath fname)
    (list :dirname  "" :topic "" :fname (file-name-sans-extension ech:org/inbox-fname)))
   ((string= ech:org/tickler-filepath fname)
    (list :dirname  "" :topic "" :fname (file-name-sans-extension ech:org/tickler-fname)))
   ((string-match ech:org/project-file-regex fname)
    (list :dirname  ech:org/project-dname :topic "" :fname (match-string 1 fname)))
   ((string-match ech:org/area-file-regex fname)
    (list :dirname  ech:org/area-dname :topic "" :fname (match-string 1 fname)))
   ((string-match ech:org/resource-file-regex fname)
    (list :dirname  ech:org/resource-dname :topic (match-string 1 fname) :fname (match-string 2 fname)))
   ((string-match ech:org/archive-file-regex fname)
    (list :dirname  (f-join ech:org/archive-dname (match-string 1 fname)) :topic (match-string 2 fname) :fname (match-string 3 fname)))
   (t nil)))

(defun ech:org/get-content-for-orga-snippet(fname part) ;;(ref:getcontent)
  "depending on the type of file will return:
       - either a title when part is the symbol :title
       - or a first header when part is the symbol :body.
       - or a string with a list of org tags when part is the symbol :tags.
     In the ysnippet $1 is the title, $2 is the tag work
      "
  (let* ((m (ech:org/orga-file-parts fname))
         (dir (if m (plist-get m :dirname) nil))
         (topic (if m (plist-get m :topic) nil))
         (fname (if m (plist-get m :fname) nil))
         )
    (cond
     ((eq part :body)
      (cond
       ((string-equal dir ech:org/resource-dname)  "* Notes\n$0")
       ((string-equal dir ech:org/area-dname)  "* Domaine\n* Projects\n* Resources\n* Tasks\n* Ticklers\n")
       ((string-equal dir ech:org/project-dname)  "* Objectifs\n* Résultats attendus\n\n* Cogitation\n** Idea1\nblalbal\n** Idea2\nblabla\n* Tasks\n* Tickler")
       ((string-equal fname ech:org/inbox-fname)  "* Tasks\n* Inbox\n")
       ((string-equal fname ech:org/tickler-fname)  "* Tasks\n* Inbox\n")
       (t "")))
     ((eq part :title)
      (cond
       ((string-equal dir ech:org/area-dname)  (concat "${1:" fname "}"))
       ((string-equal dir ech:org/resource-dname)  (concat "${1:" fname "}"))
       ((string-equal dir ech:org/project-dname)  (concat "Project ${1:" fname  "}"))
       ((string-equal fname ech:org/inbox-fname)  (concat "${1:Inbox}"))
       ((string-equal fname ech:org/tickler-fname)  (concat "${1:Ticklers}"))
       (t "")))
     ((eq part :properties)
      (cond
       ((string-equal dir ech:org/project-dname)  "\n#+PROPERTY: AREA ${2:Area}")
       (t "")))
     ((eq part :tags)
      (cond
       ((string-equal dir ech:org/project-dname)  (concat ":Project:$1:"))
       ((string-equal dir ech:org/area-dname)  (concat ":Area:$1:"))
       (t "")))
     (t ""))))

(defun ech:org/auto-expand-orga-file-buffer() ;;(ref:function)
  "Insert a header block and a heading line in file depending of its type"
  (let
      ((fname (buffer-file-name)))
    (progn
      (set (make-local-variable 'yas-indent-line) 'fixed)
      (set (make-local-variable 'yas-also-auto-indent-first-line) 'nil)
      (set (make-local-variable 'yas-also-indent-empty-lines) 'nil)
      (yas-expand-snippet(format
                          ech:org/orga-snippet
                          (ech:org/get-content-for-orga-snippet fname :title)
                          (ech:org/get-content-for-orga-snippet fname :tags)
                          (ech:org/get-content-for-orga-snippet fname :properties)
                          (ech:org/get-content-for-orga-snippet fname :body)) 1)
      (message (format "autoinsertion into %s" fname)))))
                                        ;(setq auto-insert-alist nil)
(add-to-list 'auto-insert-alist  (cons (concat ech:org/orga-dir ".*\\.org") #'ech:org/auto-expand-orga-file-buffer)) ;; (ref:auto-insert)
(ert-deftest ech:org/test-get-content-for-orga-snippet ()
  "Test function orga-agenda-file-p"
  (progn
    (should (ech:org/orga-agenda-file-p (f-join ech:org/orga-dir ech:org/inbox-fname)))
    (should (ech:org/orga-agenda-file-p (f-join ech:org/orga-dir ech:org/tickler-fname)))
    (should (ech:org/orga-agenda-file-p (f-join ech:org/area-dir "areaf.org")))
    (should (ech:org/orga-agenda-file-p (f-join ech:org/project-dir "project.org")))
    (should (not (ech:org/orga-agenda-file-p (f-join ech:org/project-dir "wrong-project.arg"))))
    (should (not (ech:org/orga-agenda-file-p (f-join ech:org/resource-dir "wrong-agenda.org"))))))

(ert-deftest ech:org/test-orga-file-parts ()
  "Test function orga-agenda-file-parts"
  (progn
    (let ((res (ech:org/orga-file-parts ech:org/inbox-filepath)))
      (should (string= (plist-get res :dirname) ""))
      (should (string= (plist-get res :topic) ""))
      (should (string= (plist-get res :fname) (file-name-sans-extension ech:org/inbox-fname))))
    (let ((res (ech:org/orga-file-parts ech:org/tickler-filepath)))
      (should (string= (plist-get res :dirname) ""))
      (should (string= (plist-get res :topic) ""))
      (should (string= (plist-get res :fname) (file-name-sans-extension ech:org/tickler-fname))))
    (let ((res (ech:org/orga-file-parts (f-join ech:org/project-dir "projectfile.org"))))
      (should (string= (plist-get res :dirname) ech:org/project-dname))
      (should (string= (plist-get res :topic) ""))
      (should (string= (plist-get res :fname) "projectfile")))
    (let ((res (ech:org/orga-file-parts (f-join ech:org/area-dir "areafile.org"))))
      (should (string= (plist-get res :dirname) ech:org/area-dname))
      (should (string= (plist-get res :topic) ""))
      (should (string= (plist-get res :fname) "areafile")))
    (let ((res (ech:org/orga-file-parts (f-join ech:org/resource-dir "resourcedir" "resourcefile.org"))))
      (should (string= (plist-get res :dirname) ech:org/resource-dname))
      (should (string= (plist-get res :topic) "resourcedir"))
      (should (string= (plist-get res :fname) "resourcefile")))
    ;;      (let ((res (ech:org/orga-file-parts (f-join ech:org/archive-dir ech:org/project-dname "aprojectdir" "archivefile.org"))))
    ;;        (should (string= (plist-get res :dirname) (f-join ech:org/archive-dname ech:org/project-dname)))
    ;;        (should (string= (plist-get res :topic) "aprojectdir"))
    ;;        (should (string= (plist-get res :fname) "archivefile")))
    ;;      (should (not (ech:org/orga-file-parts "toto.org")))))
    ))

;;--- Agenda and target files
(defun ech:org/get-org-agenda-file-list () ;;(ref:agenda)
  "get the list of agenda files by scanning directories where we can find agenda files and looking for .org files in them"
  (rec-find-filenames-in-list (list ech:org/inbox-filepath ech:org/tickler-filepath ech:org/area-dir  ech:org/project-dir) ".*\\.org$"))

(defun ech:org/get-org-resource-file-list () ;;(ref:resource-files)
  "No more used. get the list of list refile target files by scanning directories where we can find agenda files and looking for .org files in them"
  (rec-find-filenames-in-list (list ech:org/resource-dir) ".*\\.org$"))
;;(ech:org/build-org-agenda-file-list)

(defun ech:org/orga-agenda-file-p(fname)
  "test if a file is a agenda file"
  (or
   (string= ech:org/inbox-filepath fname)
   (string= ech:org/tickler-filepath fname)
   (and (string-match-p ech:org/project-file-regex fname) t)
   (and (string-match-p ech:org/area-file-regex fname) t)))

(defun ech:org/orga-refile-target-file-p(fname)
  "test if a file is a refile target file"
  (ech:org/orga-agenda-file-p fname))


(defun ech:org/complete-org-file-list()
  "Add the file in the current buffer to the list of agenda file if it is an agenda file"
  (let ((fname (buffer-file-name)))
    (when (ech:org/orga-agenda-file-p fname)
      (add-to-list `org-agenda-files fname))
    (when (ech:org/orga-refile-target-file-p fname)
      (add-to-list `org-refile-targets fname))
    ))

;; check if a file that is saved is a candidate for being an agenda file, and add it to the agenda
;; file list if it is the case.
(add-hook 'after-save-hook 'ech:org/complete-org-file-list)

  (ert-deftest ech:org/test-orga-agenda-file-p ()
    "Test function orga-agenda-file-p"
    (progn
      (should (ech:org/orga-agenda-file-p (f-join ech:org/orga-dir ech:org/inbox-fname)))
      (should (ech:org/orga-agenda-file-p (f-join ech:org/orga-dir ech:org/tickler-fname)))
      (should (ech:org/orga-agenda-file-p (f-join ech:org/area-dir "areaf.org")))
      (should (ech:org/orga-agenda-file-p (f-join ech:org/project-dir "project.org")))
      (should (not (ech:org/orga-agenda-file-p (f-join ech:org/project-dir "wrong-project.arg"))))
      (should (not (ech:org/orga-agenda-file-p (f-join ech:org/resource-dir "wrong-agenda.org"))))))
(defun ech:org/orga-config-orga-files()
  (setq org-agenda-files (ech:org/get-org-agenda-file-list))
  (setq org-refile-targets org-agenda-files)
  (message "org agenda config orga files done"))

(with-eval-after-load 'org (ech:org/orga-config-orga-files))

;;--- OrgAgendaCommand
(setq org-agenda-custom-commands
      '(("c" "Desk Work" tags-todo "computer" ;; (1) (2) (3) (4)
         ((org-agenda-files '("~/org/widgets.org" "~/org/clients.org")) ;; (5)
          (org-agenda-sorting-strategy '(priority-up effort-down))) ;; (5) cont.
         ("~/computer.html")) ;; (6)
        ;; ...other commands here
        ))
(defun ech:org/set-org-agenda-commands()
  (setq org-agenda-custom-commands
        '(
           ("Tw" "All TODO Work" tags-todo "Work+CALL|Work+ACTION")
           ("Tp" "All TODO Perso" tags-todo "Perso+CALL|Perso+ACTION")
           ("Te" "All TODO Entreprise" tags-todo "Entreprise+CALL|Entreprise+ACTION")
           ("Nw" "Next actions Work" tags-todo "Work+CALL|Work+ACTION/!+NEXT_ACTION")
           ("Np" "Next actions Perso" tags-todo "Perso+CALL|Perso+ACTION/!+NEXT_ACTION")
           ("Ne" "Next actions Entreprise" tags-todo "Entreprise+CALL|Entreprise+ACTION/!+NEXT_ACTION")
           ("W" "Non Actions Work" tags-todo "Work/!-ACTION-CALL")
           ("P" "Non Actions Perso" tags-todo "Perso/!-ACTION-CALL")
           ("E" "Non Actions Entreprise" tags-todo "Entreprise/!-ACTION-CALL")
           ("l" "Later" tags-todo "LATER+SOMEDAY_MAYBE")
           ("X" "Scheduled things" tags-todo "TODO=\"SCHEDULED\""
            ))
        )
  t)

 (with-eval-after-load 'org (ech:org/set-org-agenda-commands))
;;--- deft configuration
(defcustom deft-auto-header-alist () "Contains an association list of file name patterns (regular expressions) and a corresponding property list.
 The property list has two properties :title and :summary whose values are functions that extract respectivley the title and the summary of the file matching the regex.")
(defun ech:org/adv-deft-cache-newer-file(filename mtime)
  (let* ((func-plist (cl-some (lambda (x)  (if (string-match-p (car x) filename) (cdr x) nil)) deft-auto-header-alist))
         (title-func (and func-plist (plist-get func-plist :title)))
         (summary-func (and func-plist (plist-get func-plist :summary)))
         contents
         title
         summary)
    (progn
      (message (format "auto-header: %S" deft-auto-header-alist))
      (message (format "filename : %s, mtime: %S , plist: %S" filename mtime func-plist))
      (message (format "Title func : %S, summary func : %S" title-func summary-func))
      (puthash file mtime deft-hash-mtimes)
      ;; Contents
      (with-current-buffer (get-buffer-create "*Deft temp*")
        (insert-file-contents file nil nil nil t)
        (setq contents (concat (buffer-string))))
      (puthash file contents deft-hash-contents)
      ;; Title
      (setq title (or (and title-func (funcall title-func filename contents)) (deft-parse-title file contents)))
      (message (format "title: %s" title))
      (puthash file title deft-hash-titles)
      ;; Summary
      (setq summary (or (and summary-func (funcall summary-func filename contents)) (deft-parse-summary contents title)))
      (message (format "summary : %s" summary))
      (puthash file summary deft-hash-summaries)
      (kill-buffer "*Deft temp*"))))

(eval-after-load 'deft
  (advice-add 'deft-cache-newer-file :override #'ech:org/adv-deft-cache-newer-file))
(defun ech:org/parse-org-title (file content)
  (if (string-match "^\\s-*#\\+TITLE:\\s-*\\(.*?\\)\\s-*$" content)
      (match-string-no-properties 1 content)
    nil))

(defun ech:org/parse-org-summary (file content)
  (if (string-match "^\\s-*#\\+SUMMARY:\\s-*\\(.*?\\)\\s-*$" content)
      (match-string-no-properties 1 content)
    nil))
(use-package deft
  :no-require t
  :custom
  (deft-directory ech:org/resource-dir)
  (deft-recursive t)
  (deft-use-filename-as-title nil)
  (deft-auto-header-alist '((".*\\.org$" . (:title ech:org/parse-org-title :summary ech:org/parse-org-summary))))
  )
;;--- todos comparison

(message "org-orga-conf loaded")
(provide 'org-orga-conf)
