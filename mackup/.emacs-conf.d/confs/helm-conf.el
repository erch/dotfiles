(require 'package-conf)
(require 'core)
(require 'use-package)

(eval-and-compile
  (use-package helm
    :demand t     ;; we always need helm.
    :diminish ""
    :custom
    (helm-buffer-max-length nil) ; let enought place for buffer names in buffer list
    :bind
    (("M-x" . helm-M-x)
     ("M-y" . helm-show-kill-ring)
     ("C-x b" . helm-mini)
     ("C-; C-t" . helm-top)
     ("C-x C-f" . helm-find-files)
     ("C-x C-b" . 'helm-buffers-list)
     ("C-h r" . helm-info-emacs)
     (:map helm-map
           ("C-o" . nil)
           ;;TAB" . helm-execute-persistent-action)
           ;;("C-i" . helm-execute-persistent-action)
           ;;(C-z" . helm-select-action)
           ("C-h" . delete-backward-char))
     (:map helm-find-files-map
           ("C-h" . delete-backward-char)
           ("C-i" . helm-execute-persistent-action))
     (:map helm-grep-mode-map
           ("RET" . helm-grep-mode-jump-other-window)
           ("n" . helm-grep-mode-jump-other-window-forward)
           ("p" . helm-grep-mode-jump-other-window-backward)))
    :init
    (progn
      ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
      ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
      ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
      (custom-set-variables '(helm-command-prefix-key "C-;")))
    :config
    (progn
      (setq-default
       helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
       helm-quick-update t ; do not display invisible candidates
       helm-idle-delay 0.01 ; be idle for this many seconds, before updating in delayed sources.
       helm-input-idle-delay 0.01 ; be idle for this many seconds, before updating candidate buffer
       helm-candidate-number-limit 200 ; limit the number of displayed canidates
       helm-move-to-line-cycle-in-source nil ; move to end or beginning of source when reaching top or bottom of source.
       helm-display-function 'pop-to-buffer
       ;; helm-command
       helm-M-x-requires-pattern 0     ; show all candidates when set to 0
       )
      (when (executable-find "curl")
        (setq helm-google-suggest-use-curl-p t))
      (helm-mode 1)
      (setq-default
       helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.
       helm-boring-file-regexp-list
       '("\\.git$" "\\.hg$" "\\.svn$" "\\.CVS$" "\\._darcs$" "\\.la$" "\\.o$" "\\.i$") ; do not show these files in helm buffer
       helm-ff-file-name-history-use-recentf t
       ;; helm-buffers
       helm-buffers-fuzzy-matching t          ; fuzzy matching buffer names when non--nil
                                        ; useful in helm-mini that lists buffers
       ;; ido
       ido-use-virtual-buffers t      ; Needed in helm-buffers-list
       )
      (helm-popup-tip-mode 1)
      (helm-top-poll-mode 1)
      (setq helm-buffers-favorite-modes (append helm-buffers-favorite-modes
                                                '(picture-mode artist-mode)))))
  (use-package helm-descbinds
    :after helm
    :config
    (helm-descbinds-mode))


  (use-package helm-ag
    :after helm
    :diminish ""
    :bind (("C-c a" . helm-do-ag)
           (:map helm-map
                 ("C-a" . helm-ag)))
    :config
    (progn
      ;; helm always ask for options
      (setq helm-ag-always-set-extra-option t
            helm-ag-ignore-buffer-patterns '("\\.backup\\'" "\\.tfstate\\'" "\\.bck\\'" "\\~\\'"))))

                                        ;           ("o" . helm-occur)
                                        ;        ("y" . yas-insert-snippet))

  (use-package helm-swoop
    :after helm
    :diminish ""
    :bind (("M-o" . helm-swoop)
           ("M-O" . helm-swoop-back-to-last-point)
           ("C-c M-o" . helm-multi-swoop))
    :config
    (progn
      (setq-default
       helm-multi-swoop-edit-save t ;; Save buffer when helm-multi-swoop-edit complete
       helm-swoop-split-with-multiple-windows nil   ;; If this value is t, split window inside the current window
       helm-swoop-split-direction 'split-window-horizontally   ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
       helm-swoop-speed-or-color t ;; If nil, you can slightly boost invoke speed in exchange for text color
       ))
    :init
    ;; Save current position to mark ring
    (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring))
  (use-package helm-eww
    :after helm
    :bind
    (("C-c e" . helm-eww))
    )
  )

(provide 'helm-conf)
