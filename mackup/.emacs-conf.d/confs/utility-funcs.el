;; utility functions
(require 'base-conf)
(require 'package-conf)
(require 'calendar)
(require 'cl-lib)
(require 'ert)

(defun members (elems list)
  "test if all elements of list elemes are in list list, returns true in this case. Returns false if at least one element of elems is not in list"
  (cond
   ((eq nil elems) t)
   ((member (car elems) list) (members (cdr elems) list))
   (t nil)
   ))

(defun flattenlists(res &rest lists)
  "return a list of string with all strings find in any lists inside list and otherlist recursively"
  (cond ((null lists) res)
	((consp (car lists)) (flattenlists res (car (car lists)) (append (cdr (car lists)) (cdr lists))))
	(t (flattenlists (cons (car lists) res) (append (car (cdr lists)) (cdr (cdr lists)))))))

					; (flattenlists () '("a" "b"))
					; (flattenlists ())
					; (flattenlists () "a")
					; (cons "a" '( "b" "c"))
					; (append nil nil)
(defun list2string(list)
  "return all strings in list in one string with space as separator"
  (if (eq (cdr list) nil) (car list)
    (concat (car list) " " (list2string (cdr list)))))

(defun intercal (chars list)
  "returns a string in which the string chars is intercalled between each element in list"
  (if (eq (cdr list) nil) (car list)
    (concat (car list) chars (intercal chars (cdr list)))))

(defun file-name-os (file-name)
  "get a file name according to the os naming convention"
  (let ((fn (if (listp file-name) (list2string file-name) file-name)))
    (if (memq system-type '(windows-nt cygwin))
        (intercal "\\"  (split-string fn "/"))
      fn)))

(defun prj-path-less-path (path-to-suppress path )
  "delete  path-to-suppress in path. path are list of strings."
  (if (eq path-to-suppress nil) path
    (remove (car path-to-suppress) (prj-path-less-path (cdr path-to-suppress) path))))

(defun silently-create-file(filepath &optional initf)
  "creates a file if it not exists and triggers relative file and buffer hooks.
   if initf is not nil it is a function that will be call before saving and closing
   the newly created file (meant to be auto-insert)
   returns nil if file already existed, t if the file was created."
  (let ((notthere (not (file-exists-p filepath))))
    (when notthere
      (let* ((bname (generate-new-buffer-name " temp"))
             (newBuf (get-buffer-create bname)))
        (with-current-buffer newBuf
          (progn
            (set-visited-file-name filepath t)
            (when initf (funcall initf))
            (save-buffer 0))
          (kill-buffer newBuf))))
    notthere))

(defun find-first-named-dirs-in-list (dirs src depth exclude-list maxdepth)
  (let ((first (car dirs))
        (others (cdr dirs)))
    (cond
     ((not others) nil)
     ((or
       (not first)
       (not (file-directory-p first))
       (member (file-name-nondirectory (directory-file-name first)) exclude-list))
      (find-first-named-dirs-in-list others src depth exclude-list maxdepth))
     (t (append
         (find-first-named-dirs-in-dirtree-rec first src depth exclude-list maxdepth) (find-first-named-dirs-in-list others src depth exclude-list maxdepth))))))

(defun find-first-named-dirs-in-dirtree-rec (from src depth exclude-list maxdepth)
  "called by find-first-named-dirs-in-dirtree to initiate recursivity with depth count"
  (and (file-directory-p from)
       (or (and maxdepth (< depth maxdepth)) (not maxdepth))
       (let*
           ((dirs (directory-files from))
            (srcdir (find  src dirs :test 'string=)))
         (if srcdir
             (list (file-name-as-directory (expand-file-name srcdir from)))
           (progn
             (find-first-named-dirs-in-list
              (mapcar (lambda(x)
                        (and (not (string= "." x))
                             (not (string= ".." x))
                             (file-name-as-directory (expand-file-name x from)))) dirs) src (+  1 depth) exclude-list maxdepth))))))

(defun find-first-named-dirs-in-dirtree(from src &optional exclude-list maxdepth)
  "return the list of path to directories named src find in the directory from. Go in childs directories recursively but stop going deeper in a directory when found a matching directory"
  (and (file-directory-p from)
       (progn
         (find-first-named-dirs-in-dirtree-rec (file-name-as-directory (expand-file-name from)) src 0 exclude-list maxdepth))))

(defun print-list(x)
  (message (intercal " " x)))


(defun non-dot-directory-files (dir)
  "Return a list of file names in dir without the . and .. directories"
  (let ((dirn (or (and (null dir) nil) (file-name-nondirectory (directory-file-name dir)))))
    ;;(message "=> dir is %s,dirn is %s" dir dirn)
    (when
	(and (not (null dir))
	     (not (string= "." dirn))
	     (not (string= ".." dirn)))
      (sup-dots (directory-files dir t)))))

(defun sup-dots (list)
  "suppress . and .. from a list of file names"
  (when (not (null list))
    (let* ((first (car list))
	   (end (cdr list))
	   (fnd (or (and (null first) nil) (file-name-nondirectory (directory-file-name first)))))
      ;;(message "\t + list is %s, first is %s, end is %s, fnd is %s" list first end fnd)
      (cond
       ((null list) ())
       ((or (null first)
	    (string= "." fnd)
	    (string= ".." fnd))
	(sup-dots end))
       (t (cons first (sup-dots end)))))))

;;(non-dot-directory-files project-dir)

(defun ech:util/time-of-nth-named-day-of-week (wkday  &optional nth-week time)
  "return the time of a named day in a week, for instance monday of this week, or sunday of next week.
A wkday of 0 means Sunday, 1 means Monday, and so on.
Weeks start on Monday.
If nth-week = 0, return the date of the Nth DAYNAME of the curent week inclusive, ie if time is Monday and wkday is 1 then return this monday and if wkday is a past day returns a date in the past and if wkday is in the future return a date in the future.
If nth-week > 0, return the date of the the Nth DAYNAME after time exclusive ie if time is Monday and nth-week is 1 then return next monday, always return a date in the future.
If nth-week < 0, return the date of the the Nth DAYNAME before time  exclusive ie if time is Monday and nth-week is 1 then return this previous monday, always return a date in the past.
time is a lisp timestamp as return by encode-time, default to the current time
nth-week is default to 0
returns a lisp timestamp as return by encode-time"
  (let* (
         (nthwk (if (null nth-week) 0 nth-week))
         (wkday-from-monday (mod (+ wkday 6) 7))
         (tt (or time (time-convert nil 'integer)))
         (tl (decode-time tt))
	     (sec (nth 0 tl))
	     (min (nth 1 tl))
	     (hour (nth 2 tl))
	     (day (nth 3 tl))
	     (month (nth 4 tl))
	     (year (nth 5 tl))
         (day-of-week (mod (+ (nth 6 tl) 6) 7))
         (cal-res
	      (cond
           ((and (>= nthwk 0) (>= wkday-from-monday day-of-week)) (calendar-nth-named-day (+ 1 nthwk) wkday month year day))
           ((and (<= nthwk 0) (<= wkday-from-monday day-of-week)) (calendar-nth-named-day (+ -1 nthwk) wkday month year day))
           (t (calendar-nth-named-day nthwk wkday month year day)))))
    (encode-time 0 0 0 (nth 1 cal-res) (nth 0 cal-res) (nth 2 cal-res))))

(ert-deftest test-week-day-date-from-date ()
  (let*
      ;; test a sunday which is first week day
      ((testdata
        ;; list with <wkday> <nth-week> <input time in encode time format> <expected time in encode time format>
        `((0 0 (0 0 0 8 11 2020) (0 0 0 8 11 2020)) ;; same sunday
          (0 -1 (0 0 0 8 11 2020) (0 0 0 1 11 2020)) ;; from sunday , the sunday of the week before
          (0 1 (0 0 0 8 11 2020) (0 0 0 15 11 2020)) ;; from sunday, the sunday of the week after
          (0 2 (0 0 0 8 11 2020) (0 0 0 22 11 2020)) ;; from sunday,the sunday in two weeks after
          (0 -2 (0 0 0 8 11 2020) (0 0 0 25 10 2020)) ;; from sunday,the sunday two weeks before
          (2 0 (0 0 0 8 11 2020) (0 0 0 3 11 2020)) ;; from sunday, the Tuesday of the week
          (0 0 (0 0 0 11 11 2020) (0 0 0 15 11 2020)) ;; from wendsday, the sunday of the week
        )))
    (mapc
     (lambda (x)
       (let ((res (decode-time (apply #'ech:util/time-of-nth-named-day-of-week (list (nth 0 x) (nth 1 x) (apply #'encode-time (nth 2 x)))))))
       (should (equal (cl-subseq res 0 6) (nth 3 x)))))
     testdata)))

(defun ech:util/time-of-named-day-of-last-week-of-month (wkday &optional nth-month time)
  "time of the timestamp of the last named day of the last week of a month. Weeks starts on Monday.
   A wkday of 0 means Sunday, 1 means Monday, and so on.
   If nth-month = 0, return the timestamp of the wkday of the last week the curent month inclusive, ie if time is the Monday of the last week of the month and wkday is 1, then returns this monday.
   If nth-month > 0, return the timestamp of the wkday of the last week of the nth month after time.
   If nth-month < 0, return the timestamp of the wkday of the last week of the nth month before time
   The last week of a month is the week that contains the last day of the month even if it is Monday.
   time is a lisp timestamp as return by encode-time, default to the current time
   returns a lisp timestamp as return by encode-time"
    (let* (
         (mth (or nth-month +1))
         (tt (or time (time-convert nil 'integer)))
         (tl (decode-time tt))
	     (sec (nth 0 tl))
	     (min (nth 1 tl))
	     (hour (nth 2 tl))
	     (day (nth 3 tl))
	     (month (nth 4 tl))
	     (year (nth 5 tl)))
      (calendar-increment-month month year mth)
      (let* (
             (last-numday-of-month (calendar-last-day-of-month month year))
             (last-day-of-month (encode-time
                                 sec
                                 min
                                 hour
                                 last-numday-of-month
                                 month
                                 year))
             (day-of-week (nth 6 (decode-time last-day-of-month))))
        (ech:util/time-of-nth-named-day-of-week wkday 0 last-day-of-month))))

;;(decode-time (ech:util/time-of-nth-named-day-of-week 4 0 (encode-time 0 0 0 31 10 2020)))
;;(decode-time (time-of-last-weekday-of-month 6 -1 (encode-time 0 0 0 25 11 2020)))
;;(decode-time (time-of-last-weekday-of-month 6 -1 (encode-time 0 0 0 25 11 2020)))
(ert-deftest test-time-of-named-day-of-last-week-of-month ()
  (let*
      ((testdata
        ;; list with <wkday> <nth-month> <input time in encode time format> <expected time in encode time format>
        `((6 -1 (0 0 0 25 11 2020) (0 0 0 31 10 2020))
          (0 -1 (0 0 0 4 11 2020) (0 0 0 1 11 2020))
          (5 -1 (0 0 0 8 11 2020) (0 0 0 30 10 2020))
          (4 0  (0 0 0 8 11 2020) (0 0 0 3 12 2020))
          (0 2  (0 0 0 8 11 2020) (0 0 0 31 1 2021))
          (2 -2 (0 0 0 8 11 2020) (0 0 0 29 9 2020))
        )))
    (mapc
     (lambda (x)
       (let ((res (decode-time (apply #'ech:util/time-of-named-day-of-last-week-of-month (list (nth 0 x) (nth 1 x) (apply #'encode-time (nth 2 x)))))))
       (should (equal (cl-subseq res 0 6) (nth 3 x)))))
     testdata)))

(defun time-nth-months-back (n &optional tme)
  "get the month number for n month back than time tme which is now by default"
  (let* ((tl  (if (null tme) (decode-time) (decode-time tme)))
	 (sec (nth 0 tl))
	 (min (nth 1 tl))
	 (hour (nth 2 tl))
	 (day (nth 3 tl))
	 (month (nth 4 tl))
	 (year (nth 5 tl)))
    ;;(dest-month (+ 1 (% (- (+ month (* 12 (+ 1 (/ n 12)))) (+ 1 n)) 12)))
    ;;(dest-year (- year (/ n 12))))
    (calendar-increment-month month year (- n))
    (encode-time sec min hour day month year)))

(defun ech:util/date-timestamp-min (&rest dates)
  "returns the smallest date in the list, if there are several smallest dates returns one of them.
   nil dates are ignored.
   dates are lisp timestamp."
  (let ((res nil))
    (progn
      (mapc (lambda(x)
              (unless (null x)
                (cond
                 ((null res) (setq res x))
                 ((time-less-p x res) (setq res x)))))
            dates)
      res)))

(defun date-timestamp-max (&rest dates)
  "returns the greatest date in the list, if there are several greatests dates returns one of them.
   nil dates are ignored
   dates are lisp timestamp."
  (let ((res nil))
    (progn
      (mapc (lambda(x)
              (unless (null x)
                (cond
                 ((null res) (setq res x))
                 ((time-less-p res x) (setq res x)))))
            dates)
      res)))

;;(decode-time (date-timestamp-min (encode-time 2 0 0 2 11 2020) (encode-time 12 25 11 3 11 2020) (encode-time 1 0 0 2 11 2020)))
;;(decode-time (date-timestamp-min nil (encode-time 2 0 0 2 11 2020) nil (encode-time 12 25 11 3 11 2020) (encode-time 1 0 0 2 11 2020)))
;;(date-timestamp-min nil nil nil)
(defun rec-find-filenames-in-list (list pattern)
  "find files from a pattern name in a list of file names, search in directories if there are some in the list"
  (when (not (null list))
    (let* ((first (car list))
	   (end (cdr list))
	   (fn (or (and (null first) nil) (file-name-nondirectory (directory-file-name first)))))
      ;;(message "\t --> first is %s, end is %s, fn is %s" first end fn)
      (cond
       ((and (file-regular-p first) (string-match-p pattern fn))
	(cons first (rec-find-filenames-in-list end pattern)))
       ((and (file-directory-p first) (not (or (string= ".backup" fn) (string= ".archive" fn))))
	(append (rec-find-filename-in-dir first pattern) (rec-find-filenames-in-list end pattern)))
       (t (rec-find-filenames-in-list end pattern))))))

(defun rec-find-filename-in-dir (root pattern)
  "find files in  a directory,
     return the list of full path files."
  (when (file-directory-p root)
    (let ((files (non-dot-directory-files root)))
      ;;(message "=> root %s, files are %s" root files)
      (when (not (null files))
	(rec-find-filenames-in-list files pattern)))))
;;(rec-find-filename-in-dir project-dir)

(defun sudo-edit (&optional arg)
  "Edit currently visited file as root.

With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(defun prelude-start-or-switch-to (function buffer-name)
  "Invoke FUNCTION if there is no buffer with BUFFER-NAME.
Otherwise switch to the buffer named BUFFER-NAME.  Don't clobber
the current buffer."
  (if (not (get-buffer buffer-name))
      (progn
        (split-window-sensibly (selected-window))
        (other-window 1)
        (funcall function))
    (switch-to-buffer-other-window buffer-name)))

(defun ech-path-mungle(new-path &optional after)
  "return the content of exec-path modified by adding new path to it. new-path is added at the
   beginning of the list if after is nil, at the end otherwise"
  (let ((abs-exec-paths (mapcar (lambda (x) (expand-file-name x)) exec-path))
        (abs-new-path (expand-file-name new-path)))
    (delete abs-new-path abs-exec-paths)
    (add-to-list 'abs-exec-paths abs-new-path after)))

(provide 'utility-funcs)
