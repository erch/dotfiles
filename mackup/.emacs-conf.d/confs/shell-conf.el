;;---- loading dependencies
(require 'core)
(require 'package-conf)
;;(require 'helm-conf)
(require 'display) ;; for shackle
(require 'babel-conf)
(require 'programming) ;; for lsp

;;---- isend configuration
;;isend-mode is an Emacs extension allowing interaction with code interpreters in ansi-term or term buffers
(eval-and-compile
  (use-package isend-mode
    :custom
    (isend-forward-line nil)
    (isend-strip-empty-lines t)
    (isend-delete-indentation t)
    (isend-end-with-empty-line t)))

;;---- term line mode
;; allow to delete characters inside a line in the shell prompt
(defadvice term-line-mode (around clean-process-input-before-line-mode disable)
  (interactive)
  (let* (
	     (pos (point))
	     (proc (get-buffer-process (current-buffer)))
	     (end (progn (end-of-line) (point)))
	     (beg (progn (beginning-of-line) (term-skip-prompt) (point)))
	     (input-before (buffer-substring-no-properties beg pos))
	     (input-after (buffer-substring-no-properties pos end)))
    (message "switching to line mode: %s->%s" input-before input-after)
    ;;(insert (myterm-clear-process-input))
    ;;(goto-char pos)
    (term-send-string proc "\C-a\C-k")
    ;;(goto-char (+ 2 beg))
    ;;(set-marker (process-mark proc) (point))
    ad-do-it
    ;;(save-excursion
    ;; Insert the text, advancing the process marker.
    ;;(goto-char (process-mark proc))
    (let ((markpos (point)))
      (insert (concat input-before input-after)))
    ;;(set-marker (process-mark proc) markpos))
    ;;(goto-char pos)
    ))

;;---- keystrokes

(defun myterm-send-backward-kill-word ()
  "Backward kill word in term mode."
  (interactive)
  (term-send-raw-string "\C-w"))

(defun myterm-send-forward-kill-word ()
  "Kill word in term mode."
  (interactive)
  (term-send-raw-string "\ed"))

(defun myterm-send-backward-word ()
  "Move backward word in term mode."
  (interactive)
  (term-send-raw-string "\eb"))

(defun myterm-send-forward-word ()
  "Move forward word in term mode."
  (interactive)
  (term-send-raw-string "\ef"))

(defun myterm-send-reverse-search-history ()
  "Search history reverse."
  (interactive)
  (term-send-raw-string "\C-r"))

(defun myterm-keystroke-setup ()
  "Keystroke setup of `term-char-mode'.
By default, the key bindings of `term-char-mode' conflict with user's keystroke.
So this function unbinds some keys with `term-raw-map',
and binds some keystroke with `term-raw-map'."
  (let (bind-key bind-command)
    ;; Unbind base key that conflict with user's keys-tokes.
    (dolist (unbind-key term-unbind-key-list)
      (cond
       ((stringp unbind-key) (setq unbind-key (read-kbd-macro unbind-key)))
       ((vectorp unbind-key) nil)
       (t (signal 'wrong-type-argument (list 'array unbind-key))))
      (define-key term-raw-map unbind-key nil))
    ;; Add some i use keys.
    ;; If you don't like my keystroke,
    ;; just modified `term-bind-key-alist'
    (dolist (element term-bind-key-alist)
      (setq bind-key (car element))
      (setq bind-command (cdr element))
      (cond
       ((stringp bind-key) (setq bind-key (read-kbd-macro bind-key)))
       ((vectorp bind-key) nil)
       (t (signal 'wrong-type-argument (list 'array bind-key))))
      (define-key term-raw-map bind-key bind-command))))

(defun term-myget-new-input ()
  "Return new input. Takes chars from beginning of line to the point, and discard any initial text matching term-prompt-regexp."
  (save-excursion
    (let* ((beg (progn (beginning-of-line) (term-skip-prompt) (point)))
 	       (end (progn (end-of-line) (point)))
 	       (text (buffer-substring-no-properties beg end)))
 	  (message (concat "input is : " text))
 	  text)))

(defun comint-clear-buffer ()
  "Easily clear comint buffers."
  (interactive)
  (let ((comint-buffer-maximum-size 0))
    (comint-truncate-buffer)))

;;---- term-mode
(defun ech:shell/term-mode-hook ()
  (message "in term-mode-hook")
  (myterm-keystroke-setup)
  (toggle-truncate-lines -1)
  (auto-fill-mode -1)
  (setq-local comint-buffer-maximum-size 50000)    ; max length of the buffer in lines
  (setq-local comint-completion-addsuffix t)        ; Insert space/slash after completion
  (setq-local comint-completion-autolist t )    ; show completion list when ambiguous
  (setq-local comint-get-old-input 'term-myget-new-input) ; what to run when i press enter on a
  (setq-local comint-input-ignoredups t)          ; no duplicates in command history
  (setq-local comint-input-ring-size 5000)         ; max shell history size
  (setq-local comint-prompt-read-only nil)         ; if this is t, it breaks shell-command
  (setq-local comint-scroll-show-maximum-output t) ; scroll to show max possible output
  (setq-local comint-scroll-to-bottom-on-input t)  ; always insert at the bottom
  (setq-local comint-scroll-to-bottom-on-output nil) ; always add output at the bottom
  (setq-local overflow-newline-into-fringe t)
  (setq-local autopair-dont-activate t)
  (setq-local mouse-yank-at-point t)
  (setq-local transient-mark-mode nil)
  (setq-local tab-width 8)
  (add-hook 'comint-output-filter-functions #'comint-truncate-buffer))

(use-package term
  :straight '(term :type built-in)
  :bind
  (:map term-raw-map ("C-c C-j" . #'term-line-mode))
  :init
  (add-hook 'term-mode-hook #'ech:shell/term-mode-hook)
  ;; The key list that will need to be unbind.
  (setq term-unbind-key-list
        '("C-z" "C-x" "C-l" "C-c" "C-h" "C-y" "M-x"))

  ;; The key alist that will need to be bind.
  ;; If you do not like default setup, modify it, with (KEY . COMMAND) format.
  (setq term-bind-key-alist
        '(
          ("C-<rigth>" . myterm-send-backward-word)
          ("C-c C-c" . term-interrupt-subjob)
          ("C-p" . term-previous-prompt)
          ("C-n" . term-next-prompt)
          ("M-s" . isearch-forward)
          ("M-r" . isearch-backward)
          ("C-m" . term-send-raw)
          ("M-f" . myterm-send-forward-word)
          ("C-<left>" . myterm-send-backward-word)
          ("M-b" . myterm-send-backward-word)
          ("M-o" . term-send-backspace)
          ("M-p" . term-send-up)
          ("M-n" . term-send-down)
          ("M-M" . myterm-send-forward-kill-word)
          ("M-N" . myterm-send-backward-kill-word)
          ("C-r" . myterm-send-reverse-search-history)))
  ;; ("M-," . term-send-input)
  :bind
  (("M-." . #'comint-dynamic-complete)
   :map comint-mode-map
   ("C-c M-o" . #'comint-clear-buffer) ; Clear comint buffer
   ))

(eval-and-compile
  (use-package multi-term
    :diminish
    :custom
    (multi-term-program "/bin/bash")
    (multi-term-scroll-to-bottom-on-output t)
    (multi-term-buffer-name "terminal"))
  (use-package bash-completion            ; Bash completion for shell-mode
    :config
    :hook
    (shell-mode . #'bash-completion-setup))

  (use-package helm-mt
    :diminish ""
    :after helm
    :bind (("C-x T" . helm-mt)))
  )

;; enable lsp for shell script mode
(add-hook 'sh-mode-hook (lambda ()
                          (message "in sh-mode hook")
                          (lsp)))

;;--- where to display term windows
(add-to-list 'display-buffer-alist
             '((lambda(bufname _) (with-current-buffer bufname (derived-mode-p 'term-mode)))
               (display-buffer-reuse-window display-buffer-in-direction)
               ;;display-buffer-in-direction/direction/dedicated is added in emacs27
               (direction . bottom)
               (inhibit-same-window . t)
               (inhibit-switch-frame . t)
               (window-height . 0.3)))

;;---- babel
;; add shell as babel language
(add-to-list 'ech:babel/babel-languages 'shell)

;;---- power shell
;;(eval-and-compile
;;  (defun ech:shell/eglot-config()
;;    (message "eglot config for powershell")
;;    (add-to-list 'eglot-server-programs (cons 'powershell-mode '(
;;                                                                 "/usr/bin/pwsh"
;;                                                                 "-NoProfile"
;;                                                                 "-NonInteractive"
;;                                                                 "-NoLogo"
;;                                                                 "-OutputFormat" "Text"
;;                                                                 "-File" "/home/ech/.emacs-ech.d/.cache/lsp/pwsh/PowerShellEditorServices/PowerShellEditorServices/Start-EditorServices.ps1"
;;                                                                 "-HostName" "\"Emacs Host\""
;;                                                                 "-HostProfileId" "'Emacs.LSP'"
;;                                                                 "-HostVersion" "0.1"
;;                                                                 "-LogPath" "/home/ech/.emacs-ech.d/.cache/lsp/pwsh/logs/emacs-powershell.log"
;;                                                                 "-LogLevel" "Normal"
;;                                                                 "-SessionDetailsPath" "/home/ech/.emacs-ech.d/.cache/lsp/pwsh/logs/PSES-VSCode-41197"
;;                                                                 "-Stdio"
;;                                                                 "-BundledModulesPath"
;;                                                                 "/home/ech/.emacs-ech.d/.cache/lsp/pwsh/PowerShellEditorServices"
;;                                                                 "-FeatureFlags" "@()"))))
;;  ;; power shell mode
(use-package powershell-mode
    :demand t
    :mode
    ("\\.ps1\\'" . powershell-mode)
    :straight '(powershell-mode :type built-in)
    :config
   ;; (ech:shell/eglot-config)
    (setq
     lsp-pwsh-exe "/bin/pwsh"
     lsp-pwsh-ext-path  "~/Projects/PowerShellEditorServices/module"  ;;The path to powershell vscode extension
     lsp-pwsh-dir "~/Projects/PowerShellEditorServices/module/PowerShellEditorServices"
          )
    :init
    (add-hook 'powershell-mode-hook (lambda ()
                                      ;;(eglot-ensure)
                                      (lsp)
                                 ))
    )

(message "shell-conf loaded")
(provide 'shell-conf)
