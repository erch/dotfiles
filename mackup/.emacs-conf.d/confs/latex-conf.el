(require 'package-conf)
(require 'completion)
(require 'babel-conf)

;; need to install all tex-live debian package.
(defun ech:latex/mode-hook ()
  "hook for `LaTeX-mode'."
  (smartparens-mode +1)
  (LaTeX-math-mode 1)

  (rainbow-delimiters-mode)
  (company-mode)
  (setq TeX-PDF-mode t)
  (setq TeX-source-correlate-method 'synctex)
  (setq TeX-source-correlate-start-server t)
  (setq-local company-backends
              (append
               '(company-math-symbols-latex company-latex-commands company-math-symbols-unicode)
               company-backends))
  ;;(pdf-tools-install)
  (latex-preview-pane-enable)
  (turn-on-cdlatex))

(eval-and-compile
  (use-package auctex
    :config
    (progn
      (setq
       TeX-auto-save t
       TeX-parse-self t
       TeX-master nil
       ;; use pdflatex
       TeX-PDF-mode t
       TeX-view-program-list nil)
      ;; sensible defaults for OS X, other OSes should be covered out-of-the-box
      (when (eq system-type 'darwin)
        (setq TeX-view-program-selection
              '((output-dvi "DVI Viewer")
                (output-pdf "PDF Viewer")
                (output-html "HTML Viewer")))

        (setq TeX-view-program-list
              '(("DVI Viewer" "open %o")
                ("PDF Viewer" "open %o")
                ("HTML Viewer" "open %o"))))

      (when (eq system-type 'gnu/linux)
        (setq TeX-source-correlate-start-server t)
        (setq TeX-view-program-selection
              '((output-dvi "DVI Viewer")
                (output-pdf "pdf-tools")
                (output-html "HTML Viewer")))

        (setq TeX-view-program-list
              '(("DVI Viewer" "open %o")
                ("PDF Viewer" "open %o")
                ("HTML Viewer" "open %o")
                ("pdf-tools" "TeX-pdf-tools-sync-view")
                ))

        (setq TeX-view-program-selection '(
                                           ((output-dvi has-no-display-manager)  "dvi2tty")
                                           ((output-dvi style-pstricks)  "dvips and gv")
                                           (output-dvi "xdvi")
                                           (output-pdf "pdf-tools")
                                           (output-html "xdg-open"))))
      ))

  (use-package cdlatex
    :straight '(cdlatex :type git :host github :repo "cdominik/cdlatex")
    :after auctex)

  (use-package latex-preview-pane
    :straight '(latex-preview-pane :type git :no-byte-compile t :host github :repo "jsinglet/latex-preview-pane" :files ("*.el" "*.txt"))
    :after auctex)

  (use-package smartparens-latex
    :after auctex
    :straight smartparens
    :diminish
    )

  (use-package company-auctex
    :after (company auctex)
    :diminish
    :config
    (company-auctex-init)
    )

  (use-package company-math
    :after company
    :diminish
    ))

;;(when ech-use-cygwin
;;  (setq TeX-view-program-selection
;;        '((output-dvi "DVI Viewer")
;;          (output-pdf "PDF Viewer")
;;          (output-html "HTML Viewer")))
;;  (setq TeX-view-program-list
;;        '(("DVI Viewer" "cygstart -o %o")
;;          ("PDF Viewer" "open  -o %o")
;;          ("HTML Viewer" "open  -o %o"))))

(add-hook 'LaTeX-mode-hook 'ech:latex/mode-hook)
;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
          #'TeX-revert-document-buffer)


;; add latex to org babel languages
(add-to-list 'ech:babel/babel-languages 'latex)

(provide 'latex-conf)
