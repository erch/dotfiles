(require 'package-conf)

(eval-and-compile
  (use-package magit
    :demand t
    :diminish "")

  (use-package magit-log
    :straight '(magit-log :type built-in)
    :demand t
    :init
    (progn
      ;; Set `magit-log-margin' value in :init as many other variables will be
      ;; dynamically set based on its value when `magit-log' is loaded.
      ;; (setq magit-log-margin '(t age magit-log-margin-width t 18)) ;Default value
      ;; Show the commit ages with 1-char time units
      ;;   minute->m, hour->h, day->d, week->w, month->M, year->Y
      ;; Also reduce the author column width to 11 as the author name is being
      ;; abbreviated below.
      (setq magit-log-margin '(t age-abbreviated magit-log-margin-width :author 11)))
    :config
    (progn
      ;; Abbreviate author name. I added this so that I can view Magit log without
      ;; too much commit message truncation even on narrow screens (like on phone).
      (defun modi/magit-log--abbreviate-author (&rest args)
        "The first arg is AUTHOR, abbreviate it.
First Last  -> F Last
First.Last  -> F Last
Last, First -> F Last
First       -> First (no change).
It is assumed that the author has only one or two names."
        ;; ARGS               -> '((REV AUTHOR DATE))
        ;; (car ARGS)         -> '(REV AUTHOR DATE)
        ;; (nth 1 (car ARGS)) -> AUTHOR
        (let* ((author (nth 1 (car args)))
               (author-abbr (if (string-match-p "," author)
                                ;; Last, First -> F Last
                                (replace-regexp-in-string "\\(.*?\\), *\\(.\\).*" "\\2 \\1" author)
                              ;; First Last -> F Last
                              (replace-regexp-in-string "\\(.\\).*?[. ]+\\(.*\\)" "\\1 \\2" author))))
          (setf (nth 1 (car args)) author-abbr))
        (car args))                       ;'(REV AUTHOR-ABBR DATE)
      (advice-add 'magit-log-format-margin :filter-args #'modi/magit-log--abbreviate-author)))
  )

(provide 'magit-conf)

;; |---------+----------------------------------|
;; | Binding | Description                      |
;; |---------+----------------------------------|
;; | j n     | Jump to Untracked section        |
;; | j u     | Jump to Unstaged section         |
;; | j s     | Jump to Staged section           |
;; | j p     | Jump to Unpushed section         |
;; | M-p     | Jump to previous sibling section |
;; | M-n     | Jump to next sibling section     |
;; |---------+----------------------------------|
