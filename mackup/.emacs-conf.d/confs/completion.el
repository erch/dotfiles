(require 'package-conf)
(require 'core)

(eval-and-compile
  (defvar ysnippet-statics-dir (file-name-as-directory (expand-file-name "ysnippets" ech:core/statics-dir)) "directory where to find the ysnippets"))
(unless (file-exists-p ysnippet-statics-dir) (make-directory ysnippet-statics-dir t))
(defvar auto-insert-dir (file-name-as-directory (expand-file-name "autoinsert" ech:core/statics-dir)) "directory where to find autoinsert templates")
(unless (file-exists-p ysnippet-statics-dir) (make-directory ysnippet-statics-dir t))
(unless (file-exists-p auto-insert-dir) (make-directory auto-insert-dir t))

(eval-and-compile
  (use-package company
    :diminish ""
    :hook (prog-mode . company-mode)
    :bind  (:map company-active-map
		 ("C-/" . help-company)
		 ("C-M-/" . company-filter-candidates)
		 ("C-d" . company-show-doc-buffer)
		 ("C-c h" . company-quickhelp-manual-begin))
    :custom
    (company-idle-delay 0.2)
    (company-minimum-prefix-length 2)
    (company-require-match nil)
    (company-dabbrev-ignore-case nil)
    (company-dabbrev-downcase nil)
    :config
    (define-key company-mode-map [remap indent-for-tab-command] #'company-indent-or-complete-common))

  (use-package company-quickhelp
    :after company
    :config
    (company-quickhelp-mode))

  (use-package company-statistics
    :after company
    :hook (company-mode . company-statistics-mode)
    :custom
    (company-statistics-file (concat ech:core/cache-dir
                                     "company-statistics-cache.el")))
  (use-package fuzzy
    :after company)

  (use-package helm-company
    :after (company helm))

  (use-package yasnippet
    :demand t
    :commands (yas-global-mode)
    :diminish ""
    :config
    (yas-global-mode) ;;Toggle Yas minor mode in all buffers.
    :custom
    (yas-triggers-in-field t) ;; If non-nil, allow stacked expansions (snippets inside snippets).

    (yas-wrap-around-region t) ;;What to insert for snippet’s $0 field.
    ;; If set to a character, insert contents of corresponding register.
    ;; If non-nil insert region contents.
    ;; on multiple keys, fall back to completing read
    ;; typically this means helm
    (yas-prompt-functions '(yas-completing-prompt))
    ;; configure snippet directories
    (yas-snippet-dirs (list ysnippet-statics-dir))
    )

  (use-package auto-yasnippet
    :after yasnippet
    :bind
    ("C-c y a c" . aya-create)
    ("C-c y a e" . aya-expand)
    ("C-c y a o" . aya-open-line)
    ("C-c y a p" . aya-persist-snippet)
    )

  (use-package helm-c-yasnippet
    :after (helm yasnippet)
    :bind
    ("C-c y c" . helm-yas-complete)
    :custom
    (helm-c-yas-space-match-any-greedy t))
  )

;; package who wants to use auto-insert should addd valut to auto-insert-alist
;; Elements look like (CONDITION . ACTION) or ((CONDITION . DESCRIPTION) . ACTION).
;; CONDITION may be a regexp that must match the new file's name, or it may be
;; a symbol that must match the major mode for this element to apply.
;; Only the first matching element is effective.
;; Optional DESCRIPTION is a string for filling `auto-insert-prompt'.
;; ACTION may be a skeleton to insert (see `skeleton-insert'), an absolute
;; file-name or one relative to `auto-insert-directory' or a function to call.
;; ACTION may also be a vector containing several successive single actions as
;; described above, e.g. [\"header.insert\" date-and-author-update]."
(use-package autoinsert
  :straight '(autoinsert :type built-in)
  :demand t
  :custom
  ;; Controls automatic insertion into newly found empty files. Possible values:
  ;; nil	do nothing
  ;; t	insert if possible
  ;; other	insert if possible, but mark as unmodified.
  (auto-insert t)
  ;; Don't want to be prompted before insertion:
  (auto-insert-query nil)
  ;; directory where to find non absolute file-name specified in auto-insert-alist
  (auto-insert-directory auto-insert-dir)
  :config
  (auto-insert-mode 1))

(message "Finished loading completion")
(provide 'completion)
