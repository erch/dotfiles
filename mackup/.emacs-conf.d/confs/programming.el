(require 'package-conf)
(require 'core)
(require 'global-conf) ;; for hydra


(eval-and-compile

  (use-package ini-mode
    :straight '(ini-mode :type git :host github :repo "Lindydancer/ini-mode")
    :mode "\\.ini\\'"
    :diminish "")

  (use-package terraform-mode
    :mode "\\.tf\\'"
    :diminish "")

  (use-package company-terraform
    :after terraform-mode
    :config
    (progn (company-terraform-init)))

  (use-package dockerfile-mode
    :mode "Dockerfile\\'"
    :diminish "")

  (use-package web-mode
    :diminish ""
    :mode "\\.html\\'"
    :bind ((:map web-mode-map
		 ("C-c b" . web-beautify-html)))

    :config
    (progn
      (setq-default web-mode-markup-indent-offset 4) ; HTML
      (setq-default web-mode-css-indent-offset 4)    ; CSS
      (setq-default web-mode-code-indent-offset 4)) ; JS/PHP/etc

    )

  ;; need to do : npm install -g js-beautify.
  (use-package web-beautify
    :after web-mode)

  (use-package js2-mode
    :after web-mode
    :mode "\\.js\\'"
    :interpreter "node"
    :bind
    ((:map js2-mode-map
	   ("C-c b" . web-beautify-js)))
    )

  (defun ech-markdown-hook ()
    (make-local-variable 'before-save-hook)
    (remove-hook 'before-save-hook 'delete-trailing-whitespace))


  (use-package json-mode)

  (use-package markdown-mode
    :diminish ""
    :config
    (add-hook 'markdown-mode-hook #'ech-markdown-hook))

  (use-package yaml-mode :diminish "")

  (use-package restclient
    :mode "\\.rest$"
    :diminish ""
    :hook (restclient-mode . (lambda ()
                               (progn
				 (local-set-key (kbd "C-c C-c")
						'restclient-http-send-current-stay-in-window)
				 (set (make-local-variable 'company-backends)
                                      '(company-restclient))
				 (company-mode t)))))

  (use-package company-restclient
    :diminish ""
    :after (company restclient)
    )

  (use-package aggressive-indent)

  (use-package projectile
    :diminish ""
    :after hydra
    :demand t
    :init
    (autoload 'projectile-project-root "projectile")
    :bind (("C-c p" . #'hydra-projectile/body)
           :map projectile-mode-map
	   ("s-p" . 'projectile-command-map)
	   ("C-c p" . 'projectile-command-map))
    :config
    (progn
      (projectile-mode)

      (defhydra hydra-projectile (:color teal
			                 :columns 4)
	"Projectile"
	("f"   projectile-find-file                "Find File")
	("r"   projectile-recentf                  "Recent Files")
	("z"   projectile-cache-current-file       "Cache Current File")
	("x"   projectile-remove-known-project     "Remove Known Project")

	("d"   projectile-find-dir                 "Find Directory")
	("b"   projectile-switch-to-buffer         "Switch to Buffer")
	("c"   projectile-invalidate-cache         "Clear Cache")
	("X"   projectile-cleanup-known-projects   "Cleanup Known Projects")

	("o"   projectile-multi-occur              "Multi Occur")
	("s"   projectile-switch-project           "Switch Project")
	("k"   projectile-kill-buffers             "Kill Buffers")
	("q"   nil "Cancel" :color blue))
      ))

  (use-package helm-projectile
    :after projectile
    :config
    (setq projectile-completion-system 'helm)
    (helm-projectile-on)
    :diminish "")

  (use-package dumb-jump
    :diminish ""
    :after hydra
    :bind ("C-c j" . #'hydra-dumb-jump/body)
    :config (setq dumb-jump-selector 'helm)
    :init
    (defhydra hydra-dumb-jump (:color blue :columns 3)
      "Dumb Jump"
      ("j" dumb-jump-go "Go")
      ("o" dumb-jump-go-other-window "Other window")
      ("e" dumb-jump-go-prefer-external "Go external")
      ("x" dumb-jump-go-prefer-external-other-window "Go external other window")
      ("i" dumb-jump-go-prompt "Prompt")
      ("l" dumb-jump-quick-look "Quick look")
      ("b" dumb-jump-back "Back")))


  ;; A extensible, modular GNU Emacs front-end for interacting with external debuggers
  ;; not teste yet : To load the package:  M-x load-library realgud, To unload the package:  M-x realgud-unload-features
  (use-package realgud
    :diminish "")

  ;; --- lsp: Language Server Protocol support with multiples languages support for Emacs
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-l")

  (use-package lsp-mode
    ;; :straight '(lsp-mode :type git :host github :repo "emacs-lsp/lsp-mode.git" files (:defaults "clients/*.el"))
    :demand t
    :custom
    (lsp-enable-snipet t)
    (lsp-enable-folding t)
					;(lsp-semantic-tokens-enable t)
    (lsp-auto-guess-root t)
    (lsp-document-sync-method 'lsp--sync-incremental) ;; How to sync the document with the language serve
    (lsp-eldoc-render-all t)
    (lsp-auto-configure t)
    :init
    (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
    :commands lsp)

  (use-package lsp-ui :commands lsp-ui-mode)
  ;; if you are helm user
  (use-package helm-lsp :commands helm-lsp-workspace-symbol)
  ;;(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

  ;; optionally if you want to use debugger : need to find the receipt before using it
  ;;(use-package dap-mode)
  ;;(use-package dap-LANGUAGE) ;; to load the dap adapter for your language

  ;; installing egot: https://github.com/joaotavora/eglot
  ;; an Emacs LSP client that stays out of your way

  (defun eglot-config ()
    (when (eglot-managed-p)
      (message "eglot config")
      ))

  (use-package eglot
    :init
    (add-hook 'eglot-managed-mode-hook #'eglot-config)
    :config
    ;; workaround for error in eglot project-root not found
    (defun project-root (project)
      (projectile-project-root))
    :demand t
    :bind
    (:map eglot-mode-map
    ("C-c h" . #'eglot-help-at-point)
    ("C-M-\\" . eglot-format)
    ("C-c r" . #'eglot-rename)
    ("C-c f" . #'xref-find-definitions)
    ("C-c d" . #'eglot-find-declaration)
    ("C-c i" . #'eglot-find-implementation)
    ("C-c t" . #'eglot-find-typeDefinition)))
)

(defun describe-thing-in-popup ()
  (interactive)
  (let* ((thing (symbol-at-point)))
    (cond
     ((fboundp thing) (describe-in-popup 'describe-function))
     ((boundp thing) (describe-in-popup 'describe-variable)))))

(defun describe-in-popup (fn)
  (let* ((thing (symbol-at-point))
         (description (save-window-excursion
                        (funcall fn thing)
                        (switch-to-buffer "*Help*")
                        (buffer-string))))
    (popup-tip description
               :point (point)
               :around t
               :height 30
               :scroll-bar t
               :margin t)))

;; Key binding
(global-set-key (kbd "ESC M-p") 'describe-thing-in-popup)



(message "Finished loading programming")
(provide 'programming)
