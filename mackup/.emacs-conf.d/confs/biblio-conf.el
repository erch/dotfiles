(message "loading org-agenda-conf ...")
;;---- loading dependencies
(require 'org-conf)
(require 'package-conf)
(require 'utility-funcs)
(require 'debug-conf)
(require 'ert)                  ;; for unit tests

;;---- constant definition


;;--- ebib configuration
;; Ebib is a program with which you can manage biblatex and BibTeX database files without having to edit the raw .bib files.
(eval-and-compile
  (use-package ebib
    :diminish
    :bind
    (("C-c B" . ebib))
    :custom
    (ebib-bibtex-dialect 'biblatex)))

;;--- org-ref configuration and helm-bibtext

(message "biblio-conf loaded")
(provide 'biblio-conf)
