(require 'org-orga-conf)
(defconst ech:org-pub/emacs-conf-src-dir (file-name-as-directory (f-join ech:conf/emacs-dir "org")) "directory where the org files for the emacs configuration generation are located.")
(defconst ech:org-pub/emacs-conf-pub-dir (file-name-as-directory (f-join ech:conf/emacs-dir "doc")) "directory where to publish the emacs configuration documentation.")

(defun ech:org-pub/init-org-pub ()
  (custom-set-variables
   '(org-publish-project-alist
     (list (cons "emacs-conf" (list
                               :base-directory ech:org-pub/emacs-conf-src-dir
                               :base-extension "org"
                               :publishing-directory ech:org-pub/emacs-conf-pub-dir
                               :recursive t
                               :publishing-function 'org-html-publish-to-html
                               :headline-levels 3
                               :section-numbers 3
                               :with-toc t
                               :html-doctype    "html5"
                               :html-preamble t))))))

(add-hook 'use-package--org--post-init-hook #'ech:org-pub/init-org-pub)

(provide 'org-pub-conf)
