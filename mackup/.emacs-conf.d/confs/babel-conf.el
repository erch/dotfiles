  (require 'package-conf)
  (require 'org-conf)

;; add languages we want to use in babel
(eval-and-compile
  (defcustom ech:babel/babel-languages '("emacs-lisp") "babel languages"))
(add-hook 'ech:core/package-load-hook
          (lambda ()
            (let ((lang))
              (unless (boundp 'org-babel-load-languages)
                (setq org-babel-load-languages ()))
              (dolist (lang ech:babel/babel-languages)
                (add-to-list 'org-babel-load-languages
                             (cons (if (stringp lang)
                                       (intern lang)
                                     lang)
                                   t))))
            (message (format "babel languages : %S" org-babel-load-languages))
            (org-babel-do-load-languages
             'org-babel-load-languages
             org-babel-load-languages)))

(eval-and-compile
  (when (executable-find "jupyter")
    (defun ech:babel/python-locate ()
      (executable-find "python"))

    (defun advice-jupyter-locate-python ()
      (advice-add #'jupyter-locate-python :before-until #'ech:babel/python-locate)
      )

    (let ((noninteractive t))
      (use-package jupyter
        :no-require t
        :after org
        :diminish ""
        :config
        (add-to-list 'ech:babel/babel-languages 'jupyter)
        (advice-jupyter-locate-python)))))

;; ein is a python notebook emacs integration that support also babel
(eval-and-compile
  (use-package ein
    :after org
    :config
    (add-to-list 'ech:babel/babel-languages 'ein)
    :custom
    (ein:polymode t)
    (ein:jupyter-default-server-command "jupyter")
    (ein:jupyter-server-args '("--ip=localhost" "--no-browser"))
    :diminish ""))

(defun ein:start (notebook-directory &optional server-cmd-path
                                     no-login-p login-callback port)
  (interactive
   (let* ((notebook-directory (read-directory-name "This Notebook directory: "
                                                   (or *ein:last-jupyter-directory*
                                                       ein:jupyter-default-notebook-directory))))
     (list notebook-directory nil nil nil nil)))
  (let* ((login-callback-arg (or login-callback (lambda (buffer url-or-port) (pop-to-buffer buffer))))
         (proj-root (projectile-project-root notebook-directory))
         (jupyter-exec (if proj-root
                           (executable-find (expand-file-name ".venv/bin/jupyter" proj-root))
                         (executable-find "jupyter"))))
    (message (format "starting jupyter, server: %s, notebook dir: %s" jupyter-exec notebook-directory))
    (ein:jupyter-server-start jupyter-exec notebook-directory no-login-p login-callback-arg port)))

(defun ech:babel/execute-startup-block ()
  (message "executing ech:babel/execute-startup-block")
  (message "current buffer %s" (buffer-name))
  (when (org-babel-find-named-block  "startupblock")
    (progn
      (save-excursion
        (org-babel-goto-named-src-block "startupblock")
        (org-babel-execute-src-block)))))

(with-eval-after-load 'org
  (add-hook 'org-mode-hook #'ech:babel/execute-startup-block))

(with-eval-after-load 'org
  (setq org-confirm-babel-evaluate nil) ;; don't need to confirm each evaluation
  (add-hook 'org-mode-hook
            (lambda ()
              ;; Will tangle the org file on save.
              (message "adding hook from org-mode-hook")
              (make-local-variable 'after-save-hook) ;; this is necessary even with a non nil fourth parameter to the call to add-hook set to non nil,
              ;; otherwise the previous hooks will not be copied to the buffer local value of the hook.
              (add-hook 'after-save-hook #'org-babel-tangle 'run-at-end 'only-in-org-mode))))

  ;; Allow to display images in the org buffer
  (add-hook 'org-babel-after-execute-hook #'org-display-inline-images 'append)

(defun ech:org/eval-source-block (name)
  (when (org-babel-find-named-block name)
    (save-excursion
      (org-babel-goto-named-src-block name)
      (org-babel-execute-src-block)))
  (message (format "block %s not found" name))
  nil)

(defun ech:org/eval-source-block-list (block-list)
  (mapc (lambda (x)
          (when (org-babel-find-named-block x)
            (ech:org/eval-source-block x))) block-list))

(defun ech:org/text-no-property (txt)
  (if (char-or-string-p txt)
      (let ((res (concat txt)))
        (set-text-properties 0 (length res) nil res)
        res)
    txt))

(defun ech:org/list-or-text-no-property(lst)
  (cond
   ((consp lst)
    (mapcar #'ech:org/text-no-property lst))
   ((char-or-string-p lst)
    (ech:org/text-no-property lst))
   (t lst)))

(ert-deftest ech:org/test-text-no-property ()
  "Test text no property"
  (let ((tt "text")
        (res))
    (add-text-properties 0 (length tt) '(comment t face highlight) tt)
    (setq res (ech:org/text-no-property tt))
    (should (equal "text" res))
    (should-not (get-text-property 0 'comment res))
    (should (equal "text" tt))
    (should (get-text-property 0 'comment tt))))

(ert-deftest ech:org/test-list-or-text-no-property()
  "test list text no property"
  (let* ((txt1 "text1")
         (txt2 "text2")
         (res)
         (lst (progn
                (add-text-properties 0 (length txt1) '(comment t face highlight) txt1)
                (add-text-properties 0 (length txt2) '(comment t face highlight) txt2)
                (list txt1 txt2))))
    (setq res (ech:org/list-or-text-no-property lst))
    (should (equal "text1" (car res)))
    (should (equal "text2" (car (cdr res))))
    (should-not (get-text-property 0 'comment (car res)))
    (should-not (get-text-property 0 'comment (car (cdr res))))
    (should (get-text-property 0 'comment (car lst)))
    (should (get-text-property 0 'comment (car (cdr lst))))
    (should (equal "text1" (car lst)))
    (should (equal "text2" (car (cdr lst))))))

(provide 'babel-conf)
