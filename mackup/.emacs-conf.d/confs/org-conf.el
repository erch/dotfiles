(message "loading org-conf")
(require 'package-conf)
(require 'utility-funcs)
(require 'completion)
(require 'filenotify)
(require 'autoinsert)
(require 'helm-conf)
(require 'ert)                  ;; for unit tests

(defun ech:org/configure ()
  (progn
    (eval-after-load 'face-remap '(diminish 'buffer-face-mode))
    (eval-after-load 'simple '(diminish 'visual-line-mode))
    (add-hook 'org-mode-hook (lambda ()
                               (progn
                                (visual-line-mode)
                                (variable-pitch-mode)
                                (flyspell-mode t))))
    (require 'org-inlinetask)
    (message "ech:org/configure done")
    t
    ))
(add-hook 'use-package--org--post-config-hook #'ech:org/configure)

(add-to-list 'load-path (f-join ech:core/local-dir "org-mode" "lisp"))
(add-to-list 'load-path (f-join ech:core/local-dir "org-mode" "contrib" "lisp"))

(use-package org
  :demand t
  :init
  (require 'org-loaddefs)
  ;;:straight '(org :type built-in)
  :bind
  ("C-c o l" . org-store-link)
  ("C-c o a" . org-agenda)
  ("A-h" . org-mark-element)
  ("C-c o c" . org-capture)
  :custom
  (org-log-done t)
  (org-src-fontify-natively t)
  (org-src-tab-acts-natively t)
  (org-tags-column 0)
  (org-hide-emphasis-markers t)
  (org-log-into-drawer t)
  (org-list-indent-offset 4)
  (org-indent-indentation-per-level 4)
  (org-startup-indented t)
  :custom-face
  (org-indent                ((t (:inherit (org-hide fixed-pitch)))))
  ;:config
                                        ;(require 'ox-confluence)
  )



(defun ech:org/init-custom-face ()
  (progn
    (custom-set-faces
     ;; org-hide: "Face used to hide leading stars in headlines.The foreground color of this face should be equal to the background color of the frame."
     '(variable-pitch            ((t (:family "Source Sans Pro" :height 130 :weight normal))))
     '(fixed-pitch               ((t (:family "Inconsolata" :slant normal :weight normal :height 1.0 :width normal))))
     '(org-block                 ((t (:family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal))))
     '(org-code                  ((t (:family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal))))
     '(org-document-info         ((t (:foreground "dark orange"))))
     '(org-document-info-keyword ((t (:inherit shadow :family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal))))
     '(org-document-title        ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro" :height 2 :underline nil)))
     '(org-level-1               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro" :height 1.8)))
     '(org-level-2               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro" :height 1.6)))
     '(org-level-3               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro" :height 1.4)))
     '(org-level-4               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro" :height 1.2)))
     '(org-level-5               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro")))
     '(org-level-6               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro")))
     '(org-level-7               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro")))
     '(org-level-8               ((t (:inherit default :weight bold :foreground (face-foreground 'default nil 'default)) :font "Source Sans Pro")))
     '(org-link                  ((t (:foreground "royal blue" :underline t))))
     '(org-meta-line             ((t (:inherit (font-lock-comment-face fixed-pitch shadow) :height 0.9))))
     '(org-property-value        ((t (:family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal))))
     '(org-special-keyword       ((t (:inherit font-lock-comment-face :family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal)))) ; "Face used for special keywords."
     '(org-tag                   ((t (:inherit shadow :family "Inconsolata" :slant normal :weight bold :height 0.9))))
     '(org-verbatim              ((t (:inherit shadow :family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal))))
     '(org-default      ((t (:inherit variable-pitch :height 0.8)))) ; "Face used for default text."
     '(org-table   ((t (:family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal)))) ;  "Face used for tables."
     '(org-formula ((t (:family "Inconsolata" :slant normal :weight normal :height 0.85 :width normal)))) ;  " Face for formulas."
     )
    t
    ))

(add-hook 'use-package--org--post-init-hook #'ech:org/init-custom-face)

(defun ech:org/latex-init ()
  (progn
    (custom-set-variables
     ;; hack , would have been better to change only the :programs , :latex-compiler and :image-converter parts
     '(org-preview-latex-process-alist
       '((dvipng :programs ("/usr/bin/latex" "dvipng")
                 :description "dvi > png"
                 :message "you need to install the programs: latex and dvipng."
                 :image-input-type "dvi"
                 :image-output-type "png"
                 :image-size-adjust (1.5 . 1.5)
                 :latex-compiler ("/usr/bin/latex -interaction nonstopmode -output-directory %o %f")
                 :image-converter ("/usr/bin/dvipng -D %D -T tight -o %O %f"))
         (dvisvgm :programs ("/usr/bin/latex" "/usr/bin/dvisvgm")
                  :description "dvi > svg"
                  :message "you need to install the programs: latex and dvisvgm."
                  :image-input-type "dvi"
                  :image-output-type "svg"
                  :image-size-adjust (1.75 . 1.75)
                  :latex-compiler ("/usr/bin/latex -interaction nonstopmode -output-directory %o %f")
                  :image-converter ("/usr/bin/dvisvgm %f -n -b min -c %S -o %O"))
         (imagemagick :programs ("/usr/bin/latex" "/usr/bin/convert")
                      :description "pdf > png"
                      :message "you need to install the programs: latex and imagemagick."
                      :image-input-type "pdf"
                      :image-output-type "png"
                      :image-size-adjust (1.0 . 1.0)
                      :latex-compiler ("/usr/bin/pdflatex -interaction nonstopmode -output-directory %o %f")
                      :image-converter ("/usr/bin/convert -density %D -trim -antialias %f -quality 100 %O"))))
     ;;'(org-preview-latex-default-process  'dvisvgm)
     '(org-preview-latex-default-process  'dvipng)
     )
    t))

(defun ech:org/latex-config ()
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.25))
  (add-hook 'org-mode-hook (lambda ()
                             #'turn-on-org-cdlatex
                             ;; annoying keybiding in cdlatex , not replace for now.
                             (local-unset-key (kbd "M-")))))

(add-hook 'use-package--org--post-init-hook #'ech:org/latex-init)
(add-hook 'use-package--org--post-config-hook #'ech:org/latex-config)

(use-package org-indent
  :after org
  :straight '(org-indent :type built-in)
  :custom
  (org-list-indent-offset 4)
  (org-indent-indentation-per-level 4)
  (org-startup-indented t)
  :custom-face
  (org-indent                ((t (:inherit (org-hide fixed-pitch)))))
  :diminish "")

(eval-and-compile
  (use-package org-superstar
    :hook
    (org-mode . (lambda () (org-superstar-mode 1)))))

(eval-and-compile
  (use-package toc-org
    :after org
    :hook
    (org-mode . toc-org-enable)))

(defun ech:org/add-capture-template(template)
  (unless (boundp 'org-capture-templates) (setq org-capture-templates ()))
  (add-to-list 'org-capture-templates template))

(progn
  (ech:org/add-capture-template
   '("t" "Todo [inbox]" entry
     (file+olp+datetree ech:org/inbox-filepath "Tasks")
     "* INBOX %i%?\n%T\n" :empty-lines-after 1))
  (ech:org/add-capture-template
   '("T" "Tickler" entry
     (file+headline ech:org/ticklers-file "Tickler")
     "* INBOX %i%? \n %U" :empty-lines-after 1))
  (ech:org/add-capture-template
   '("C" "Clip" entry
     (file+olp+datetree ech:org/inbox-filepath "Notes")
     "* Clip\n %U\n%i\n\s" :empty-lines-after 1 :tree-type "week")))

(progn
  (ech:org/add-capture-template
   '("p" "Protocol content"
     entry (file+headline ech:org/inbox-filepath "Notes")
     "* [[%:link][%:description]]\n%Captured on :%U\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n" :immediate-finish t :empty-lines-after 1 :tree-type "week"))
  (ech:org/add-capture-template
   '("L" "Protocol Link" entry (file+headline ech:org/inbox-filepath "Notes")
     "* %? [[%:link][%:description]] \nCaptured On: %U" :immediate-finish t :empty-lines-after 1 :tree-type "week")))

(use-package  org-protocol-capture-html
  :after org
  :straight '(org-protocol-capture-html :type built-in))

(ech:org/add-capture-template
 '("w" "web site" entry (file+headline ech:org/inbox-filepath "Notes")
   "* [[%:link][%:description]]\nCaptured on : %U\n%i\n" :immediate-finish t :empty-lines-after 1 :tree-type "week"))

(eval-and-compile
  (use-package org-cliplink
    :after org
    :bind
    ("C-x p i" . org-cliplink)))

(eval-and-compile
  (use-package ox-clip
    :after org
    :bind
    ("C-c o k" . ox-clip-formatted-copy)
    ))

(defun ech:org/insert-uuid ()
  "Insert a UUID.
This commands calls “uuidgen” on MacOS, Linux, and calls PowelShell on Microsoft Windows.
URL `http://ergoemacs.org/emacs/elisp_generate_uuid.html'
Version 2020-06-04"
  (interactive)
  (cond
   ((string-equal system-type "windows-nt")
    (shell-command "pwsh.exe -Command [guid]::NewGuid().toString()" t))
   ((string-equal system-type "darwin") ; Mac
    (shell-command "uuidgen" t))
   ((string-equal system-type "gnu/linux")
    (shell-command "uuidgen" t))
   (t
    ;; code here by Christopher Wellons, 2011-11-18.
    ;; and editted Hideki Saito further to generate all valid variants for "N" in xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx format.
    (let ((myStr (md5 (format "%s%s%s%s%s%s%s%s%s%s"
                              (user-uid)
                              (emacs-pid)
                              (system-name)
                              (user-full-name)
                              (current-time)
                              (emacs-uptime)
                              (garbage-collect)
                              (buffer-string)
                              (random)
                              (recent-keys)))))
      (insert (format "%s-%s-4%s-%s%s-%s"
                      (substring myStr 0 8)
                      (substring myStr 8 12)
                      (substring myStr 13 16)
                      (format "%x" (+ 8 (random 4)))
                      (substring myStr 17 20)
                      (substring myStr 20 32)))))))

  (provide 'org-conf)
