(setq ech:debug/dbg-buff nil)

(defun ech:debug/init-dbg (&optional name)
  (interactive "sDebug buffer name (*Debug*):")
  (let* ((buff-name (if (and name (not (string-blank-p name))) name "*Debug*"))
        (dbg (get-buffer-create buff-name)))
    (with-current-buffer dbg
      (fundamental-mode)
      (setq-local inhibit-read-only t)
      (erase-buffer)
      (goto-char (point-min))
      )
    (setq ech:debug/dbg-buff dbg)))

(defun ech:debug/dbg-print(&rest objs)
  (when ech:debug/dbg-buff
    (with-current-buffer ech:debug/dbg-buff
	  (progn
	    (goto-char (point-max))
      (setq-local inhibit-read-only t)
	    (mapc (lambda(x)
		        (insert
		         (if (stringp x)
		             x
		           (format "%S\n" x))))
		      objs)
        (setq-local inhibit-read-only nil)))))

(defun ech:debug/dbg-show-buffer()
  (progn
    (with-current-buffer ech:debug/dbg-buff
      (help-mode))
    (pop-to-buffer ech:debug/dbg-buff
                   '(
                     (display-buffer-reuse-window display-buffer-in-direction display-buffer-use-some-window display-buffer-pop-up-window  )
                     (direction . below)
                     (inhibit-same-window . t)
                     (inhibit-switch-frame . t)
                     (window-width 0.5)
                     (window-height . 0.5)
                     )
                   nil)))

(defun dbg-truncate-string (string maxchar)
  "cut a string to maxchar if its length exceeds it"
  (substring string  0 (min maxchar (- 1 (length string)))))

(provide 'debug-conf)
