;; General Settings
(require 'core)
(require 'uniquify)
(require 'package-conf)

(setq uniquify-buffer-name-style 'forward)

(setq
 calendar-week-start-day 1 ; week starts on Monday)
 apropos-do-all t
 auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
 backup-by-copying t    ; Don't delink hardlinks
 backup-directory-alist `(("." . ,(concat ech:core/cache-dir  "backups")))
 column-number-mode t                   ;show the column number
 confirm-nonexistent-file-or-buffer nil ;don't ask to create a buffer
 create-lockfiles nil
 delete-old-versions t  ; Automatically delete excess backups
 ediff-window-setup-function 'ediff-setup-windows-plain
 enable-recursive-minibuffers t
 eval-expression-print-length nil       ;do not truncate printed expressions
 gc-cons-threshold 50000000  ;; reduce the frequency of garbage collection by making it happen on each 50MB of allocated data (the default is on every 0.76MB)
 history-length 250                     ;default is 30
 indicate-buffer-boundaries 'left       ;fringe markers
 inhibit-startup-message t
 kept-new-versions 20   ; how many of the newest backup versions to keep
 kept-old-versions 5    ; and how many of the old backup to keep
 kill-ring-max 5000                     ;truncate kill ring after 5000 entries
 large-file-warning-threshold 100000000 ;; warn when opening files bigger than 100MB
 load-prefer-newer t                    ;prefer newer .el instead of the .elc
 locale-coding-system 'utf-8            ;utf-8 is default
 mark-ring-max 5000                     ;truncate mark ring after 5000 entries
 max-lisp-eval-depth 2000
 max-specpdl-size 2000
 mouse-autoselect-window -.1            ;window focus follows the mouse pointer
 mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control))) ;make mouse scrolling smooth
 mouse-yank-at-point t                  ;middle click with the mouse yanks at point
 require-final-newline t                ;auto add newline at the end of file
 save-interprogram-paste-before-kill t
 save-place-file (concat user-emacs-directory "places")
 sentence-end-double-space nil
 split-height-threshold 110             ;more readily split horziontally
 tab-always-indent 'complete            ;try to complete before identing
 user-full-name "Eric Chastan"
 user-mail-address "eric@chastan.consulting"
 vc-follow-symlinks t                   ;follow symlinks automatically
 version-control t      ; Use version numbers on backups
 visible-bell t
 )
 ;;browse-url-browser-function 'eww-browse-url ; browse with emacs eww
 (setq browse-url-browser-function 'browse-url-xdg-open)


;; Default Settings
;; Some variables like tab-width cannot be set globally: tab-width is a variable defined in `C source code’. Automatically becomes buffer-local when set.
;; Whenever they are set the value becomes buffer-local. To be able to set such a variable globally we have to use setq-default which modifies theR default value of the variable.
(setq-default
 tab-width 4
 indent-tabs-mode nil                   ;use spaces instead of tabs
 c-basic-offset 4                       ;"tab" with in c-related modes
 c-hungry-delete-key t                  ;delete more than one space
 fill-column 110 ; to avoid warning in language modes with layout checker
 )

;;Global Modes
(auto-compression-mode 1) ; Use compressed files as if they were normal
(blink-cursor-mode -1)       ;no cursor blinking
(column-number-mode 1)			;show column number
(display-time-mode 1)        ; Time in the modeline
(global-auto-revert-mode 1)  ;auto revert buffers when changed on disk
(show-paren-mode 1)

(menu-bar-mode -1)           ;no menu, you can toggle it with C-c m
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

;; These are built-in global modes/settings. Not sure where to put them so they ended up here…
;; Prompt Behavior
(defalias 'yes-or-no-p 'y-or-n-p)

;; enabled change region case commands
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; enable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

;; enable erase-buffer command
(put 'erase-buffer 'disabled nil)

;; encoding in utf-8
(set-terminal-coding-system 'utf-8-emacs)
(set-keyboard-coding-system 'utf-8-emacs)
(set-language-environment 'utf-8)

;; start emacs-server if not running must be moved elsewhere
(require 'server)
(add-hook 'emacs-startup-hook
          (lambda ()
            (unless server-process
              (server-start))))

;; never want whitespace at the end of lines. Remove it on save.
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Fix problem with unsolicitated paste when opening the first buffer.
;; see: https://github.com/syl20bnr/spacemacs/issues/5435#issuecomment-195862080
;; This problem may be related with wid-edit.el and mouse-1-click-follows-link.
;; Now I can avoid automatic yank by putting the following config in spacemacs/user-config.
(add-hook 'prog-mode-hook (lambda () (set (make-local-variable 'mouse-1-click-follows-link) nil)))

;; Load common utility packages
(eval-and-compile
  (use-package dash :demand t)  ;; A modern list api for Emacs
  (use-package f :demand t) ;; f.el is a modern API for working with files and directories in Emacs.
  (use-package s :demand t) ; The long lost Emacs string manipulation library.
)
;; load cl-seq once here (no require in it ??), no need to require it if utility is loaded.
;; advanced functions for operating on sequences or lists
(load-library "cl-seq")

(message "Finished loading base-conf")
(provide 'base-conf)
