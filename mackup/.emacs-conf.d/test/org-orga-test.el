(require 'org-agenda)
(defun get-org-test-buffer (name)
  (let* ((buff (get-buffer name))
         (file-name (buffer-file-name buff)))
    (when buff
      (progn
        (with-current-buffer buff
          (save-buffer 0))
        (kill-buffer buff)))
    (when (and file-name (f-exists? file-name))
      (f-delete file-name)))
  (let ((buff (get-buffer-create name)))
    (with-current-buffer buff
      (org-mode)
      (setq-local inhibit-read-only t)
      (erase-buffer)
      (goto-char (point-min))
      )
    buff))

(defun add-todo (buff &rest args)
  "take a plist as parameter
     :kwd
     :title
     :tags
     :deadline
     :scheduled"
  (with-current-buffer buff
    (let*
        ((kwd (concat (plist-get args :kwd)))
         (title (concat (plist-get args :title)))
         (tags  (plist-get args :tags))
         (deadline (plist-get  args :deadline))
         (scheduled (plist-get args :scheduled))
         (org-deadline (plist-get  args :org-deadline))
         (org-scheduled (plist-get args :org-scheduled))
         (deadline-str (cond
                        (org-deadline org-deadline)
                        (deadline (concat "DEADLINE: <" (format-time-string "%Y-%m-%d %a" deadline) ">"))
                        (t nil)))
         (scheduled-str (cond
                         (org-scheduled org-scheduled)
                         (scheduled (concat "SCHEDULED: <" (format-time-string "%Y-%m-%d %a" scheduled) ">"))
                         (t nil)))
         (tags-str (if tags (mapconcat #'identity tags ":") nil))
         )

      (goto-char (point-max))
      (insert (format "* %s  %s " kwd title))
      (when tags-str (insert (format ":%s:" tags-str)))
      (insert "\n")
      (when deadline-str (insert (format "%s\n" deadline-str)))
      (when scheduled-str (insert (format "%s\n" scheduled-str))))))

(defun test-buffer-show(buff)
  (progn
    (with-current-buffer buff
      (pop-to-buffer buff
                     '(
                       (display-buffer-reuse-window display-buffer-in-direction display-buffer-use-some-window   display-buffer-pop-up-window)
                       (direction . bellow)
                       (inhibit-same-window . t)
                       (inhibit-switch-frame . t)
                       (window-width 0.5)
                       (window-height . 0.5)
                       )
                     nil))))

  (defun ech-print-todo (hl)
    ;;(message (format "dans print todo hl= %S" hl))
    (let* ((title (ech:org/text-no-property (org-element-property :title hl)))
           (kwd (ech:org/text-no-property (org-element-property :todo-keyword hl)))
           (tags (ech:org/list-text-no-property (org-element-property :tags hl)))
           (deadline-raw (org-element-property :deadline hl))
           (scheduled-raw (org-element-property :scheduled hl))
           (deadline (if deadline-raw (org-timestamp-to-time deadline-raw) nil))
           (scheduled (if scheduled-raw (org-timestamp-to-time scheduled-raw) nil))
           (plan-date (ech:org/plan-date tags deadline scheduled)))
      (ech:debug/dbg-print
       (format "Headline: %s\n" title)
       (format "\tKeyword: %S\n" kwd)
       (format "\ttags: %S\n" tags)
       (format "\tdeadline-raw: %S\n" deadline-raw)
       (format "\tscheduled-raw: %S\n" scheduled-raw)
       (format "\tdeadline: %S\n" (if deadline (decode-time deadline) nil))
       (format "\tscheduled: %S\n" (if scheduled (decode-time scheduled) nil))
       (format "\tPlan date: %S\n" (if plan-date (decode-time plan-date) nil))
       )))

(defun test-debug ()
  (ech:debug/init-dbg "*Test Debug*")
  (ech:debug/dbg-print "OK\n")
  (ech:debug/dbg-show-buffer)
  (ech:debug/dbg-print "reOk\n"))
;;(test-debug)

(defun interactive-test-plan-date ()
  (ech:org/eval-source-block-list '("print-todo" "org-buffer" "compare-todo"))
  (let ((buff (get-org-test-buffer "*test plan*")))
    (progn
      (ech:debug/init-dbg "*Debug Test Plan")
      (add-todo buff :kwd "TODO" :title "this week" :tags '("perso" "THIS_WEEK" "ACTION") :scheduled (encode-time 0 0 0 25 11 2020))
      (add-todo buff :kwd "INBOX" :title "scheduled" :tags '("NEXT_WEEK") :deadline (encode-time 0 0 0 25 11 2020))
      (add-todo buff :kwd "IN_PROGRESS" :title "this month" :tags '("THIS_MONTH") :deadline (encode-time 0 0 0 31 1 2021) :scheuled (encode-time 0 0 0 20 2 2021))
      (add-todo buff :kwd "IN_PROGRESS" :title "this month" :tags '("NEXT_MONTH") :deadline (encode-time 0 0 0 31 1 2021) :scheduled (encode-time 0 0 0 20 2 2021))

      (with-current-buffer buff
        (progn
          (org-element-map
              (org-element-parse-buffer 'headline)
              'headline
            (lambda (hl)
              (when (org-element-property :todo-keyword hl)
                (progn
                  (message "avant print todo")
                  (ech-print-todo hl)))))))
      (ech:debug/dbg-show-buffer)
      (test-buffer-show buff))))

;;(interactive-test-plan-date)

(ert-deftest ech:org/test-plan-date ()
  (let (
        (test-data `(
                     (:now ,(encode-time 0 0 0 11 11 2020) :deadline ,(encode-time 0 0 0 20 11 2020) :scheduled ,(encode-time 0 0 0 22 11 2020) :tags ("THIS_WEEK" "perso") :res (15 11 2020))
                     (:now ,(encode-time 0 0 0 11 11 2020) :deadline ,(encode-time 0 0 0 12 11 2020) :scheduled ,(encode-time 0 0 0 22 11 2020) :tags ("THIS_WEEK" "perso") :res (12 11 2020))
                     (:now ,(encode-time 0 0 0 15 11 2020) :deadline ,(encode-time 0 0 0 20 11 2020) :scheduled ,(encode-time 0 0 0 22 11 2020) :tags ("THIS_WEEK" "perso") :res (15 11 2020))
                     (:now ,(encode-time 0 0 0 16 11 2020) :deadline ,(encode-time 0 0 0 23 11 2020) :scheduled ,(encode-time 0 0 0 23 11 2020) :tags ("THIS_WEEK" "perso") :res (22 11 2020))
                     (:now ,(encode-time 0 0 0 16 11 2020) :deadline ,(encode-time 0 0 0 11 11 2020) :scheduled ,(encode-time 0 0 0 10 11 2020) :tags ("THIS_MONTH" "perso") :res (6 12 2020))
                     (:now ,(encode-time 0 0 0 19 11 2020) :scheduled ,(encode-time 0 0 0 19 11 2020) :tags ("AGENDA" "perso") :res (19 11 2020))
                     )))
    (ech:debug/init-dbg "*Debug Test Plan*")
    (mapc (lambda(x)
            (ech:debug/dbg-print (format "got x = %S\n" x))
            (let ((expected (plist-get x :res))
                  (res (decode-time (ech:org/plan-date (plist-get x :tags)  (plist-get x :deadline)  (plist-get x :scheduled) (plist-get x :now)))))
              (should (equal (nth 0 expected) (nth 3 res)))
              (should         (equal (nth 1 expected) (nth 4 res)))
              (should       (equal (nth 2 expected) (nth 5 res))))) test-data)))

(ert-deftest ech:org/test-compare ()
  (let* (
         (seconds-in-day (* 3600 24))
         (seconds-in-week (* 7 seconds-in-day))
         (test-plists
          `(
            (:first
             (:kwd "TODO" :title "first" :tags ("perso" "THIS_WEEK" "ACTION") :scheduled ,(time-add nil (+ seconds-in-week seconds-in-day)))
             :second
             (:kwd "INBOX" :title "second" :tags ("NEXT_WEEK") :deadline ,(time-add nil (+ (* 2 seconds-in-week) seconds-in-day)))
             :res -1)
            (:first
             (:kwd "TODO" :title "first" :tags ("perso" "THIS_MONTH" "ACTION") :scheduled ,(time-add nil (* 6 seconds-in-week)))
             :second
             (:kwd "INBOX" :title "second" :tags ("NEXT_WEEK") :deadline ,(time-add nil (* 3 seconds-in-week)))
             :res 1)
            (:first
             (:kwd "TODO" :title "first" :tags ("perso" "THIS_WEEK" "ACTION") :scheduled ,(time-add nil (* 6 seconds-in-week)))
             :second
             (:kwd "TODO" :title "second" :tags ("ACTION" "THIS_WEEK") :deadline ,(time-add nil (* 3 seconds-in-week)))
             :res nil)
            )))
    (mapc #'test-compare-two-todo test-plists)
    ))

(defun test-compare-two-todo (test-plist)
  (let* (
         (file-name "/tmp/org-test.org")
         (buff (get-org-test-buffer file-name))
         (first (plist-get test-plist :first))
         (second  (plist-get test-plist :second))
         (expected (plist-get test-plist :res))
         (todos)
         (res)
         )
    (ech:debug/init-dbg "Debug Compare Todos")

    (apply #'add-todo (cons buff first))
    (apply #'add-todo (cons buff second))
    (with-current-buffer buff
      (progn
        (set-visited-file-name file-name t)
        (save-buffer 0)
        (setq todos (org-agenda-get-todos))
    (setq res (ech:org/compare-todo (car todos) (car (cdr todos))))
    (ech:debug/dbg-show-buffer)
    (kill-buffer buff)
    (should (equal res expected))
    ))))

(defun test-show-todo ()
  (ech:org/eval-source-block-list '("print-todo" "org-buffer" "compare-todo"))
  (let ((buff (get-org-test-buffer "*test plan*"))
        (test `(:kwd "TODO" :title "first" :tags ("perso" "THIS_WEEK" "ACTION") :scheduled ,(encode-time 0 0 0 25 11 2020)))
        (todos))
    (ech:debug/init-dbg "*Debug show todo*")

    (apply #'add-todo (cons buff test))
    (with-current-buffer buff
      (progn
        (set-visited-file-name  "org-orga-test.org" t)

        (setq todos (org-agenda-get-todos))
        (ech:debug/dbg-print (format "todos= %S" todos))))
    (ech:debug/dbg-show-buffer)
    (test-buffer-show buff)
    ))

;;(test-show-todo)
;;(ech:debug/init-dbg "*Debug*")
;;(ech:org/test-compare)
