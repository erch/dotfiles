(message "Start loading package-init ...")

(defvar bootstrap-version)
(setq straight-check-for-modifications nil)
(setq straight-recipes-gnu-elpa-use-mirror t)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq straight-enable-use-package-integration t)

;; by default use package will defer the load of features.
(setq use-package-always-defer t)

(let ((here  (file-name-directory load-file-name)))
  (add-to-list 'load-path here)
  (byte-compile-file (expand-file-name "test.el" here))
  (require 'test))
