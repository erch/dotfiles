
(use-package cdlatex
  :after tex
  :straight '(cdlatex :type git :host github :repo "cdominik/cdlatex")
  :diminish
  :config
  (turn-on-cdlatex)
  )

(use-package terraform-mode
  :mode "\\.tf\\'"
  :diminish "")
(provide 'test)
