 #!/usr/bin/python3

"""
Doc of the module
"""

# python 2 compatibility with 3
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import sys
import logging
import re

MYNAME = os.path.realpath(__file__)
MYDIR = os.path.dirname(MYNAME)

LOGGING_LEVELS = {'critical': logging.CRITICAL,
                  'error': logging.ERROR,
                  'warning': logging.WARNING,
                  'info': logging.INFO,
                  'debug': logging.DEBUG}

logging.basicConfig(level=logging.CRITICAL,format='%(funcName)s: %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')
logger = logging.getLogger()

def test(mess:str):
    pass

class ExtractDocError(Exception):
    """Handling error for this module."""
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

def do_extract():
    pass

def main():
    parser = argparse.ArgumentParser(description="""
    Extract pages from a pdf document.
    The pages to extract are read from a file that specifies how to extract the pages. Each of its lines are the specification for an extraction and has the following format:
    first page,last page,type,date

    type defines where to move the extracted page and how to rename it with a file name pattern that contains a placeholder for the date.
    """)
    parser.add_argument('-p','--pfile',help="Page specification file",default=None,required=True)
    parser.add_argument('--verbosity', '-v', choices=LOGGING_LEVELS , default='critical',help='logging level')
    parser.add_argument('pdf_file',help="the pddf file from where to extract the pages", type=argparse.FileType('r'))
    args = parser.parse_args()
    logger.setLevel(LOGGING_LEVELS[args.verbosity])

    try:
        do_extract()
    except ExtractDocError as err:
        logging.getLogger().exception('Application Error')
        return 1
    except Exception as err:
        logging.getLogger().exception('Error in execution')
        return 2

if __name__ == "__main__":
    try:
        status = main()
    except KeyboardInterrupt:
        logging.getLogger().error("Interrupted by user ...")
        status = 3
    sys.exit(status)
