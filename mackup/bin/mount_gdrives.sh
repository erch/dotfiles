#! /bin/bash

# set -x
echo "Mounting Google Drives"  | systemd-cat -t ech -p info
ECH_DRIVE_MOUNT_POINT="$HOME/gdrives/google-drive-ech/"
ENTREPRISE_DRIVE_MOUNT_POINT="$HOME/gdrives/google-drive-entreprise/"
CYCLOPE_DRIVE_MOUNT_POINT="$HOME/gdrives/google-drive-cyclope"
MY_UID=$(id -u)
MY_GID=$(id -g)
#BIN="/usr/bin/google-drive-ocamlfuse -o allow_other,uid=${MY_UID},gid=${MY_GID}"
BIN="/usr/bin/google-drive-ocamlfuse -o uid=${MY_UID},gid=${MY_GID}"

mkdir -p "$ECH_DRIVE_MOUNT_POINT"
mkdir -p "$ENTREPRISE_DRIVE_MOUNT_POINT"
mkdir -p "$CYCLOPE_DRIVE_MOUNT_POINT"

mount | grep "${ECH_DRIVE_MOUNT_POINT%%/}" > /dev/null
if [[ $? -ne 0 ]] ; then
    $BIN  "$ECH_DRIVE_MOUNT_POINT"
fi

mount | grep "${ENTREPRISE_DRIVE_MOUNT_POINT%%/}" > /dev/null
if [[ $? -ne 0 ]] ; then
    $BIN -label entreprise "$ENTREPRISE_DRIVE_MOUNT_POINT"
fi

mount | grep "${CYCLOPE_DRIVE_MOUNT_POINT%%/}" > /dev/null
if [[ $? -ne 0 ]] ; then
    $BIN -label cyclope "$CYCLOPE_DRIVE_MOUNT_POINT"
fi
