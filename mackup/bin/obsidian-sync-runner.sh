#! /bin/bash

PRG="$0"
# What's my name?
read MY_NAME MY_DIR <<< `perl -e 'use Cwd;use File::Basename;print File::Basename::basename("'$PRG'") . " " . Cwd::abs_path( File::Basename::dirname("'$PRG'") ) . "\n";'`

function usage() {
	[ -n "$DEBUG" ] && set -x

	cat 1>&2 <<[]
    $1
    $MY_NAME [-D-v]

    -D : debug mode
    -v : verbose mode

    Keep in sync the web-clips directories for obsidian.
    commande to update :  systemctl --user restart obsidian-webclips
[]
}


function cleanup() {
    echo 'killed'
}

#trap stops signals
trap 'cleanup ; exit 1'  1 2 3 15


#
# extracting parameters
#
#[ $# -eq 0 ] && usage "missing parameter" && exit 1

while getopts D,v OPTSTR
do
    case $OPTSTR in
        D) DEBUG=OK
            set -x
        ;;
        v) VERBOSE=OK
        ;;
        *)
            usage
            exit 1;;
    esac
done
[ $OPTIND -gt 1 ] && shift `expr $OPTIND - 1`

[ $# -gt 1 ] && usage "too many parameters" && exit 1

# local variables initialization. get $PROFILE_D_DIR
HOST_FILE="$HOME/.$(hostname)-conf.sh"
if [[ -f  "$HOST_FILE" ]] ; then
    . $HOST_FILE
fi

# read obsidian conf file , get $OBSIDIAN_ROOT_DIR $OBSIDIAN_VAULT $WEB_CLIPS_DOWNLOAD_DIR $WEB_CLIPS_VAULT_DIR
. ${PROFILE_D_DIR}/obsidian.sh

export OPTIONS=" -x Unconfirmed "
[ -n "$DEBUG" ] && OPTIONS="$OPTIONS -D"
[ -n "$VERBOSE" ] && OPTIONS="$OPTIONS -v"

( ${MY_DIR}/syncdir.sh $OPTIONS -D -v "${WEB_CLIPS_DOWNLOAD_DIR}" "${WEB_CLIPS_VAULT_DIR}" "webclip_obs_sync") &
( ${MY_DIR}/syncdir.sh $OPTIONS -D -v "${WEB_CLIPS_VAULT_DIR}" "${WEB_CLIPS_DOWNLOAD_DIR}" "webclip_obs_sync") &

wait
echo "ending"
