#! /bin/bash

PRG="$0"
# What's my name?
read MY_NAME MY_DIR <<< `perl -e 'use Cwd;use File::Basename;print File::Basename::basename("'$PRG'") . " " . Cwd::abs_path( File::Basename::dirname("'$PRG'") ) . "\n";'`

function usage() {
	  [ -n "$DEBUG" ] && set -x

	  cat 1>&2 <<[]
    $1
    $MY_NAME [-D-n] -[r|i|s] java_home

    manage java alternatives for java programs in java_home directory

	  -D: Debug mode.
    -n: Dry run (does nothing)
    -r: Remove alternatives
    -i: install alternatives
    -s: set alternatives
[]
}

PROGS="bin/appletviewer bin/extcheck bin/idlj bin/jar bin/jarsigner bin/java bin/javac bin/javadoc bin/javafxpackager bin/javah bin/javap bin/javapackager bin/javaws bin/jcmd bin/jconsole bin/jcontrol bin/jdb bin/jdeps jre/lib/jexec bin/jhat bin/jinfo bin/jjs bin/jmap bin/jmc bin/jps bin/jrunscript bin/jsadebugd bin/jstack bin/jstatd bin/jvisualvm bin/keytool bin/native2ascii bin/orbd bin/pack200 bin/policytool bin/rmic bin/rmid bin/rmiregistry bin/schemagen bin/serialver bin/servertool bin/tnameserv bin/unpack200 bin/wsgen bin/wsimport bin/xjc"

DEBUG=
DRY_RUN=
DO_INSTALL=
DO_REMOVE=
DO_SET=
JAVA_HOME=
CMD=

function do_it() {
    for PROG in $PROGS ; do
        PROG_PATH="$JAVA_HOME/$PROG"
        PROG_NAME=`basename $PROG_PATH`
        if [[ ! -e $PROG_PATH && ! -n ${DO_REMOVE} ]] ; then
            echo "skipping $PROG_PATH : not exist"
            continue
        fi
        if [[ -n "$DO_INSTALL" ]] ; then
            echo "installing $PROG_PATH as /usr/bin/$PROG_NAME"
            ${CMD} --install "/usr/bin/$PROG_NAME" "$PROG_NAME" "$PROG_PATH" 180
        elif [[ -n "${DO_REMOVE}" ]] ; then
            echo "removing $PROG_PATH from alternative $PROG_NAME"
            $CMD --remove "$PROG_NAME" "$PROG_PATH"
        elif [[ -n "${DO_SET}" ]] ; then
            echo "setting $PROG_PATH as alternative for $PROG_NAME"
            $CMD --set  "$PROG_NAME" "$PROG_PATH"
        else

            echo "don't know what to do" >&2 && exit 1
        fi
    done
}


# extract parameters


#
# extracting parameters
#
#[ $# -eq 0 ] && usage "missing parameter" && exit 1

while getopts D,n,r,i,s OPTSTR
do
    case $OPTSTR in
	      D) DEBUG=OK
	         set -x ;;
        n)
            DRY_RUN=OK;;
        r)
            DO_REMOVE=OK;;
        i)
            DO_INSTALL=OK;;
        s)
            DO_SET=OK;;
        *)
            usage
            exit 1;;
    esac
done
[ $OPTIND -gt 1 ] && shift `expr $OPTIND - 1`

[ $# -lt  1 ] && usage "missing parameter" && exit 1
[ $# -gt  1 ] && usage "too many parameters" && exit 1

if [[ -z ${DO_REMOVE} && -z ${DO_SET} && -z ${DO_INSTALL} ]] ; then
    usage "missing option" && exit 1
fi

JAVA_HOME=$1 

if [[ ! -x $JAVA_HOME/bin/java ]] ; then
    echo '$JAVA_HOME/bin/java is not an executable' >&2 && exit 1
fi

if [[ -n "${DRY_RUN}" ]] ; then
    CMD="echo sudo update-alternatives "
else
    CMD="sudo update-alternatives "
fi

do_it

### Local Variables:
### mode: Shell-script
### fill-column:110
### ispell-local-dictionary: "english"
### End:



