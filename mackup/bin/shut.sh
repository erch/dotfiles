#!/bin/bash

# This file must be sourced with it directory as first parameter.
MY_DIR=$(realpath "$1")

# avoid perl complaints
export LC_ALL=${LC_ALL:-C}

# test where mustashe.sh is:
set +e
MUSTACHE=$(type -p mustashe.sh)
if [[ -z "${MUSTACHE}" ]] ; then
    MUSTACHE=${MY_DIR}/mustache.sh
fi


# source all files that are readable in the directory given as parameter in alphanumeric order
# $1: the directory where to look for files
# $2: an optional suffix for the files to source, typically file extension with the dot, but can
#   be any suffix.
# $3: the name of a variable that is an  associative array with the file name that has already
#     been sourced as key associated to a value, will be expanded as file are sourced.
#
# sourced file received their directory and the associative array as parameter in
# order for them to be able to source other files.
function source_dir() {
    local DIRPATH="$1"
    local EXT=$2
    local SOURCED=$3

    if [[ -d "${DIRPATH}" ]] ; then
        for file in $(ls ${DIRPATH}) ; do
            echo $file | grep -e ".*${EXT}$" >&2
            if [[ $? -eq 0 && -r "${DIRPATH}/${file}" ]] ; then
                source_file "${DIRPATH}" "${file}" "${SOURCED}"
            fi
        done
    fi
}

# source a file but check before that it has not been already sourced
# $1: the file directory
# $2: the file name
# $3: if set an associative array with the file name that has already been sourced,
#     will be expanded as file are sourced.
#
# sourced file received the file directory and the associative array as parameter in
# order for them to be able to source other files.
function source_file() {
    local DIRPATH="$1"
    local FILE=$2
    local SOURCED=$3
    local FILE_PATH=${DIRPATH}/${FILE}
    # array index doesn't support some char, for robustness we keep only alpha chars
    # array index can't neither start with a number
    local IDX=_$(echo $FILE | tr -c "A-Za-z0-9" "_")
    local DONE

    if [[ -n "${SOURCED}" ]] ; then
        DONE="$(eval echo \${$SOURCED[${IDX}]})"
         if [[ -z "${DONE}" ]] ; then
            source "${FILE_PATH}" "$DIRPATH" "$SOURCED"
            eval ${SOURCED}[${IDX}]=1
        fi
    else
        source "${FILE_PATH}"
    fi
}

# create a copy of a file given as parameter with an .back_N extension where N represents the Nth backup of the file
# $1: the file to backup.
function backupf () {
    local MAX=20
    local ITER=0
    local FILE="$1"
    local VERS=0
    local EXT=.back_
    local BCK_FILE=$(dirname ${FILE})/.$(basename ${FILE})${EXT}${VERS}
    while [[ -e "${BCK_FILE}" && ${ITER} -lt ${MAX} ]] ; do
        ((++VERS))
        ((++ITER))
        BCK_FILE=$(dirname ${FILE})/.$(basename ${FILE})${EXT}${VERS}
    done

    if [[ ${ITER} -lt ${MAX} ]]; then
        cp "${FILE}" "${BCK_FILE}"
        return 1
    fi
    return 0
}

# replace something by something else in a file or several files in a directory tree.
#
# Warning: the function do not backup the original file before running, use backupf is necessary!
#
# $1: a filename or a glob for a file name
# $2: a directory where to find $1 -
#     if equals '-', then consider $1 as a full path of a file and will try to perform the replacement only in this file.
#     otherwise expects a directory where to find recursively files matching $1 under $2 and all directories under $2.
# $3: a perl regex for matching a line where something is to be replaced. It can have groups.
#       for instance '^\s*(\S+)\s*:\s*(\S+)[$|\s]' will capture two groups: one with the left word of a ':' character
#       and the other one with the first word after it.
# $4: a perl expression to replace the matches. The expression should use $1,$n as matched groups.
#     for instance with the above match it can be : "$1 = $2" in order to replace the ':' by an equal sign
# $5: optional : a default line to append to the file if there was no match for $1. If the value is 1 will use $4 as the default string,
#     in this case be aware that it is not possible to use match groups.
#
# use \n if you want new lines to be inserted.
function replace_in_file() {
    local FILE="$1"
    local DIR="$2"
    local MATCH=$(echo $3)
    local REPL=$(echo $4)
    local DEF_LINE=${5:-}
    local TMP_F

    if [[ "$DIR" == "-" ]]; then
        if [[ ! -e "$FILE" ]] ; then
            echo "file $FILE not found" >&2
            return 255
        fi
        CMD="echo $FILE"
    else
        if [[ ! -d "$DIR" ]] ; then
            echo "directory $DIR not found" >&2
            return 255
        fi
        CMD="find \"$DIR\" -type f -name \"$FILE\" -print"
    fi

    $CMD 2>/dev/null | while read F ; do
        TMP_F=/tmp/$(basename $FILE)_$$.tmp
        # the mess with quote is for preventing the shell to do a word split in the variables if they
        # are not double quoted in the shell: so in : "'"${REPL}"'" , first 2xquote is to create a perl string,
        # second single quote is to get out of perl and entering the shell.
        # inside the shell we don't want word spliting if there is spaces in the variable , so we add a 2xquote
        cat "$F" | perl -ne '
            BEGIN {
                $res=246;
            }
            if (/'"${MATCH}"'/) {
                $res=0;
                print (qq('"${REPL}"'))
            }
            else {
                print
            }
            END { exit $res }
        ' > "${TMP_F}"
        RES=$?
        if [[ $RES -eq 0 ]] ; then
            mv "${TMP_F}" "${F}"
        elif [[ $RES -eq 246 && -n "$DEF_LINE" ]] ; then
            # before adding a line at the end of the file we check that the file ends with a new line char.
            if [[ $(tail -c1 "${TMP_F}" | wc -l) -eq 0 ]] ; then
                echo "" >> "${TMP_F}"
            fi
            if [ $DEF_LINE -eq 1 ] ; then
                echo -e "$REPL" >> "${TMP_F}"
            else
                echo -e "$DEF_LINE" >> "${TMP_F}"
            fi
            # better to write in tmp file and make a move after to avoid write conflict with another process ?? other process data will be lost
            mv "${TMP_F}" "${F}"
        else
            rm "${TMP_F}"
        fi
    done
}

# Insert or replace a block of text in file given as first parameter
# the block is delimited by a markers given as second parameter
# the delimiters are preceded by a comment string which is given a third parameter.
# Of course the comment string depends on the type of file being modified, ie for shell script
# the comment string should be '#'
#
# the part to insert into the file is read from stdin.
#
# the beginning and finishing delimiters are prefixed respectively by the strings _begin and _end.
# Each delimiters are suffix with a '_' and a string warning to not delete the block.
#
# If an existing bloc begins with a marker which itself begins with the same string as the marker given as second parameter, then this bloc is
# replaced by the new bloc.
# This is useful when you want to remove several related blocks from a file. For instance if the file has 3 inserted blocs with 3 markers
# "mark1", "mark2" and "mark3". Then it's possible to remove all of them by inserting a blanks line with marker "mark".
#
# Be careful : the blocs are always inserted at the end of the file. Thus the position of the blocs are not respected if
#              the bloc is replaced: for instance a bloc will be removed in the middle of the file and replaced at its end.
#
# The script take care of adding a new line before the bloc to insert if there is no new line at the end of the file.
#
# Warning: the script do not backup the original file.
#          If necessary do a backupf before calling the function and do a rm on the part file.
#
# $1: the file name
# $2: the name of the bloc which is used to recognize it
# $3: the comment sequence
# Reads the content of the bloc from stdin
#
# it can be used with here document like this, the pipe after the EOF makes the trick:
# cat <<-EOF |
# line of text
# line of text
# EOF
# insert_bloc_infile 'my delimiter' ';;;;'
#
function insert_bloc_infile() {
    local CONTAINER="$1"
    local PART_NAME="$2"
    local COMMENT="$3"
    local RES=0
    local IS_CONTENT
    local CONTAINER_TMP_FILEPATH="/tmp/$(basename ${CONTAINER})_$$"

    # read -t 0 just tell if there is something waiting on stdin
    read -t 0
    IS_CONTENT=$?

    if [[ $IS_CONTENT -eq 0 ]] ; then
        if [[ -e "${CONTAINER}" ]] ; then
            cat "${CONTAINER}" | perl -ne '
                BEGIN{$in=0};
                chomp;
                /_begin '"${PART_NAME}"'(?:[^_]+_|_)/ and $in=1;
                if (! $in) {
                    {print "$_\n"};
                }
                /_end '"${PART_NAME}"'(?:[^_]+_|_)/ and $in=0;
            ' > "${CONTAINER_TMP_FILEPATH}"
            RES=$?
        else
            :> "${CONTAINER_TMP_FILEPATH}"
        fi

        if [[ $RES -eq 0 ]]; then
            echo "${COMMENT} _begin ${PART_NAME}_  ;; do not delete this marker" >> "${CONTAINER_TMP_FILEPATH}"
            cat - >> "${CONTAINER_TMP_FILEPATH}"
            echo "${COMMENT} _end ${PART_NAME}_  ;; do not delete this marker" >> "${CONTAINER_TMP_FILEPATH}"
            mv "${CONTAINER_TMP_FILEPATH}" "${CONTAINER}"
        else
            rm -f "${CONTAINER_TMP_FILEPATH}"
        fi
    fi
}

# fill a mustache template. based on : https://github.com/rcrowley/mustache.sh
# install mustache.sh as explained in its README.
# Reads a mustache template from stdin and replace tags with values from shell
# environment variables with same name.
# Has following limitation compared to mustache:
# does not descend into a new scope within {{#tag}} or {{^tag}} sections.
# doesn't support the --compile or --tokens command-line options and does not accept input file(s) as arguments.
# doesn't care about escaping output as HTML.
# doesn't support list sections in the traditional sense: it requires the section tag be a shell command and processes the section once for each line on standard output with the line available in _M_LINE.
# will execute tag names surrounded by backticks as shell commands.
fill_template() {
    local PREV_IFS=${IFS}
    . ${MUSTACHE}
    mustache
    IFS=${PREV_IFS}
    set +e
    3>&-
    4>&-
    5>&-
}

# Clean the PATH environment variable from duplicates.
# if a directory is given as first parameter then add it at the begining of the PATH
# if the second parameter is the string "after" the directory is added at the end of the PATH
# if the second parameter is the strint "remove" the directory is removed from the PATH
function pathmunge () {
    local DIR=$1
    local ACTION=$2
    PATH=$(echo $PATH | perl -ne '
        my @path=split(":",$_);
        #print STDERR join("\n",@path) . "\n+++++\n";
        my %seen;
        my $action="'$ACTION'";
        my $dir="'$DIR'";
        @res=grep { $_ ne $dir && !$seen{$_}++ } @path;
        #print STDERR join("\n",@res) . "\n";
        if ($action ne "remove") {
          if ($dir && $action eq "after") {
              push(@res,$dir);
          }
          elsif ($dir) {
              unshift(@res,$dir);
          }
        }
        print join(":",@res)
    ')
}
