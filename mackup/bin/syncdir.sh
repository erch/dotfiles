#! /bin/bash

PRG="$0"
# What's my name?
read MY_NAME MY_DIR <<< `perl -e 'use Cwd;use File::Basename;print File::Basename::basename("'$PRG'") . " " . Cwd::abs_path( File::Basename::dirname("'$PRG'") ) . "\n";'`

function usage() {
	[ -n "$DEBUG" ] && set -x

	cat 1>&2 <<[]
    $1
    $MY_NAME [-D-v] SOURCE DEST LOCK_NAME

    -D : debug mode
    -v : verbose mode
    -x : exclusion patterns. Can be used multiple times with the file name patterns that are not to be monitored.

    SOURCE: The source directoy
    DEST: The destination directory to keep in sync
    LOCK_NAME: a lock name that must be identical to all instances of this script that deal with these 2 directories (for 2 ways synchronization)

    Synchronize all changes in $SOURCE to $DEST
    - monitor '$SOURCE' and sync it with '$DEST' when a change occurs on '$SOURCE'
    - For 2 ways sync must be run twice with symetric SOURCE and DEST and the same lock name

    commande to update :  systemctl --user restart obsidian-webclips
[]
}

function cleanup() {
    echo 'killed'
}

#
# process inotify events
#
function sync() {
    [ -n "$DEBUG" ] && set -x

    local ROOT_DIR="$1"
    local DEST_DIR="$2"
    local LOCK_FILE="$3"
    local ACTION="$4"
    local TARGET="$5"

    local ACTION_1=$(echo "${ACTION}" | cut -d ',' -f 1)
    local ACTION_2=$(echo "${ACTION}" | cut -d ',' -f 2)
    local TARGET_REL_PATH=$(realpath -m --relative-to "${ROOT_DIR}" "${TARGET}")
    local DEST_PATH=$(realpath -m "${DEST_DIR}"/"${TARGET_REL_PATH}")

    # protection
    [ -z "$TARGET" ] || [ -z "$TARGET_REL_PATH" ] || [ -z "$DEST_PATH" ] && echo "wrong path" 1>&2 && exit 1
    if [[ ${#DEST_DIR} -lt 3 && ${#DEST_PATH} -lt ${#DEST_DIR} ]] ; then
        # very small paths are suspect
        echo "dangerous path destdir=|${DEST_DIR}| and destpath=|${DEST_PATH}|" 1>&2 && exit 1
    fi
    # run this part in a subshell (parenthesis) in order to automatically release the lock when we reach the closing parenthesis.
    (
        # to use flock we need a file node descriptor: exec 8 > lock -> affect file descriptor 8 to the lock file for output.
        exec 8>"${LOCK_FILE}";
        # this takes the lock exclusively (-x) for 2 seconds (-w 2) on file descriptor 8, if another process try to use take a lock on the same file descriptor it will need to wait that
        # we have released it at the end of this subshell.
        flock -x -w 2 8
        #sleep 1
        if [[ -e "$TARGET" && -e "$DEST_PATH" && ! -d "$TARGET" && ! `cmp --silent "$TARGET" "$DEST_PATH"` ]] ; then
            # source and dest exists, target is not a directory so both are files , and they are identical => nothing to do
            # Note: cmp return 0 if files are identical knowing that 0 is the error code for bash meaning it is equivalent to false in tests
            [ -n "$VERBOSE" ] && echo "Identical content on files $TARGET and $DEST_PATH: SKIPPING event ${ACTION}"
        elif [[ "${ACTION_1}" == "CREATE" ]] ; then
            if [[ "${ACTION_2}" == "ISDIR" ]] ; then
                # we use the create event only for directory , in case of files we wait for the CLOSE_WRITE event to occur
                if mkdir -p "$DEST_PATH" ; then
                    [ -n "$VERBOSE" ] && echo "directory $DEST_PATH CREATED"
                else
                    echo "Error on command: mkdir -p $DEST_PATH" 1>&2
                fi
            fi
        elif [[ "${ACTION_1}" == 'CLOSE_WRITE' ]] ; then
            # a file has been created and its content written
            if cp "${TARGET}" "${DEST_PATH}" ; then
                [ -n "$VERBOSE" ] && echo "file ${TARGET} COPIED to ${DEST_PATH} on CLOSE_WRITE event"
            else
                echo "Error on command: cp ${TARGET} ${DEST_PATH}" 1>&2
            fi
        elif [[ "${ACTION_1}" == "MOVED_TO" ]] ; then
            # same action for file and directory move.
            if cp -R "${TARGET}" "$(dirname ${DEST_PATH})" ; then
                [ -n "$VERBOSE" ] && echo "${TARGET} COPIED to ${DEST_PATH} on MOVED_TO event"
            else
                echo "Error on command: cp -R ${TARGET} dirname ${DEST_PATH}" 1>&2
            fi
        elif [[ -e "${DEST_PATH}" && ( "${ACTION_1}" == "DELETE" ||  "${ACTION_1}" == "MOVED_FROM" ) ]] ; then
            # for the deletion case (rm or mv) we do something only if the destination target exists. The actions differ for directories and files
            if [[ "${ACTION_2}" == "ISDIR" ]] ; then
                if rm -rf "${DEST_PATH}" ; then
                    [ -n "$VERBOSE" ] && echo "Directory ${DEST_PATH} DELETED"
                else
                    echo "Error on command: rm -rf ${DEST_PATH}" 1>&2
                fi
            else
                if rm "${DEST_PATH}" ; then
                    [ -n "$VERBOSE" ] && echo "File ${DEST_PATH} DELETED"
                else
                    echo "Error on command: rm -${DEST_PATH}" 1>&2
                fi
            fi
        else
             [ -n "$VERBOSE" ] && echo "Action $ACTION on file $DEST_PATH IGNORED"
        fi
    ) &
}

#
# initiate the monitoring of the source directory by inotify and call sync on each event detected
#
function monitor_dir() {
    [ -n "$DEBUG" ] && set -x

    local ROOT_DIR=$(realpath -Pe "$1")
    local DEST_DIR=$(realpath -Pe "$2")
    local LOCK_NAME="$3"

    [ -z "$ROOT_DIR" ] || [ -z "$DEST_DIR" ] && echo "wrong dir" 1>&2 && exit 1
    [ -z "$LOCK_NAME" ] && echo "wrong lock" 1>&2 && exit 1

    local LOCK_DIR=/var/tmp/"${LOCK_NAME}.$$"
    local LOCK_FILE="${LOCK_DIR}"/locker

    local _ROOT
    local _ACTION
    local _FILE
    local _TO_PROCESS
    local _VAL

    mkdir -p "${LOCK_DIR}"

    # start inotify as a daemon (-m option) to monitor the ${ROOT_DIR} directory and all its sub-directories (-r option for recursive).
    inotifywait -m "${ROOT_DIR}" -r -e close_write -e create -e move -e delete |
    while read _ROOT _ACTION _FILE; do
        [ -n "$VERBOSE" ] && echo "Event caught: $_ROOT $_ACTION $_FILE" 1>&2

        # Check if we have the file name in the exclusion patterns
        _TO_PROCESS="OK"
        for _VAL in "${EXCLUDE_PATTERNS[@]}"; do
           if [[ "$_FILE" =~ ${_VAL} ]] ; then
                [ "$VERBOSE" ] && echo "Skipping excluded file name : $_ROOT $_ACTION $_FILE" 1>&2
                _TO_PROCESS=""
                break
            fi
        done
        if [[ -n "${_TO_PROCESS}" ]] ; then
            # get the absolute path with realapth (-m allow non existing files)
            local TARGET=$( realpath -m "${_ROOT}"/"${_FILE}" )
            # call sync to do the job
            sync "${ROOT_DIR}" "${DEST_DIR}" "${LOCK_FILE}" "${_ACTION}" "${TARGET}"
        fi
    done

    rm -rf "${LOCK_DIR}"
}

#trap stops signals
trap 'cleanup ; exit 1'  1 2 3 15


#
# extracting parameters
#
#[ $# -eq 0 ] && usage "missing parameter" && exit 1

while getopts D,v,x: OPTSTR
do
    case $OPTSTR in
        D) DEBUG=OK
            set -x
        ;;
        v) VERBOSE=OK
        ;;
        x) EXCLUDE_PATTERNS+=("$OPTARG")
        ;;
        *)
            usage
            exit 1;;
    esac
done

echo $EXCLUDE_PATTERNS
[ -n "$DEBUG" ] && VERBOSE=OK
export VERBOSE
export DEBUG
[ $OPTIND -gt 1 ] && shift `expr $OPTIND - 1`

[ $# -gt 3 ] && usage "too many parameters" && exit 1
[ $# -lt 3 ] && usage "missing parameter" && exit 1

[ -n "$VERBOSE" ] && echo "starting to sync $1 to $2"
monitor_dir "$1" "$2" "$3"
