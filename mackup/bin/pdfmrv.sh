#/bin/sh

#set -x
PRG="$0"
# What's my name?
read MY_NAME MY_DIR <<< `perl -e 'use Cwd;use File::Basename;print File::Basename::basename("'$PRG'") . " " . Cwd::abs_path( File::Basename::dirname("'$PRG'") ) . "\n";'`

function usage() {
	  [ ! -z "$DEBUG" ] && set -x

	  cat 1>&2 <<[]
    $1
    $MY_NAME [-D] [-f] verso recto

    merge two pdf files to reassemble recto and verso of scanned sheets.

    -f: flip the recto
    verso: path to the file with the verso sheets
    recto: path to the file with the recto sheets
	  -D: Debug mode.
[]
}

function gen_page_spec() {
    local VERSO=$1
    local RECTO=$2
    local NB_PAGES=$3
    local FLIP=$4

    local PAGE_SPEC
    local IDX=1
    while [[ $IDX -le $NB_PAGES ]] ; do
        if [[ -n "$FLIP" ]] ; then
            REC_IDX=$(( NB_PAGES - IDX + 1 ))
        else
            REC_IDX=$IDX
        fi
        PAGE_SPEC="$PAGE_SPEC $VERSO $IDX $RECTO $REC_IDX"
        (( IDX += 1 ))
    done
    echo "$PAGE_SPEC"
}

# trap stops signals
trap 'cleanup ; exit 1'  1 2 3 15

# local variables initialization.
RECTO=
VERSO=
FLIP=
#
# extracting parameters
#
#[ $# -eq 0 ] && usage "missing parameter" && exit 1

while getopts D,f OPTSTR
do
    case $OPTSTR in
	      D) DEBUG=OK
	         set -x ;;
        f)
            FLIP=OK;;
        *)
            usage
            exit 1;;
    esac
done
[ $OPTIND -gt 1 ] && shift `expr $OPTIND - 1`

[ $# -lt  2 ] && usage "missing parameter" && exit 1
[ $# -gt  2 ] && usage "too many parameters" && exit 1

VERSO=$1
RECTO=$2
#
# End of parameters extract
#

pdfinfo $VERSO > /dev/null 2>&1
if [[ $? -ne 0 ]] ; then
    usage "$VERSO is not a pdf file"  && exit 1
fi

pdfinfo $RECTO > /dev/null 2>&1
if [[ $? -ne 0 ]] ; then
    usage "$RECTO is not a pdf file"  && exit 1
fi

NB_PAGES=$(pdfinfo "$VERSO" | perl -ne '/^Pages:\s+(\d+)\s*$/ and print "$1\n"')

OUTPUT="${VERSO%%.*}_${RECTO%%.*}.pdf"
PAGE_SPEC=$(gen_page_spec "$VERSO" "$RECTO" $NB_PAGES $FLIP)
pdfjam --fitpaper 'true' --rotateoversize 'false' --suffix joined -o "$OUTPUT" -- $PAGE_SPEC


### Local Variables:
### mode: Shell-script
### fill-column:110
### ispell-local-dictionary: "english"
### End:
