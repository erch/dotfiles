#! /bin/bash
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

#minimum PATH
export PATH=/bin:/usr/bin:/usr/local/bin

# detect system.
unset OS
case "$(uname -s)" in
    Darwin)
        OS='MacOSX'
        ;;
    CYGWIN*)
        OS='Cygwin'
        ;;
    Linux)
        OS='Linux'
        ;;
    *)
        echo 'other OS'
        OS='Unknown'
        ;;
esac
export OS

CONTAINER=none
if cat /proc/self/cgroup  | grep -q docker ; then
    CONTAINER=docker
fi
DETECT_RES=$(systemd-detect-virt)

if [[ ! $DETECT_RES = 'none' && $DETECT_RES = 'wsl' ]] ; then
    CONTAINER=wls
fi

if [[ ! $DETECT_RES = 'none' && $DETECT_RES = 'lxc' ]] ; then
    CONTAINER=lxc
fi

export CONTAINER

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	    . "$HOME/.bashrc"
    fi
fi

if [[  -n $(type keychain) && ($CONTAINER = "wls" || $CONTAINER = "none") ]] ; then
    # kill all ssh-agent in order to restart one with the correct configuration
    keychain -k all > /dev/null
    # start a ssh-agent with a fix socket name that can be used to be bound to container sockets.
    eval $(ssh-agent -a /tmp/"${USER}"-ssh-agent.unix -s)
    # ask keychain to manage the agent.
    keychain --inherit any
elif [ $CONTAINER = "wls" ]  ; then
    export SSH_AUTH_SOCK=${WSL_AUTH_SOCK}
fi
