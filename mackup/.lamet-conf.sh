export EMACS_BIN=/usr/local/bin/emacs
export EMACSCLI_BIN=/usr/local/bin/emacsclient
export WORKON_HOME=$HOME/.local/share/virtualenvs/
export PROJECT_DIR=$HOME/Projects
export PYTHONZ_ROOT=$HOME/.pythonz
export OPT_HOME=$HOME/opt
export PROFILE_D_DIR=$HOME/.profile.d
