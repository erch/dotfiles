if [[ -n "$INTERACTIVE_MODE" ]] ; then
    function kchain() {
        if [[ $# -gt 0 ]] ; then
	          KEYS=$*
        else
	          KEYS=$(ls ~/.ssh/*.pub)
        fi
        for F in $KEYS ; do
		        eval $(keychain --eval --ignore-missing ${F%%.pub})
        done
    }
fi
