# Variables used by ~/bin/obsidian_inotifywait.sh
# systemctl --user status obsidian-webclips

export OBSIDIAN_ROOT_DIR="$HOME/Dropbox/pkm"
export OBSIDIAN_VAULT="Orga"
export WEB_CLIPS_DOWNLOAD_DIR=$HOME/Downloads/web-clips/
export WEB_CLIPS_VAULT_DIR="${OBSIDIAN_ROOT_DIR}/${OBSIDIAN_VAULT}/100.Captures/140.Web_clips"

