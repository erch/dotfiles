export PYTHON_CONF_RESOURCE_DIR="$1/resources"
export LANG_ALL=C
export LANG=C

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"