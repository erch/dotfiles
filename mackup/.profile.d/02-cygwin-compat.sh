
# transform a string that represents a path in windows form to cygwin form
function _str2path() {
    set -- ${@:-$(</dev/stdin)}
    local RES
    local PRINT
    if  [[ "$OS" == "Cygwin" ]] ; then
        PRINT="cygpath -u "
    else
        PRINT="echo"
    fi
    for F in $@ ; do
        RES="$RES $($PRINT $F)"
    done
    echo $RES
}

alias str2path=_str2path

if [[ "$OS" == "Cygwin" ]] ; then
    # workaround for a mackup issue : this file contains a UTF character not decoded in python cygwin
    RES=$('ls' /usr/lib/python*/site-packages/mackup/applications/ubersicht.cfg 2>&1) 
    if [[ $? -eq 0 ]] ; then
        mv $RES $RES.bad
    fi

    # solve ping issue with cygwin
    function doping() {
        /cygdrive/c/Windows/system32/PING  $@ | iconv -f CP850
    }
    alias ping=doping
fi
