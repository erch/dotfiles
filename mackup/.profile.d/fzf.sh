# https://github.com/junegunn/fzf
#
# cd ~/opt
# git clone --depth 1 https://github.com/junegunn/fzf.git
# cd fzf
# cd fzf
# ./install # answer yes everywhre.

FZF_HOME="$HOME/opt/fzf/"
FZF_BIN="$FZF_HOME/bin/fzf"

if [[ -n "$INTERACTIVE_MODE" && -x "$FZF_BIN" ]] ; then
    pathmunge "${FZF_HOME}/bin/"

    # Auto-completion
    # ---------------
    source "${FZF_HOME}/shell/completion.bash" 2> /dev/null

    # Key bindings
    # ------------
    source "${FZF_HOME}/shell/key-bindings.bash"

    export FZF_DEFAULT_OPTS='--height 60% --layout=reverse  --border'

    # Using highlight (http://www.andre-simon.de/doku/highlight/en/highlight.html)
    export FZF_CTRL_T_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
    # preview causes error : Failed to read /dev/tty
    #export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:wrap"
    export FZF_CTRL_R_OPTS="--preview-window down:3:wrap"
    export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"
    export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"
fi
