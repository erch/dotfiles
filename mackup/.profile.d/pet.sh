# wget https://github.com/knqyf263/pet/releases/download/v0.3.2/pet_0.3.2_linux_amd64.deb
# sudo apt-get install ./pet_0.3.2_linux_amd64.deb
#

PET_BIN=/usr/local/bin/pet

if [[ -x "$PET_BIN" && -n "$INTERACTIVE_MODE" ]] ; then
    function prev() {
        PREV=$(echo $(history | tail -n2 | head -n1) | perl -ne '/\S+ \S+ \S+ (.*)/ and print "$1\n"')
        sh -c "${PET_BIN} new `printf %q "$PREV"`"
    }

    function pet-select() {
        BUFFER=$(pet search --query "$READLINE_LINE")
        READLINE_LINE=$BUFFER
        READLINE_POINT=${#BUFFER}
    }
    bind -x '"\C-x\C-r": pet-select'
fi
