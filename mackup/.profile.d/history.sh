# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoredups:ignorespace:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=1000000
export HISTFILESIZE=1000000

# ensure synchronization between Bash memory and history file
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

export HISTTIMEFORMAT="%y/%m/%d %T "

# see fzh for history tool manipulation.
