# Tools that don't need configuration

The goal is to document their installation - installation of tools with configurations is done in their corresponding .sh files.

## Chromedriver

```bash
$ cd ~/opt/bin
$ wget https://chromedriver.storage.googleapis.com/2.45/chromedriver_linux64.zip
$ unzip  chromedriver_linux64.zip
$ rm chromedriver_linux64.zip
```
## Taskwarrior
```bash
$ cd ~/opt
$ wget https://taskwarrior.org/download/task-2.5.1.tar.gz
$ tar xzvf task-2.5.1.tar.gz
$ cd task-2.5.1/
$ sudo apt-get install uuid-dev
$ sudo apt-get install libgnutls28-dev
$ sudo apt install cmake
$ cmake -DCMAKE_BUILD_TYPE=release .
$ sudo make install
```
