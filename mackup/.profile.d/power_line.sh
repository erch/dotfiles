# https://github.com/justjanne/powerline-go
#
# wget https://github.com/justjanne/powerline-go/releases/download/v1.11.0/powerline-go-linux-amd64
# mv powerline-go-linux-amd64 ~/opt
# chmod a+x ~/opt/powerline-go-linux-amd64
#
# powerline fonts: https://github.com/powerline/fonts
# cd ~/Projects/
# git clone git@github.com:powerline/fonts.git
# cd fonts
# ./install.sh
#
# Use one of the patched fonts in terminal
#

POWER_LINE="$HOME/opt/bin/powerline-go-linux-amd64"
if [[ -x "$POWER_LINE" && "$TERM" != "${TERM##xterm}" && -n "$INTERACTIVE_MODE" ]] ; then
    function _update_ps1() {
        PS1="$($POWER_LINE -cwd-max-depth 3  -cwd-max-dir-size 10 -modules "docker,dotenv,venv,user,host,ssh,cwd,perms,exit,root" -theme low-contrast -newline -mode patched -error $?)"
    }
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi
