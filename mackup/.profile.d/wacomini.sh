if [[ -n "$INTERACTIVE_MODE" ]] ; then
    function wacomini(){
        eval $(xsetwacom --list devices | perl -ne '/(.*?)\s+id:\s+(\d+)\s+type:\s+(\w+)\s*$/ and print "$3=\"$1\"\n"')
        DEVICE="HEAD-2"
        if [[ -n "$WACOM_SCREEN" ]] ; then
                DEVICE="${WACOM_SCREEN}"
        fi
        for DEV in "$STYLUS" "$ERASER" ; do
            xsetwacom  set "$DEV" MapToOutput "$DEVICE"
            xsetwacom set "$DEV" Mode "Absolute"
            xsetwacom set "$DEV" TabletPCButton "off"
        done
        TYPE=$(echo $PAD | perl -ne '/Wacom\s+(\w+)\s*.*$/ and print "$1\n"')
        echo "$TYPE"
        if [[ $TYPE == 'Intuos' ]] ; then
            echo "setting Buttons for little tablet"
        else
            echo "setting Buttons for $TYPE"
            xsetwacom set "$PAD" Button 1 'key b'
            xsetwacom set "$PAD" Button 2 'key shift'
            xsetwacom set "$PAD" Button 3 'key ctrl'
            xsetwacom set "$PAD" Button 8 'key alt'
            xsetwacom set "$PAD" Button 9 'key t'
            xsetwacom set "$PAD" Button 10 'key j'
            xsetwacom set "$PAD" Button 11 'key x'
            xsetwacom set "$PAD" Button 12 'key z'
            xsetwacom set "$PAD" Button 13 'key g'
        fi
    }
    export WACOM_SCREEN="HEAD-2"
    wacomini
fi
