#!/bin/bash

GO_HOME=$HOME/go

if [[ ! -d "$GO_HOME" ]] ; then
    mkdir -p "$GO_HOME"
fi
export GOPATH="$GO_HOME"
