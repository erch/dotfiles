if type ondir > /dev/null 2>&1 ; then
    if [[ -n "$INTERACTIVE_MODE" ]] ; then
	    function ondir_cd()
	    {
	        builtin cd "$@" && eval "$(/usr/bin/ondir "$OLDPWD" "$PWD")"
	    }
	    export -f ondir_cd
	    alias cd=ondir_cd

	    function ondir_pushd()
	    {
	        builtin pushd "$@" && eval "$(/usr/bin/ondir "$OLDPWD" "$PWD")"
	    }
	    export -f ondir_pushd
	    alias pushd=ondir_pushd

	    function ondir_popd()
	    {
	        builtin popd "$@" && eval "$(/usr/bin/ondir "$OLDPWD" "$PWD")"
	    }
	    export -f ondir_popd
	    alias ondir_popd=popd

        # call ondir_in case the shell starts within a directory with ondir actions
        eval "$(ondir $HOME $(pwd))"

    fi
fi

#eval "$(direnv hook bash)"
