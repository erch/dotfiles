if type vagrant > /dev/null 2>&1 ; then
    alias vag=vagrant
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
